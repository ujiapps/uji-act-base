class FerControlActesPage {

    fieldMain = {
        comboCurso: 'input[name=comboCurso]',
        comboConvocatoria: 'input[name=comboConvocatoria]'
    }

    fieldFiltros = {
        comboDepartament: 'input[name=comboDepartament]',
        comboActasGeneradas: 'input[name=comboActasGeneradas]',
        comboConvocatoria: 'input[name=comboConvocatoria]',
        comboTipusEstudi: 'input[name=comboTipusEstudi]',
        checkBoxForaTermini: 'input[name=checkBoxForaTermini]',
        comboActesTraspassades: 'input[name=comboActesTraspassades]'
    }

    constructor(browser) {
        this.browser = browser
        this.hastag = '#fer-control-actes'
    }

    visitSection() {
        this.browser.visit("/")
        this.getMenuItem().click()
    }

    getMenuItem() {
        return this.browser.contains("Control d'actes")
    }

    /** Main **/
    getActaAdminMain() {
        return this.browser.get('[id^=actaAdminMain]')
    }

    getComboCurso() {
        return this.getActaAdminMain().get(this.fieldMain.comboCurso)
    }

    getComboConvocatoria() {
        return this.getActaAdminMain().get(this.fieldMain.comboConvocatoria)
    }

    selectComboMain(combo, text) {
        if (combo == 1)
            this.getActaAdminMain().selectItemValue(this.fieldMain.comboCurso, text)

        if (combo == 2)
            this.getActaAdminMain().selectItemValue(this.fieldMain.comboConvocatoria, text)
    }

    /** Filtros **/
    getActaAdminPanelFiltros() {
        return this.browser.get('[id^=actaAdminPanelFiltros]')
    }

    selectComboFiltros(combo, text) {
        if (combo == 1)
            this.getActaAdminPanelFiltros().selectItemValue(this.fieldFiltros.comboDepartament, text)

        if (combo == 2)
            this.getActaAdminPanelFiltros().selectItemValue(this.fieldFiltros.comboActasGeneradas, text)

        if (combo == 3)
            this.getActaAdminPanelFiltros().selectItemValue(this.fieldFiltros.comboTipusEstudi, text)

        if (combo == 4)
            this.getActaAdminPanelFiltros().selectItemValue(this.fieldFiltros.checkBoxForaTermini, text)

        if (combo == 5)
            this.getActaAdminPanelFiltros().selectItemValue(this.fieldFiltros.comboActesTraspassades, text)
    }

    /** Grid **/
    getActaAdminGrid() {
        return this.browser.get('[id^=actaAdminGrid]')
    }

    getEnviarGridBtn() {
        return this.getActaAdminGrid().getButton('Enviar notificació a les')
    }

    /** Window envio **/
    getFormEnvioRecordatorio() {
        return this.browser.get('[id^=formEnvioRecordatorio]')
    }

    getEnviarFormBtn() {
        return this.getFormEnvioRecordatorio().getButton('Enviar notificació')
    }

    getCancelarBtn() {
        return this.getFormEnvioRecordatorio().find('.x-autocontainer-innerCt').contains('Cancel·lar')
    }

    getToolClose() {
        return this.browser.get('.x-tool-close').eq(1)
    }
}

export default FerControlActesPage