class NotasDesglosadasPage {

    campos = {
        // actaEstudiantePanelFiltros
        comboCurso: 'input[name=comboCurso]',
        comboActa: 'input[name=comboActa]',
        // panelDesgloseGridGrupos
        nombre: 'input[name=nombre]',
        etiqueta: 'input[name=etiqueta]',
        notaMinima: 'input[name=notaMinima]',
        notaMaxima: 'input[name=notaMaxima]',
        notaMinimaAprobado: 'input[name=notaMinimaAprobado]',
        // panelDesgloseGridPreguntas
        agrupacion: 'input[name=desgloseGrupoId]',
        parcialActaId: 'input[name=parcialActaId]',
    }

    constructor(browser) {
        this.browser = browser;
        this.hashtag = '#actos';
    }

    getActaEstudiantePanelFiltros() {
        return this.browser.get('[id^=actaEstudiantePanelFiltros]');
    }

    buscaCurso(curso){
        this.getActaEstudiantePanelFiltros().selectItemValue(this.campos.comboCurso, curso);
    }

    buscaActa(acta) {
        this.getActaEstudiantePanelFiltros().get(this.campos.comboActa).type(acta).type('{enter}');
    }

    getMenuSecciones() {
        return this.browser.get('[id^=ext-treelistitem]');
    }

    visitaSeccion() {
        this.browser.visit("/");
        this.getMenuSecciones().contains("Gestió d'actes").click();
    }

    getActaEstudianteGridConvocatoria() {
        return this.browser.get('[id^=actaEstudianteGridConvocatoria]');
    }

    getFilaExamenParcial() {
        return this.getActaEstudianteGridConvocatoria().find('.x-grid-cell').contains('Examen parcial');
    }

    getActaEstudiantesTabs() {
        return this.browser.get('[id^=actaEstudianteTabs]');
    }

    getActaEstudiantesPanelDesglose() {
        return this.browser.get('[id^=actaEstudiantePanelDesglose]');
    }

    getActaEstudiantesGridDesgloseNotas() {
        return this.browser.get('[id^=actaEstudianteGridDesgloseNotas]');
    }

    selectDetalleDesglosamientoTab() {
        this.getActaEstudiantesTabs().find('.x-tab-bar').contains('Detall desglossament').click();
    }

    selectNotasDesglosadasTab() {
        this.getActaEstudiantesTabs().find('.x-tab-bar').contains('Introducció de notes desglossades').click();
    }

    selectPreguntasTab() {
        this.getActaEstudiantesTabs().find('.x-tab-bar').contains('Preguntes').click();
    }

    getPanelDesgloseGridGrupos() {
        return this.browser.get('[id^=panelDesgloseGridGrupos]');
    }

    getPanelDesgloseGridPreguntas() {
        return this.browser.get('[id^=panelDesgloseGridPreguntas]');
    }

    setAgrupacion(nombreGrupo, etiquetaGrupo, notaMinGrupo, notaMaxGrupo, notaMinAprobadoGrupo) {
        this.getPanelDesgloseGridGrupos().getButton('Afegir').click();
        let editorAgrupacion = this.getPanelDesgloseGridGrupos().getEditor();
        editorAgrupacion.get(this.campos.nombre).type(nombreGrupo);
        editorAgrupacion.get(this.campos.etiqueta).type(etiquetaGrupo);
        editorAgrupacion.get(this.campos.notaMinima).type(notaMinGrupo);
        editorAgrupacion.get(this.campos.notaMaxima).type(notaMaxGrupo);
        editorAgrupacion.get(this.campos.notaMinimaAprobado).type(notaMinAprobadoGrupo);
        this.getPanelDesgloseGridGrupos().editorAcceptBtn().click();
    }

    setPregunta(nombrePregunta, etiquetaPregunta, notaMinGrupo, notaMaxGrupo) {
        this.getPanelDesgloseGridPreguntas().getButton('Afegir').click();
        let editorPregunta = this.getPanelDesgloseGridPreguntas().getEditor();
        editorPregunta.selectFirstItem(this.campos.agrupacion);
        editorPregunta.get(this.campos.nombre).type(nombrePregunta);
        editorPregunta.get(this.campos.etiqueta).type(etiquetaPregunta);
        editorPregunta.get(this.campos.notaMinima).type(notaMinGrupo);
        editorPregunta.get(this.campos.notaMaxima).type(notaMaxGrupo);
        this.getPanelDesgloseGridPreguntas().editorAcceptBtn().click();
    }

    setNotaDesglosada(notaDesglosada) {
        let filaNotaDesglosada = this.getActaEstudiantesGridDesgloseNotas().find('.greeniconcolor').first().parents('tr');
        filaNotaDesglosada.contains('AE1005').type('{rightarrow}{rightarrow}{rightarrow}{rightarrow}{enter}' + notaDesglosada + '{enter}');
    }
}

export default NotasDesglosadasPage