class PonerNotasPage {

    filtroFields = {
        curso:'input[name=comboCurso]',
        acta: 'input[name=comboActa]',
    }

    gridFields = {
        nota: 'input[name=nota]',
        comentarios: 'input[name=comentarios]'
    }

    testStrings = {
        parcial: 'Examen parcial',
        anyo: '2018',
        acta: 'AE1005',
        dni: '35605505X'
    }

    constructor(browser) {
        this.browser = browser;
        this.hashtag = '#poner-notas';
    }

    getMenuItem() {
        return this.browser.get('.x-treelist-item').contains("Gestió d'actes");
    }

    visitSection() {
        this.browser.visit("/");
        this.getMenuItem().click();
    }

    getPanelFiltros() {
        return this.browser.get('[id^=actaEstudiantePanelFiltros]');
    }

    getConvocatoriaGrid() {
        return this.browser.get('[id^=actaEstudianteGridConvocatoria]');
    }

    selectConvocatoriaGridRow() {
        this.getConvocatoriaGrid().find('.x-grid-cell').contains(this.testStrings.parcial).click();
    }

    selectCurso() {
        this.browser.selectItemValue(this.filtroFields.curso, this.testStrings.anyo);
    }

    selectActa(){
        this.browser.get(this.filtroFields.acta).type(this.testStrings.acta + "{enter}");
    }

    getNotasGrid() {
        return this.browser.get('[id^=actaEstudianteGrid]');
    }

    getDesarNotesBtn() {
        return this.getNotasGrid().getButton('Desar notes');
    }

    getNotasGridRow() {
        this.getNotasGrid().find('.x-grid-cell').contains(this.testStrings.dni).type('{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{enter}{backspace}'+ Math.floor(Math.random() * 10) + '{enter}');
    }

    ponerNota() {
        this.getNotasGridRow();
        this.getDesarNotesBtn().click();
    }





}

export default PonerNotasPage