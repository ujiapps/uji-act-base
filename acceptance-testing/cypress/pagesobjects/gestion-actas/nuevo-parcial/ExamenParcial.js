class ExamenParcialPage {
    campos = {
        // actaEstudiantePanelFiltros
        comboCurso: 'input[name=comboCurso]',
        comboActa: 'input[name=comboActa]',
        // formNuevoParcial
        descripcion: 'input[name=descripcion]',
        // actaEstudianteGridConvocatoria
        borraParcial: 'div[data-qtip=Esborrar]'
    }

    constructor(browser) {
        this.browser = browser;
        this.hashtag = '#actos';
    }

    visitaSeccion() {
        this.browser.visit("/");
        this.getMenuSecciones().contains("Gestió d'actes").click();
    }

    getMenuSecciones() {
        return this.browser.get('[id^=ext-treelistitem]');
    }

    getActaEstudiantePanelFiltros() {
        return this.browser.get('[id^=actaEstudiantePanelFiltros]');
    }

    getActaEstudianteGridConvocatoria() {
        return this.browser.get('[id^=actaEstudianteGridConvocatoria]');
    }

    getBotonCrearExamenParcial() {
        return this.getActaEstudianteGridConvocatoria().find('.x-btn').contains('Crear un nou examen parcial');
    }

    getVentanaNuevoParcial() {
        return this.browser.get('[id^=formNuevoParcial]');
    }

    getBotonGuardarDescripcion() {
        return this.getVentanaNuevoParcial().find('.x-btn').contains('Desar');
    }

    setDescripcionNuevoParcial(descripcion) {
        this.getVentanaNuevoParcial().get(this.campos.descripcion).type(descripcion);
    }

    getFilaExamenParcial() {
        return this.getActaEstudianteGridConvocatoria().get(this.campos.borraParcial);
    }

    borraExamenParcial() {
        this.getFilaExamenParcial().within(($list) => {
           $list[1].click();
        });
    }

    buscaCurso(curso){
        this.getActaEstudiantePanelFiltros().selectItemValue(this.campos.comboCurso, curso);
    }

    buscaActa(acta) {
        this.getActaEstudiantePanelFiltros().get(this.campos.comboActa).type(acta).type('{enter}');
    }
}

export default ExamenParcialPage