class FerDiligenciesPage {

    fieldPanelFiltros = {
        comboCurso: 'input[name=comboCurso]',
        comboConvocatoria: 'input[name=comboConvocatoria]',
        comboActa: 'input[name=comboActa]'
    }

    fieldDiligenciasComentario = {
        comentario: 'input[name=comentario]'
    }

    constructor(browser) {
        this.browser = browser
        this.hastag = '#fer-diligencies'
    }

    visitSection() {
        this.browser.visit("/")
        this.getMenuItem().click()
    }

    getMenuItem() {
        return this.browser.contains("Diligències")
    }

    getPanelFiltros() {
        return this.browser.get('[id^=diligenciasPanelFiltros]')
    }

    getDiligenciasGrid() {
        return this.browser.get('[id^=diligenciasGrid]')
    }

    getFormDiligenciasComentario() {
        return this.browser.get('[id^=formDiligenciasComentario]')
    }

    getFormResumenDiligencias() {
        return this.browser.get('[id^=formResumenDiligencias]')
    }

    selectItemFromComboBox(combo, text) {
        if (combo == 1)
            this.getPanelFiltros().selectItemValue(this.fieldPanelFiltros.comboCurso, text)

        if (combo == 2)
            this.getPanelFiltros().selectItemValue(this.fieldPanelFiltros.comboConvocatoria, text)

        if (combo == 3)
            this.getPanelFiltros().selectItemValueByArrowKey(this.fieldPanelFiltros.comboActa, text)
    }

    selectActionColumnComentari() {
        this.getDiligenciasGrid().get('.x-grid-cell').contains('Albalate').click()
            .type('{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{enter}')
    }

    getDesarBtn() {
        return this.getFormDiligenciasComentario().getButton('Desar')
    }

    writeTextAreaFormComentari(text) {
        this.getFormDiligenciasComentario().get('.x-form-textarea').eq(1).type(text)
    }

    clearTextAreaFormComentari() {
        this.getFormDiligenciasComentario().get('.x-form-textarea').eq(1).click().clear()
    }

    setEditorNota(elem) {
        this.getDiligenciasGrid().get('x-editor').clear().type(elem)
    }

    setEditorQualificacio(text) {
        this.getDiligenciasGrid().get('x-editor').selectItemValue(text)
    }

    getTraspasarBtn() {
        return this.getDiligenciasGrid().getButton('Traspassar i signar')
    }

    getCancelarBtn() {
        return this.getFormResumenDiligencias().find('.x-autocontainer-innerCt').contains('Cancel·lar')
    }

    getTraspasarResumBtn() {
        return this.getFormResumenDiligencias().getButton('Traspassar diligències')
    }

    getToolClose() {
        return this.browser.get('.x-tool-close').eq(0)
    }
}

export default FerDiligenciesPage