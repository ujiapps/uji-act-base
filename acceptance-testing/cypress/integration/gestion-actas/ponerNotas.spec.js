import PonerNotasPage from "../../pagesobjects/gestion-actas/PonerNotas";

describe('Testing Poner Notas', () => {

    let ponerNotas;

    beforeEach(() => {
        ponerNotas = new PonerNotasPage(cy);

        cy.intercept('GET', '/act/classic.json*').as('loadJson');
        cy.intercept('GET', '/act/rest/usuario/preferencias*').as('loadPreferencias');
        cy.intercept('GET', '/act/rest/navigation/class*').as('loadNavigation');
        cy.intercept('GET', '/act/rest/userinfo*').as('loadUserInfo');
        cy.intercept('GET', '/act/rest/informaciones/*').as('loadInformaciones');
        cy.intercept('GET', '/act/rest/profesor/diligencias/resumen/*').as('loadProfesorResumen');

        cy.intercept('GET', '/act/rest/cursos/*').as('loadCursos');
        cy.intercept('GET', '/act/rest/convocatorias/detalle/*').as('loadConvocatoriasDetalle');
        cy.intercept('GET', '/act/rest/cursos/*/actas*').as('loadActas');
        cy.intercept('GET', '/act/rest/cursos/*/actaconvocatorias*').as('loadActaconvocatorias');

        cy.intercept('GET', '/act/rest/actas/*/actaestudiantes*').as('loadEstudiantes');
        cy.intercept('PUT', '/act/rest/actas/*/actaestudiantes/sincroniza').as('sincroniza');
        cy.intercept('PUT', '/act/rest/actaestudiantes/*').as('poneNota');


        ponerNotas.visitSection();
        cy.waitRequest('@loadJson', 200);
        cy.waitRequest('@loadPreferencias', 200);
        cy.waitRequest('@loadNavigation', 200);
        cy.waitRequest('@loadUserInfo', 200);
        cy.waitRequest('@loadInformaciones', 200);
        cy.waitRequest('@loadProfesorResumen', 200);

        cy.waitRequest('@loadConvocatoriasDetalle', 200);
        cy.waitRequest('@loadCursos', 200);
    });

    it('Poner nota a un alumno', () => {
        ponerNotas.selectCurso();
        cy.waitRequest('@loadActas', 200);
        ponerNotas.selectActa();
        cy.waitRequest('@loadActaconvocatorias', 200);
        ponerNotas.selectConvocatoriaGridRow();
        cy.waitRequest('@sincroniza', 200);
        cy.waitRequest('@loadEstudiantes', 200);
        ponerNotas.ponerNota();
        cy.waitRequest('@poneNota', 200);
    })
})