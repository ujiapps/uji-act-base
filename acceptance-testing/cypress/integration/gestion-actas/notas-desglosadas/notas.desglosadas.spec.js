import NotasDesglosadasPage from "../../../pagesobjects/gestion-actas/notas-desglosadas/NotasDesglosadas";

describe('Test notas desglosadas', () => {

    let page;
    let curso = '2018';
    let acta = 'AE1005, EC1005 i FC1005';
    let nombreGrupo = 'descripcion grupo e2e';
    let etiquetaGrupo = 'cytag'; // sin espacios en blanco
    let notaMinGrupo = '0';
    let notaMaxGrupo = '10';
    let notaMinAprobadoGrupo = '5';
    let nombrePregunta = 'nombre pregunta e2e';
    let etiquetaPregunta = 'e2etag';
    let notaDesglosada = '6';

    beforeEach(() => {
        cy.intercept('GET', '/act/classic.json*').as('loadJson');
        cy.intercept('GET', '/act/rest/usuario/preferencias*').as('loadPreferencias');
        cy.intercept('GET', '/act/rest/navigation/class*').as('loadNavigation');
        cy.intercept('GET', '/act/rest/userinfo*').as('loadUserInfo');
        cy.intercept('GET', '/act/rest/informaciones/*').as('loadInformaciones');
        cy.intercept('GET', '/act/rest/profesor/diligencias/resumen/*').as('loadProfesorResumen');

        cy.intercept('GET', '/act/rest/cursos/*').as('loadCursos');
        cy.intercept('GET', '/act/rest/convocatorias/detalle/*').as('loadConvocatoriasDetalle');
        cy.intercept('GET', '/act/rest/actas/*/desglosenotas*').as('loadDesglosenotas');
        cy.intercept('GET', '/act/rest/cursos/*/actas*').as('loadActas');
        cy.intercept('GET', '/act/rest/cursos/*/actaconvocatorias*').as('loadActaconvocatorias');
        cy.intercept('PUT', '/act/rest/actas/*/actaestudiantes/sincroniza').as('sincroniza');

        page = new NotasDesglosadasPage(cy);
        page.visitaSeccion();
        cy.waitRequest('@loadJson', 200);
        cy.waitRequest('@loadPreferencias', 200);
        cy.waitRequest('@loadNavigation', 200);
        cy.waitRequest('@loadUserInfo', 200);
        cy.waitRequest('@loadInformaciones', 200);
        cy.waitRequest('@loadProfesorResumen', 200);

        cy.waitRequest('@loadConvocatoriasDetalle', 200);
        cy.waitRequest('@loadCursos', 200);
    });

    it('Test nueva notas desglosadas', () => {
        page.buscaCurso(curso);
        cy.waitRequest('@loadActas', 200);
        page.buscaActa(acta);
        cy.waitRequest('@loadActaconvocatorias', 200);
        page.getFilaExamenParcial().click();
        cy.waitRequest('@sincroniza', 200);
        page.selectNotasDesglosadasTab();
        cy.waitRequest('@loadDesglosenotas', 200).then(() => {
            page.setNotaDesglosada(notaDesglosada);
        });

    });
});