import ExamenParcialPage from "../../../pagesobjects/gestion-actas/nuevo-parcial/ExamenParcial";

describe('Test examen parcial', () => {

    let page;
    let curso = "2019";
    let acta = "PE0932 Treball de Final de Grau";
    let descripcionNuevoParcial = "Descripción nuevo parcial e2e";

    beforeEach(() => {
        page = new ExamenParcialPage(cy);
        cy.intercept('GET', '/act/classic.json*').as('loadJson');
        cy.intercept('GET', '/act/rest/usuario/preferencias*').as('loadPreferencias');
        cy.intercept('GET', '/act/rest/navigation/class*').as('loadNavigation');
        cy.intercept('GET', '/act/rest/userinfo*').as('loadUserInfo');
        cy.intercept('GET', '/act/rest/informaciones/*').as('loadInformaciones');
        cy.intercept('GET', '/act/rest/profesor/diligencias/resumen/*').as('loadProfesorResumen');

        cy.intercept('GET', '/act/rest/cursos/*').as('loadCursos');
        cy.intercept('GET', '/act/rest/convocatorias/detalle/*').as('loadConvocatoriasDetalle');
        cy.intercept('GET', '/act/rest/cursos/*/actas*').as('loadActas');
        cy.intercept('GET', '/act/rest/cursos/*/actaconvocatorias*').as('loadActaconvocatorias');

        cy.intercept('POST', 'act/rest/cursos/' + curso + '/actas').as('addExamenParcial');
        cy.intercept('DELETE', 'act/rest/cursos/' + curso + '/actas/*').as('deleteExamenParcial');
        page.visitaSeccion();
        cy.waitRequest('@loadJson', 200);
        cy.waitRequest('@loadPreferencias', 200);
        cy.waitRequest('@loadNavigation', 200);
        cy.waitRequest('@loadUserInfo', 200);
        cy.waitRequest('@loadInformaciones', 200);
        cy.waitRequest('@loadProfesorResumen', 200);

        cy.waitRequest('@loadConvocatoriasDetalle', 200);
        cy.waitRequest('@loadCursos', 200);
    });

    it('POST examen parcial', () => {
        page.buscaCurso(curso);
        cy.waitRequest('@loadActas', 200);
        page.buscaActa(acta);
        cy.waitRequest('@loadActaconvocatorias', 200);
        page.getBotonCrearExamenParcial().click();

        page.setDescripcionNuevoParcial(descripcionNuevoParcial);
        page.getBotonGuardarDescripcion().click();
        cy.waitRequest('@addExamenParcial', 200);
        cy.waitRequest('@loadActaconvocatorias', 200);
    });

    it('DELETE examen parcial', () => {
        page.buscaCurso(curso);
        cy.waitRequest('@loadActas', 200);
        page.buscaActa(acta);
        cy.waitRequest('@loadActaconvocatorias', 200);
        page.borraExamenParcial();
        cy.messageBoxAcceptBtn().click();
        cy.waitRequest('@deleteExamenParcial', 200);
        cy.waitRequest('@loadActaconvocatorias', 200);
    });
});