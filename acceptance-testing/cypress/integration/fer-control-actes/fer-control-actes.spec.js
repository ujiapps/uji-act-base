import FerControlActesPage from "../../pagesobjects/fer-control-actes/FerControlActes";

describe('Testing control d\'actes', () => {
    let page;

    /*** NO SE COMPRUEBA POR COMPLETO EL ENVIO PORQUE NO TENEMOS ACCESO AL ENVIO ***/

    beforeEach(() => {
        page = new FerControlActesPage(cy);

        cy.intercept('GET', '/act/classic.json*').as('loadJson');
        cy.intercept('GET', '/act/rest/usuario/preferencias*').as('loadPreferencias');
        cy.intercept('GET', '/act/rest/navigation/class*').as('loadNavigation');
        cy.intercept('GET', '/act/rest/userinfo*').as('loadUserInfo');
        cy.intercept('GET', '/act/rest/informaciones/*').as('loadInformaciones');
        cy.intercept('GET', '/act/rest/profesor/diligencias/resumen/*').as('loadProfesorResumen');

        cy.intercept('GET', '/act/rest/cursos/*').as('loadCursos');
        cy.intercept('GET', '/act/rest/usuario/roles/*').as('loadRoles');
        cy.intercept('GET', '/act/rest/convocatorias/*').as('loadConvocatorias');

        cy.intercept('GET', '/act/rest/convocatorias/detalle/*').as('loadConvocatoriasDetalle');
        cy.intercept('GET', '/act/rest/cursos/2018/actas/admin*').as('loadActasAdmin');
        cy.intercept('POST', '/act/rest/cursos/*/convocatorias/1/enviarnotificacion').as('postEnvio');

        page.visitSection();
        cy.waitRequest('@loadJson', 200);
        cy.waitRequest('@loadPreferencias', 200);
        cy.waitRequest('@loadNavigation', 200);
        cy.waitRequest('@loadUserInfo', 200);
        cy.waitRequest('@loadInformaciones', 200);
        cy.waitRequest('@loadProfesorResumen', 200);

        cy.waitRequest('@loadCursos', 200);
        cy.waitRequest('@loadRoles', 200);
        cy.waitRequest('@loadConvocatorias', 200);
    })

    it('Enviar notificación', () => {
        page.selectComboMain(1, '2018');
        cy.waitRequest('@loadConvocatoriasDetalle', 200);
        page.selectComboMain(2, '1a ordinària (sem 1)');
        cy.waitRequest('@loadActasAdmin', 200);

        /** Si entra en el if es porque el usuario tiene acceso a las actas **/
        cy.get('@loadActasAdmin').should((response) => {
            if (response.response.body.data.length != 0) {
                page.selectComboFiltros(1, 'Economia');
                page.selectComboFiltros(2, 'Generades');
                page.selectComboFiltros(3, 'Master');
                page.selectComboFiltros(5, 'Traspassades');

                page.getEnviarGridBtn().click();

                /** Aquí realizamos la peticion ( que dará error ) **/
                // page.getEnviarFormBtn().click()
                // cy.wait('@postEnvio').its('response.statusCode').should('eq',500);
                // page.getToolClose().click()

                page.getCancelarBtn().click();
            }
        })
    })
})