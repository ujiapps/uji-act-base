import FerDiligenciesPage from "../../pagesobjects/fer-diligencies/FerDiligencies";

describe('Testing Diligencies', () => {

    let page;

    /*** DEJARÁ DE FUNCIONAR CUANDO CAMBIEN EN LA BBDD LAS ACTAS SEAN EDITABLES SIEMPRE ***/
    /*** NO SE COMPRUEBA QUE SE COMPLETE LA DILIGENCIA PORQUE ENVIA UN MENSAJE AL FINAL DEL RECORRIDO Y NO TENEMOS ACCESO ***/
    /*** SE DEBE REALIZAR EL TEST CON EL USUARIO ADECUADO ***/

    beforeEach(() => {
        page = new FerDiligenciesPage(cy);

        cy.intercept('GET', '/act/classic.json*').as('loadJson');
        cy.intercept('GET', '/act/rest/usuario/preferencias*').as('loadPreferencias');
        cy.intercept('GET', '/act/rest/navigation/class*').as('loadNavigation');
        cy.intercept('GET', '/act/rest/userinfo*').as('loadUserInfo');
        cy.intercept('GET', '/act/rest/informaciones/*').as('loadInformaciones');
        cy.intercept('GET', '/act/rest/profesor/diligencias/resumen/*').as('loadProfesorResumen');

        cy.intercept('GET', '/act/rest/convocatorias/detalle/*').as('loadConvocatoriasDetalle');
        cy.intercept('GET', '/act/rest/cursos/*').as('loadCursos');
        cy.intercept('GET', '/act/rest/convocatorias/oficiales/*').as('loadConvocatorias');
        cy.intercept('GET', '/act/rest/cursos/*/actas/traspasadas*').as('loadActas');
        cy.intercept('GET', '/act/rest/actas/*/actadiligencias*').as('loadActasDiligencias');
        cy.intercept('POST', '/act/rest/actas/*/actadiligencias').as('postActasDiligencias');

        page.visitSection();
        cy.waitRequest('@loadJson', 200);
        cy.waitRequest('@loadPreferencias', 200);
        cy.waitRequest('@loadNavigation', 200);
        cy.waitRequest('@loadUserInfo', 200);
        cy.waitRequest('@loadInformaciones', 200);
        cy.waitRequest('@loadProfesorResumen', 200);

        cy.waitRequest('@loadConvocatoriasDetalle', 200);
        cy.waitRequest('@loadCursos', 200);
        cy.waitRequest('@loadConvocatorias', 200);
    });

    it('Testear sección Diligencias', () => {
        page.selectItemFromComboBox(1, '2018');
        page.selectItemFromComboBox(2, '2a ordinària (sem 1)');
        cy.waitRequest('@loadActas', 200);

        /** Si entra en el if es porque el usuario tiene acceso a las actas **/
        cy.get('@loadActas').should((response) => {
            if (response.response.body.data.length != 0){
                page.selectItemFromComboBox(3);
                cy.waitRequest('@loadActasDiligencias', 200);

                page.getTraspasarBtn().click();
                cy.uniqueMessageBoxAcceptBtn().click();

                page.selectActionColumnComentari();
                page.writeTextAreaFormComentari('Cypress Test');
                page.getDesarBtn().click();

                /*** Aquí realizamos la peticion ( que dará error ) ***/
                // page.getTraspasarBtn().click()
                // page.getTraspasarResumBtn().click()
                // cy.wait('@postActasDiligencias').its('response.statusCode').should('eq',500);
                // page.getToolClose().click()
                // page.getCancelarBtn().click()

                page.selectActionColumnComentari();
                page.clearTextAreaFormComentari();
                page.getDesarBtn().click();
            }
        })
    })
})