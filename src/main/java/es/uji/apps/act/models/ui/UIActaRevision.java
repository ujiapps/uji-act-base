package es.uji.apps.act.models.ui;

public class UIActaRevision
{
    String nombre;

    String asignaturas;

    String grupos;

    String fecha;

    String horaInicio;

    String horaFin;

    String lugar;

    String observaciones;

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getAsignaturas()
    {
        return asignaturas;
    }

    public void setAsignaturas(String asignaturas)
    {
        this.asignaturas = asignaturas;
    }

    public String getGrupos()
    {
        return grupos;
    }

    public void setGrupos(String grupos)
    {
        this.grupos = grupos;
    }

    public String getFecha()
    {
        return fecha;
    }

    public void setFecha(String fecha)
    {
        this.fecha = fecha;
    }

    public String getHoraInicio()
    {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio)
    {
        this.horaInicio = horaInicio;
    }

    public String getHoraFin()
    {
        return horaFin;
    }

    public void setHoraFin(String horaFin)
    {
        this.horaFin = horaFin;
    }

    public String getLugar()
    {
        return lugar;
    }

    public void setLugar(String lugar)
    {
        this.lugar = lugar;
    }

    public String getObservaciones()
    {
        return observaciones;
    }

    public void setObservaciones(String observaciones)
    {
        this.observaciones = observaciones;
    }
}
