package es.uji.apps.act.models.views;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;

import es.uji.apps.act.exceptions.NotaNoValidaException;
import es.uji.apps.act.models.Persona;
import es.uji.apps.act.models.enums.Calificacion;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_VW_ACTAS_DILIGENCIAS")
public class ActaDiligenciaVirtual implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "acta_estudiante_id")
    private Long actaEstudianteId;

    @Column(name = "acta_id")
    private Long actaId;

    @Column(name = "alumno_id")
    private Long alumnoId;

    @Column(name = "nombre_alumno")
    private String alumnoNombre;

    @Column(name = "identificacion")
    private String identificacion;

    @Column(name = "identificacion_ofuscado")
    private String identificacionOfuscado;

    @Column(name = "persona_id_alta")
    private Long personaIdAlta;

    @Column(name = "persona_alta")
    private String personaAlta;

    @Column(name = "persona_id_traspaso")
    private Long personaIdTraspaso;

    @ManyToOne
    @JoinColumn(name = "persona_id_nota")
    private Persona personaNota;

    @Column(name = "fecha_nota")
    private Date fechaNota;

    @Column(name = "persona_traspaso")
    private String personaTraspaso;

    @Column(name = "nota")
    private Double nota;

    @Column(name = "calificacion_id")
    private Long calificacionId;

    @Column(name = "grupo_id")
    private String grupoId;

    @Column(name = "comentario_diligencia")
    private String comentario;

    @Column(name = "comentario_traspaso")
    private String comentarioTraspaso;

    @Column(name = "tiene_diligencias")
    private Boolean tieneDiligencias;

    @Column(name = "no_presentado")
    private Boolean noPresentado;

    @Column(name = "fecha_traspaso")
    private Date fechaTraspaso;

    @Column(name = "motivo_exclusion")
    private String motivoExclusion;

    @Column(name = "curso_academico_id")
    private Long cursoId;

    @Column(name = "asignatura_id")
    private String asignaturaId;

    @Column(name = "cuenta")
    private String cuenta;

    public Long getActaEstudianteId()
    {
        return actaEstudianteId;
    }

    public void setActaEstudianteId(Long actaEstudianteId)
    {
        this.actaEstudianteId = actaEstudianteId;
    }

    public Long getActaId()
    {
        return actaId;
    }

    public void setActaId(Long actaId)
    {
        this.actaId = actaId;
    }

    public Long getAlumnoId()
    {
        return alumnoId;
    }

    public void setAlumnoId(Long alumnoId)
    {
        this.alumnoId = alumnoId;
    }

    public String getAlumnoNombre()
    {
        return alumnoNombre;
    }

    public void setAlumnoNombre(String alumnoNombre)
    {
        this.alumnoNombre = alumnoNombre;
    }

    public Double getNota()
    {
        return nota;
    }

    public void setNota(Double nota)
    {
        this.nota = nota;
    }

    public Long getCalificacionId()
    {
        return calificacionId;
    }

    public void setCalificacionId(Long calificacionId)
    {
        this.calificacionId = calificacionId;
    }

    public String getIdentificacion()
    {
        return identificacion;
    }

    public void setIdentificacion(String identificacion)
    {
        this.identificacion = identificacion;
    }

    public String getIdentificacionOfuscado()
    {
        return identificacionOfuscado;
    }

    public void setIdentificacionOfuscado(String identificacionOfuscado)
    {
        this.identificacionOfuscado = identificacionOfuscado;
    }

    public String getComentario()
    {
        return comentario;
    }

    public void setComentario(String comentario)
    {
        this.comentario = comentario;
    }

    public void checkNotaAcordeConCalificacion() throws NotaNoValidaException
    {
        Long calificacionByNota = Calificacion.getCalificacionIdByNota(nota);

        if (calificacionByNota == null && calificacionId == 0)
        {
            return;
        }

        if (calificacionByNota.equals(Calificacion.SOBRESALIENTE.getId())
                && calificacionId.equals(Calificacion.MATRICULA_HONOR.getId()))
        {
            return;
        }

        if (!calificacionId.equals(calificacionByNota))
        {
            throw new NotaNoValidaException(MessageFormat.format(
                    "La nota introduïda {0} per al alumne/a {1} no és correcta respecte a la qualificació",
                    nota, alumnoNombre));
        }
    }

    public Boolean isTieneDiligencias()
    {
        return tieneDiligencias;
    }

    public void setTieneDiligencias(Boolean tieneDiligencias)
    {
        this.tieneDiligencias = tieneDiligencias;
    }

    public String getMotivoExclusion()
    {
        return motivoExclusion;
    }

    public void setMotivoExclusion(String motivoExclusion)
    {
        this.motivoExclusion = motivoExclusion;
    }

    public Long getCursoId()
    {
        return cursoId;
    }

    public void setCursoId(Long cursoId)
    {
        this.cursoId = cursoId;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public String getGrupoId()
    {
        return grupoId;
    }

    public void setGrupoId(String grupoId)
    {
        this.grupoId = grupoId;
    }

    public Long getPersonaIdAlta()
    {
        return personaIdAlta;
    }

    public void setPersonaIdAlta(Long personaIdAlta)
    {
        this.personaIdAlta = personaIdAlta;
    }

    public String getPersonaAlta()
    {
        return personaAlta;
    }

    public void setPersonaAlta(String personaAlta)
    {
        this.personaAlta = personaAlta;
    }

    public Long getPersonaIdTraspaso()
    {
        return personaIdTraspaso;
    }

    public void setPersonaIdTraspaso(Long personaIdTraspaso)
    {
        this.personaIdTraspaso = personaIdTraspaso;
    }

    public String getPersonaTraspaso()
    {
        return personaTraspaso;
    }

    public void setPersonaTraspaso(String personaTraspaso)
    {
        this.personaTraspaso = personaTraspaso;
    }

    public void setPersonaNota(Persona personaNota)
    {
        this.personaNota = personaNota;
    }

    public Persona getPersonaNota()
    {
        return personaNota;
    }

    public Boolean getNoPresentado()
    {
        return noPresentado;
    }

    public void setNoPresentado(Boolean noPresentado)
    {
        this.noPresentado = noPresentado;
    }

    public String getComentarioTraspaso()
    {
        return comentarioTraspaso;
    }

    public void setComentarioTraspaso(String comentarioTraspaso)
    {
        this.comentarioTraspaso = comentarioTraspaso;
    }

    public String getCuenta()
    {
        return cuenta;
    }

    public void setCuenta(String cuenta)
    {
        this.cuenta = cuenta;
    }

    public Date getFechaTraspaso()
    {
        return fechaTraspaso;
    }

    public void setFechaTraspaso(Date fechaTraspaso)
    {
        this.fechaTraspaso = fechaTraspaso;
    }

    public Date getFechaNota()
    {
        return fechaNota;
    }

    public void setFechaNota(Date fechaNota)
    {
        this.fechaNota = fechaNota;
    }

    public String getCalificacionAsString()
    {
        if (calificacionId == null || calificacionId == 0)
        {
            return "No presentat";
        }
        Calificacion calificacion = Calificacion.getCalificacionById(calificacionId);
        return calificacion.getNombre();
    }
}