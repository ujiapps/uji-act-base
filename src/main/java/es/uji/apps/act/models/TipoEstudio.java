package es.uji.apps.act.models;

import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_TIPOS_ESTUDIOS")
public class TipoEstudio implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Column
    private String nombre;

    @Column
    private Boolean oficial;

    @OneToMany(mappedBy = "tipoEstudio")
    private Set<Acta> actas;

    @OneToMany(mappedBy = "tipoEstudio")
    private Set<FechaConvocatoria> fechasConvocatoria;

    @OneToMany(mappedBy = "tipoEstudio")
    private Set<Asignatura> asignaturas;

    @OneToMany(mappedBy = "tipoEstudio")
    private Set<Estudio> estudios;

    public TipoEstudio()
    {
    }

    public TipoEstudio(String tipoEstudioId)
    {
        id = tipoEstudioId;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Boolean isOficial()
    {
        return oficial;
    }

    public void setOficial(Boolean oficial)
    {
        this.oficial = oficial;
    }

    public Set<Acta> getActas()
    {
        return actas;
    }

    public void setActas(Set<Acta> actas)
    {
        this.actas = actas;
    }

    public Set<FechaConvocatoria> getFechasConvocatoria()
    {
        return fechasConvocatoria;
    }

    public void setFechasConvocatoria(Set<FechaConvocatoria> fechasConvocatoria)
    {
        this.fechasConvocatoria = fechasConvocatoria;
    }

    public Set<Asignatura> getAsignaturas()
    {
        return asignaturas;
    }

    public void setAsignaturas(Set<Asignatura> asignaturas)
    {
        this.asignaturas = asignaturas;
    }

    public Set<Estudio> getEstudios()
    {
        return estudios;
    }

    public void setEstudios(Set<Estudio> estudios)
    {
        this.estudios = estudios;
    }
}