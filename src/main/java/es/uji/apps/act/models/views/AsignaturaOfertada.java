package es.uji.apps.act.models.views;

import org.hibernate.annotations.BatchSize;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_EXT_ASIGNATURAS_OFERTADAS")
public class AsignaturaOfertada implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "CURSO_ACA")
    private Long curso;

    @Id
    @Column(name = "ASI_ID")
    private String asignaturaId;

    @Column(name = "ESTUDIO_ID")
    private Long estudioId;

    @Column(name = "TIPO")
    private String tipo;

    @Column(name = "NOMBRE_CA")
    private String nombre;

    public Long getCurso() {
        return curso;
    }

    public void setCurso(Long curso) {
        this.curso = curso;
    }

    public String getAsignaturaId() {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId) {
        this.asignaturaId = asignaturaId;
    }

    public Long getEstudioId() {
        return estudioId;
    }

    public void setEstudioId(Long estudioId) {
        this.estudioId = estudioId;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}