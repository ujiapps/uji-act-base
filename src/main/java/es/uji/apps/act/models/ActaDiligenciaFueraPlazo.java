package es.uji.apps.act.models;

import es.uji.apps.act.exceptions.NotaNoValidaException;
import es.uji.apps.act.models.enums.Calificacion;
import es.uji.apps.act.models.enums.EstadoDiligenciaFueraPlazo;
import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


@Entity
@BatchSize(size = 100)
@Table(name = "ACT_ACTAS_DILIGENCIAS_F_PLAZO")
public class ActaDiligenciaFueraPlazo implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "acta_id")
    private Acta acta;

    @ManyToOne
    @JoinColumn(name = "acta_estudiante_id")
    private ActaEstudiante actaEstudiante;

    @ManyToOne
    @JoinColumn(name = "persona_id")
    private Persona persona;

    @Column(name = "fecha")
    private Date fecha;

    @Column(name = "estado")
    private Long estado;

    @Column(name = "nota_anterior")
    private Double notaAnterior;

    @Column(name = "calificacion_anterior")
    private Long calificacionAnteriorId;

    @Column(name = "nota_nueva")
    private Double notaNueva;

    @Column(name = "calificacion_nueva")
    private Long calificacionNuevaId;

    @Column(name = "comentario_diligencia")
    private String comentarioDiligencia;

    @Column(name = "IP")
    private String ip;

    @Column(name = "fecha_moderacion")
    private Date fechaModeracion;

    @Column(name = "fecha_firma")
    private Date fechaFirma;

    @Column(name = "comentario_denegacion")
    private String comentarioDenegacion;

    @Column(name = "admins")
    private String admins;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Acta getActa() {
        return acta;
    }

    public void setActa(Acta acta) {
        this.acta = acta;
    }

    public ActaEstudiante getActaEstudiante() {
        return actaEstudiante;
    }

    public void setActaEstudiante(ActaEstudiante actaEstudiante) {
        this.actaEstudiante = actaEstudiante;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Double getNotaAnterior() {
        return notaAnterior;
    }

    public void setNotaAnterior(Double notaAnterior) {
        this.notaAnterior = notaAnterior;
    }

    public Long getCalificacionAnteriorId() {
        return calificacionAnteriorId;
    }

    public void setCalificacionAnteriorId(Long calificacionAnteriorId) {
        this.calificacionAnteriorId = calificacionAnteriorId;
    }

    public Double getNotaNueva() {
        return notaNueva;
    }

    public void setNotaNueva(Double notaNueva) {
        this.notaNueva = notaNueva;
    }

    public Long getCalificacionNuevaId() {
        return calificacionNuevaId;
    }

    public void setCalificacionNuevaId(Long calificacionNuevaId) {
        this.calificacionNuevaId = calificacionNuevaId;
    }

    public String getComentarioDiligencia() {
        return comentarioDiligencia;
    }

    public void setComentarioDiligencia(String comentarioDiligencia) {
        this.comentarioDiligencia = comentarioDiligencia;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Date getFechaModeracion() {
        return fechaModeracion;
    }

    public void setFechaModeracion(Date fechaModeracion) {
        this.fechaModeracion = fechaModeracion;
    }

    public String getComentarioDenegacion() {
        return comentarioDenegacion;
    }

    public void setComentarioDenegacion(String comentarioDenegacion) {
        this.comentarioDenegacion = comentarioDenegacion;
    }

    public Date getFechaFirma() {
        return fechaFirma;
    }

    public void setFechaFirma(Date fechaFirma) {
        this.fechaFirma = fechaFirma;
    }

    public Long getEstado() {
        return estado;
    }

    public void setEstado(Long estado) {
        this.estado = estado;
    }

    public String getEstadoTxt() {
        EstadoDiligenciaFueraPlazo estadoDiligenciaFueraPlazo = EstadoDiligenciaFueraPlazo.getEstadoDiligenciaFueraPlazoById(estado);
        return estadoDiligenciaFueraPlazo.getNombre();
    }

    public Boolean isCaducada() {
        if (fechaModeracion == null) {
            return false;
        }
        Date fechaCaducidad = addDays(fechaModeracion, 7);
        return (estado == EstadoDiligenciaFueraPlazo.APROBADA.getId()) && fechaCaducidad.before(new Date());
    }

    private Date addDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }

    public void checkNotaAcordeConCalificacion() throws NotaNoValidaException {
        Long calificacionByNota = Calificacion.getCalificacionIdByNota(notaNueva);

        if (calificacionByNota == null && (calificacionNuevaId == null || calificacionNuevaId == 0)) {
            return;
        }

        if (calificacionByNota.equals(Calificacion.SOBRESALIENTE.getId())
                && calificacionNuevaId.equals(Calificacion.MATRICULA_HONOR.getId())) {
            return;
        }

        if (!calificacionNuevaId.equals(calificacionByNota)) {
            throw new NotaNoValidaException(MessageFormat.format(
                    "La nota introduïda {0} no és correcta respecte a la qualificació",
                    notaNueva));
        }
    }

    public List<String> getAdmins() {
        return Arrays.asList(this.admins.split(","));
    }

    public String getCalificacionNuevaAsString() {
        if (calificacionNuevaId == null) {
            return "No presentat";
        }
        Calificacion calificacion = Calificacion.getCalificacionById(calificacionNuevaId);
        return calificacion.getNombre();
    }
}