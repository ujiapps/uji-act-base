package es.uji.apps.act.models;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import es.uji.apps.act.exceptions.TraspasarDiligenciaException;

@Component
public class RemoteTraspasarDiligenciaProcedure {
    private TraspasarDiligenciaProcedure traspasarDiligenciaProcedure;
    private static DataSource dataSource;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        RemoteTraspasarDiligenciaProcedure.dataSource = dataSource;
    }

    public void init() {
        this.traspasarDiligenciaProcedure = new TraspasarDiligenciaProcedure(dataSource);
    }

    public Long execute(Long actaId, Long perId) throws IOException, TraspasarDiligenciaException {
        return traspasarDiligenciaProcedure.execute(actaId, perId);
    }

    private class TraspasarDiligenciaProcedure extends StoredProcedure {
        private static final String SQL = "pack_actas.traspasar_diligencias";
        private static final String ACTA_ID = "p_acta";
        private static final String PERSONA_ID = "p_per_id";

        public TraspasarDiligenciaProcedure(DataSource dataSource) {
            super(dataSource, SQL);
            declareParameter(new SqlOutParameter("codigoError", Types.BIGINT));
            declareParameter(new SqlParameter(ACTA_ID, Types.BIGINT));
            declareParameter(new SqlParameter(PERSONA_ID, Types.BIGINT));
            setFunction(true);
            compile();
        }

        public Long execute(Long actaId, Long perId)
                throws IOException, TraspasarDiligenciaException {
            Map<String, Object> inParams = new HashMap();
            Map<String, Object> results;
            inParams.put(ACTA_ID, actaId);
            inParams.put(PERSONA_ID, perId);
            try {
                results = execute(inParams);
            } catch (Exception e) {
                e.printStackTrace();
                SQLException sqlException = (SQLException) e.getCause();
                System.out.println(sqlException);
                throw new TraspasarDiligenciaException();
            }

            return !results.isEmpty() ? (Long) results.get("codigoError") : null;
        }
    }
}
