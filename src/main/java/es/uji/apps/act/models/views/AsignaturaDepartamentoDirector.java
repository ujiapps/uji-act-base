package es.uji.apps.act.models.views;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_EXT_ASIGNATURAS_DEP_DIR")
public class AsignaturaDepartamentoDirector implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "CURSO_ACA")
    private Long curso;

    @Id
    @Column(name = "ASI_ID")
    private Long asignaturaId;

    @Id
    @Column(name = "UEST_ID")
    private Long departamentoId;

    @Column(name = "NOMBRE")
    private String departamentoNombre;

    @Id
    @Column(name = "TIPO")
    private String tipo;

    @Id
    @Column(name = "DIR_DEP_PER_ID")
    private Long directorDepartamentoId;

    @Id
    @Column(name = "TIT_ID")
    private Long titulacionId;

    @Column(name = "ESTUDIO")
    private String titulacionNombre;

    @Id
    @Column(name = "CENTRO_ID")
    private Long centroId;

    @Id
    @Column(name = "DIR_CENTRO_PER_ID")
    private Long directorCentroId;

    public Long getCurso()
    {
        return curso;
    }

    public void setCurso(Long curso)
    {
        this.curso = curso;
    }

    public Long getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(Long asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public Long getDepartamentoId()
    {
        return departamentoId;
    }

    public void setDepartamentoId(Long departamentoId)
    {
        this.departamentoId = departamentoId;
    }

    public String getDepartamentoNombre()
    {
        return departamentoNombre;
    }

    public void setDepartamentoNombre(String departamentoNombre)
    {
        this.departamentoNombre = departamentoNombre;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public Long getDirectorDepartamentoId()
    {
        return directorDepartamentoId;
    }

    public void setDirectorDepartamentoId(Long directorDepartamentoId)
    {
        this.directorDepartamentoId = directorDepartamentoId;
    }

    public Long getTitulacionId()
    {
        return titulacionId;
    }

    public void setTitulacionId(Long titulacionId)
    {
        this.titulacionId = titulacionId;
    }

    public String getTitulacionNombre()
    {
        return titulacionNombre;
    }

    public void setTitulacionNombre(String titulacionNombre)
    {
        this.titulacionNombre = titulacionNombre;
    }

    public Long getCentroId()
    {
        return centroId;
    }

    public void setCentroId(Long centroId)
    {
        this.centroId = centroId;
    }

    public Long getDirectorCentroId()
    {
        return directorCentroId;
    }

    public void setDirectorCentroId(Long directorCentroId)
    {
        this.directorCentroId = directorCentroId;
    }
}