package es.uji.apps.act.models.enums;

public enum EstadoDiligenciaFueraPlazo
{
    PENDIENTE(0L, "PENDENT"), APROBADA(1L, "APROVADA"), FIRMADA(2L, "SIGNADA"), DENEGADA(3L, "DENEGADA");

    private final Long id;
    private final String nombre;

    EstadoDiligenciaFueraPlazo(Long id, String nombre)
    {
        this.id = id;
        this.nombre = nombre;
    }

    public Long getId()
    {
        return id;
    }

    public String getNombre()
    {
        return nombre;
    }

    @Override
    public String toString()
    {
        return nombre;
    }

    public static EstadoDiligenciaFueraPlazo getEstadoDiligenciaFueraPlazoById(Long estadoId)
    {
        for (EstadoDiligenciaFueraPlazo estadoDiligenciaFueraPlazo : EstadoDiligenciaFueraPlazo.values())
        {
            if (estadoDiligenciaFueraPlazo.getId().equals(estadoId))
            {
                return estadoDiligenciaFueraPlazo;
            }
        }
        return null;
    }
}
