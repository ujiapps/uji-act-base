package es.uji.apps.act.models;

import es.uji.apps.act.exceptions.NoAutorizadoException;
import es.uji.apps.act.exceptions.PasarNotasExpedienteException;
import es.uji.apps.act.exceptions.TraspasarActaException;
import es.uji.commons.rest.annotations.DataTag;
import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.io.IOException;
import java.io.Serializable;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;


@Entity
@BatchSize(size = 100)
@Table(name = "ACT_ACTAS")
public class Acta implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "curso_academico_id")
    private Curso curso;

    @ManyToOne
    @JoinColumn(name = "convocatoria_id")
    private Convocatoria convocatoria;

    @ManyToOne
    @JoinColumn(name = "tipo_Estudio_id")
    private TipoEstudio tipoEstudio;

    @Column(name = "acta_unica")
    private Boolean actaUnica;

    @Column(name = "fecha_alta")
    private Date fechaAlta;

    @Column(name = "oficial")
    private Boolean oficial;

    @DataTag
    @Column
    private String descripcion;

    @Column(name = "per_id")
    private Long personaId;

    @Column(name = "ubicacion_id")
    private String ubicacionId;

    @Column(name = "fecha_traspaso")
    private Date fechaTraspaso;

    @Column(name = "persona_id_traspaso")
    private Long personaIdTraspaso;

    @Column(name = "fecha_firma_digital")
    private Date fechaFirmaDigital;

    @Column(name = "referencia")
    private String referencia;

    @Column(name = "permite_detalle_notas")
    private Boolean desgloseActivo;

    @Column(name = "codigo")
    private String codigo;

    @Column(name = "IP")
    private String ip;

    @OneToMany(mappedBy = "acta", cascade = CascadeType.REMOVE)
    private Set<ActaAsignatura> actasAsignaturas;

    @OneToMany(mappedBy = "acta")
    private Set<ActaDiligencia> actaDiligencias;

    @OneToMany(mappedBy = "acta")
    private Set<ActaDiligenciaFueraPlazo> actaDiligenciasFueraPlazo;

    @OneToMany(mappedBy = "acta", cascade = CascadeType.REMOVE)
    private Set<ActaRevision> actasRevisiones;

    @OneToMany(mappedBy = "acta", cascade = CascadeType.REMOVE)
    private Set<DesgloseGrupo> desglosesGrupos;

    @OneToMany(mappedBy = "acta", cascade = CascadeType.REMOVE)
    private Set<DesglosePregunta> desglosePreguntas;

    @OneToMany(mappedBy = "parcialActa", cascade = CascadeType.REMOVE)
    private Set<DesglosePregunta> actaParcialDesglosePreguntas;

    @OneToMany(mappedBy = "acta", cascade = CascadeType.REMOVE)
    private Set<Notificacion> actaNotificaciones;

    public Acta()
    {
    }

    public Acta(Long actaId)
    {
        id = actaId;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Curso getCurso()
    {
        return curso;
    }

    public void setCurso(Curso curso)
    {
        this.curso = curso;
    }

    public Convocatoria getConvocatoria()
    {
        return convocatoria;
    }

    public void setConvocatoria(Convocatoria convocatoria)
    {
        this.convocatoria = convocatoria;
    }

    public TipoEstudio getTipoEstudio()
    {
        return tipoEstudio;
    }

    public void setTipoEstudio(TipoEstudio tipoEstudio)
    {
        this.tipoEstudio = tipoEstudio;
    }

    public Boolean isActaUnica()
    {
        return actaUnica;
    }

    public void setActaUnica(Boolean actaUnica)
    {
        this.actaUnica = actaUnica;
    }

    public Date getFechaAlta()
    {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta)
    {
        this.fechaAlta = fechaAlta;
    }

    public Boolean isOficial()
    {
        return oficial;
    }

    public void setOficial(Boolean oficial)
    {
        this.oficial = oficial;
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public String getUbicacionId()
    {
        return ubicacionId;
    }

    public void setUbicacionId(String ubicacionId)
    {
        this.ubicacionId = ubicacionId;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public Date getFechaTraspaso()
    {
        return fechaTraspaso;
    }

    public void setFechaTraspaso(Date fechaTraspaso)
    {
        this.fechaTraspaso = fechaTraspaso;
    }

    public Long getPersonaIdTraspaso()
    {
        return personaIdTraspaso;
    }

    public void setPersonaIdTraspaso(Long personaIdTraspaso)
    {
        this.personaIdTraspaso = personaIdTraspaso;
    }

    public Date getFechaFirmaDigital()
    {
        return fechaFirmaDigital;
    }

    public void setFechaFirmaDigital(Date fechaFirmaDigital)
    {
        this.fechaFirmaDigital = fechaFirmaDigital;
    }

    public String getReferencia()
    {
        return referencia;
    }

    public void setReferencia(String referencia)
    {
        this.referencia = referencia;
    }

    public Boolean isDesgloseActivo()
    {
        return desgloseActivo;
    }

    public void setDesgloseActivo(Boolean permiteDetalleNotas)
    {
        this.desgloseActivo = permiteDetalleNotas;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    public String getIp()
    {
        return ip;
    }

    public void setIp(String ip)
    {
        this.ip = ip;
    }

    public Set<ActaAsignatura> getActasAsignaturas()
    {
        return actasAsignaturas;
    }

    public void setActasAsignaturas(Set<ActaAsignatura> actasAsignaturas)
    {
        this.actasAsignaturas = actasAsignaturas;
    }

    public Set<ActaDiligencia> getActaDiligencias()
    {
        return actaDiligencias;
    }

    public void setActaDiligencias(Set<ActaDiligencia> actaDiligencias)
    {
        this.actaDiligencias = actaDiligencias;
    }

    public Set<ActaRevision> getActasRevisiones()
    {
        return actasRevisiones;
    }

    public void setActasRevisiones(Set<ActaRevision> actasRevisiones)
    {
        this.actasRevisiones = actasRevisiones;
    }

    public Set<DesgloseGrupo> getDesglosesGrupos()
    {
        return desglosesGrupos;
    }

    public void setDesglosesGrupos(Set<DesgloseGrupo> desglosesGrupos)
    {
        this.desglosesGrupos = desglosesGrupos;
    }

    public Set<DesglosePregunta> getDesglosePreguntas()
    {
        return desglosePreguntas;
    }

    public void setDesglosePreguntas(Set<DesglosePregunta> actasPreguntas)
    {
        this.desglosePreguntas = actasPreguntas;
    }

    public Set<DesglosePregunta> getActaParcialDesglosePreguntas()
    {
        return actaParcialDesglosePreguntas;
    }

    public void setActaParcialDesglosePreguntas(Set<DesglosePregunta> actaParcialDesglosePreguntas)
    {
        this.actaParcialDesglosePreguntas = actaParcialDesglosePreguntas;
    }

    public Set<Notificacion> getActaNotificaciones()
    {
        return actaNotificaciones;
    }

    public void setActaNotificaciones(Set<Notificacion> actaNotificaciones)
    {
        this.actaNotificaciones = actaNotificaciones;
    }

    public Set<ActaDiligenciaFueraPlazo> getActaDiligenciasFueraPlazo()
    {
        return actaDiligenciasFueraPlazo;
    }

    public void setActaDiligenciasFueraPlazo(Set<ActaDiligenciaFueraPlazo> actaDiligenciasFueraPlazo)
    {
        this.actaDiligenciasFueraPlazo = actaDiligenciasFueraPlazo;
    }

    public void traspasar(Long connectedUserId, String ip)
            throws IOException, TraspasarActaException
    {
        RemoteTraspasarActaProcedure remoteTraspasarActaProcedure = new RemoteTraspasarActaProcedure();
        remoteTraspasarActaProcedure.init();
        Long result = remoteTraspasarActaProcedure.execute(this.getId(), connectedUserId, ip);

        if (result != 1)
        {
            throw new TraspasarActaException("No s'ha pogut traspassar l'acta");
        }
    }

    public void destraspasar(Long connectedUserId) throws IOException, TraspasarActaException
    {
        RemoteDestraspasarActaProcedure remoteDestraspasarActaProcedure = new RemoteDestraspasarActaProcedure();
        remoteDestraspasarActaProcedure.init();
        remoteDestraspasarActaProcedure.execute(this.getId(), connectedUserId);
    }

    public boolean sePuedeSincronizar()
    {
        return (fechaTraspaso == null);
    }

    public boolean isEditable()
    {
        return (fechaTraspaso == null) && getCurso().getFechaFinModificacion().after(new Date());
    }

    public boolean isDiligenciable()
    {
        return (fechaTraspaso != null) && getCurso().getFechaFinModificacion().after(new Date());
    }

    public boolean isDiligenciableFueraPlazo()
    {
        return (fechaTraspaso != null) && getCurso().getFechaFinModificacion().before(new Date());
    }

    public void comprobarActaTraspasable() throws TraspasarActaException
    {
        if (fechaTraspaso != null || fechaFirmaDigital != null)
        {
            throw new TraspasarActaException("L'acta ja ha sigut traspassada.");
        }

        if (convocatoria.getId().equals(Convocatoria.EXAMEN_PARCIAL))
        {
            throw new TraspasarActaException(
                    "No es poden traspassar les notes associada a un examen parcial");
        }

        Long estudiantesValidos = getActaEstudiantes().stream().filter(ae -> ae.getMotivoExclusion() == null || ae.getMotivoExclusion().isEmpty()).count();
        if (estudiantesValidos == 0)
        {
            throw new TraspasarActaException("No es pot traspassar l'acta perquè no té cap alumne sense motiu d'exclusió");
        }

        checkFechasTraspaso();
        Boolean todosLosEstudiantesNoPresentados = getTodosLostEstudiantesNoPresentados();
        checkFechasRevision(todosLosEstudiantesNoPresentados);
        checkNotasValidas();
        checkRevisionTodosGrupos();
    }

    private Boolean getTodosLostEstudiantesNoPresentados()
    {
        for (ActaEstudiante actaEstudiante : getActaEstudiantes())
        {
            if (actaEstudiante.getCalificacionId() != null && actaEstudiante.getCalificacionId() > 0 && actaEstudiante.getNoPresentado() == false)
            {
                return false;
            }
        }

        return true;
    }

    private void checkRevisionTodosGrupos() throws TraspasarActaException
    {
        if (actasRevisiones.stream().anyMatch(ar -> ar.isConjunta()) || actaUnica || !isEditable())
        {
            return;
        }

        List<String> asignaturasGrupos = new ArrayList<>();
        for (ActaAsignatura actaAsignatura : actasAsignaturas)
        {
            String asignaturaId = actaAsignatura.getAsignatura().getId();
            List<String> gruposAsignatura = actaAsignatura.getActasEstudiantes().stream()
                    .map(ae -> ae.getGrupoId()).distinct().sorted().collect(Collectors.toList());

            for (String grupoId : gruposAsignatura)
            {
                if (actasRevisiones.stream().filter(ar -> ar.getAsignaturas().contains(asignaturaId)
                        && ar.getGrupos().contains(grupoId)).count() == 0)
                {
                    asignaturasGrupos
                            .add(MessageFormat.format("[{0} grup {1}] ", asignaturaId, grupoId));
                }
            }
        }

        if (!asignaturasGrupos.isEmpty())
        {
            throw new TraspasarActaException(
                    "L'acta no es pot traspassar perquè no hi ha una revisió definida per "
                            + asignaturasGrupos.stream().collect(Collectors.joining(", ")));
        }
    }

    private void checkNotasValidas() throws TraspasarActaException
    {
        for (ActaAsignatura actaAsignatura : actasAsignaturas)
        {
            if (actaAsignatura.getActasEstudiantes().stream()
                    .anyMatch(ae -> ae.getNotaEfectiva() != null && ae.getNotaEfectiva() > 10))
            {
                throw new TraspasarActaException(
                        "L'acta no es pot traspassar perquè hi ha notes no vàlides.");
            }

            if (actaAsignatura.getActasEstudiantes().stream()
                    .anyMatch(ae -> ae.getNotaEfectiva() != null && ae.getNotaEfectiva() < 9
                            && ae.isMatriculaHonor()))
            {
                throw new TraspasarActaException(
                        "L'acta no es pot traspassar perquè hi ha matrícules d'honor associades a notes inferiors a 9.");
            }
        }
    }

    private void checkFechasRevision(Boolean todosLosEstudiantesNoPresentados) throws TraspasarActaException
    {
        Date ahora = new Date();

        if (actasRevisiones.isEmpty() && !todosLosEstudiantesNoPresentados && !actaUnica && isEditable())
        {
            throw new TraspasarActaException("No hi ha dates de revisió definides.");
        }

        for (ActaRevision actaRevision : actasRevisiones)
        {
            if (actaRevision.getFecha() == null
                    || actaRevision.getFechaFin() == null && actaRevision.getLugar() == null)
            {
                throw new TraspasarActaException(
                        "Les dates de revisió no estan totalment completades en "
                                + actaRevision.getNombre() + ".");
            }

            if (actaRevision.getFecha().after(ahora) || actaRevision.getFechaFin().after(ahora))
            {
                throw new TraspasarActaException(
                        "Les dates de revisió no poden ser posteriors al traspàs.");
            }
        }
    }

    private void checkFechasTraspaso() throws TraspasarActaException
    {
        Date ahora = new Date();
        FechaConvocatoria fechaConvocatoria = getFechaConvocatoria();

        if (fechaConvocatoria != null && fechaConvocatoria.getFechaInicioTraspaso().after(ahora))
        {
            SimpleDateFormat humanDateFormat = new SimpleDateFormat("dd/MM/yyyy");
            throw new TraspasarActaException("No es pot traspassar l´acta fins el dia "
                    + humanDateFormat.format(fechaConvocatoria.getFechaInicioTraspaso()));
        }
    }

    private void checkFechasTraspasoNotasExpediente() throws TraspasarActaException
    {
        Date ahora = new Date();
        FechaConvocatoria fechaConvocatoria = getFechaConvocatoria();

        if (fechaConvocatoria.getFechaInicioActaUnica() == null)
        {
            throw new TraspasarActaException(
                    "N'hi ha un problema amb la convocatòria d'aquesta assignatura i no es poden passar les notes al expedient. Per favor, fica't en contacte amb el CAU per solucionar-ho.");
        }

        if (fechaConvocatoria.getFechaInicioActaUnica().after(ahora))
        {
            SimpleDateFormat humanDateFormat = new SimpleDateFormat("dd/MM/yyyy");
            throw new TraspasarActaException("No es poden passar les notes fins el dia "
                    + humanDateFormat.format(fechaConvocatoria.getFechaInicioActaUnica()));
        }
    }

    public FechaConvocatoria getFechaConvocatoria()
    {
        return convocatoria.getFechasConvocatoria().stream()
                .filter(fc -> fc.getTipoEstudio().equals(tipoEstudio)
                        && fc.getCurso().getId().equals(getCurso().getId()))
                .findFirst().orElse(null);
    }

    public Date getFechaLimiteAnularTraspaso()
    {
        if (convocatoria == null || fechaTraspaso == null)
        {
            return null;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(fechaTraspaso);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.add(Calendar.DATE, convocatoria.getDiasModificarDespuesTraspaso().intValue());
        return cal.getTime();
    }

    public Long getNumeroEstudiantesActivos()
    {
        if (actasAsignaturas == null)
        {
            return null;
        }

        Long numeroEstudiantes = 0L;
        for (ActaAsignatura actaAsignatura : actasAsignaturas)
        {
            numeroEstudiantes += actaAsignatura.getActasEstudiantes().stream()
                    .filter(ae -> ae.getMotivoExclusion() == null).count();
        }
        return numeroEstudiantes;
    }

    public Long getNumeroMaximoMatriculas()
    {
        Long NUMERO_ALUMNOS_POR_MATRICULA = 20L;
        Long numeroEstudiantesActivos = getNumeroEstudiantesActivos();
        if (numeroEstudiantesActivos == null)
        {
            return null;
        }

        Long numeroMatriculas = numeroEstudiantesActivos / NUMERO_ALUMNOS_POR_MATRICULA;

        /* El acta MP1040 de 2020 debe tener una matrícula más, solicitado por Ximo Beltran en correo del 10/Nov/2021 a ferrerq */
        if (getId() == 1184785)
        {
            numeroMatriculas = numeroMatriculas + 1;
        }

        return (numeroMatriculas == 0L) ? 1 : numeroMatriculas;
    }

    public Long getNumeroMatriculas()
    {
        if (actasAsignaturas == null)
        {
            return null;
        }

        Long numeroMatriculas = 0L;
        for (ActaAsignatura actaAsignatura : actasAsignaturas)
        {
            numeroMatriculas += actaAsignatura.getActasEstudiantes().stream()
                    .filter(ae -> ae.isMatriculaHonor()).count();
        }
        return numeroMatriculas;
    }

    public void pasarNotasExpediente(Long actaEstudianteId, Long connectedUserId)
            throws IOException, PasarNotasExpedienteException
    {
        RemotePasarNotasExpedienteProcedure pasarNotasExpedienteProcedure = new RemotePasarNotasExpedienteProcedure();
        pasarNotasExpedienteProcedure.init();
        pasarNotasExpedienteProcedure.execute(actaEstudianteId, connectedUserId);
    }

    public void comprobarSePuedePasarNotasExpediente()
            throws NoAutorizadoException, TraspasarActaException
    {
        if (actaUnica == null || actaUnica == false || fechaTraspaso != null
                || convocatoria.isExtraordinaria())
        {
            throw new NoAutorizadoException();
        }
        checkFechasTraspasoNotasExpediente();
    }

    public List<ActaRevision> getActaRevisionesByAsignaturaYGrupo(String asignaturaId,
                                                                  String grupoId)
    {
        return actasRevisiones.stream()
                .filter(r -> r.isConjunta() || (r.getAsignaturas().indexOf(asignaturaId) != -1
                        && r.getGrupos().indexOf(grupoId) != -1))
                .collect(Collectors.toList());
    }

    public List<ActaEstudiante> getActaEstudiantes()
    {
        List<ActaEstudiante> actaEstudiantes = new ArrayList<>();
        for (ActaAsignatura actaAsignatura : actasAsignaturas)
        {
            actaEstudiantes.addAll(actaAsignatura.getActasEstudiantes());
        }
        return actaEstudiantes;
    }

    public String getNombreFichero(String asignatura, String grupo)
    {
        String asignaturaTxt = (asignatura == null) ? codigo : asignatura;
        String grupoTxt = (grupo == null) ? "" : " grup " + grupo;
        return MessageFormat.format("{0}{1} {2} {3}", asignaturaTxt, grupoTxt,
                convocatoria.getNombre(), curso.getId()).replace(",", "").replace(".", "");
    }

    public List<DesglosePregunta> getPreguntasOrdenadas()
    {
        List<DesglosePregunta> preguntas = new ArrayList<>();
        for (DesgloseGrupo grupo : desglosesGrupos.stream().sorted().collect(Collectors.toList()))
        {
            preguntas.addAll(
                    grupo.getDesglosePreguntas().stream().sorted().collect(Collectors.toList()));
        }
        return preguntas;
    }

    public List<String> getCodigosAsignaturas()
    {
        return actasAsignaturas.stream().map(aa -> aa.getAsignatura().getId()).collect(Collectors.toList());
    }

    public Long getConvocatoriaIdUji()
    {
        ConvocatoriaTipoEstudio convocatoriaTipoEstudio = convocatoria.getConvocatoriaTipoEstudios().stream().filter(ct -> ct.getTipoEstudioId().equals(tipoEstudio.getId())).findFirst().orElse(null);
        return (convocatoriaTipoEstudio != null) ? convocatoriaTipoEstudio.getUjiId() : null;
    }

    public Boolean existeOtroAlumnoConMismoNombre(Persona alumno)
    {
        return getActaEstudiantes().stream().filter(ae -> ae.getPersona().tieneMismoNombreyApellidos(alumno)).count() > 1;
    }
}