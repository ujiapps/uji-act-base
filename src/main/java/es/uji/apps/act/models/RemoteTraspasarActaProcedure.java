package es.uji.apps.act.models;

import es.uji.apps.act.exceptions.TraspasarActaException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

@Component
public class RemoteTraspasarActaProcedure
{
    private TraspasarActaProcedure traspasarActaProcedure;
    private static DataSource dataSource;

    @Autowired
    public void setDataSource(DataSource dataSource)
    {
        RemoteTraspasarActaProcedure.dataSource = dataSource;
    }

    public void init()
    {
        this.traspasarActaProcedure = new TraspasarActaProcedure(dataSource);
    }

    public Long execute(Long actaId, Long perId, String ip) throws IOException, TraspasarActaException
    {
        return traspasarActaProcedure.execute(actaId, perId, ip);
    }

    private class TraspasarActaProcedure extends StoredProcedure
    {
        private static final String SQL = "pack_actas.traspasar_acta";
        private static final String ACTA_ID = "p_acta";
        private static final String PERSONA_ID = "p_per_id";
        private static final String IP = "p_ip";

        public TraspasarActaProcedure(DataSource dataSource)
        {
            super(dataSource, SQL);
            declareParameter(new SqlOutParameter("codigoError", Types.BIGINT));
            declareParameter(new SqlParameter(ACTA_ID, Types.BIGINT));
            declareParameter(new SqlParameter(PERSONA_ID, Types.BIGINT));
            declareParameter(new SqlParameter(IP, Types.VARCHAR));
            setFunction(true);
            compile();
        }

        public Long execute(Long actaId, Long perId, String ip) throws IOException, TraspasarActaException
        {
            Map<String, Object> inParams = new HashMap();
            Map<String, Object> results;
            inParams.put(ACTA_ID, actaId);
            inParams.put(PERSONA_ID, perId);
            inParams.put(IP, ip);
            try
            {
                results = execute(inParams);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                SQLException sqlException = (SQLException) e.getCause();
                System.out.println(sqlException);
                throw new TraspasarActaException();
            }

            return !results.isEmpty() ? (Long) results.get("codigoError") : null;
        }
    }
}
