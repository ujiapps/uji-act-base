package es.uji.apps.act.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "ACT_LOGS")
public class Log implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "FECHA")
    private Date fecha;

    @Column(name = "nota")
    private Double nota;

    @Column(name = "ACTA_ESTUDIANTE_ID")
    private Long actaEstudianteId;

    @Column(name = "CALIFICACION_ID")
    private Long calificacionId;

    @Column(name = "ACTA_ESTUDIANTE_NOTA_ID")
    private Long actaEstudianteNotaId;

    @Column(name = "COMENTARIO")
    private String comentario;

    @Column(name = "IP")
    private String ip;

    @ManyToOne
    @JoinColumn(name = "ACTA_ID")
    private Acta acta;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID")
    private Persona persona;

    @ManyToOne
    @JoinColumn(name = "ESTUDIANTE_ID")
    private Persona estudiante;

    public Log()
    {
        fecha = new Date();
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Date getFecha()
    {
        return fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public Double getNota()
    {
        return nota;
    }

    public void setNota(Double nota)
    {
        this.nota = nota;
    }

    public Long getActaEstudianteId()
    {
        return actaEstudianteId;
    }

    public void setActaEstudianteId(Long actaEstudianteId)
    {
        this.actaEstudianteId = actaEstudianteId;
    }

    public Long getCalificacionId()
    {
        return calificacionId;
    }

    public void setCalificacionId(Long calificacionId)
    {
        this.calificacionId = calificacionId;
    }

    public Long getActaEstudianteNotaId()
    {
        return actaEstudianteNotaId;
    }

    public void setActaEstudianteNotaId(Long actaEstudianteNotaId)
    {
        this.actaEstudianteNotaId = actaEstudianteNotaId;
    }

    public Acta getActa()
    {
        return acta;
    }

    public String getComentario()
    {
        return comentario;
    }

    public void setComentario(String comentario)
    {
        this.comentario = comentario;
    }

    public void setActa(Acta acta)
    {
        this.acta = acta;
    }

    public Persona getPersona()
    {
        return persona;
    }

    public void setPersona(Persona persona)
    {
        this.persona = persona;
    }

    public Persona getEstudiante()
    {
        return estudiante;
    }

    public void setEstudiante(Persona estudiante)
    {
        this.estudiante = estudiante;
    }

    public String getIp()
    {
        return ip;
    }

    public void setIp(String ip)
    {
        this.ip = ip;
    }
}