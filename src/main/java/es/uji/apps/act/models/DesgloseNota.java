package es.uji.apps.act.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_ACTAS_ESTUDIANTES_NOTAS")
public class DesgloseNota implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "estudiante_id")
    private ActaEstudiante actaEstudiante;

    @ManyToOne
    @JoinColumn(name = "pregunta_id")
    private DesglosePregunta desglosePregunta;

    @Column(name = "nota")
    private Double nota;

    @Column(name = "IP")
    private String ip;

    @ManyToOne
    @JoinColumn(name = "persona_id_nota")
    private Persona personaNota;

    @Column(name = "FECHA_NOTA")
    private Date fechaNota;

    public DesgloseNota()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Double getNota()
    {
        return nota;
    }

    public void setNota(Double nota)
    {
        this.nota = nota;
    }

    public ActaEstudiante getActaEstudiante()
    {
        return actaEstudiante;
    }

    public void setActaEstudiante(ActaEstudiante actaEstudiante)
    {
        this.actaEstudiante = actaEstudiante;
    }

    public DesglosePregunta getDesglosePregunta()
    {
        return desglosePregunta;
    }

    public void setDesglosePregunta(DesglosePregunta desglosePregunta)
    {
        this.desglosePregunta = desglosePregunta;
    }

    public boolean isEditable()
    {
        return actaEstudiante.isEditable() && desglosePregunta.getParcialActa() == null;
    }

    public Long getOrden()
    {
        return desglosePregunta.getOrden() + desglosePregunta.getDesgloseGrupo().getOrden() * 100000;
    }

    public String getIp()
    {
        return ip;
    }

    public void setIp(String ip)
    {
        this.ip = ip;
    }


    public Date getFechaNota()
    {
        return fechaNota;
    }

    public void setFechaNota(Date fechaNota)
    {
        this.fechaNota = fechaNota;
    }

    public Persona getPersonaNota()
    {
        return personaNota;
    }

    public void setPersonaNota(Persona personaNota)
    {
        this.personaNota = personaNota;
    }
}