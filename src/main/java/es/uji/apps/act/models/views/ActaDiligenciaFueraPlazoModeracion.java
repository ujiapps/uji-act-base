package es.uji.apps.act.models.views;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;


@Entity
@BatchSize(size = 100)
@Table(name = "ACT_VW_DILIGENCIAS_MODERACION")
public class ActaDiligenciaFueraPlazoModeracion implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "acta_id")
    private Long actaId;

    @Id
    @Column(name = "DIRECTOR_DEPARTAMENTO_ID")
    private Long directorDepartamentoId;

    @Id
    @Column(name = "DIRECTOR_CENTRO_ID")
    private Long directorCentroId;

    @Column(name = "CODIGO")
    private String codigo;

    @Column(name = "DESCRIPCION")
    private String descripcion;

    @Column(name = "CONVOCATORIA_ID")
    private Long convocatoriaId;

    @Column(name = "CURSO_ACADEMICO_ID")
    private Long cursoId;

    public Long getActaId()
    {
        return actaId;
    }

    public void setActaId(Long actaId)
    {
        this.actaId = actaId;
    }

    public Long getDirectorDepartamentoId()
    {
        return directorDepartamentoId;
    }

    public void setDirectorDepartamentoId(Long directorDepartamentoId)
    {
        this.directorDepartamentoId = directorDepartamentoId;
    }

    public Long getDirectorCentroId()
    {
        return directorCentroId;
    }

    public void setDirectorCentroId(Long directorCentroId)
    {
        this.directorCentroId = directorCentroId;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public Long getConvocatoriaId()
    {
        return convocatoriaId;
    }

    public void setConvocatoriaId(Long convocatoriaId)
    {
        this.convocatoriaId = convocatoriaId;
    }

    public Long getCursoId()
    {
        return cursoId;
    }

    public void setCursoId(Long cursoId)
    {
        this.cursoId = cursoId;
    }
}