package es.uji.apps.act.models;

import es.uji.commons.rest.annotations.DataTag;

import java.io.Serializable;

public class NotaDesgloseCalculada implements Serializable
{
    @DataTag
    private Double nota;

    @DataTag
    private String resultado;

    @DataTag
    public String resultadoExtendido;

    public NotaDesgloseCalculada()
    {
    }

    public NotaDesgloseCalculada(Double nota)
    {
        this.nota = nota;
        this.resultado = "";
        this.resultadoExtendido = "";
    }

    public NotaDesgloseCalculada(Double nota, String resultado)
    {
        this.nota = nota;
        this.resultado = resultado;
        this.resultadoExtendido = resultado;
    }

    public Double getNota()
    {
        return nota;
    }

    public void setNota(Double nota)
    {
        this.nota = nota;
    }

    public String getResultado()
    {
        return resultado;
    }

    public void setResultado(String resultado)
    {
        this.resultado = resultado;
    }

    public String getResultadoExtendido()
    {
        return resultadoExtendido;
    }

    public void setResultadoExtendido(String resultadoExtendido)
    {
        this.resultadoExtendido = resultadoExtendido;
    }
}