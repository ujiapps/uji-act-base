package es.uji.apps.act.models.views;

import es.uji.apps.act.models.Persona;
import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_VM_ACTAS_DEPARTAMENTO")
public class ActaDepartamento implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @Column(name = "ACTA_ID")
    private Long actaId;

    @Column(name = "CURSO_ACADEMICO_ID")
    private Long cursoId;

    @Column(name = "CONVOCATORIA_ID")
    private Long convocatoriaId;

    @Column(name = "CONVOCATORIA_NOMBRE")
    private String convocatoriaNombre;

    @Column(name = "PER_ID")
    private Long personaId;

    @Column(name = "NOMBRE")
    private String personaNombre;

    @Column(name = "CODIGO")
    private String codigo;

    @Column(name = "NUMERO_ALUMNOS")
    private Long numeroAlumnos;

    @Column(name = "NUMERO_ALUMNOS_TOTAL")
    private Long numeroAlumnosTotal;

    @Column(name = "DEPARTAMENTO_ID")
    private Long departamentoId;

    @Column(name = "DEPARTAMENTO_NOMBRE")
    private String departamentoNombre;

    @Column(name = "FECHA_ALTA")
    private Date fechaAlta;

    @Column(name = "FECHA_TRASPASO")
    private Date fechaTraspaso;

    @Column(name = "FECHA_FIRMA_DIGITAL")
    private Date fechaFirmaDigital;

    @Column(name = "REFERENCIA")
    private String referencia;

    @Column(name = "TIPO_ESTUDIO_ID")
    private String tipoEstudioId;

    @Column(name = "DEPARTAMENTO_RECIBE_ACTA")
    private String departamentoRecibeActaId;

    @Column(name = "NOMBRE_DEP_RECIBE_ACTA")
    private String nombreDepartamentoRecibeActa;

    @ManyToOne
    @JoinColumn(name = "PERSONA_ID_TRASPASO")
    private Persona personaTraspaso;

    @Column(name = "FECHA_FIN_MODIFICACION")
    private Date fechaFinModificacion;

    @Transient
    private String tipoEstudioNombre;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getActaId()
    {
        return actaId;
    }

    public void setActaId(Long actaId)
    {
        this.actaId = actaId;
    }

    public Long getCursoId()
    {
        return cursoId;
    }

    public void setCursoId(Long cursoId)
    {
        this.cursoId = cursoId;
    }

    public Long getConvocatoriaId()
    {
        return convocatoriaId;
    }

    public void setConvocatoriaId(Long convocatoriaId)
    {
        this.convocatoriaId = convocatoriaId;
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    public Long getDepartamentoId()
    {
        return departamentoId;
    }

    public void setDepartamentoId(Long departamentoId)
    {
        this.departamentoId = departamentoId;
    }

    public String getDepartamentoNombre()
    {
        return departamentoNombre;
    }

    public void setDepartamentoNombre(String departamentoNombre)
    {
        this.departamentoNombre = departamentoNombre;
    }

    public Date getFechaAlta()
    {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta)
    {
        this.fechaAlta = fechaAlta;
    }

    public Date getFechaTraspaso()
    {
        return fechaTraspaso;
    }

    public void setFechaTraspaso(Date fechaTraspaso)
    {
        this.fechaTraspaso = fechaTraspaso;
    }

    public Date getFechaFirmaDigital()
    {
        return fechaFirmaDigital;
    }

    public void setFechaFirmaDigital(Date fechaFirmaDigital)
    {
        this.fechaFirmaDigital = fechaFirmaDigital;
    }

    public String getReferencia()
    {
        return referencia;
    }

    public void setReferencia(String referencia)
    {
        this.referencia = referencia;
    }

    public String getConvocatoriaNombre()
    {
        return convocatoriaNombre;
    }

    public void setConvocatoriaNombre(String convocatoriaNombre)
    {
        this.convocatoriaNombre = convocatoriaNombre;
    }

    public String getPersonaNombre()
    {
        return personaNombre;
    }

    public void setPersonaNombre(String personaNombre)
    {
        this.personaNombre = personaNombre;
    }

    public Long getNumeroAlumnos()
    {
        return numeroAlumnos;
    }

    public void setNumeroAlumnos(Long numeroAlumnos)
    {
        this.numeroAlumnos = numeroAlumnos;
    }

    public String getTipoEstudioId()
    {
        return tipoEstudioId;
    }

    public void setTipoEstudioId(String tipoEstudioId)
    {
        this.tipoEstudioId = tipoEstudioId;
    }

    public Persona getPersonaTraspaso()
    {
        return personaTraspaso;
    }

    public void setPersonaTraspaso(Persona personaTraspaso)
    {
        this.personaTraspaso = personaTraspaso;
    }

    public void setTipoEstudioNombre(String tipoEstudioNombre)
    {
        this.tipoEstudioNombre = tipoEstudioNombre;
    }

    public String getTipoEstudioNombre()
    {
        return tipoEstudioId.trim().equals("G") ? "Grau" : "Master";
    }

    public String getDepartamentoRecibeActaId()
    {
        return departamentoRecibeActaId;
    }

    public void setDepartamentoRecibeActaId(String departamentoRecibeActaId)
    {
        this.departamentoRecibeActaId = departamentoRecibeActaId;
    }

    public String getNombreDepartamentoRecibeActa()
    {
        return nombreDepartamentoRecibeActa;
    }

    public void setNombreDepartamentoRecibeActa(String nombreDepartamentoRecibeActa)
    {
        this.nombreDepartamentoRecibeActa = nombreDepartamentoRecibeActa;
    }

    public Long getNumeroAlumnosTotal()
    {
        return numeroAlumnosTotal;
    }

    public void setNumeroAlumnosTotal(Long numeroAlumnosTotal)
    {
        this.numeroAlumnosTotal = numeroAlumnosTotal;
    }

    public Date getFechaFinModificacion()
    {
        return fechaFinModificacion;
    }

    public void setFechaFinModificacion(Date fechaFinModificacion)
    {
        this.fechaFinModificacion = fechaFinModificacion;
    }
}