package es.uji.apps.act.models;

import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_FECHAS_CONVOCATORIAS_EST")
public class FechaConvocatoriaEstudio implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "FECHAS_CONVOCATORIAS_ID")
    private FechaConvocatoria fechaConvocatoria;

    @ManyToOne
    @JoinColumn(name = "ESTUDIO_ID")
    private Estudio estudio;

    @Column(name = "FECHA_INICIO_TRASPASO")
    private Date fechaInicioTraspaso;

    @Column(name = "FECHA_FIN_TRASPASO")
    private Date fechaFinTraspaso;

    @Column(name = "DILIGENCIAS_DIAS")
    private Long diligenciasDias;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public FechaConvocatoria getFechaConvocatoria()
    {
        return fechaConvocatoria;
    }

    public void setFechaConvocatoria(FechaConvocatoria fechaConvocatoria)
    {
        this.fechaConvocatoria = fechaConvocatoria;
    }

    public Estudio getEstudio()
    {
        return estudio;
    }

    public void setEstudio(Estudio estudio)
    {
        this.estudio = estudio;
    }

    public Date getFechaInicioTraspaso()
    {
        return fechaInicioTraspaso;
    }

    public void setFechaInicioTraspaso(Date fechaInicioTraspaso)
    {
        this.fechaInicioTraspaso = fechaInicioTraspaso;
    }

    public Date getFechaFinTraspaso()
    {
        return fechaFinTraspaso;
    }

    public void setFechaFinTraspaso(Date fechaFinTraspaso)
    {
        this.fechaFinTraspaso = fechaFinTraspaso;
    }

    public Long getDiligenciasDias()
    {
        return diligenciasDias;
    }

    public void setDiligenciasDias(Long diligenciasDias)
    {
        this.diligenciasDias = diligenciasDias;
    }
}