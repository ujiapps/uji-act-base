package es.uji.apps.act.models.views;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.BatchSize;

import es.uji.commons.rest.annotations.DataTag;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_VM_ACTAS")
public class ActaPersona implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Column(name = "CURSO_ACA")
    private Long cursoId;

    @Column(name = "ASI_CODIGOS")
    private String asiCodigos;

    @Column(name = "ASI_NOMBRE")
    private String asiNombre;

    @Column(name = "PER_ID")
    private Long perId;

    @Column(name = "TIPO_ESTUDIO_ID")
    private String tipoEstudio;

    @Column(name = "OFICIAL")
    private String oficial;

    @Column(name = "EXTRAORDINARIA")
    public Long extraordinariaId;

    @Column(name = "ORDINARIA_1")
    public Long ordinaria1Id;

    @Column(name = "ORDINARIA_2")
    public Long ordinaria2Id;

    @Column(name = "PUEDE_TRASPASAR")
    public Boolean puedeTraspasar;

    @Column(name = "DEPARTAMENTO_ID")
    public Long departamentoId;

    @Column(name = "DEPARTAMENTO_NOMBRE")
    public String departamentoNombre;

    @Column(name = "NUMERO_ESTUDIANTES")
    public Long numeroEstudiantes;

    @Column(name = "NUMERO_ESTUDIANTES_TOTAL")
    public Long numeroEstudiantesTotal;

    public Long getCursoId()
    {
        return cursoId;
    }

    public void setCursoId(Long cursoId)
    {
        this.cursoId = cursoId;
    }

    public String getAsiCodigos()
    {
        return asiCodigos;
    }

    public void setAsiCodigos(String asiCodigos)
    {
        this.asiCodigos = asiCodigos;
    }

    public String getAsiNombre()
    {
        return asiNombre;
    }

    public void setAsiNombre(String asiNombre)
    {
        this.asiNombre = asiNombre;
    }

    public Long getPerId()
    {
        return perId;
    }

    public void setPerId(Long perId)
    {
        this.perId = perId;
    }

    public Long getExtraordinariaId()
    {
        return extraordinariaId;
    }

    public void setExtraordinariaId(Long extraordinariaId)
    {
        this.extraordinariaId = extraordinariaId;
    }

    public Long getOrdinaria1Id()
    {
        return ordinaria1Id;
    }

    public void setOrdinaria1Id(Long ordinaria1Id)
    {
        this.ordinaria1Id = ordinaria1Id;
    }

    public Long getOrdinaria2Id()
    {
        return ordinaria2Id;
    }

    public void setOrdinaria2Id(Long ordinaria2Id)
    {
        this.ordinaria2Id = ordinaria2Id;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getTipoEstudio()
    {
        return tipoEstudio;
    }

    public void setTipoEstudio(String tipoEstudio)
    {
        this.tipoEstudio = tipoEstudio;
    }

    public String getOficial()
    {
        return oficial;
    }

    public void setOficial(String oficial)
    {
        this.oficial = oficial;
    }

    public Boolean getPuedeTraspasar()
    {
        return puedeTraspasar;
    }

    public void setPuedeTraspasar(Boolean puedeTraspasar)
    {
        this.puedeTraspasar = puedeTraspasar;
    }

    public Long getDepartamentoId()
    {
        return departamentoId;
    }

    public void setDepartamentoId(Long departamentoId)
    {
        this.departamentoId = departamentoId;
    }

    public String getDepartamentoNombre()
    {
        return departamentoNombre;
    }

    public void setDepartamentoNombre(String departamentoNombre)
    {
        this.departamentoNombre = departamentoNombre;
    }

    public Long getNumeroEstudiantes()
    {
        return numeroEstudiantes;
    }

    public void setNumeroEstudiantes(Long numeroEstudiantes)
    {
        this.numeroEstudiantes = numeroEstudiantes;
    }

    public Long getNumeroEstudiantesTotal()
    {
        return numeroEstudiantesTotal;
    }

    public void setNumeroEstudiantesTotal(Long numeroEstudiantesTotal)
    {
        this.numeroEstudiantesTotal = numeroEstudiantesTotal;
    }
}