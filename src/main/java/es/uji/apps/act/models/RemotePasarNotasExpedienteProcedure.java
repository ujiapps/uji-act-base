package es.uji.apps.act.models;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import es.uji.apps.act.exceptions.PasarNotasExpedienteException;

@Component
public class RemotePasarNotasExpedienteProcedure
{
    private PasarNotasExpedienteProcedure traspasarActaProcedure;
    private static DataSource dataSource;

    @Autowired
    public void setDataSource(DataSource dataSource)
    {
        RemotePasarNotasExpedienteProcedure.dataSource = dataSource;
    }

    public void init()
    {
        this.traspasarActaProcedure = new PasarNotasExpedienteProcedure(dataSource);
    }

    public Long execute(Long actaEstudianteId, Long perId)
            throws IOException, PasarNotasExpedienteException
    {
        return traspasarActaProcedure.execute(actaEstudianteId, perId);
    }

    private class PasarNotasExpedienteProcedure extends StoredProcedure
    {
        private static final String SQL = "pack_actas.pasar_nota_expediente";
        private static final String ACTA_ESTUDIANTE_ID = "p_acta_estudiante_id";
        private static final String PERSONA_ID = "p_per_id";

        public PasarNotasExpedienteProcedure(DataSource dataSource)
        {
            super(dataSource, SQL);
            declareParameter(new SqlOutParameter("codigoError", Types.BIGINT));
            declareParameter(new SqlParameter(ACTA_ESTUDIANTE_ID, Types.BIGINT));
            declareParameter(new SqlParameter(PERSONA_ID, Types.BIGINT));
            setFunction(true);
            compile();
        }

        public Long execute(Long actaEstudianteId, Long perId)
                throws IOException, PasarNotasExpedienteException
        {
            Map<String, Object> inParams = new HashMap();
            Map<String, Object> results;
            inParams.put(ACTA_ESTUDIANTE_ID, actaEstudianteId);
            inParams.put(PERSONA_ID, perId);
            try
            {
                results = execute(inParams);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                SQLException sqlException = (SQLException) e.getCause();
                System.out.println(sqlException);
                throw new PasarNotasExpedienteException();
            }

            return !results.isEmpty() ? (Long) results.get("codigoError") : null;
        }
    }
}
