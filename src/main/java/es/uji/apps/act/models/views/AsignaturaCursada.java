package es.uji.apps.act.models.views;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_EXT_ASIGNATURAS_CURSADAS")
public class AsignaturaCursada implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "MAT_CURSO_ACA")
    private Long curso;

    @Id
    @Column(name = "MAT_EXP_PER_ID")
    private Long personaId;

    @Id
    @Column(name = "MAT_EXP_TIT_ID")
    private Long titulacionId;

    @Id
    @Column(name = "ASI_ID")
    private String asignaturaId;

    @Column(name = "EXPEDIENTE_ABIERTO")
    private Boolean expedienteAbierto;

    public Long getCurso()
    {
        return curso;
    }

    public void setCurso(Long curso)
    {
        this.curso = curso;
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public Long getTitulacionId()
    {
        return titulacionId;
    }

    public void setTitulacionId(Long titulacionId)
    {
        this.titulacionId = titulacionId;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public Boolean isExpedienteAbierto()
    {
        return expedienteAbierto;
    }

    public void setExpedienteAbierto(Boolean expedienteAbierto)
    {
        this.expedienteAbierto = expedienteAbierto;
    }
}