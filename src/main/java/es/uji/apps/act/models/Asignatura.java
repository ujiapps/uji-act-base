package es.uji.apps.act.models;

import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_EXT_ASIGNATURAS")
public class Asignatura implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Column(name = "nombre")
    private String nombre;

    @ManyToOne
    @JoinColumn(name = "tipo")
    private TipoEstudio tipoEstudio;

    public Asignatura()
    {
    }

    public Asignatura(String asignaturaId)
    {
        this.id = asignaturaId;
    }

    @Override
    public String toString()
    {
        return id;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public TipoEstudio getTipoEstudio()
    {
        return tipoEstudio;
    }

    public void setTipoEstudio(TipoEstudio tipoEstudio)
    {
        this.tipoEstudio = tipoEstudio;
    }
}