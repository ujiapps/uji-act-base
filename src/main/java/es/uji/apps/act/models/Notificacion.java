package es.uji.apps.act.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_NOTIFICACIONES")
public class Notificacion implements Serializable
{
    private static final long serialVersionUID = 1L;

    public static final String TIPO_AUTO = "AUTO";
    public static final String TIPO_MANUAL = "MANUAL";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "acta_id")
    private Acta acta;

    @Column(name = "FECHA")
    private Date fecha;

    @Column(name = "ASUNTO")
    private String asunto;

    @Column(name = "MENSAJE")
    private String mensaje;

    @Column(name = "TIPO")
    private String tipo;

    @ManyToOne
    @JoinColumn(name = "acta_revision_id")
    private ActaRevision actaRevision;

    public Notificacion()
    {
        fecha = new Date();
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Date getFecha()
    {
        return fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public String getAsunto()
    {
        return asunto;
    }

    public void setAsunto(String asunto)
    {
        this.asunto = asunto;
    }

    public String getMensaje()
    {
        return mensaje;
    }

    public void setMensaje(String mensaje)
    {
        this.mensaje = mensaje;
    }

    public Acta getActa()
    {
        return acta;
    }

    public void setActa(Acta acta)
    {
        this.acta = acta;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public ActaRevision getActaRevision()
    {
        return actaRevision;
    }

    public void setActaRevision(ActaRevision actaRevision)
    {
        this.actaRevision = actaRevision;
    }
}