package es.uji.apps.act.models;

import es.uji.apps.act.exceptions.TraspasarActaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

@Component
public class RemoteDestraspasarActaProcedure
{
    private DestraspasarActaProcedure destraspasarActaProcedure;
    private static DataSource dataSource;

    @Autowired
    public void setDataSource(DataSource dataSource)
    {
        RemoteDestraspasarActaProcedure.dataSource = dataSource;
    }

    public void init()
    {
        this.destraspasarActaProcedure = new DestraspasarActaProcedure(dataSource);
    }

    public Long execute(Long actaId, Long perId) throws IOException, TraspasarActaException
    {
        return destraspasarActaProcedure.execute(actaId, perId);
    }

    private class DestraspasarActaProcedure extends StoredProcedure
    {
        private static final String SQL = "pack_actas.destraspasar_acta";
        private static final String ACTA_ID = "p_acta";
        private static final String PERSONA_ID = "p_per_id";

        public DestraspasarActaProcedure(DataSource dataSource)
        {
            super(dataSource, SQL);
            declareParameter(new SqlOutParameter("codigoError", Types.BIGINT));
            declareParameter(new SqlParameter(ACTA_ID, Types.BIGINT));
            declareParameter(new SqlParameter(PERSONA_ID, Types.BIGINT));
            setFunction(true);
            compile();
        }

        public Long execute(Long actaId, Long perId) throws IOException, TraspasarActaException
        {
            Map<String, Object> inParams = new HashMap();
            Map<String, Object> results = new HashMap();
            inParams.put(ACTA_ID, actaId);
            inParams.put(PERSONA_ID, perId);
            try
            {
                results = execute(inParams);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                SQLException sqlException = (SQLException) e.getCause();
                System.out.println(sqlException);
                throw new TraspasarActaException();
            }

            return !results.isEmpty() ? (Long) results.get("codigoError") : null;
        }
    }
}
