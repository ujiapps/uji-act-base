package es.uji.apps.act.models;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import es.uji.apps.act.exceptions.TraspasarNotaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

@Component
public class RemoteTraspasarNotaProcedure
{
    private TraspasarNotaProcedure traspasarNotaProcedure;
    private static DataSource dataSource;

    @Autowired
    public void setDataSource(DataSource dataSource)
    {

        RemoteTraspasarNotaProcedure.dataSource = dataSource;
    }

    public void init()
    {
        this.traspasarNotaProcedure = new TraspasarNotaProcedure(dataSource);
    }

    public Long execute(ActaEstudiante actaEstudiante, Long connectedUserId)
            throws IOException, TraspasarNotaException
    {
        return traspasarNotaProcedure.execute(actaEstudiante, connectedUserId);
    }

    private class TraspasarNotaProcedure extends StoredProcedure
    {
        private static final String SQL = "pack_actas.traspasar_nota";
        private static final String ACTA_ID = "p_acta";
        private static final String ESTUDIANTE_ID = "p_estudiante_id";
        private static final String NOTA = "p_numerica";
        private static final String CALIFICACION = "p_calificacion";
        private static final String PERSONA_ID = "p_per_id";

        public TraspasarNotaProcedure(DataSource dataSource)
        {
            super(dataSource, SQL);
            declareParameter(new SqlOutParameter("codigoError", Types.BIGINT));
            declareParameter(new SqlParameter(ACTA_ID, Types.BIGINT));
            declareParameter(new SqlParameter(ESTUDIANTE_ID, Types.BIGINT));
            declareParameter(new SqlParameter(NOTA, Types.DOUBLE));
            declareParameter(new SqlParameter(CALIFICACION, Types.BIGINT));
            declareParameter(new SqlParameter(PERSONA_ID, Types.BIGINT));

            setFunction(true);
            compile();
        }

        public Long execute(ActaEstudiante actaEstudiante, Long connectedUserId)
                throws IOException, TraspasarNotaException
        {
            Map<String, Object> inParams = new HashMap();
            Map<String, Object> results = new HashMap();
            inParams.put(ACTA_ID, actaEstudiante.getActaAsignatura().getActa().getId());
            inParams.put(ESTUDIANTE_ID, actaEstudiante.getPersona().getId());
            inParams.put(NOTA, actaEstudiante.getNota());
            inParams.put(CALIFICACION, actaEstudiante.getCalificacionId());
            inParams.put(PERSONA_ID, connectedUserId);

            try
            {
                results = execute(inParams);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                SQLException sqlException = (SQLException) e.getCause();
                System.out.println(sqlException);
                throw new TraspasarNotaException();
            }

            return !results.isEmpty() ? (Long) results.get("codigoError") : null;
        }
    }
}
