package es.uji.apps.act.models.views;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_EXT_ASIGNATURAS_EXP")
public class AsignaturaExpediente implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ASC_MAT_EXP_TIT_ID")
    private Long titullacionId;

    @Id
    @Column(name = "ASC_MAT_EXP_PER_ID")
    private Long personaId;

    @Id
    @Column(name = "ASC_MAT_CURSO_ACA")
    private Long curso;

    @Id
    @Column(name = "ASC_ASI_ID")
    private String asignaturaId;

    @Id
    @Column(name = "CON_ID")
    private Long convocatoriaId;

    @Column(name = "CAL_ID")
    private String calificacionId;

    @Column(name = "NOTA")
    private Double nota;

    @Column(name = "ORDEN")
    private Long orden;

    @Column(name = "CONV_POST")
    private Boolean calificadoConvocatoriaPosterior;

    @Column(name = "CONV_ANT_APRO")
    private Boolean aprobadoConvocatoriaAnterior;

    public Long getTitullacionId()
    {
        return titullacionId;
    }

    public void setTitullacionId(Long titullacionId)
    {
        this.titullacionId = titullacionId;
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public Long getCurso()
    {
        return curso;
    }

    public void setCurso(Long curso)
    {
        this.curso = curso;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public Long getConvocatoriaId()
    {
        return convocatoriaId;
    }

    public void setConvocatoriaId(Long convocatoriaId)
    {
        this.convocatoriaId = convocatoriaId;
    }

    public String getCalificacionId()
    {
        return calificacionId;
    }

    public void setCalificacionId(String calificacionId)
    {
        this.calificacionId = calificacionId;
    }

    public Double getNota()
    {
        return nota;
    }

    public void setNota(Double nota)
    {
        this.nota = nota;
    }

    public Long getOrden()
    {
        return orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public Boolean isCalificadoConvocatoriaPosterior()
    {
        return calificadoConvocatoriaPosterior;
    }

    public void setCalificadoConvocatoriaPosterior(Boolean calificadoConvocatoriaPosterior)
    {
        this.calificadoConvocatoriaPosterior = calificadoConvocatoriaPosterior;
    }

    public Boolean isAprobadoConvocatoriaAnterior()
    {
        return aprobadoConvocatoriaAnterior;
    }

    public void setAprobadoConvocatoriaAnterior(Boolean aprobadoConvocatoriaAnterior)
    {
        this.aprobadoConvocatoriaAnterior = aprobadoConvocatoriaAnterior;
    }
}