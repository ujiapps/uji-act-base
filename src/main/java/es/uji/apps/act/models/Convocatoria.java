package es.uji.apps.act.models;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.BatchSize;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_CONVOCATORIAS")
public class Convocatoria implements Serializable
{
    private static final long serialVersionUID = 1L;

    public static final long PRIMERA_ORDINARIA_FEBRERO = 1L;
    public static final long PRIMERA_ORDINARIA_MAYO = 2L;
    public static final long SEGUNDA_ORDINARIA_1_SEM = 3L;
    public static final long SEGUNDA_ORDINARIA_2_SEM = 4L;
    public static final long EXTRAORDINARIA_FINAL_GRADO = 5L;
    public static final long EXTRAORDINARIA_FINAL_MASTER = 10L;

    public static final long EXAMEN_PARCIAL = 99L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;

    private Boolean extraordinaria;

    private Long orden;

    private Boolean ordinaria;

    private String semestre;

    @Column(name = "ACTA_UNICA")
    private Boolean actaUnica;

    @Column(name = "CERTIFICADO_CA")
    private String certificadoCA;

    @Column(name = "CERTIFICADO_ES")
    private String certificadoES;

    @Column(name = "CERTIFICADO_UK")
    private String certificadoUK;

    @Column(name = "DIAS_PARA_MODIFICAR")
    private Long diasModificarDespuesTraspaso;

    @OneToMany(mappedBy = "convocatoria")
    private Set<Acta> actas;

    @OneToMany(mappedBy = "convocatoria")
    private Set<FechaConvocatoria> fechasConvocatoria;

    @OneToMany(mappedBy = "convocatoria")
    private Set<ConvocatoriaTipoEstudio> convocatoriaTipoEstudios;

    public Convocatoria()
    {
    }

    public Convocatoria(Long convocatoriaId)
    {
        this.id = convocatoriaId;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Set<Acta> getActas()
    {
        return actas;
    }

    public void setActas(Set<Acta> actas)
    {
        this.actas = actas;
    }

    public Set<FechaConvocatoria> getFechasConvocatoria()
    {
        return fechasConvocatoria;
    }

    public void setFechasConvocatoria(Set<FechaConvocatoria> fechasConvocatoria)
    {
        this.fechasConvocatoria = fechasConvocatoria;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Boolean isExtraordinaria()
    {
        return extraordinaria;
    }

    public void setExtraordinaria(Boolean extraordinaria)
    {
        this.extraordinaria = extraordinaria;
    }

    public Long getOrden()
    {
        return orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public String getSemestre()
    {
        return semestre;
    }

    public void setSemestre(String semestre)
    {
        this.semestre = semestre;
    }

    public Boolean isOrdinaria()
    {
        return ordinaria;
    }

    public void setOrdinaria(Boolean ordinaria)
    {
        this.ordinaria = ordinaria;
    }

    public Boolean isActaUnica()
    {
        return actaUnica;
    }

    public void setActaUnica(Boolean actaUnica)
    {
        this.actaUnica = actaUnica;
    }

    public String getCertificadoCA()
    {
        return certificadoCA;
    }

    public void setCertificadoCA(String certificadoCA)
    {
        this.certificadoCA = certificadoCA;
    }

    public String getCertificadoES()
    {
        return certificadoES;
    }

    public void setCertificadoES(String certificadoES)
    {
        this.certificadoES = certificadoES;
    }

    public String getCertificadoUK()
    {
        return certificadoUK;
    }

    public void setCertificadoUK(String certificadoUK)
    {
        this.certificadoUK = certificadoUK;
    }

    public Long getDiasModificarDespuesTraspaso()
    {
        return diasModificarDespuesTraspaso;
    }

    public void setDiasModificarDespuesTraspaso(Long diasModificarDespuesTraspaso)
    {
        this.diasModificarDespuesTraspaso = diasModificarDespuesTraspaso;
    }

    public Set<ConvocatoriaTipoEstudio> getConvocatoriaTipoEstudios()
    {
        return convocatoriaTipoEstudios;
    }

    public void setConvocatoriaTipoEstudios(Set<ConvocatoriaTipoEstudio> convocatoriaTipoEstudios)
    {
        this.convocatoriaTipoEstudios = convocatoriaTipoEstudios;
    }
}