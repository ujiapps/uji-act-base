package es.uji.apps.act.models;

import org.hibernate.annotations.BatchSize;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@BatchSize(size = 100)
@Table(name = "ASI_EXT_COMUNES")
public class AsignaturaComun implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ASIGNATURA_ID")
    private Long asignaturaId;

    @Id
    @Column(name = "ASIGNATURA_ID_COMUN")
    private Long asignaturaComunId;

    @Id
    @Column(name = "CURSO_ACA")
    private Long cursoId;

    private Long orden;

    public Long getOrden()
    {
        return orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public Long getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(Long asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public Long getAsignaturaComunId()
    {
        return asignaturaComunId;
    }

    public void setAsignaturaComunId(Long asignaturaComunId)
    {
        this.asignaturaComunId = asignaturaComunId;
    }

    public Long getCursoId()
    {
        return cursoId;
    }

    public void setCursoId(Long cursoId)
    {
        this.cursoId = cursoId;
    }
}