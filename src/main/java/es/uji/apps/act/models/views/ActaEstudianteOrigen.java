package es.uji.apps.act.models.views;

import java.io.Serializable;
import java.util.Random;

import javax.persistence.*;

import org.hibernate.annotations.BatchSize;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_VW_ACTAS_ESTUDIANTES")
public class ActaEstudianteOrigen implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    private String id;

    @Column(name = "ACTA_ID")
    private Long actaId;

    @Column(name = "CURSO_ACADEMICO_ID")
    private Long curso;

    @Column(name = "CONVOCATORIA_ID")
    private Long convocatoriaId;

    @Column(name = "ASIGNATURA_ID")
    private String asignaturaId;

    @Column(name = "GRUPO_ID")
    private String grupoId;

    @Column(name = "PERSONA_ID")
    private Long personaId;

    @Column(name = "MOTIVO_EXCLUSION")
    private String motivoExclusion;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public Long getActaId()
    {
        return actaId;
    }

    public void setActaId(Long actaId)
    {
        this.actaId = actaId;
    }

    public Long getCurso()
    {
        return curso;
    }

    public void setCurso(Long curso)
    {
        this.curso = curso;
    }

    public Long getConvocatoriaId()
    {
        return convocatoriaId;
    }

    public void setConvocatoriaId(Long convocatoriaId)
    {
        this.convocatoriaId = convocatoriaId;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public String getGrupoId()
    {
        return grupoId;
    }

    public void setGrupoId(String grupoId)
    {
        this.grupoId = grupoId;
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public String getMotivoExclusion()
    {
        return motivoExclusion;
    }

    public void setMotivoExclusion(String motivoExclusion)
    {
        this.motivoExclusion = motivoExclusion;
    }
}