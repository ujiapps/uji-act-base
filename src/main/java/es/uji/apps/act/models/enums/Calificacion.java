package es.uji.apps.act.models.enums;

public enum Calificacion
{
    SUSPENSO(1L, "Suspens"), APROBADO(2L, "Aprovat"), NOTABLE(3L, "Notable"), SOBRESALIENTE(4L,
            "Excel·lent"), MATRICULA_HONOR(5L, "Matricula Honor");

    private final Long id;
    private final String nombre;

    Calificacion(Long id, String nombre)
    {
        this.id = id;
        this.nombre = nombre;
    }

    public Long getId()
    {
        return id;
    }

    public String getNombre()
    {
        return nombre;
    }

    @Override
    public String toString()
    {
        return nombre;
    }

    public static Calificacion getCalificacionById(Long calificacionId)
    {
        for (Calificacion calificacion : Calificacion.values())
        {
            if (calificacion.getId().equals(calificacionId))
            {
                return calificacion;
            }
        }
        return null;
    }

    public static Long getCalificacionIdByNota(Double nota)
    {
        if (nota == null)
        {
            return null;
        }
        if (nota < 5)
        {
            return Calificacion.SUSPENSO.getId();
        }
        if (nota < 7)
        {
            return Calificacion.APROBADO.getId();
        }
        if (nota < 9)
        {
            return Calificacion.NOTABLE.getId();
        }

        return Calificacion.SOBRESALIENTE.getId();
    }

}
