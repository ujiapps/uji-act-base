package es.uji.apps.act.models.ui;

public class UIDiligencia
{
    private String asignatura;
    private String calificacion;
    private Double nota;
    private String comentario;
    private String alumno;
    private String email;

    public String getAsignatura()
    {
        return asignatura;
    }

    public void setAsignatura(String asignatura)
    {
        this.asignatura = asignatura;
    }

    public String getCalificacion()
    {
        return calificacion;
    }

    public void setCalificacion(String calificacion)
    {
        this.calificacion = calificacion;
    }

    public Double getNota()
    {
        return nota;
    }

    public void setNota(Double nota)
    {
        this.nota = nota;
    }

    public String getComentario()
    {
        return comentario;
    }

    public void setComentario(String comentario)
    {
        this.comentario = comentario;
    }

    public String getAlumno()
    {
        return alumno;
    }

    public void setAlumno(String alumno)
    {
        this.alumno = alumno;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }
}
