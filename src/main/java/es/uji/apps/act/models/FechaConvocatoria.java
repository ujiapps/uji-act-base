package es.uji.apps.act.models;

import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_FECHAS_CONVOCATORIAS")
public class FechaConvocatoria implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "FECHA_INICIO_TRASPASO")
    private Date fechaInicioTraspaso;

    @Column(name = "FECHA_FIN_TRASPASO")
    private Date fechaFinTraspaso;

    @Column(name = "FECHA_INICIO_ACTA_UNICA")
    private Date fechaInicioActaUnica;

    @Column(name = "FECHA_FIN_ACTA_UNICA")
    private Date fechaFinActaUnica;

    @Column(name = "DILIGENCIAS_DIAS")
    private Long diligenciasDias;

    @ManyToOne
    @JoinColumn(name = "TIPO_ESTUDIO_ID")
    private TipoEstudio tipoEstudio;

    @ManyToOne
    @JoinColumn(name = "CURSO_ACADEMICO_ID")
    private Curso curso;

    @ManyToOne
    @JoinColumn(name = "CONVOCATORIA_ID")
    private Convocatoria convocatoria;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Date getFechaInicioTraspaso()
    {
        return fechaInicioTraspaso;
    }

    public void setFechaInicioTraspaso(Date fechaInicioTraspaso)
    {
        this.fechaInicioTraspaso = fechaInicioTraspaso;
    }

    public Date getFechaFinTraspaso()
    {
        return fechaFinTraspaso;
    }

    public void setFechaFinTraspaso(Date fechaFinTraspaso)
    {
        this.fechaFinTraspaso = fechaFinTraspaso;
    }

    public Long getDiligenciasDias()
    {
        return diligenciasDias;
    }

    public void setDiligenciasDias(Long diligenciasDias)
    {
        this.diligenciasDias = diligenciasDias;
    }

    public TipoEstudio getTipoEstudio()
    {
        return tipoEstudio;
    }

    public void setTipoEstudio(TipoEstudio tipoEstudio)
    {
        this.tipoEstudio = tipoEstudio;
    }

    public Curso getCurso()
    {
        return curso;
    }

    public void setCurso(Curso curso)
    {
        this.curso = curso;
    }

    public Convocatoria getConvocatoria()
    {
        return convocatoria;
    }

    public void setConvocatoria(Convocatoria convocatoria)
    {
        this.convocatoria = convocatoria;
    }

    public Date getFechaInicioActaUnica()
    {
        return fechaInicioActaUnica;
    }

    public void setFechaInicioActaUnica(Date fechaInicioActaUnica)
    {
        this.fechaInicioActaUnica = fechaInicioActaUnica;
    }

    public Date getFechaFinActaUnica()
    {
        return fechaFinActaUnica;
    }

    public void setFechaFinActaUnica(Date fechaFinActaUnica)
    {
        this.fechaFinActaUnica = fechaFinActaUnica;
    }
}