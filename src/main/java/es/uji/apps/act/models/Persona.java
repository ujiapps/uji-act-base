package es.uji.apps.act.models;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;

import es.uji.apps.act.models.views.ActaDiligenciaVirtual;
import es.uji.apps.act.models.views.ResponsableAsignatura;
import es.uji.commons.rest.annotations.DataTag;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_EXT_PERSONAS")
public class Persona implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    @DataTag
    private String nombre;

    @Column
    @DataTag
    private String apellidos;

    @Column(name = "CUENTA")
    private String email;

    @Column(name = "IDENTIFICACION")
    private String identificacion;

    @Column(name = "IDENTIFICACION_OFUSCADO")
    @DataTag
    private String identificacionOfuscado;

    @OneToMany(mappedBy = "persona")
    private Set<ActaEstudiante> actasEstudiantes;

    @OneToMany(mappedBy = "persona")
    private Set<ActaDiligencia> actaDiligencias;

    @OneToMany(mappedBy = "actaEstudiante")
    private Set<ActaDiligenciaFueraPlazo> actaDiligenciasFueraPlazo;

    @OneToMany(mappedBy = "persona")
    private Set<ResponsableAsignatura> responsableAsignaturas;

    @OneToMany(mappedBy = "personaNota")
    private Set<ActaDiligenciaVirtual> actaDiligenciasVirtual;

    public Persona()
    {
    }

    public Persona(Long personaId)
    {
        this.id = personaId;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getApellidos()
    {
        return apellidos;
    }

    public void setApellidos(String apellidos)
    {
        this.apellidos = apellidos;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getIdentificacion()
    {
        return identificacion;
    }

    public String getIdentificacionOfuscado()
    {
        return identificacionOfuscado;
    }

    public void setIdentificacionOfuscado(String identificacionOfuscado)
    {
        this.identificacionOfuscado = identificacionOfuscado;
    }

    public void setIdentificacion(String identificacion)
    {
        this.identificacion = identificacion;
    }

    public Set<ActaEstudiante> getActasEstudiantes()
    {
        return actasEstudiantes;
    }

    public void setActasEstudiantes(Set<ActaEstudiante> actasEstudiantes)
    {
        this.actasEstudiantes = actasEstudiantes;
    }

    public Set<ActaDiligencia> getActaDiligencias()
    {
        return actaDiligencias;
    }

    public void setActaDiligencias(Set<ActaDiligencia> actaDiligencias)
    {
        this.actaDiligencias = actaDiligencias;
    }

    public String getNombreCompleto()
    {
        return MessageFormat.format("{0}, {1}", apellidos, nombre);
    }

    public Set<ResponsableAsignatura> getResponsableAsignaturas()
    {
        return responsableAsignaturas;
    }

    public void setResponsableAsignaturas(Set<ResponsableAsignatura> responsableAsignaturas)
    {
        this.responsableAsignaturas = responsableAsignaturas;
    }

    public Set<ActaDiligenciaFueraPlazo> getActaDiligenciasFueraPlazo()
    {
        return actaDiligenciasFueraPlazo;
    }

    public void setActaDiligenciasFueraPlazo(Set<ActaDiligenciaFueraPlazo> actaDiligenciasFueraPlazo)
    {
        this.actaDiligenciasFueraPlazo = actaDiligenciasFueraPlazo;
    }

    public Set<ActaDiligenciaVirtual> getActaDiligenciasVirtual()
    {
        return actaDiligenciasVirtual;
    }

    public void setActaDiligenciasVirtual(Set<ActaDiligenciaVirtual> actaDiligenciasVirtual)
    {
        this.actaDiligenciasVirtual = actaDiligenciasVirtual;
    }

    public boolean tieneMismoNombreyApellidos(Persona otraPersona)
    {
        return otraPersona.nombre.toLowerCase().equals(nombre.toLowerCase()) && otraPersona.apellidos.toLowerCase().equals(apellidos.toLowerCase());
    }
}