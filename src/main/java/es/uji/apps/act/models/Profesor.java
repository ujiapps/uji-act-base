package es.uji.apps.act.models;

import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_EXT_PROFESORES")
public class Profesor implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "curso_Academico_id")
    private Long cursoAcademicoId;

    @Column(name = "estudio_id")
    private Long estudioId;

    @Column(name = "asignatura_id")
    private String asignaturaId;

    @Column(name = "grupo_id")
    private String grupoId;

    @Column(name = "persona_id")
    private Long personaId;

    @Column(name = "ubicacion_id")
    private Long ubicacionId;

    @Column(name = "area_id")
    private Long areaId;

    @Column(name = "puede_traspasar")
    private Boolean puedeTraspasar;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getCursoAcademicoId()
    {
        return cursoAcademicoId;
    }

    public void setCursoAcademicoId(Long cursoAcademicoId)
    {
        this.cursoAcademicoId = cursoAcademicoId;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public String getGrupoId()
    {
        return grupoId;
    }

    public void setGrupoId(String grupoId)
    {
        this.grupoId = grupoId;
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public Long getUbicacionId()
    {
        return ubicacionId;
    }

    public void setUbicacionId(Long ubicacionId)
    {
        this.ubicacionId = ubicacionId;
    }

    public Long getAreaId()
    {
        return areaId;
    }

    public void setAreaId(Long areaId)
    {
        this.areaId = areaId;
    }

    public Boolean isPuedeTraspasar()
    {
        return puedeTraspasar;
    }

    public void setPuedeTraspasar(Boolean puedeTraspasar)
    {
        this.puedeTraspasar = puedeTraspasar;
    }
}