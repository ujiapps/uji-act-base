package es.uji.apps.act.models;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;

import es.uji.apps.act.exceptions.TraspasarNotaException;
import es.uji.apps.act.models.enums.Calificacion;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_ACTAS_ESTUDIANTES")
public class ActaEstudiante implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "acta_asignatura_id")
    private ActaAsignatura actaAsignatura;

    @ManyToOne
    @JoinColumn(name = "persona_id")
    private Persona persona;

    @Column(name = "grupo_id")
    private String grupoId;

    @Column(name = "nota")
    private Double nota;

    @Column(name = "no_presentado")
    private Boolean noPresentado;

    @Column(name = "matricula_honor")
    private Boolean matriculaHonor;

    @Column(name = "fecha_traspaso")
    private Date fechaTraspaso;

    @Column(name = "comentario")
    private String comentario;

    @Column(name = "comentario_traspaso")
    private String comentarioTraspaso;

    @Column(name = "motivo_exclusion")
    private String motivoExclusion;

    @ManyToOne
    @JoinColumn(name = "persona_id_nota")
    private Persona personaNota;

    @Column(name = "FECHA_NOTA")
    private Date fechaNota;

    @Column(name = "CALIFICACION_ID")
    private Long calificacionTraspaso;

    @Column(name = "IP")
    private String ip;

    @OneToMany(mappedBy = "actaEstudiante")
    private Set<ActaDiligencia> actaDiligencias;

    @OneToMany(mappedBy = "actaEstudiante")
    private Set<ActaDiligenciaFueraPlazo> actaDiligenciasFueraPlazo;

    @OneToMany(mappedBy = "actaEstudiante", cascade = CascadeType.REMOVE)
    private Set<DesgloseNota> desgloseNotas;

    @Transient
    private NotaDesgloseCalculada notaDesgloseCalculada;

    @Transient
    private Boolean expedienteAbierto;

    public ActaEstudiante()
    {
        expedienteAbierto = null;
    }

    public ActaEstudiante(Long actaEstudianteId)
    {
        id = actaEstudianteId;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public ActaAsignatura getActaAsignatura()
    {
        return actaAsignatura;
    }

    public void setActaAsignatura(ActaAsignatura actaAsignatura)
    {
        this.actaAsignatura = actaAsignatura;
    }

    public Persona getPersona()
    {
        return persona;
    }

    public void setPersona(Persona persona)
    {
        this.persona = persona;
    }

    public String getGrupoId()
    {
        return grupoId;
    }

    public void setGrupoId(String grupoId)
    {
        this.grupoId = grupoId;
    }

    public Boolean isNoPresentado()
    {
        return noPresentado;
    }

    public void setNoPresentado(Boolean noPresentado)
    {
        this.noPresentado = noPresentado;
    }

    public Boolean isMatriculaHonor()
    {
        return matriculaHonor;
    }

    public void setMatriculaHonor(Boolean matriculaHonor)
    {
        this.matriculaHonor = matriculaHonor;
    }

    public Date getFechaTraspaso()
    {
        return fechaTraspaso;
    }

    public void setFechaTraspaso(Date fechaTraspaso)
    {
        this.fechaTraspaso = fechaTraspaso;
    }

    public String getComentario()
    {
        return comentario;
    }

    public void setComentario(String comentario)
    {
        this.comentario = comentario;
    }

    public String getComentarioTraspaso()
    {
        return comentarioTraspaso;
    }

    public void setComentarioTraspaso(String comentarioTraspaso)
    {
        this.comentarioTraspaso = comentarioTraspaso;
    }

    public Double getNota()
    {
        return nota;
    }

    public void setNota(Double nota)
    {
        this.nota = nota;
    }

    public String getMotivoExclusion()
    {
        return motivoExclusion;
    }

    public void setMotivoExclusion(String motivoExclusion)
    {
        this.motivoExclusion = motivoExclusion;
    }

    public Set<ActaDiligenciaFueraPlazo> getActaDiligenciasFueraPlazo()
    {
        return actaDiligenciasFueraPlazo;
    }

    public void setActaDiligenciasFueraPlazo(Set<ActaDiligenciaFueraPlazo> actaDiligenciasFueraPlazo)
    {
        this.actaDiligenciasFueraPlazo = actaDiligenciasFueraPlazo;
    }

    public Long getCalificacionId()
    {
        Double nota = getNotaEfectiva();
        if (nota == null || noPresentado)
        {
            return null;
        }
        if (nota < 5)
        {
            return Calificacion.SUSPENSO.getId();
        }
        if (nota < 7)
        {
            return Calificacion.APROBADO.getId();
        }
        if (nota < 9)
        {
            return Calificacion.NOTABLE.getId();
        }

        return matriculaHonor ? Calificacion.MATRICULA_HONOR.getId()
                : Calificacion.SOBRESALIENTE.getId();
    }

    public void traspasarNota(Long connectedUserId) throws IOException, TraspasarNotaException
    {
        RemoteTraspasarNotaProcedure remoteTraspasarNotaProcedure = new RemoteTraspasarNotaProcedure();
        remoteTraspasarNotaProcedure.init();
        remoteTraspasarNotaProcedure.execute(this, connectedUserId);
    }

    public boolean isEditable()
    {
        Acta acta = actaAsignatura.getActa();
        return acta.isEditable() && (fechaTraspaso == null) && !isExcluido() && (expedienteAbierto != null) && (expedienteAbierto);
    }

    public boolean isExcluido()
    {
        return (motivoExclusion != null && !motivoExclusion.trim().isEmpty());
    }

    public Set<ActaDiligencia> getActaDiligencias()
    {
        return actaDiligencias;
    }

    public void setActaDiligencias(Set<ActaDiligencia> actaDiligencias)
    {
        this.actaDiligencias = actaDiligencias;
    }

    public Set<DesgloseNota> getDesgloseNotas()
    {
        return desgloseNotas;
    }

    public void setDesgloseNotas(Set<DesgloseNota> desgloseNotas)
    {
        this.desgloseNotas = desgloseNotas;
    }

    public Double getNotaEfectiva()
    {
        if (noPresentado) return null;
        return (nota != null) ? nota : getNotaDesgloseCalculada().getNota();
    }

    public DesgloseNota getDesgloseNotaByPreguntaId(Long preguntaId)
    {
        return desgloseNotas.stream()
                .filter(dn -> dn.getDesglosePregunta().getId().equals(preguntaId)).findFirst().orElse(null);
    }

    public Long getNumeroNotasDesgloseIntroducidas()
    {
        return desgloseNotas.stream().filter(dn -> dn.getNota() != null).count();
    }

    public NotaDesgloseCalculada getNotaDesgloseCalculada()
    {
        if (notaDesgloseCalculada == null)
        {
            System.out.print("AVISO: No se ha obtenido la nota calculada");
            return null;
        }
        return notaDesgloseCalculada;
    }

    public void setNotaDesgloseCalculada(NotaDesgloseCalculada notaDesgloseCalculada)
    {
        this.notaDesgloseCalculada = notaDesgloseCalculada;
    }


    public Persona getPersonaNota()
    {
        return personaNota;
    }

    public void setPersonaNota(Persona personaNota)
    {
        this.personaNota = personaNota;
    }

    public String getIp()
    {
        return ip;
    }

    public void setIp(String ip)
    {
        this.ip = ip;
    }

    public Long getCalificacionTraspaso()
    {
        return calificacionTraspaso;
    }

    public void setCalificacionTraspaso(Long calificacionTraspaso)
    {
        this.calificacionTraspaso = calificacionTraspaso;
    }

    public Boolean getNoPresentado()
    {
        return noPresentado;
    }

    public Boolean getMatriculaHonor()
    {
        return matriculaHonor;
    }

    public Boolean isExpedienteAbierto()
    {
        return expedienteAbierto;
    }

    public Date getFechaNota()
    {
        return fechaNota;
    }

    public void setFechaNota(Date fechaNota)
    {
        this.fechaNota = fechaNota;
    }

    public void setExpedienteAbierto(Boolean expedienteAbierto)
    {
        this.expedienteAbierto = expedienteAbierto;
    }
}