package es.uji.apps.act.models.ui;

import java.text.MessageFormat;
import java.text.Normalizer;

public class UIActaEstudiante
{

    private String asignaturaId;

    private String dni;

    private String dniOfuscado;

    private String nombre;

    private String apellidos;

    private String grupoId;

    private Double nota;

    private Boolean matriculaHonor;

    private Long numeroConvocatoria;

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public String getDni()
    {
        return dni;
    }

    public String getDniOfuscado()
    {
        return dniOfuscado;
    }

    public void setDni(String dni)
    {
        this.dni = dni;
    }

    public void setDniOfuscado(String dniOfuscado)
    {
        this.dniOfuscado = dniOfuscado;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getApellidos()
    {
        return apellidos;
    }

    public void setApellidos(String apellidos)
    {
        this.apellidos = apellidos;
    }

    public String getGrupoId()
    {
        return grupoId;
    }

    public void setGrupoId(String grupoId)
    {
        this.grupoId = grupoId;
    }

    public Double getNota()
    {
        return nota;
    }

    public void setNota(Double nota)
    {
        this.nota = nota;
    }

    public Boolean getMatriculaHonor()
    {
        return matriculaHonor;
    }

    public void setMatriculaHonor(Boolean matriculaHonor)
    {
        this.matriculaHonor = matriculaHonor;
    }

    public Long getNumeroConvocatoria()
    {
        return numeroConvocatoria;
    }

    public void setNumeroConvocatoria(Long numeroConvocatoria)
    {
        this.numeroConvocatoria = numeroConvocatoria;
    }

    public String getNombreCompletoAscii()
    {
        String nombreCompleto = MessageFormat.format("{0} {1}", apellidos, nombre);
        nombreCompleto = Normalizer.normalize(nombreCompleto, Normalizer.Form.NFD);
        return nombreCompleto.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
    }
}
