package es.uji.apps.act.models;

import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_ACTAS_DILIGENCIAS")
public class ActaDiligencia implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "acta_id")
    private Acta acta;

    @ManyToOne
    @JoinColumn(name = "acta_estudiante_id")
    private ActaEstudiante actaEstudiante;

    @ManyToOne
    @JoinColumn(name = "persona_id")
    private Persona persona;

    @Column(name = "fecha")
    private Date fecha;

    @Column(name = "nota_anterior")
    private Double notaAnterior;

    @Column(name = "calificacion_anterior")
    private Long calificacionAnteriorId;

    @Column(name = "nota_nueva")
    private Double notaNueva;

    @Column(name = "calificacion_nueva")
    private Long calificacionNuevaId;

    @Column(name = "comentario_diligencia")
    private String comentarioDiligencia;

    @Column(name = "comentario_traspaso")
    private String comentarioTraspaso;

    @Column(name = "IP")
    private String ip;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Acta getActa()
    {
        return acta;
    }

    public void setActa(Acta acta)
    {
        this.acta = acta;
    }

    public ActaEstudiante getActaEstudiante()
    {
        return actaEstudiante;
    }

    public void setActaEstudiante(ActaEstudiante actaEstudiante)
    {
        this.actaEstudiante = actaEstudiante;
    }

    public Persona getPersona()
    {
        return persona;
    }

    public void setPersona(Persona persona)
    {
        this.persona = persona;
    }

    public Date getFecha()
    {
        return fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public Double getNotaAnterior()
    {
        return notaAnterior;
    }

    public void setNotaAnterior(Double notaAnterior)
    {
        this.notaAnterior = notaAnterior;
    }

    public Long getCalificacionAnteriorId()
    {
        return calificacionAnteriorId;
    }

    public void setCalificacionAnteriorId(Long calificacionAnteriorId)
    {
        this.calificacionAnteriorId = calificacionAnteriorId;
    }

    public Double getNotaNueva()
    {
        return notaNueva;
    }

    public void setNotaNueva(Double notaNueva)
    {
        this.notaNueva = notaNueva;
    }

    public Long getCalificacionNuevaId()
    {
        return calificacionNuevaId;
    }

    public void setCalificacionNuevaId(Long calificacionNuevaId)
    {
        this.calificacionNuevaId = calificacionNuevaId;
    }

    public String getComentarioDiligencia()
    {
        return comentarioDiligencia;
    }

    public void setComentarioDiligencia(String comentarioDiligencia)
    {
        this.comentarioDiligencia = comentarioDiligencia;
    }

    public String getIp()
    {
        return ip;
    }

    public void setIp(String ip)
    {
        this.ip = ip;
    }

    public String getComentarioTraspaso()
    {
        return comentarioTraspaso;
    }

    public void setComentarioTraspaso(String comentarioTraspaso)
    {
        this.comentarioTraspaso = comentarioTraspaso;
    }
}