package es.uji.apps.act.models.views;

import es.uji.apps.act.models.Persona;
import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_VM_EXT_PERSONAS_ASIGNATURAS")
public class ResponsableAsignatura implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "CURSO_ACA")
    private Long curso;

    @Id
    @Column(name = "ASI_ID")
    private String asignaturaId;

    @Id
    @Column(name = "GRUPO_ID")
    private String grupoId;

    @Column(name = "PUEDE_TRASPASAR")
    private String puedeTraspasar;

    @Column(name = "ADMINISTRADOR")
    private String administrador;

    @Column(name = "EMAIL")
    private String email;

    @Id
    @ManyToOne
    @JoinColumn(name = "PER_ID")
    private Persona persona;

    public Long getCurso()
    {
        return curso;
    }

    public void setCurso(Long curso)
    {
        this.curso = curso;
    }

    public String getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public Persona getPersona()
    {
        return persona;
    }

    public void setPersona(Persona persona)
    {
        this.persona = persona;
    }

    public String getGrupoId()
    {
        return grupoId;
    }

    public void setGrupoId(String grupoId)
    {
        this.grupoId = grupoId;
    }

    public String getPuedeTraspasar()
    {
        return puedeTraspasar;
    }

    public void setPuedeTraspasar(String puedeTraspasar)
    {
        this.puedeTraspasar = puedeTraspasar;
    }

    public String getAdministrador()
    {
        return administrador;
    }

    public void setAdministrador(String administrador)
    {
        this.administrador = administrador;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }
}