package es.uji.apps.act.models;

import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_EXT_ASIGNATURAS_AREA")
public class AsignaturaArea implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "CURSO_ACADEMICO_ID")
    private Long cursoId;

    @Column(name = "ASIGNATURA_ID")
    private Long asignaturaId;

    @Column(name = "DEPARTAMENTO_ID")
    private Long departamentoId;

    @Column(name = "AREA_ID")
    private Long areaId;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getDepartamentoId()
    {
        return departamentoId;
    }

    public void setDepartamentoId(Long departamentoId)
    {
        this.departamentoId = departamentoId;
    }

    public Long getAreaId()
    {
        return areaId;
    }

    public void setAreaId(Long areaId)
    {
        this.areaId = areaId;
    }

    public Long getCursoId()
    {
        return cursoId;
    }

    public void setCursoId(Long cursoId)
    {
        this.cursoId = cursoId;
    }

    public Long getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(Long asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }
}