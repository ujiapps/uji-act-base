package es.uji.apps.act.models;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import es.uji.apps.act.exceptions.SincronizarActaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import es.uji.apps.act.exceptions.TraspasarNotaException;

@Component
public class RemoteSincronizarActaProcedure
{
    private SincronizarActaProcedure sincronizarActaProcedure;
    private static DataSource dataSource;

    @Autowired
    public void setDataSource(DataSource dataSource)
    {

        RemoteSincronizarActaProcedure.dataSource = dataSource;
    }

    public void init()
    {
        this.sincronizarActaProcedure = new SincronizarActaProcedure(dataSource);
    }

    public Long execute(Long actaId, Long connectedUserId)
            throws IOException, SincronizarActaException
    {
        return sincronizarActaProcedure.execute(actaId, connectedUserId);
    }

    private class SincronizarActaProcedure extends StoredProcedure
    {
        private static final String SQL = "pack_actas.sincronizar_estudiantes_acta";
        private static final String ACTA_ID = "p_acta";
        private static final String RESULTADO = "p_resultado";

        public SincronizarActaProcedure(DataSource dataSource)
        {
            super(dataSource, SQL);
            declareParameter(new SqlOutParameter(RESULTADO, Types.BIGINT));
            declareParameter(new SqlParameter(ACTA_ID, Types.BIGINT));
            setFunction(true);
            compile();
        }

        public Long execute(Long actaId, Long connectedUserId)
                throws IOException, SincronizarActaException
        {
            String resultado = "";
            Map<String, Object> inParams = new HashMap();
            Map<String, Object> results = new HashMap();
            inParams.put(ACTA_ID, actaId);

            try
            {
                results = execute(inParams);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                SQLException sqlException = (SQLException) e.getCause();
                System.out.println(sqlException);
                throw new SincronizarActaException();
            }

            return !results.isEmpty() ? (Long) results.get("codigoError") : null;
        }
    }
}
