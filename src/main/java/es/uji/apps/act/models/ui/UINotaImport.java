package es.uji.apps.act.models.ui;

public class UINotaImport
{
    private Long id;
    private Long actaEstudianteId;
    private Long desgloseNotaId;
    private Long personaId;
    private String nombre;
    private String dni;
    private Double nota;
    private String notaTxt;
    private Double notaAnterior;
    private Long calificacionId;
    private Long calificacionIdAnterior;
    private Long estado;
    private Long estadoAlumno;
    private String mensaje;
    private Long desglosePreguntaId;
    private String desglosePreguntaEtiqueta;
    private Long desgloseGrupoId;
    private String desgloseGrupoEtiqueta;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getActaEstudianteId()
    {
        return actaEstudianteId;
    }

    public void setActaEstudianteId(Long actaEstudianteId)
    {
        this.actaEstudianteId = actaEstudianteId;
    }

    public Long getDesgloseNotaId()
    {
        return desgloseNotaId;
    }

    public void setDesgloseNotaId(Long desgloseNotaId)
    {
        this.desgloseNotaId = desgloseNotaId;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public Double getNota()
    {
        return nota;
    }

    public void setNota(Double nota)
    {
        this.nota = nota;
    }

    public String getNotaTxt()
    {
        return notaTxt;
    }

    public void setNotaTxt(String notaTxt)
    {
        this.notaTxt = notaTxt;
    }

    public Double getNotaAnterior()
    {
        return notaAnterior;
    }

    public void setNotaAnterior(Double notaAnterior)
    {
        this.notaAnterior = notaAnterior;
    }

    public Long getCalificacionId()
    {
        return calificacionId;
    }

    public void setCalificacionId(Long calificacionId)
    {
        this.calificacionId = calificacionId;
    }

    public Long getCalificacionIdAnterior()
    {
        return calificacionIdAnterior;
    }

    public void setCalificacionIdAnterior(Long calificacionIdAnterior)
    {
        this.calificacionIdAnterior = calificacionIdAnterior;
    }

    public Long getEstado()
    {
        return estado;
    }

    public void setEstado(Long estado)
    {
        this.estado = estado;
    }

    public Long getEstadoAlumno()
    {
        return estadoAlumno;
    }

    public void setEstadoAlumno(Long estadoAlumno)
    {
        this.estadoAlumno = estadoAlumno;
    }

    public String getMensaje()
    {
        return mensaje;
    }

    public void setMensaje(String mensaje)
    {
        this.mensaje = mensaje;
    }

    public Long getDesglosePreguntaId()
    {
        return desglosePreguntaId;
    }

    public void setDesglosePreguntaId(Long desglosePreguntaId)
    {
        this.desglosePreguntaId = desglosePreguntaId;
    }

    public String getDesglosePreguntaEtiqueta()
    {
        return desglosePreguntaEtiqueta;
    }

    public void setDesglosePreguntaEtiqueta(String desglosePreguntaEtiqueta)
    {
        this.desglosePreguntaEtiqueta = desglosePreguntaEtiqueta;
    }

    public Long getDesgloseGrupoId()
    {
        return desgloseGrupoId;
    }

    public void setDesgloseGrupoId(Long desgloseGrupoId)
    {
        this.desgloseGrupoId = desgloseGrupoId;
    }

    public String getDesgloseGrupoEtiqueta()
    {
        return desgloseGrupoEtiqueta;
    }

    public void setDesgloseGrupoEtiqueta(String desgloseGrupoEtiqueta)
    {
        this.desgloseGrupoEtiqueta = desgloseGrupoEtiqueta;
    }

    public void setIdentificacion(String dni)
    {
        this.dni = dni;
    }

    public String getDni()
    {
        return dni;
    }

    public void setDni(String dni)
    {
        this.dni = dni;
    }
}
