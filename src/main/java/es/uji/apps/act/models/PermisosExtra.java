package es.uji.apps.act.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "ACT_PERSONAS_ASIG_EXTRAS")
public class PermisosExtra implements Serializable
{

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "FECHA")
    private Date fecha;

    @Column(name = "FECHA_FIN")
    private Date fechaFin;

    @Column(name = "CURSO_ACA")
    private Long cursoAcademicoId;

    @Column(name = "ASI_ID")
    private String asignaturaId;

    @ManyToOne
    @JoinColumn(name = "per_id")
    private Persona persona;

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Long getCursoAcademicoId() {
        return cursoAcademicoId;
    }

    public void setCursoAcademicoId(Long cursoAcademicoId) {
        this.cursoAcademicoId = cursoAcademicoId;
    }

    public String getAsignaturaId() {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId) {
        this.asignaturaId = asignaturaId;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }
}