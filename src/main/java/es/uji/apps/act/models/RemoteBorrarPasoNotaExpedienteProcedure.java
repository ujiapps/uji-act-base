package es.uji.apps.act.models;

import es.uji.apps.act.exceptions.ExpedienteBloqueadoException;
import es.uji.apps.act.exceptions.TraspasarActaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

@Component
public class RemoteBorrarPasoNotaExpedienteProcedure
{
    private BorrarPasoNotaExpedienteProcedure borrarPasoNotaExpedienteProcedure;
    private static DataSource dataSource;

    @Autowired
    public void setDataSource(DataSource dataSource)
    {
        RemoteBorrarPasoNotaExpedienteProcedure.dataSource = dataSource;
    }

    public void init()
    {
        this.borrarPasoNotaExpedienteProcedure = new BorrarPasoNotaExpedienteProcedure(dataSource);
    }

    public Long execute(Long actaEstudianteId)
            throws IOException, TraspasarActaException, ExpedienteBloqueadoException
    {
        return borrarPasoNotaExpedienteProcedure.execute(actaEstudianteId);
    }

    private class BorrarPasoNotaExpedienteProcedure extends StoredProcedure
    {
        private static final String SQL = "pack_actas.borrar_paso_nota_expediente";
        private static final String ACTA_ESTUDIANTE_ID = "p_acta";

        public BorrarPasoNotaExpedienteProcedure(DataSource dataSource)
        {
            super(dataSource, SQL);
            declareParameter(new SqlOutParameter("codigoError", Types.BIGINT));
            declareParameter(new SqlParameter(ACTA_ESTUDIANTE_ID, Types.BIGINT));
            setFunction(true);
            compile();
        }

        public Long execute(Long actaEstudianteId)
                throws IOException, TraspasarActaException, ExpedienteBloqueadoException
        {
            Map<String, Object> inParams = new HashMap();
            Map<String, Object> results = new HashMap();
            inParams.put(ACTA_ESTUDIANTE_ID, actaEstudianteId);
            try
            {
                results = execute(inParams);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                SQLException sqlException = (SQLException) e.getCause();
                System.out.println(sqlException);

                if (sqlException.getErrorCode() == 20014)
                {
                    throw new ExpedienteBloqueadoException();
                }
                throw new TraspasarActaException();
            }

            return !results.isEmpty() ? (Long) results.get("codigoError") : null;
        }
    }
}
