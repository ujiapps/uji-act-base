package es.uji.apps.act.models.views;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_EXT_LISTA_ACTAS")
public class ActaVirtual implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "curso_aca")
    private Long curso;

    @Id
    @Column(name = "codigo")
    private String codigo;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "grupo_id")
    private String grupoId;

    @Column(name = "tipo_estudio_id")
    private String tipoEstudioId;

    @Column(name = "oficial")
    private String oficial;

    @Column(name = "acta_unica")
    private String actaUnica;

    @Column(name = "caracter")
    private String caracter;

    @Column(name = "eep")
    private String eep;

    @Column(name = "ep")
    private String ep;

    @Column(name = "tipo")
    private String tipo;

    @Column(name = "semestre")
    private String semestre;

    public Long getCurso()
    {
        return curso;
    }

    public void setCurso(Long curso)
    {
        this.curso = curso;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getGrupoId()
    {
        return grupoId;
    }

    public void setGrupoId(String grupoId)
    {
        this.grupoId = grupoId;
    }

    public String getTipoEstudioId()
    {
        return tipoEstudioId;
    }

    public void setTipoEstudioId(String tipoEstudioId)
    {
        this.tipoEstudioId = tipoEstudioId;
    }

    public String getOficial()
    {
        return oficial;
    }

    public void setOficial(String oficial)
    {
        this.oficial = oficial;
    }

    public String getActaUnica()
    {
        return actaUnica;
    }

    public void setActaUnica(String actaUnica)
    {
        this.actaUnica = actaUnica;
    }

    public String getCaracter()
    {
        return caracter;
    }

    public void setCaracter(String caracter)
    {
        this.caracter = caracter;
    }

    public String getEep()
    {
        return eep;
    }

    public void setEep(String eep)
    {
        this.eep = eep;
    }

    public String getEp()
    {
        return ep;
    }

    public void setEp(String ep)
    {
        this.ep = ep;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public String getSemestre()
    {
        return semestre;
    }

    public void setSemestre(String semestre)
    {
        this.semestre = semestre;
    }
}