package es.uji.apps.act.models.views;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;

@Entity
@BatchSize(size = 100)
    @Table(name = "ACT_VW_ACTAS_CONVOCATORIAS")
public class ActaConvocatoria implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Column(name = "CURSO_ACA")
    private Long cursoAcademicoId;

    @Column(name = "ASI_CODIGOS")
    private String asiCodigos;

    @Column(name = "CONVOCATORIA_ID")
    private Long convocatoriaId;

    @Column(name = "CONVOCATORIA_NOMBRE")
    private String convocatoriaNombre;

    @Column(name = "ORDEN")
    private Long orden;

    @Column(name = "ORDINARIA")
    private Long ordinariaId;

    @Column(name = "EXTRAORDINARIA")
    private Boolean extraordinaria;

    @Column(name = "MATRICULAS")
    private Long matriculas;

    @Id
    @Column(name = "ACTA_ID")
    private Long actaId;

    @Column(name = "FECHA_ALTA")
    private Date fechaAlta;

    @Column(name = "DESCRIPCION")
    private String descripcion;

    @Column(name = "SEMESTRE")
    private String semestreId;

    @Column(name = "FECHA_ULTIMA_MODIFICACION")
    private Date fechaLimiteModificacion;

    @Column(name = "MUESTRA_NOTAS_INICIO")
    private Date muestraNotasInicio;

    @Column(name = "MUESTRA_NOTAS_FIN")
    private Date muestraNotasFin;

    @Column(name = "FECHA_TRASPASO")
    private Date fechaTraspaso;

    @Column(name = "FECHA_INICIO_TRASPASO")
    private Date fechaInicioTraspaso;

    @Column(name = "FECHA_FIN_TRASPASO")
    private Date fechaFinTraspaso;

    @Column(name = "REFERENCIA")
    private String referencia;

    @Column(name = "ACTA_UNICA")
    private Boolean actaUnica;

    @Column(name = "PERMITE_DETALLE_NOTAS")
    private Boolean desgloseActivo;

    @Column(name = "FECHA_EXAMEN")
    private Date fechaExamen;

    @Column(name = "FECHA_INICIO_ACTA_UNICA")
    private Date fechaInicioActaUnica;

    @Column(name = "FECHA_FIN_MODIFICACION")
    private Date fechaFinModificacion;

    public Long getCursoAcademicoId()
    {
        return cursoAcademicoId;
    }

    public void setCursoAcademicoId(Long cursoAcademicoId)
    {
        this.cursoAcademicoId = cursoAcademicoId;
    }

    public String getAsiCodigos()
    {
        return asiCodigos;
    }

    public void setAsiCodigos(String asiCodigos)
    {
        this.asiCodigos = asiCodigos;
    }

    public Long getConvocatoriaId()
    {
        return convocatoriaId;
    }

    public void setConvocatoriaId(Long convocatoriaId)
    {
        this.convocatoriaId = convocatoriaId;
    }

    public String getConvocatoriaNombre()
    {
        return convocatoriaNombre;
    }

    public void setConvocatoriaNombre(String convocatoriaNombre)
    {
        this.convocatoriaNombre = convocatoriaNombre;
    }

    public Long getOrden()
    {
        return orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public Long getOrdinariaId()
    {
        return ordinariaId;
    }

    public void setOrdinariaId(Long ordinariaId)
    {
        this.ordinariaId = ordinariaId;
    }

    public Long getActaId()
    {
        return actaId;
    }

    public void setActaId(Long actaId)
    {
        this.actaId = actaId;
    }

    public Date getFechaAlta()
    {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta)
    {
        this.fechaAlta = fechaAlta;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public String getSemestreId()
    {
        return semestreId;
    }

    public void setSemestreId(String semestreId)
    {
        this.semestreId = semestreId;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public Date getMuestraNotasInicio()
    {
        return muestraNotasInicio;
    }

    public void setMuestraNotasInicio(Date muestraNotasInicio)
    {
        this.muestraNotasInicio = muestraNotasInicio;
    }

    public Date getMuestraNotasFin()
    {
        return muestraNotasFin;
    }

    public void setMuestraNotasFin(Date muestraNotasFin)
    {
        this.muestraNotasFin = muestraNotasFin;
    }

    public Date getFechaTraspaso()
    {
        return fechaTraspaso;
    }

    public void setFechaTraspaso(Date fechaTraspaso)
    {
        this.fechaTraspaso = fechaTraspaso;
    }

    public Boolean isExtraordinaria()
    {
        return extraordinaria;
    }

    public void setExtraordinaria(Boolean extraordinaria)
    {
        this.extraordinaria = extraordinaria;
    }

    public Date getFechaLimiteModificacion()
    {
        return fechaLimiteModificacion;
    }

    public void setFechaLimiteModificacion(Date fechaLimiteModificacion)
    {
        this.fechaLimiteModificacion = fechaLimiteModificacion;
    }

    public boolean isEditable()
    {
        return fechaTraspaso == null && getFechaFinModificacion().after(new Date());
    }

    public boolean isTraspasable()
    {
        return fechaTraspaso == null && fechaInicioTraspaso != null
                && fechaInicioTraspaso.before(new Date());
    }

    public boolean isPuedePasarNotasAExpediente()
    {
        return fechaTraspaso == null && actaUnica != null && actaUnica == true && fechaInicioActaUnica != null && fechaInicioActaUnica.before(new Date());
    }

    public boolean isDestraspasable()
    {
        return fechaTraspaso != null && fechaLimiteModificacion != null
                && getFechaLimiteModificacion().after(new Date());
    }

    public Long getMatriculas()
    {
        return matriculas;
    }

    public void setMatriculas(Long matriculas)
    {
        this.matriculas = matriculas;
    }

    public String getReferencia()
    {
        return referencia;
    }

    public void setReferencia(String referencia)
    {
        this.referencia = referencia;
    }

    public Date getFechaInicioTraspaso()
    {
        return fechaInicioTraspaso;
    }

    public void setFechaInicioTraspaso(Date fechaInicioTraspaso)
    {
        this.fechaInicioTraspaso = fechaInicioTraspaso;
    }

    public Date getFechaFinTraspaso()
    {
        return fechaFinTraspaso;
    }

    public void setFechaFinTraspaso(Date fechaFinTraspaso)
    {
        this.fechaFinTraspaso = fechaFinTraspaso;
    }

    public Boolean isActaUnica()
    {
        return actaUnica;
    }

    public void setActaUnica(Boolean actaUnica)
    {
        this.actaUnica = actaUnica;
    }

    public Boolean isPuedeGuardarNotas()
    {
        return actaUnica != null && actaUnica && fechaTraspaso == null && !extraordinaria
                && (fechaInicioActaUnica == null || fechaInicioActaUnica.before(new Date()));
    }

    public Boolean isDesgloseActivo()
    {
        return desgloseActivo;
    }

    public void setDesgloseActivo(Boolean permiteDetalleNotas)
    {
        this.desgloseActivo = permiteDetalleNotas;
    }

    public Date getFechaExamen()
    {
        return fechaExamen;
    }

    public void setFechaExamen(Date fechaExamen)
    {
        this.fechaExamen = fechaExamen;
    }

    public Date getFechaInicioActaUnica()
    {
        return fechaInicioActaUnica;
    }

    public void setFechaInicioActaUnica(Date fechaInicioActaUnica)
    {
        this.fechaInicioActaUnica = fechaInicioActaUnica;
    }

    public Date getFechaFinModificacion()
    {
        return fechaFinModificacion;
    }

    public void setFechaFinModificacion(Date fechaFinModificacion)
    {
        this.fechaFinModificacion = fechaFinModificacion;
    }
}