package es.uji.apps.act.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.BatchSize;

import es.uji.commons.rest.annotations.DataTag;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_ACTAS_REVISIONES")
public class ActaRevision implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "acta_id")
    private Acta acta;

    @DataTag
    @Column(name = "nombre")
    private String nombre;

    @Column(name = "fecha")
    private Date fecha;

    @Column(name = "fecha_fin")
    private Date fechaFin;

    @Column(name = "lugar")
    private String lugar;

    @DataTag
    @Column(name = "grupos")
    private String grupos;

    @DataTag
    @Column(name = "asignaturas")
    private String asignaturas;

    @Column(name = "observaciones")
    private String observaciones;

    @Column(name = "conjunta")
    private Boolean conjunta;

    @Column(name = "MUESTRA_NOTAS_INICIO")
    private Date muestraNotasInicio;

    @Column(name = "MUESTRA_NOTAS_FIN")
    private Date muestraNotasFin;

    @OneToMany(mappedBy = "actaRevision", cascade = CascadeType.REMOVE)
    private Set<Notificacion> notificaciones;

    public ActaRevision()
    {
    }

    public ActaRevision(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Acta getActa()
    {
        return acta;
    }

    public void setActa(Acta acta)
    {
        this.acta = acta;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Date getFecha()
    {
        return fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public Date getFechaFin()
    {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin)
    {
        this.fechaFin = fechaFin;
    }

    public String getLugar()
    {
        return lugar;
    }

    public void setLugar(String lugar)
    {
        this.lugar = lugar;
    }

    public String getGrupos()
    {
        return grupos;
    }

    public void setGrupos(String grupos)
    {
        this.grupos = grupos;
    }

    public String getAsignaturas()
    {
        return asignaturas;
    }

    public void setAsignaturas(String asignaturas)
    {
        this.asignaturas = asignaturas;
    }

    public Boolean isConjunta()
    {
        return conjunta;
    }

    public void setConjunta(Boolean conjunta)
    {
        this.conjunta = conjunta;
    }

    public String getObservaciones()
    {
        return observaciones;
    }

    public void setObservaciones(String observaciones)
    {
        this.observaciones = observaciones;
    }

    public List<String> getGruposAsList()
    {
        return new ArrayList<String>(Arrays.asList(grupos.split(",")));
    }

    public List<String> getAsignaturasAsList()
    {
        return new ArrayList<String>(Arrays.asList(asignaturas.split(",")));
    }

    public Set<Notificacion> getNotificaciones()
    {
        return notificaciones;
    }

    public void setNotificaciones(Set<Notificacion> notificaciones)
    {
        this.notificaciones = notificaciones;
    }

    public Date getMuestraNotasInicio()
    {
        return muestraNotasInicio;
    }

    public void setMuestraNotasInicio(Date muestraNotasInicio)
    {
        this.muestraNotasInicio = muestraNotasInicio;
    }

    public Date getMuestraNotasFin()
    {
        return muestraNotasFin;
    }

    public void setMuestraNotasFin(Date muestraNotasFin)
    {
        this.muestraNotasFin = muestraNotasFin;
    }
}