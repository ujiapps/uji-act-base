package es.uji.apps.act.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "act_asignaturas_formacion_dual")
public class FormacionDual implements Serializable
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "CURSO_ACA")
    private Long cursoAcademicoId;

    @Column(name = "ASI_ID")
    private String asignaturaId;

    @Column(name = "FECHA")
    private Date fecha;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Long getCursoAcademicoId() {
        return cursoAcademicoId;
    }

    public void setCursoAcademicoId(Long cursoAcademicoId) {
        this.cursoAcademicoId = cursoAcademicoId;
    }

    public String getAsignaturaId() {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId) {
        this.asignaturaId = asignaturaId;
    }

}