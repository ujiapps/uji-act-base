package es.uji.apps.act.models.views;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_VW_ALUMNOS_CONVOCATORIA")
public class EstudianteConvocatoria implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "CURSO_ACADEMICO_ID")
    private Long cursoId;

    @Id
    @Column(name = "ACTA_ID")
    private Long actaId;

    @Id
    @Column(name = "PERSONA_ID")
    private Long perId;

    @Id
    @Column(name = "ASIGNATURA_ID")
    private String asignaturaId;

    @Column(name = "GRUPO_ID")
    private String grupoId;


    @Column(name = "NUM_CONVOCATORIA")
    private Long numeroConvocatoria;

    public Long getCursoId()
    {
        return cursoId;
    }

    public void setCursoId(Long cursoId)
    {
        this.cursoId = cursoId;
    }

    public Long getActaId()
    {
        return actaId;
    }

    public void setActaId(Long actaId)
    {
        this.actaId = actaId;
    }

    public Long getPerId()
    {
        return perId;
    }

    public void setPerId(Long perId)
    {
        this.perId = perId;
    }

    public Long getNumeroConvocatoria()
    {
        return numeroConvocatoria;
    }

    public void setNumeroConvocatoria(Long numeroConvocatoria)
    {
        this.numeroConvocatoria = numeroConvocatoria;
    }

    public String getAsignaturaId() {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId) {
        this.asignaturaId = asignaturaId;
    }

    public String getGrupoId() {
        return grupoId;
    }

    public void setGrupoId(String grupoId) {
        this.grupoId = grupoId;
    }
}