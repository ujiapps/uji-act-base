package es.uji.apps.act.models;

import org.hibernate.annotations.BatchSize;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_CURSOS_ACADEMICOS")
public class Curso implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    private Boolean activo;

    @Column(name = "FECHA_FIN_MODIFICACION")
    private Date fechaFinModificacion;

    @OneToMany(mappedBy = "curso")
    private Set<Acta> actas;

    @OneToMany(mappedBy = "curso")
    private Set<FechaConvocatoria> fechasConvocatoria;

    public Curso()
    {
    }

    public Curso(Long cursoId)
    {
        this.id = cursoId;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Boolean isActivo()
    {
        return activo;
    }

    public void setActivo(Boolean activo)
    {
        this.activo = activo;
    }

    public Date getFechaFinModificacion()
    {
        return fechaFinModificacion;
    }

    public void setFechaFinModificacion(Date fechaUltimaModificacion)
    {
        this.fechaFinModificacion = fechaUltimaModificacion;
    }

    public Set<Acta> getActas()
    {
        return actas;
    }

    public void setActas(Set<Acta> actas)
    {
        this.actas = actas;
    }

    public Set<FechaConvocatoria> getFechasConvocatoria()
    {
        return fechasConvocatoria;
    }

    public void setFechasConvocatoria(Set<FechaConvocatoria> fechasConvocatoria)
    {
        this.fechasConvocatoria = fechasConvocatoria;
    }
}