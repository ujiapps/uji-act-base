package es.uji.apps.act.models;

import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_EXT_ESTUDIANTES")
public class Estudiante implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "PERSONA_ID")
    private Long id;

    private String nombre;

    @Column(name = "ASIGNATURA_ID")
    private Long asignaturaId;

    @Column(name = "NOMBRE_ASIGNATURA")
    private String nombreAsignatura;

    @Column(name = "GRUPO_ID")
    private String grupoId;

    @Column(name = "TIPO_ESTUDIO_ID")
    private Long tipoEstudioId;

    @Column(name = "ESTUDIO_ID")
    private Long estudioId;

    @Column(name = "CURSO_ACADEMICO_ID")
    private Long cursoId;

    @Column(name = "CURSADA_EXTRANJERO")
    private Boolean cursadaEstranjero;

    @Column(name = "APARECE_EN_ACTA")
    private Boolean apareceEnActa;

    private Boolean superada;

    private Double nota;

    @Column(name = "VECES_PRESENTADA")
    private Long vecesPresentada;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getAsignaturaId()
    {
        return asignaturaId;
    }

    public void setAsignaturaId(Long asignaturaId)
    {
        this.asignaturaId = asignaturaId;
    }

    public String getNombreAsignatura()
    {
        return nombreAsignatura;
    }

    public void setNombreAsignatura(String nombreAsignatura)
    {
        this.nombreAsignatura = nombreAsignatura;
    }

    public String getGrupoId()
    {
        return grupoId;
    }

    public void setGrupoId(String grupoId)
    {
        this.grupoId = grupoId;
    }

    public Long getTipoEstudioId()
    {
        return tipoEstudioId;
    }

    public void setTipoEstudioId(Long tipoEstudioId)
    {
        this.tipoEstudioId = tipoEstudioId;
    }

    public Long getEstudioId()
    {
        return estudioId;
    }

    public void setEstudioId(Long estudioId)
    {
        this.estudioId = estudioId;
    }

    public Long getCursoId()
    {
        return cursoId;
    }

    public void setCursoId(Long cursoId)
    {
        this.cursoId = cursoId;
    }

    public Boolean isCursadaEstranjero()
    {
        return cursadaEstranjero;
    }

    public void setCursadaEstranjero(Boolean cursadaEstranjero)
    {
        this.cursadaEstranjero = cursadaEstranjero;
    }

    public Boolean isApareceEnActa()
    {
        return apareceEnActa;
    }

    public void setApareceEnActa(Boolean apareceEnActa)
    {
        this.apareceEnActa = apareceEnActa;
    }

    public Boolean isSuperada()
    {
        return superada;
    }

    public void setSuperada(Boolean superada)
    {
        this.superada = superada;
    }

    public Double getNota()
    {
        return nota;
    }

    public void setNota(Double nota)
    {
        this.nota = nota;
    }

    public Long getVecesPresentada()
    {
        return vecesPresentada;
    }

    public void setVecesPresentada(Long vecesPresentada)
    {
        this.vecesPresentada = vecesPresentada;
    }
}