package es.uji.apps.act.models.ui;

import java.util.*;
import java.util.stream.Collectors;

public class UIActa
{
    Long curso;

    String departamento;

    String convocatoria;

    String codigosAsignatura;

    String nombreAsignatura;

    String grupos;

    String firmaDigital;

    Long numeroAsignaturas;

    List<UIActaEstudiante> estudiantes;

    List<UIActaRevision> revisiones;

    public UIActa()
    {
        estudiantes = new ArrayList<>();
        revisiones = new ArrayList<>();
    }

    public Long getCurso()
    {
        return curso;
    }

    public void setCurso(Long curso)
    {
        this.curso = curso;
    }

    public String getDepartamento()
    {
        return departamento;
    }

    public void setDepartamento(String departamento)
    {
        this.departamento = departamento;
    }

    public String getConvocatoria()
    {
        return convocatoria;
    }

    public void setConvocatoria(String convocatoria)
    {
        this.convocatoria = convocatoria;
    }

    public String getNombreAsignatura()
    {
        return nombreAsignatura;
    }

    public void setNombreAsignatura(String nombreAsignatura)
    {
        this.nombreAsignatura = nombreAsignatura;
    }

    public List<UIActaEstudiante> getEstudiantes()
    {
        return estudiantes;
    }

    public List<UIActaEstudiante> getEstudiantesOrdenadosPorNombre()
    {
        return estudiantes.stream()
                .sorted(Comparator.comparing(UIActaEstudiante::getNombreCompletoAscii))
                .collect(Collectors.toList());
    }

    public void setEstudiantes(List<UIActaEstudiante> estudiantes)
    {
        this.estudiantes = estudiantes;
    }

    public List<UIActaRevision> getRevisiones()
    {
        return revisiones;
    }

    public void setRevisiones(List<UIActaRevision> revisiones)
    {
        this.revisiones = revisiones;
    }

    public long getNumeroAlumnos()
    {
        return estudiantes.size();
    }

    public long getNumeroAlumnosPresentados()
    {
        return estudiantes.stream().filter(e -> e.getNota() != null).count();
    }

    public Double getNotaMaxima()
    {
        Optional<Double> max = estudiantes.stream().filter(e -> e.getNota() != null)
                .map(a -> a.getNota()).max((p1, p2) -> Double.compare(p1, p2));
        return (max.isPresent()) ? max.get() : null;
    }

    public Double getNotaMinima()
    {
        Optional<Double> min = estudiantes.stream().filter(e -> e.getNota() != null)
                .map(a -> a.getNota()).min((p1, p2) -> Double.compare(p1, p2));
        return (min.isPresent()) ? min.get() : null;
    }

    public Double getNotaMedia()
    {
        OptionalDouble avg = estudiantes.stream().filter(e -> e.getNota() != null)
                .mapToDouble(a -> a.getNota()).average();
        return (avg.isPresent()) ? avg.getAsDouble() : null;
    }

    public String getCodigosAsignatura()
    {
        return codigosAsignatura;
    }

    public void setCodigosAsignatura(String codigosAsignatura)
    {
        this.codigosAsignatura = codigosAsignatura;
    }

    public String getGrupos()
    {
        return grupos;
    }

    public void setGrupos(String grupos)
    {
        this.grupos = grupos;
    }

    public Long getNumeroAsignaturas()
    {
        return numeroAsignaturas;
    }

    public void setNumeroAsignaturas(Long numeroAsignaturas)
    {
        this.numeroAsignaturas = numeroAsignaturas;
    }

    public String getFirmaDigital()
    {
        return firmaDigital;
    }

    public void setFirmaDigital(String firmaDigital)
    {
        this.firmaDigital = firmaDigital;
    }
}
