package es.uji.apps.act.models;

import es.uji.commons.rest.annotations.DataTag;
import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_ACTAS_ASIGNATURAS")
public class ActaAsignatura implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "acta_id")
    private Acta acta;

    @DataTag
    @ManyToOne
    @JoinColumn(name = "asignatura_id")
    private Asignatura asignatura;

    @Column(name = "grupo_id")
    private String grupoId;

    @OneToMany(mappedBy = "actaAsignatura", cascade = CascadeType.REMOVE)
    private Set<ActaEstudiante> actasEstudiantes;

    public ActaAsignatura()
    {
    }

    public ActaAsignatura(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Acta getActa()
    {
        return acta;
    }

    public void setActa(Acta acta)
    {
        this.acta = acta;
    }

    public Asignatura getAsignatura()
    {
        return asignatura;
    }

    public void setAsignatura(Asignatura asignatura)
    {
        this.asignatura = asignatura;
    }

    public String getGrupoId()
    {
        return grupoId;
    }

    public void setGrupoId(String grupoId)
    {
        this.grupoId = grupoId;
    }

    public Set<ActaEstudiante> getActasEstudiantes()
    {
        return actasEstudiantes;
    }

    public void setActasEstudiantes(Set<ActaEstudiante> actasEstudiantes)
    {
        this.actasEstudiantes = actasEstudiantes;
    }
}