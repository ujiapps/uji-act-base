package es.uji.apps.act.models;

import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_VW_PDI_ACTIVOS")
public class PDIActivo implements Serializable
{
    @Id
    @Column(name = "per_id")
    private Long id;

    @Column(name = "nombre_min")
    private String nombre;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}