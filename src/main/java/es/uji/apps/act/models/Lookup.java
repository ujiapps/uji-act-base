package es.uji.apps.act.models;

import java.io.Serializable;

public class Lookup implements Serializable
{
    private static final long serialVersionUID = 1L;

    public Lookup(String id, String nombre)
    {
        this.id = id;
        this.nombre = nombre;
    }

    private String id;

    private String nombre;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }
}