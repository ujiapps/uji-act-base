package es.uji.apps.act.models.enums;

public enum Acceso
{
    LECTURA(1L, "Lectura"), ESCRITURA(2L, "Escritura");

    private final Long id;
    private final String nombre;

    Acceso(Long id, String nombre)
    {
        this.id = id;
        this.nombre = nombre;
    }

    public Long getId()
    {
        return id;
    }

    public String getNombre()
    {
        return nombre;
    }

    @Override
    public String toString()
    {
        return nombre;
    }
}
