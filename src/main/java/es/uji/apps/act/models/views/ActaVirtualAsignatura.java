package es.uji.apps.act.models.views;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_EXT_LISTA_ACTAS_ASI")
public class ActaVirtualAsignatura implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "curso_aca")
    private Long curso;

    @Id
    @Column(name = "codigo")
    private String codigo;

    @Id
    @Column(name = "asi_id")
    private String asignaturaId;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "grupo_id")
    private String grupoId;

    public Long getCurso()
    {
        return curso;
    }

    public void setCurso(Long curso)
    {
        this.curso = curso;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getGrupoId() {
        return grupoId;
    }

    public void setGrupoId(String grupoId) {
        this.grupoId = grupoId;
    }

    public String getAsignaturaId() {
        return asignaturaId;
    }

    public void setAsignaturaId(String asignaturaId) {
        this.asignaturaId = asignaturaId;
    }
}