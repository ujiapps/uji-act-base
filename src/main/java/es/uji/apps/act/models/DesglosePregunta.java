package es.uji.apps.act.models;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.BatchSize;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_ACTAS_PREGUNTAS")
public class DesglosePregunta implements Serializable, Comparable<DesglosePregunta>
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "etiqueta")
    private String etiqueta;

    @Column(name = "orden")
    private Long orden;

    @Column(name = "visible")
    private Boolean visible;

    @Column(name = "peso")
    private Double peso;

    @Column(name = "nota_minima")
    private Double notaMinima;

    @Column(name = "nota_maxima")
    private Double notaMaxima;

    @ManyToOne
    @JoinColumn(name = "acta_id")
    private Acta acta;

    @ManyToOne
    @JoinColumn(name = "parcial_acta_id")
    private Acta parcialActa;

    @ManyToOne
    @JoinColumn(name = "grupo_id")
    private DesgloseGrupo desgloseGrupo;

    @OneToMany(mappedBy = "desglosePregunta")
    private Set<DesgloseNota> desgloseNotas;

    public DesglosePregunta()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Acta getActa()
    {
        return acta;
    }

    public void setActa(Acta acta)
    {
        this.acta = acta;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getEtiqueta()
    {
        return etiqueta;
    }

    public void setEtiqueta(String etiqueta)
    {
        this.etiqueta = etiqueta;
    }

    public Long getOrden()
    {
        return orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public Boolean isVisible()
    {
        return visible;
    }

    public void setVisible(Boolean visible)
    {
        this.visible = visible;
    }

    public Double getPeso()
    {
        return peso;
    }

    public void setPeso(Double peso)
    {
        this.peso = peso;
    }

    public Double getNotaMinima()
    {
        return notaMinima;
    }

    public void setNotaMinima(Double notaMinima)
    {
        this.notaMinima = notaMinima;
    }

    public Double getNotaMaxima()
    {
        return notaMaxima;
    }

    public void setNotaMaxima(Double notaMaxima)
    {
        this.notaMaxima = notaMaxima;
    }

    public Set<DesgloseNota> getDesgloseNotas()
    {
        return desgloseNotas;
    }

    public void setDesgloseNotas(Set<DesgloseNota> desgloseNotas)
    {
        this.desgloseNotas = desgloseNotas;
    }

    public DesgloseGrupo getDesgloseGrupo()
    {
        return desgloseGrupo;
    }

    public void setDesgloseGrupo(DesgloseGrupo desgloseGrupo)
    {
        this.desgloseGrupo = desgloseGrupo;
    }

    public Acta getParcialActa()
    {
        return parcialActa;
    }

    public void setParcialActa(Acta parcialActa)
    {
        this.parcialActa = parcialActa;
    }

    @Override
    public int compareTo(DesglosePregunta other)
    {
        int compare = Long.compare(getOrden(), other.getOrden());
        if (compare == 0)
        {
            compare = getEtiqueta().compareTo(other.getEtiqueta());
        }
        if (compare == 0)
        {
            compare = Long.compare(getId(), other.getId());
        }
        return compare;
    }

    public DesglosePregunta clonar()
    {
        return null;
    }
}