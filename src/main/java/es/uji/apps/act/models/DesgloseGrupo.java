package es.uji.apps.act.models;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;

import es.uji.commons.rest.annotations.DataTag;

import org.hibernate.annotations.BatchSize;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_ACTAS_GRUPOS")
public class DesgloseGrupo implements Serializable, Comparable<DesgloseGrupo>
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "acta_id")
    private Acta acta;

    @Column(name = "nombre")
    private String nombre;

    @DataTag
    @Column(name = "etiqueta")
    private String etiqueta;

    @Column(name = "orden")
    private Long orden;

    @Column(name = "visible")
    private Boolean visible;

    @Column(name = "peso")
    private Double peso;

    @Column(name = "nota_minima")
    private Double notaMinima;

    @Column(name = "nota_maxima")
    private Double notaMaxima;

    @Column(name = "nota_minima_aprobado")
    private Double notaMinimaAprobado;

    @OneToMany(mappedBy = "desgloseGrupo", cascade = CascadeType.REMOVE)
    private Set<DesglosePregunta> desglosePreguntas;

    public DesgloseGrupo()
    {
    }

    public DesgloseGrupo(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Acta getActa()
    {
        return acta;
    }

    public void setActa(Acta acta)
    {
        this.acta = acta;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getEtiqueta()
    {
        return etiqueta;
    }

    public void setEtiqueta(String etiqueta)
    {
        this.etiqueta = etiqueta;
    }

    public Long getOrden()
    {
        return orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public Double getPeso()
    {
        return peso;
    }

    public void setPeso(Double peso)
    {
        this.peso = peso;
    }

    public Double getNotaMinima()
    {
        return notaMinima;
    }

    public void setNotaMinima(Double notaMinima)
    {
        this.notaMinima = notaMinima;
    }

    public Double getNotaMaxima()
    {
        return notaMaxima;
    }

    public void setNotaMaxima(Double notaMaxima)
    {
        this.notaMaxima = notaMaxima;
    }

    public Double getNotaMinimaAprobado()
    {
        return notaMinimaAprobado;
    }

    public void setNotaMinimaAprobado(Double notaMinimaAprobado)
    {
        this.notaMinimaAprobado = notaMinimaAprobado;
    }

    public Set<DesglosePregunta> getDesglosePreguntas()
    {
        return desglosePreguntas;
    }

    public void setDesglosePreguntas(Set<DesglosePregunta> desglosePreguntas)
    {
        this.desglosePreguntas = desglosePreguntas;
    }

    public Boolean isVisible()
    {
        return visible;
    }

    public void setVisible(Boolean visible)
    {
        this.visible = visible;
    }

    @Override
    public int compareTo(DesgloseGrupo other)
    {
        int compare = Long.compare(getOrden(), other.getOrden());
        if (compare == 0)
        {
            compare = getEtiqueta().compareTo(other.getEtiqueta());
        }
        if (compare == 0)
        {
            compare = Long.compare(getId(), other.getId());
        }
        return compare;
    }

    public DesgloseGrupo clonar()
    {
        return null;
    }
}