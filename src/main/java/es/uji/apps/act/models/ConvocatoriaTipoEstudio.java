package es.uji.apps.act.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_CONVOCATORIAS_TIPOS_EST")
public class ConvocatoriaTipoEstudio implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "CONVOCATORIA_ID")
    private Convocatoria convocatoria;

    @Column(name = "TIPO_ESTUDIO_ID")
    private String tipoEstudioId;

    @Column(name = "ID_UJI")
    private Long ujiId;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getTipoEstudioId()
    {
        return tipoEstudioId;
    }

    public void setTipoEstudioId(String tipoEstudioId)
    {
        this.tipoEstudioId = tipoEstudioId;
    }

    public Convocatoria getConvocatoria()
    {
        return convocatoria;
    }

    public void setConvocatoria(Convocatoria convocatoria)
    {
        this.convocatoria = convocatoria;
    }

    public Long getUjiId()
    {
        return ujiId;
    }

    public void setUjiId(Long ujiId)
    {
        this.ujiId = ujiId;
    }
}