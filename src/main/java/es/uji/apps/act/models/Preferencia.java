package es.uji.apps.act.models;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.BatchSize;

import es.uji.apps.act.models.views.ResponsableAsignatura;
import es.uji.commons.rest.annotations.DataTag;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_PERSONAS_PREFERENCIAS")
public class Preferencia implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "PER_ID")
    private Long perId;

    @Column
    private String tema;

    public Long getPerId()
    {
        return perId;
    }

    public void setPerId(Long perId)
    {
        this.perId = perId;
    }

    public String getTema()
    {
        return tema;
    }

    public void setTema(String tema)
    {
        this.tema = tema;
    }
}