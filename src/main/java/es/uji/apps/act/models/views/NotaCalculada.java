package es.uji.apps.act.models.views;

import org.hibernate.annotations.BatchSize;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity @BatchSize(size = 100) @Table(name = "ACT_VW_NOTAS_CALCULADAS") public class NotaCalculada
        implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id private String id;

    @Column(name = "ACTA_ID") private Long actaId;

    @Column(name = "ESTUDIANTE_ID") private Long estudianteId;

    @Column(name = "PERSONA_ID") private Long personaId;

    @Column(name = "NOTA_CALCULADA") private Double notaCalculada;

    @Column(name = "RDO") private String formula;

    @Column(name = "PERMITE_DETALLE_NOTAS") private Boolean notaDesglosada;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public Long getActaId()
    {
        return actaId;
    }

    public void setActaId(Long actaId)
    {
        this.actaId = actaId;
    }

    public Long getEstudianteId()
    {
        return estudianteId;
    }

    public void setEstudianteId(Long estudianteId)
    {
        this.estudianteId = estudianteId;
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public Double getNotaCalculada()
    {
        return notaCalculada;
    }

    public void setNotaCalculada(Double notaCalculada)
    {
        this.notaCalculada = notaCalculada;
    }

    public String getFormula()
    {
        return formula;
    }

    public void setFormula(String formula)
    {
        this.formula = formula;
    }

    public Boolean getNotaDesglosada()
    {
        return notaDesglosada;
    }

    public void setNotaDesglosada(Boolean notaDesglosada)
    {
        this.notaDesglosada = notaDesglosada;
    }
}