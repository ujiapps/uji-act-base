package es.uji.apps.act.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;

@Entity
@BatchSize(size = 100)
@Table(name = "ACT_EXT_ACTAS_CURSOS_MOODLE")
public class CursoMoodle implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Column(name = "ASI_ID")
    private String asiId;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "PER_ID")
    private Long perId;

    @Id
    @ManyToOne
    @JoinColumn(name = "CURSO_ACADEMICO_ID")
    private Curso curso;

    @Column(name = "F_ALTA")
    private Date fechaAlta;

    @Id
    @Column(name = "ID")
    private Long id;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getAsiId()
    {
        return asiId;
    }

    public void setAsiId(String asiId)
    {
        this.asiId = asiId;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Curso getCurso()
    {
        return curso;
    }

    public void setCurso(Curso curso)
    {
        this.curso = curso;
    }

    public Long getPerId()
    {
        return perId;
    }

    public void setPerId(Long perId)
    {
        this.perId = perId;
    }

    public Date getFechaAlta()
    {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta)
    {
        this.fechaAlta = fechaAlta;
    }

    public Long getPmeId()
    {
        return id;
    }

    public void setPmeId(Long id)
    {
        this.id = id;
    }
}