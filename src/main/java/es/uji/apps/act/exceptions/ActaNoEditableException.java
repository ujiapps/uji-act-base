package es.uji.apps.act.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;


public class ActaNoEditableException extends CoreBaseException
{
    public ActaNoEditableException()
    {
        super("No es pot modificar l'acta");
    }

    public ActaNoEditableException(String message)
    {
        super(message);
    }
}
