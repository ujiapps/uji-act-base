package es.uji.apps.act.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class NotaNoValidaException extends CoreBaseException
{
    public NotaNoValidaException()
    {
        super("La nota introduïda no és correcta");
    }

    public NotaNoValidaException(String message)
    {
        super(message);
    }
}
