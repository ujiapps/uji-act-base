package es.uji.apps.act.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class ImportacionException extends CoreBaseException
{
    public ImportacionException()
    {
        super("Error al importar les dades");
    }

    public ImportacionException(String message)
    {
        super(message);
    }
}
