package es.uji.apps.act.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class TraspasarDiligenciaException extends CoreBaseException
{
    public TraspasarDiligenciaException()
    {
        super("Error traspasant les diligències");
    }

    public TraspasarDiligenciaException(String message)
    {
        super(message);
    }
}
