package es.uji.apps.act.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class NoAutorizadoException extends CoreBaseException
{
    public NoAutorizadoException()
    {
        super("No autoritzat");
    }

    public NoAutorizadoException(String message)
    {
        super(message);
    }
}
