package es.uji.apps.act.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class EnviarNotificacionException extends CoreBaseException
{
    public EnviarNotificacionException()
    {
        super("No s'ha pogut enviar la notificació");
    }

    public EnviarNotificacionException(String message)
    {
        super(message);
    }
}
