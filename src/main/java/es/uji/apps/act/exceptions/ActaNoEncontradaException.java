package es.uji.apps.act.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class ActaNoEncontradaException extends CoreBaseException
{
    public ActaNoEncontradaException()
    {
        super("No es pot trobar l'acta");
    }

    public ActaNoEncontradaException(String message)
    {
        super(message);
    }
}
