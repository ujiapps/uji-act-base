package es.uji.apps.act.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class NotaNoEditableException extends CoreBaseException
{
    public NotaNoEditableException()
    {
        super("No es pot editar la nota");
    }

    public NotaNoEditableException(String message)
    {
        super(message);
    }
}
