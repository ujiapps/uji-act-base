package es.uji.apps.act.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class FormatoNoValidoException extends CoreBaseException
{
    public FormatoNoValidoException()
    {
        super("El format introduït no és correcte");
    }

    public FormatoNoValidoException(String message)
    {
        super(message);
    }
}
