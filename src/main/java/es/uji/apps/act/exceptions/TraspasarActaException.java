package es.uji.apps.act.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class TraspasarActaException extends CoreBaseException
{
    public TraspasarActaException()
    {
        super("Error traspasant l'acta");
    }

    public TraspasarActaException(String message)
    {
        super(message);
    }
}
