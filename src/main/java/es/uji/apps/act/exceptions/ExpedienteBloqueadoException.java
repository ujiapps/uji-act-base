package es.uji.apps.act.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class ExpedienteBloqueadoException extends CoreBaseException
{
    public ExpedienteBloqueadoException()
    {
        super("El expedient de l'alumne/a està bloquejat.");
    }

    public ExpedienteBloqueadoException(String message)
    {
        super(message);
    }
}
