package es.uji.apps.act.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class ActaNoPermitidaException extends CoreBaseException
{
    public ActaNoPermitidaException()
    {
        super("No es pot borrar l'acta");
    }

    public ActaNoPermitidaException(String message)
    {
        super(message);
    }
}
