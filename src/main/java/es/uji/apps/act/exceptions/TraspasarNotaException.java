package es.uji.apps.act.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class TraspasarNotaException extends CoreBaseException
{
    public TraspasarNotaException()
    {
        super("Error traspasant la nota");
    }

    public TraspasarNotaException(String message)
    {
        super(message);
    }
}
