package es.uji.apps.act.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class ActaSinEstudiantesException extends CoreBaseException
{
    public ActaSinEstudiantesException()
    {
        super("L'acta no té cap estudiant.");
    }

    public ActaSinEstudiantesException(String message)
    {
        super(message);
    }
}
