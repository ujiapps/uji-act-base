package es.uji.apps.act.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class InconsistenciaDeDatosException extends CoreBaseException
{
    public InconsistenciaDeDatosException()
    {
        super("Error d'inconsistència de dades");
    }

    public InconsistenciaDeDatosException(String message)
    {
        super(message);
    }
}
