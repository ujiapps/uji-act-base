package es.uji.apps.act.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class PasarNotasExpedienteException extends CoreBaseException
{
    public PasarNotasExpedienteException()
    {
        super("Error traspasant la nota");
    }

    public PasarNotasExpedienteException(String message)
    {
        super(message);
    }
}
