package es.uji.apps.act.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class DesgloseNoPermitidoException extends CoreBaseException
{
    public DesgloseNoPermitidoException()
    {
        super("No es pot activar el desglossament de notes");
    }

    public DesgloseNoPermitidoException(String message)
    {
        super(message);
    }
}
