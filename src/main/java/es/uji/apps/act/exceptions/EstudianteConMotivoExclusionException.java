package es.uji.apps.act.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class EstudianteConMotivoExclusionException extends CoreBaseException
{
    public EstudianteConMotivoExclusionException()
    {
        super("El alumne té un motiu d'exclusió, pero la cual cosa no pot modificar-se");
    }

    public EstudianteConMotivoExclusionException(String message)
    {
        super(message);
    }
}
