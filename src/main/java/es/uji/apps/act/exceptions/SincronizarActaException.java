package es.uji.apps.act.exceptions;

import es.uji.commons.rest.exceptions.CoreBaseException;

public class SincronizarActaException extends CoreBaseException
{
    public SincronizarActaException()
    {
        super("Error sincronitzant l'acta");
    }

    public SincronizarActaException(String message)
    {
        super(message);
    }
}
