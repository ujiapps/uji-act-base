package es.uji.apps.act.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.act.models.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ConvocatoriaDAO extends BaseDAODatabaseImpl
{
    public List<Convocatoria> getConvocatoriasOficiales()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QConvocatoria qConvocatoria = QConvocatoria.convocatoria;
        return query.from(qConvocatoria).where(qConvocatoria.id.ne(Convocatoria.EXAMEN_PARCIAL))
                .list(qConvocatoria);
    }
}
