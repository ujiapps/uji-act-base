package es.uji.apps.act.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.act.models.QActa;
import es.uji.apps.act.models.views.ActaConvocatoria;
import es.uji.apps.act.models.views.QActaConvocatoria;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ActaConvocatoriaDAO extends BaseDAODatabaseImpl
{
    public List<ActaConvocatoria> getActaConvocatoriasByCursoAndCodigo(Long cursoId, String codigo)
    {
        QActaConvocatoria qActaConvocatoria = QActaConvocatoria.actaConvocatoria;
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qActaConvocatoria)
                .where(qActaConvocatoria.cursoAcademicoId.eq(cursoId)
                        .and(qActaConvocatoria.asiCodigos.eq(codigo)))
                .orderBy(qActaConvocatoria.orden.asc()).distinct().list(qActaConvocatoria);
    }

    public ActaConvocatoria getActaConvocatoriaByActaId(Long actaId)
    {
        QActaConvocatoria qActaConvocatoria = QActaConvocatoria.actaConvocatoria;
        QActa qActa = QActa.acta;
        JPAQuery query = new JPAQuery(entityManager);
        List<ActaConvocatoria> result = query.from(qActaConvocatoria, qActa)
                .where(qActa.id.eq(actaId)
                        .and(qActaConvocatoria.cursoAcademicoId.eq(qActa.curso.id))
                        .and(qActaConvocatoria.asiCodigos.eq(qActa.codigo))
                        .and(qActaConvocatoria.convocatoriaId.eq(qActa.convocatoria.id)))
                .list(qActaConvocatoria);

        return (result.isEmpty()) ? null : result.get(0);
    }
}
