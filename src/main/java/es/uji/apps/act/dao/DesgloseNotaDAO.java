package es.uji.apps.act.dao;

import java.util.List;

import es.uji.apps.act.models.*;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class DesgloseNotaDAO extends BaseDAODatabaseImpl
{
    public List<DesgloseNota> getDesgloseNotasByActaId(Long actaId)
    {
        QDesgloseNota qDesgloseNota = QDesgloseNota.desgloseNota;
        QDesglosePregunta qDesglosePregunta = QDesglosePregunta.desglosePregunta;
        QDesgloseGrupo qDesgloseGrupo = QDesgloseGrupo.desgloseGrupo;
        QActaEstudiante qActaEstudiante = QActaEstudiante.actaEstudiante;
        QPersona qPersona = QPersona.persona;
        QActaAsignatura qActaAsignatura = QActaAsignatura.actaAsignatura;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qDesgloseNota).join(qDesgloseNota.desglosePregunta, qDesglosePregunta).fetch()
                .join(qDesglosePregunta.desgloseGrupo, qDesgloseGrupo).fetch()
                .join(qDesgloseNota.actaEstudiante, qActaEstudiante).fetch()
                .join(qActaEstudiante.persona, qPersona).fetch()
                .join(qActaEstudiante.actaAsignatura, qActaAsignatura).fetch()
                .where(qActaAsignatura.acta.id.eq(actaId));

        return query.list(qDesgloseNota);
    }

    public DesgloseNota getDesgloseNotaById(Long desgloseNotaId)
            throws RegistroNoEncontradoException
    {
        QDesgloseNota qDesgloseNota = QDesgloseNota.desgloseNota;
        QDesglosePregunta qDesglosePregunta = QDesglosePregunta.desglosePregunta;
        QDesgloseGrupo qDesgloseGrupo = QDesgloseGrupo.desgloseGrupo;
        QActaEstudiante qActaEstudiante = QActaEstudiante.actaEstudiante;
        QPersona qPersona = QPersona.persona;
        QActaAsignatura qActaAsignatura = QActaAsignatura.actaAsignatura;
        QActa qActa = QActa.acta;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qDesgloseNota).join(qDesgloseNota.desglosePregunta, qDesglosePregunta).fetch()
                .join(qDesglosePregunta.desgloseGrupo, qDesgloseGrupo).fetch()
                .join(qDesgloseNota.actaEstudiante, qActaEstudiante).fetch()
                .join(qActaEstudiante.persona, qPersona).fetch()
                .join(qActaEstudiante.actaAsignatura, qActaAsignatura).fetch()
                .join(qActaAsignatura.acta, qActa).fetch()
                .where(qDesgloseNota.id.eq(desgloseNotaId));

        List<DesgloseNota> notas = query.list(qDesgloseNota);
        if (notas.isEmpty())
        {
            throw new RegistroNoEncontradoException();
        }

        return notas.get(0);
    }
}
