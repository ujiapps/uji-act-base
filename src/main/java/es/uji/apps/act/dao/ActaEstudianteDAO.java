package es.uji.apps.act.dao;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.act.models.*;
import es.uji.apps.act.models.views.ActaEstudianteOrigen;
import es.uji.apps.act.models.views.QActaEstudianteOrigen;
import es.uji.commons.db.BaseDAODatabaseImpl;

import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ActaEstudianteDAO extends BaseDAODatabaseImpl
{
    public List<ActaEstudiante> getActaEstudiantesByActaId(Long actaId)
    {
        QActaEstudiante qActaEstudiante = QActaEstudiante.actaEstudiante;
        QActaAsignatura qActaAsignatura = QActaAsignatura.actaAsignatura;
        QActa qActa = QActa.acta;
        QDesgloseGrupo qDesgloseGrupo = QDesgloseGrupo.desgloseGrupo;
        QDesglosePregunta qDesglosePregunta = QDesglosePregunta.desglosePregunta;
        QDesgloseNota qDesgloseNota = QDesgloseNota.desgloseNota;
        QActaRevision qActaRevision = QActaRevision.actaRevision;
        QPersona qPersona = QPersona.persona;
        QCurso qCurso = QCurso.curso;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qActaEstudiante).join(qActaEstudiante.persona, qPersona).fetch()
                .join(qActaEstudiante.actaAsignatura, qActaAsignatura).fetch()
                .join(qActaAsignatura.acta, qActa).fetch()
                .leftJoin(qActa.actasRevisiones, qActaRevision).fetch()
                .leftJoin(qActa.desglosesGrupos, qDesgloseGrupo).fetch()
                .leftJoin(qActa.curso, qCurso).fetch()
                .leftJoin(qDesgloseGrupo.desglosePreguntas, qDesglosePregunta).fetch()
                .leftJoin(qActaEstudiante.desgloseNotas, qDesgloseNota).fetch()
                .where(qActaEstudiante.actaAsignatura.acta.id.eq(actaId));

        return query.distinct().list(qActaEstudiante);
    }

    public List<ActaEstudiante> getActaEstudiantesByActaRevision(ActaRevision actaRevision)
    {
        QActaEstudiante qActaEstudiante = QActaEstudiante.actaEstudiante;
        QActaAsignatura qActaAsignatura = QActaAsignatura.actaAsignatura;
        QActa qActa = QActa.acta;
        QDesgloseGrupo qDesgloseGrupo = QDesgloseGrupo.desgloseGrupo;
        QDesglosePregunta qDesglosePregunta = QDesglosePregunta.desglosePregunta;
        QDesgloseNota qDesgloseNota = QDesgloseNota.desgloseNota;
        QActaRevision qActaRevision = QActaRevision.actaRevision;
        QPersona qPersona = QPersona.persona;
        QCurso qCurso = QCurso.curso;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qActaEstudiante).join(qActaEstudiante.persona, qPersona).fetch()
                .join(qActaEstudiante.actaAsignatura, qActaAsignatura).fetch()
                .join(qActaAsignatura.acta, qActa).fetch()
                .leftJoin(qActa.actasRevisiones, qActaRevision).fetch()
                .leftJoin(qActa.desglosesGrupos, qDesgloseGrupo).fetch()
                .leftJoin(qActa.curso, qCurso).fetch()
                .leftJoin(qDesgloseGrupo.desglosePreguntas, qDesglosePregunta).fetch()
                .leftJoin(qActaEstudiante.desgloseNotas, qDesgloseNota).fetch()
                .where(qActaEstudiante.actaAsignatura.acta.id.eq(actaRevision.getActa().getId()));

        if (!actaRevision.isConjunta())
        {
            query.where(qActaEstudiante.grupoId.in(actaRevision.getGruposAsList()).and(qActaAsignatura.asignatura.id.in(actaRevision.getAsignaturasAsList())));
        }

        return query.distinct().list(qActaEstudiante);
    }


    public ActaEstudiante getActaEstudianteById(Long actaEstudianteId)
    {
        QActaEstudiante qActaEstudiante = QActaEstudiante.actaEstudiante;
        QActaAsignatura qActaAsignatura = QActaAsignatura.actaAsignatura;
        QActa qActa = QActa.acta;
        QCurso qCurso = QCurso.curso;
        QConvocatoria qConvocatoria = QConvocatoria.convocatoria;
        QDesgloseGrupo qDesgloseGrupo = QDesgloseGrupo.desgloseGrupo;
        QDesglosePregunta qDesglosePregunta = QDesglosePregunta.desglosePregunta;
        QDesgloseNota qDesgloseNota = QDesgloseNota.desgloseNota;
        QFechaConvocatoria qFechaConvocatoria = QFechaConvocatoria.fechaConvocatoria;

        JPAQuery query = new JPAQuery(entityManager);
        List<ActaEstudiante> results = query.from(qActaEstudiante)
                .join(qActaEstudiante.actaAsignatura, qActaAsignatura).fetch()
                .join(qActaAsignatura.acta, qActa).fetch()
                .join(qActa.curso, qCurso).fetch()
                .join(qActa.convocatoria, qConvocatoria).fetch()
                .leftJoin(qActa.desglosesGrupos, qDesgloseGrupo).fetch()
                .leftJoin(qDesgloseGrupo.desglosePreguntas, qDesglosePregunta).fetch()
                .leftJoin(qActaEstudiante.desgloseNotas, qDesgloseNota).fetch()
                .leftJoin(qConvocatoria.fechasConvocatoria, qFechaConvocatoria).fetch()
                .where(qActaEstudiante.id.eq(actaEstudianteId)).distinct().list(qActaEstudiante);

        return (results.isEmpty()) ? null : results.get(0);
    }
}
