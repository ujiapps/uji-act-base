package es.uji.apps.act.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.act.models.PermisosExtra;
import es.uji.apps.act.models.QActa;
import es.uji.apps.act.models.QPermisosExtra;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class PermisosExtraDAO extends BaseDAODatabaseImpl {
    public List<PermisosExtra> getPermisosExtra() {
        QPermisosExtra qPermisosExtra = QPermisosExtra.permisosExtra;
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qPermisosExtra).list(qPermisosExtra);
    }

    public List<PermisosExtra> getPermisosExtraByConnectedUserId(Long connectedUserId) {
        QPermisosExtra qPermisosExtra = QPermisosExtra.permisosExtra;
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qPermisosExtra).where(qPermisosExtra.persona.id.eq(connectedUserId).and(qPermisosExtra.fechaFin.isNull().or(qPermisosExtra.fechaFin.after(new Date())))).list(qPermisosExtra);
    }

    public Boolean isReponsableActa(Long actaId, Long personaId) {
        QPermisosExtra qPermisosExtra = QPermisosExtra.permisosExtra;
        QActa qActa = QActa.acta;

        JPAQuery query = new JPAQuery(entityManager);
        List<PermisosExtra> listaPermisos = query.from(qPermisosExtra).where(qPermisosExtra.persona.id.eq(personaId).and(qPermisosExtra.fechaFin.isNull().or(qPermisosExtra.fechaFin.after(new Date())))).list(qPermisosExtra);

        List<Long> listaActasIds = new ArrayList<>();
        for (PermisosExtra permiso : listaPermisos) {
            JPAQuery query2 = new JPAQuery(entityManager);
            List<Long> listaActas = query2.from(qActa).where(qActa.curso.id.eq(permiso.getCursoAcademicoId()).and(qActa.codigo.eq(permiso.getAsignaturaId()))).list(qActa).stream().map(a -> a.getId()).collect(Collectors.toList());
            listaActasIds.addAll(listaActas);
        }

        return listaActasIds.contains(actaId);
    }
}
