package es.uji.apps.act.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import es.uji.apps.act.models.*;
import es.uji.apps.act.models.views.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ActaDAO extends BaseDAODatabaseImpl
{

    public List<Acta> getActasByCurso(Long cursoId)
    {
        QActa qActa = QActa.acta;
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qActa).where(qActa.curso.id.eq(cursoId));

        return query.list(qActa);
    }

    public List<Acta> getActasByCursoAndQuery(Long cursoId, String q)
    {
        QActa qActa = QActa.acta;
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qActa).where(qActa.curso.id.eq(cursoId).and(qActa.descripcion.like("%" + q + "%")));

        return query.list(qActa);
    }

    public Acta getActasByCursoAndCodigo(Long cursoId, String codigo)
    {
        QActa qActa = QActa.acta;
        QActaAsignatura qActaAsignatura = QActaAsignatura.actaAsignatura;
        QConvocatoria qConvocatoria = QConvocatoria.convocatoria;
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qActa).join(qActa.convocatoria, qConvocatoria).fetch()
                .join(qActa.actasAsignaturas, qActaAsignatura)
                .where(qActa.curso.id.eq(cursoId).and(qActaAsignatura.asignatura.id.eq(codigo)));

        List<Acta> listaActas = query.list(qActa);

        if (listaActas.size() == 0)
        {
            return null;
        }

        return listaActas.get(0);
    }

    public List<Acta> getActasTraspasadasByCurso(Long cursoId)
    {
        QActa qActa = QActa.acta;
        QConvocatoria qConvocatoria = QConvocatoria.convocatoria;
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qActa).join(qActa.convocatoria, qConvocatoria).fetch()
                .where(qActa.curso.id.eq(cursoId).and(qActa.fechaTraspaso.isNotNull()));

        return query.list(qActa);
    }

    public List<NotaCalculada> getNotasCalculadasByActa(Long actaId)
    {
        QNotaCalculada qNotaCalculada = QNotaCalculada.notaCalculada1;
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qNotaCalculada).where(qNotaCalculada.actaId.eq(actaId));

        return query.list(qNotaCalculada);
    }

    public ActaVirtual getActaFuturaByCursoAndCodigo(Long curso, String codigo)
    {
        QActaVirtual qActaFutura = QActaVirtual.actaVirtual;
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qActaFutura)
                .where(qActaFutura.curso.eq(curso).and(qActaFutura.codigo.eq(codigo)));

        List<ActaVirtual> actasVirtuales = query.list(qActaFutura);
        return (actasVirtuales.isEmpty()) ? null : actasVirtuales.get(0);
    }

    public List<ActaVirtualAsignatura> getActaFuturaAsignaturasByCursoAndCodigo(Long curso,
                                                                                String codigo)
    {
        QActaVirtualAsignatura qActaFuturaAsignatura = QActaVirtualAsignatura.actaVirtualAsignatura;
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qActaFuturaAsignatura).where(
                qActaFuturaAsignatura.curso.eq(curso).and(qActaFuturaAsignatura.codigo.eq(codigo)));

        return query.list(qActaFuturaAsignatura);
    }

    public Acta getActaById(Long actaId)
    {
        QActa qActa = QActa.acta;
        QConvocatoria qConvocatoria = QConvocatoria.convocatoria;
        QFechaConvocatoria qFechaConvocatoria = QFechaConvocatoria.fechaConvocatoria;
        QActaAsignatura qActaAsignaturas = QActaAsignatura.actaAsignatura;
        QActaRevision qActaRevision = QActaRevision.actaRevision;

        JPAQuery query = new JPAQuery(entityManager);
        List<Acta> actas = query.from(qActa).join(qActa.convocatoria, qConvocatoria).fetch()
                .join(qActa.actasAsignaturas, qActaAsignaturas).fetch()
                .leftJoin(qConvocatoria.fechasConvocatoria, qFechaConvocatoria).fetch()
                .leftJoin(qActa.actasRevisiones, qActaRevision).fetch()
                .where(qActa.id.eq(actaId)
                        .and(qActa.convocatoria.id.eq(Convocatoria.EXAMEN_PARCIAL)
                                .or(qActa.curso.eq(qFechaConvocatoria.curso).and(
                                        qActa.tipoEstudio.eq(qFechaConvocatoria.tipoEstudio)))))
                .distinct().list(qActa);

        return (actas.isEmpty()) ? null : actas.get(0);
    }

    public Acta getActaByIdConNotasCalculadas(Long actaId)
    {
        QActa qActa = QActa.acta;
        QConvocatoria qConvocatoria = QConvocatoria.convocatoria;
        QFechaConvocatoria qFechaConvocatoria = QFechaConvocatoria.fechaConvocatoria;
        QActaAsignatura qActaAsignaturas = QActaAsignatura.actaAsignatura;
        QActaRevision qActaRevision = QActaRevision.actaRevision;
        QPersona qPersona = QPersona.persona;
        QActaEstudiante qActaEstudiante = QActaEstudiante.actaEstudiante;
        QDesgloseGrupo qDesgloseGrupo = QDesgloseGrupo.desgloseGrupo;
        QDesglosePregunta qDesglosePregunta = QDesglosePregunta.desglosePregunta;
        QDesgloseNota qDesgloseNota = QDesgloseNota.desgloseNota;
        QConvocatoriaTipoEstudio qConvocatoriaTipoEstudio = QConvocatoriaTipoEstudio.convocatoriaTipoEstudio;

        JPAQuery query = new JPAQuery(entityManager);
        List<Acta> actas = query.from(qActa).join(qActa.convocatoria, qConvocatoria).fetch()
                .join(qActa.actasAsignaturas, qActaAsignaturas).fetch()
                .leftJoin(qActa.desglosesGrupos, qDesgloseGrupo).fetch()
                .leftJoin(qDesgloseGrupo.desglosePreguntas, qDesglosePregunta).fetch()
                .leftJoin(qActaAsignaturas.actasEstudiantes, qActaEstudiante).fetch()
                .leftJoin(qConvocatoria.fechasConvocatoria, qFechaConvocatoria).fetch()
                .leftJoin(qConvocatoria.convocatoriaTipoEstudios, qConvocatoriaTipoEstudio).fetch()
                .leftJoin(qActa.actasRevisiones, qActaRevision).fetch()
                .leftJoin(qActaEstudiante.persona, qPersona).fetch()
                .leftJoin(qActaEstudiante.desgloseNotas, qDesgloseNota).fetch()
                .where(qActa.id.eq(actaId)
                        .and(qActa.convocatoria.id.eq(Convocatoria.EXAMEN_PARCIAL)
                                .or(qActa.curso.eq(qFechaConvocatoria.curso)
                                        .and(qActa.tipoEstudio.eq(qFechaConvocatoria.tipoEstudio))))
                        .and(qDesgloseNota.desglosePregunta.id.eq(qDesglosePregunta.id)
                                .or(qDesgloseNota.desglosePregunta.id.isNull())))
                .distinct().list(qActa);

        return (actas.isEmpty()) ? null : actas.get(0);
    }

    public Acta getActaConEstudiantes(Long actaId)
    {
        QActa qActa = QActa.acta;
        QActaAsignatura qActaAsignatura = QActaAsignatura.actaAsignatura;
        QActaEstudiante qActaEstudiante = QActaEstudiante.actaEstudiante;
        QConvocatoria qConvocatoria = QConvocatoria.convocatoria;
        QPersona qPersona = QPersona.persona;
        QDesgloseGrupo qDesgloseGrupo = QDesgloseGrupo.desgloseGrupo;
        QDesglosePregunta qDesglosePregunta = QDesglosePregunta.desglosePregunta;
        QDesgloseNota qDesgloseNota = QDesgloseNota.desgloseNota;

        JPAQuery query = new JPAQuery(entityManager);
        List<Acta> actas = query.from(qActa).join(qActa.convocatoria, qConvocatoria)
                .leftJoin(qActa.actasAsignaturas, qActaAsignatura).fetch()
                .leftJoin(qActaAsignatura.actasEstudiantes, qActaEstudiante).fetch()
                .leftJoin(qActaEstudiante.persona, qPersona).fetch().where(qActa.id.eq(actaId))
                .leftJoin(qActa.desglosesGrupos, qDesgloseGrupo).fetch()
                .leftJoin(qDesgloseGrupo.desglosePreguntas, qDesglosePregunta).fetch()
                .leftJoin(qActaEstudiante.desgloseNotas, qDesgloseNota).fetch().distinct().fetch()
                .distinct().list(qActa);

        return (actas.isEmpty()) ? null : actas.get(0);
    }

    public Acta getActaConEstudiantes(Long actaId, String asignatura, String grupo)
    {
        QActa qActa = QActa.acta;
        QActaAsignatura qActaAsignatura = QActaAsignatura.actaAsignatura;
        QActaRevision qActaRevision = QActaRevision.actaRevision;
        QActaEstudiante qActaEstudiante = QActaEstudiante.actaEstudiante;
        QConvocatoria qConvocatoria = QConvocatoria.convocatoria;
        QPersona qPersona = QPersona.persona;
        QDesgloseGrupo qDesgloseGrupo = QDesgloseGrupo.desgloseGrupo;
        QDesglosePregunta qDesglosePregunta = QDesglosePregunta.desglosePregunta;
        QDesgloseNota qDesgloseNota = QDesgloseNota.desgloseNota;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qActa).join(qActa.convocatoria, qConvocatoria)
                .leftJoin(qActa.actasAsignaturas, qActaAsignatura).fetch()
                .leftJoin(qActa.actasRevisiones, qActaRevision).fetch()
                .leftJoin(qActaAsignatura.actasEstudiantes, qActaEstudiante).fetch()
                .leftJoin(qActaEstudiante.persona, qPersona).fetch()
                .leftJoin(qActa.desglosesGrupos, qDesgloseGrupo).fetch()
                .leftJoin(qDesgloseGrupo.desglosePreguntas, qDesglosePregunta).fetch()
                .leftJoin(qActaEstudiante.desgloseNotas, qDesgloseNota).fetch().distinct()
                .where(qActa.id.eq(actaId));

        if (asignatura != null && !asignatura.isEmpty())
        {
            query.where(qActaAsignatura.asignatura.id.eq(asignatura));
        }

        if (grupo != null && !grupo.isEmpty())
        {
            query.where(qActaEstudiante.grupoId.eq(grupo));
        }

        List<Acta> actas = query.distinct().list(qActa);
        return (actas.isEmpty()) ? null : actas.get(0);
    }

    public List<Acta> getActasByActaEstudianteIds(List<Long> actaEstudianteIds)
    {
        QActa qActa = QActa.acta;
        QConvocatoria qConvocatoria = QConvocatoria.convocatoria;
        QFechaConvocatoria qFechaConvocatoria = QFechaConvocatoria.fechaConvocatoria;
        QActaAsignatura qActaAsignatura = QActaAsignatura.actaAsignatura;
        QActaEstudiante qActaEstudiante = QActaEstudiante.actaEstudiante;
        QConvocatoriaTipoEstudio qConvocatoriaTipoEstudio = QConvocatoriaTipoEstudio.convocatoriaTipoEstudio;

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qActa).join(qActa.convocatoria, qConvocatoria).fetch()
                .join(qActa.actasAsignaturas, qActaAsignatura).fetch()
                .join(qActaAsignatura.actasEstudiantes, qActaEstudiante).fetch()
                .leftJoin(qConvocatoria.fechasConvocatoria, qFechaConvocatoria).fetch()
                .leftJoin(qConvocatoria.convocatoriaTipoEstudios, qConvocatoriaTipoEstudio).fetch()
                .where(qActaEstudiante.id.in(actaEstudianteIds)
                        .and(qActa.curso.eq(qFechaConvocatoria.curso)
                                .and(qActa.tipoEstudio.eq(qFechaConvocatoria.tipoEstudio))))
                .distinct().list(qActa);
    }

    public void setPermiteDetalle(Long actaId, Boolean permiteDetalle)
    {
        QActa qActa = QActa.acta;
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qActa);
        updateClause.where(qActa.id.eq(actaId)).set(qActa.desgloseActivo, permiteDetalle).execute();
    }

    public List<Acta> getActasParcialesByCursoAndCodigo(Long cursoId, String codigo)
    {
        QActa qActa = QActa.acta;
        QConvocatoria qConvocatoria = QConvocatoria.convocatoria;
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qActa).join(qActa.convocatoria, qConvocatoria).fetch()
                .where(qActa.curso.id.eq(cursoId).and(qActa.codigo.eq(codigo))
                        .and(qActa.convocatoria.id.eq(Convocatoria.EXAMEN_PARCIAL)));

        return query.list(qActa);
    }

    public List<Acta> getListActasByCursoAndConvocatoria(Long cursoId, Long convocatoriaId)
    {
        QActa qActa = QActa.acta;
        QConvocatoria qConvocatoria = QConvocatoria.convocatoria;

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qActa).join(qActa.convocatoria, qConvocatoria).fetch()
                .where(qActa.curso.id.eq(cursoId).and(qActa.convocatoria.id.eq(convocatoriaId)))
                .distinct().list(qActa);
    }

    public List<EstudianteConvocatoria> getEstudianteConvocatoria(Long actaId, String asignatura,
                                                                  String grupo)
    {
        QEstudianteConvocatoria qEstudianteConvocatoria = QEstudianteConvocatoria.estudianteConvocatoria;
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qEstudianteConvocatoria).where(qEstudianteConvocatoria.actaId.eq(actaId));

        if (asignatura != null && !asignatura.isEmpty())
        {
            query.where(qEstudianteConvocatoria.asignaturaId.eq(asignatura));
        }

        if (grupo != null && !grupo.isEmpty())
        {
            query.where(qEstudianteConvocatoria.grupoId.eq(grupo));
        }

        return query.distinct().list(qEstudianteConvocatoria);

    }

    public Acta getActaByCursoAndCodigoAndConvocatoria(Long cursoId, String codigo,
                                                       Long convocatoriaId)
    {
        QActa qActa = QActa.acta;
        QActaAsignatura qActaAsignatura = QActaAsignatura.actaAsignatura;
        QConvocatoria qConvocatoria = QConvocatoria.convocatoria;
        QFechaConvocatoria qFechaConvocatoria = QFechaConvocatoria.fechaConvocatoria;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qActa).join(qActa.convocatoria, qConvocatoria).fetch()
                .join(qActa.actasAsignaturas, qActaAsignatura)
                .join(qConvocatoria.fechasConvocatoria, qFechaConvocatoria).fetch()
                .where(qActa.curso.id.eq(cursoId).and(qActa.descripcion.eq(codigo))
                        .and(qActa.convocatoria.id.eq(convocatoriaId)));

        List<Acta> listaActas = query.list(qActa);

        if (listaActas.size() == 0)
        {
            return null;
        }

        return listaActas.get(0);
    }

    public List<CursoMoodle> getCursosMoodleByCursoAcademicoYAsignatura(Long cursoId, List<String> listaAsignaturas, Long connectedUserId)
    {
        QCursoMoodle qCursoMoodle = QCursoMoodle.cursoMoodle;
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qCursoMoodle).where(qCursoMoodle.curso.id.eq(cursoId).and(qCursoMoodle.asiId.in(listaAsignaturas)).and(qCursoMoodle.perId.eq(connectedUserId)));

        return query.list(qCursoMoodle);

    }
}
