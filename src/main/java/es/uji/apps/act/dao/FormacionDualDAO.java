package es.uji.apps.act.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.act.models.*;
import es.uji.apps.act.models.views.AsignaturaOfertada;
import es.uji.apps.act.models.views.QAsignaturaOfertada;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class FormacionDualDAO extends BaseDAODatabaseImpl {
    public List<FormacionDual> getFormacionDual(Long connectedUserId) {
        QFormacionDual qFormacionDual = QFormacionDual.formacionDual;
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qFormacionDual).list(qFormacionDual);
    }

    public List<FormacionDual> getFormacionDualByConnectedUserId(Long connectedUserId) {
        QFormacionDual qFormacionDual = QFormacionDual.formacionDual;
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qFormacionDual).list(qFormacionDual);
    }

    public List<AsignaturaOfertada> getAsignaturasOfertadas(List<String> listaFormacionDual, Long connectedUserId) {
        QAsignaturaOfertada qAsignaturaOfertada = QAsignaturaOfertada.asignaturaOfertada;
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qAsignaturaOfertada).where(qAsignaturaOfertada.asignaturaId.in(listaFormacionDual)).list(qAsignaturaOfertada);
    }
}
