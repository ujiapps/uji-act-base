package es.uji.apps.act.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.act.models.views.AsignaturaOfertada;
import es.uji.apps.act.models.views.QAsignaturaOfertada;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.db.Utils;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.rest.json.lookup.LookupItem;
import org.springframework.stereotype.Repository;

import javax.ws.rs.QueryParam;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Repository
public class AsignaturaOfertadaDAO extends BaseDAODatabaseImpl implements LookupDAO<LookupItem>
{
    public List<AsignaturaOfertada> getAsignaturasOfertadasByCurso(Long curso, Long estudioId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QAsignaturaOfertada qAsignaturaOfertada = QAsignaturaOfertada.asignaturaOfertada;

        query.from(qAsignaturaOfertada).where(qAsignaturaOfertada.curso.eq(curso));

        if (estudioId != null) {
            query.where(qAsignaturaOfertada.estudioId.eq(estudioId));
        }

        return query.distinct().list(qAsignaturaOfertada);
    }

    @Override
    public List<LookupItem> search(String s) {
        JPAQuery query = new JPAQuery(entityManager);
        QAsignaturaOfertada qAsignaturaOfertada = QAsignaturaOfertada.asignaturaOfertada;

        query.from(qAsignaturaOfertada);

        if(!s.equals("")) {
            query.where(qAsignaturaOfertada.asignaturaId.containsIgnoreCase(s.toUpperCase()));
        }

        return query.list(qAsignaturaOfertada)
                .stream().map(asignaturaOfertada -> {
                    LookupItem item = new LookupItem();
                    item.setId(asignaturaOfertada.getAsignaturaId());
                    item.setNombre(asignaturaOfertada.getNombre());

                    return item;
                }).collect(Collectors.toList());
    }
}