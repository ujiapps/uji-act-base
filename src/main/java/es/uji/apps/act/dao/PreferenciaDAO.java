package es.uji.apps.act.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.act.models.Preferencia;
import es.uji.apps.act.models.QPreferencia;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PreferenciaDAO extends BaseDAODatabaseImpl
{

    public Preferencia getPreferencia(Long connectedUserId)
    {
        QPreferencia qPreferencia = QPreferencia.preferencia;
        JPAQuery query = new JPAQuery(entityManager);
        List<Preferencia> preferencia = query.from(qPreferencia)
                .where(qPreferencia.perId.eq(connectedUserId)).list(qPreferencia);

        return preferencia.size() == 1 ? preferencia.get(0) : null;
    }
}
