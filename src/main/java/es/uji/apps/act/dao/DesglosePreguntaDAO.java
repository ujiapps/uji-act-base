package es.uji.apps.act.dao;

import java.util.List;

import es.uji.apps.act.models.*;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class DesglosePreguntaDAO extends BaseDAODatabaseImpl
{
    public List<DesglosePregunta> getDesglosePreguntasByActaId(Long actaId)
    {
        QDesglosePregunta qDesglosePregunta = QDesglosePregunta.desglosePregunta;
        QDesgloseGrupo qDesgloseGrupo = QDesgloseGrupo.desgloseGrupo;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qDesglosePregunta).join(qDesglosePregunta.desgloseGrupo, qDesgloseGrupo)
                .where(qDesglosePregunta.desgloseGrupo.acta.id.eq(actaId));

        return query.list(qDesglosePregunta);
    }

    public List<DesglosePregunta> getDesglosePreguntasByParcialId(Long parcialId)
    {
        QDesglosePregunta qDesglosePregunta = QDesglosePregunta.desglosePregunta;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qDesglosePregunta).where(qDesglosePregunta.parcialActa.id.eq(parcialId));

        return query.list(qDesglosePregunta);
    }

    public DesglosePregunta getDesglosePreguntaByDesgloseNotaId(Long desgloseNotaId)
    {
        QDesglosePregunta qDesglosePregunta = QDesglosePregunta.desglosePregunta;
        QDesgloseNota qDesgloseNota = QDesgloseNota.desgloseNota;

        JPAQuery query = new JPAQuery(entityManager);
        List<DesglosePregunta> result = query.from(qDesglosePregunta)
                .join(qDesglosePregunta.desgloseNotas, qDesgloseNota)
                .where(qDesgloseNota.id.eq(desgloseNotaId)).distinct().list(qDesglosePregunta);

        return (result.isEmpty()) ? null : result.get(0);
    }

    public DesglosePregunta getPreguntaConGrupo(Long actaPreguntaId)
    {
        QDesglosePregunta qDesglosePregunta = QDesglosePregunta.desglosePregunta;
        QDesgloseGrupo qDesgloseGrupo = QDesgloseGrupo.desgloseGrupo;

        JPAQuery query = new JPAQuery(entityManager);
        List<DesglosePregunta> result = query.from(qDesglosePregunta)
                .join(qDesglosePregunta.desgloseGrupo, qDesgloseGrupo)
                .where(qDesglosePregunta.id.eq(actaPreguntaId)).list(qDesglosePregunta);

        return (result.isEmpty()) ? null : result.get(0);
    }
}
