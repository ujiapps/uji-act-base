package es.uji.apps.act.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.act.models.*;
import es.uji.apps.act.models.enums.EstadoDiligenciaFueraPlazo;
import es.uji.apps.act.models.views.ActaDiligenciaFueraPlazoModeracion;
import es.uji.apps.act.models.views.QActaDiligenciaFueraPlazoModeracion;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class ActaDiligenciaFueraPlazoDAO extends BaseDAODatabaseImpl {
    public ActaDiligenciaFueraPlazo getActaDiligenciaFueraPlazoById(Long actaDiligenciaFueraPlazoId) {
        JPAQuery query = new JPAQuery(entityManager);
        QActaDiligenciaFueraPlazo qActaDiligenciaFueraPlazo = QActaDiligenciaFueraPlazo.actaDiligenciaFueraPlazo;
        QActa qActa = QActa.acta;
        QActaEstudiante qActaEstudiante = QActaEstudiante.actaEstudiante;
        QPersona qEstudiante = QPersona.persona;
        QPersona qProfesor = QPersona.persona;
        QActaAsignatura qActaAsignatura = QActaAsignatura.actaAsignatura;
        QAsignatura qAsignatura = QAsignatura.asignatura;

        List<ActaDiligenciaFueraPlazo> results = query.from(qActaDiligenciaFueraPlazo)
                .join(qActaDiligenciaFueraPlazo.acta, qActa).fetch()
                .join(qActa.actasAsignaturas, qActaAsignatura).fetch()
                .join(qActaAsignatura.asignatura, qAsignatura).fetch()
                .join(qActaDiligenciaFueraPlazo.actaEstudiante, qActaEstudiante).fetch()
                .join(qActaEstudiante.persona, qEstudiante).fetch()
                .join(qActaDiligenciaFueraPlazo.persona, qProfesor).fetch()
                .where(qActaDiligenciaFueraPlazo.id.eq(actaDiligenciaFueraPlazoId))
                .list(qActaDiligenciaFueraPlazo);

        return (results.isEmpty()) ? null : results.get(0);
    }

    public List<ActaDiligenciaFueraPlazo> getActaDiligenciaFueraPlazoByPersonaId(Long personaId) {
        JPAQuery query = new JPAQuery(entityManager);
        QActaDiligenciaFueraPlazo qActaDiligenciaFueraPlazo = QActaDiligenciaFueraPlazo.actaDiligenciaFueraPlazo;
        query.from(qActaDiligenciaFueraPlazo)
                .where(qActaDiligenciaFueraPlazo.persona.id.eq(personaId).or(qActaDiligenciaFueraPlazo.admins.stringValue().contains(personaId.toString()))).orderBy(qActaDiligenciaFueraPlazo.fecha.desc());

        return query.list(qActaDiligenciaFueraPlazo);
    }

    public List<ActaDiligenciaFueraPlazo> getActaDiligenciaFueraPlazoByModerador(Long personaId) {
        JPAQuery query = new JPAQuery(entityManager);
        QActaDiligenciaFueraPlazo qActaDiligenciaFueraPlazo = QActaDiligenciaFueraPlazo.actaDiligenciaFueraPlazo;
        QActaDiligenciaFueraPlazoModeracion qActaDiligenciaFueraPlazoModeracion = QActaDiligenciaFueraPlazoModeracion.actaDiligenciaFueraPlazoModeracion;

        query.from(qActaDiligenciaFueraPlazo, qActaDiligenciaFueraPlazoModeracion)
                .where(qActaDiligenciaFueraPlazo.acta.id.eq(qActaDiligenciaFueraPlazoModeracion.actaId).and(qActaDiligenciaFueraPlazoModeracion.directorCentroId.eq(personaId)).and(qActaDiligenciaFueraPlazo.estado.eq(EstadoDiligenciaFueraPlazo.PENDIENTE.getId())));

        return query.list(qActaDiligenciaFueraPlazo);
    }

    public List<Long> getActaIdsByDirectorCentro(Long personaId) {
        JPAQuery query = new JPAQuery(entityManager);
        QActaDiligenciaFueraPlazoModeracion qActaDiligenciaFueraPlazoModeracion = QActaDiligenciaFueraPlazoModeracion.actaDiligenciaFueraPlazoModeracion;
        query.from(qActaDiligenciaFueraPlazoModeracion)
                .where(qActaDiligenciaFueraPlazoModeracion.directorCentroId.eq(personaId));
        return query.list(qActaDiligenciaFueraPlazoModeracion).stream().map(r -> r.getActaId()).collect(Collectors.toList());
    }

    public List<Long> getActaIdsByDirectorDepartamento(Long personaId, Long cursoId, Long convocatoriaId) {
        JPAQuery query = new JPAQuery(entityManager);
        QActaDiligenciaFueraPlazoModeracion qActaDiligenciaFueraPlazoModeracion = QActaDiligenciaFueraPlazoModeracion.actaDiligenciaFueraPlazoModeracion;
        query.from(qActaDiligenciaFueraPlazoModeracion)
                .where(qActaDiligenciaFueraPlazoModeracion.directorDepartamentoId.eq(personaId).and(qActaDiligenciaFueraPlazoModeracion.cursoId.eq(cursoId)).and(qActaDiligenciaFueraPlazoModeracion.convocatoriaId.eq(convocatoriaId))).distinct();
        return query.list(qActaDiligenciaFueraPlazoModeracion).stream().map(r -> r.getActaId()).collect(Collectors.toList());
    }

    public List<String> getEmailsDirectoresCentroByActaId(Long actaId) {
        List<Long> responsablesCentroIds = getResponsablesByActaId(actaId).stream().map(r -> r.getDirectorCentroId()).distinct().collect(Collectors.toList());

        JPAQuery query = new JPAQuery(entityManager);
        QPersona qPersona = QPersona.persona;
        query.from(qPersona).where(qPersona.id.in(responsablesCentroIds));
        return query.list(qPersona).stream().map(r -> r.getEmail()).collect(Collectors.toList());
    }

    public List<ActaDiligenciaFueraPlazoModeracion> getResponsablesByActaId(Long actaId) {
        JPAQuery query = new JPAQuery(entityManager);
        QActaDiligenciaFueraPlazoModeracion qActaDiligenciaFueraPlazoModeracion = QActaDiligenciaFueraPlazoModeracion.actaDiligenciaFueraPlazoModeracion;
        query.from(qActaDiligenciaFueraPlazoModeracion)
                .where(qActaDiligenciaFueraPlazoModeracion.actaId.eq(actaId));
        return query.distinct().list(qActaDiligenciaFueraPlazoModeracion);
    }

}
