package es.uji.apps.act.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.act.models.Notificacion;
import es.uji.apps.act.models.QLog;
import es.uji.apps.act.models.QNotificacion;
import org.springframework.stereotype.Repository;

import es.uji.commons.db.BaseDAODatabaseImpl;

import java.util.List;

@Repository
public class NotificacionDAO extends BaseDAODatabaseImpl
{

    public List<Notificacion> getNotificacionesByActa(Long actaId) {
        QNotificacion qNotificacion = QNotificacion.notificacion;
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qNotificacion).where(qNotificacion.acta.id.eq(actaId)).list(qNotificacion);
    }
}
