package es.uji.apps.act.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.act.models.Convocatoria;
import es.uji.apps.act.models.FechaConvocatoria;
import es.uji.apps.act.models.QConvocatoria;
import es.uji.apps.act.models.QFechaConvocatoria;
import es.uji.apps.act.models.QTipoEstudio;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class FechaConvocatoriaDAO extends BaseDAODatabaseImpl {
    public List<FechaConvocatoria> getConvocatoriasOficiales(Long cursoId) {
        JPAQuery query = new JPAQuery(entityManager);
        QConvocatoria qConvocatoria = QConvocatoria.convocatoria;
        QTipoEstudio qTipoEstudio = QTipoEstudio.tipoEstudio;
        QFechaConvocatoria qFechaConvocatoria = QFechaConvocatoria.fechaConvocatoria;
        return query.from(qFechaConvocatoria).join(qFechaConvocatoria.convocatoria, qConvocatoria)
                .join(qFechaConvocatoria.tipoEstudio, qTipoEstudio).fetch()
                .where(qConvocatoria.id.ne(Convocatoria.EXAMEN_PARCIAL)
                        .and(qFechaConvocatoria.curso.id.eq(cursoId))).orderBy(qTipoEstudio.id.asc(), qConvocatoria.orden.asc())
                .list(qFechaConvocatoria);
    }

    public FechaConvocatoria getConvocatoriaById(Long convocatoriaDetalleId) {
        JPAQuery query = new JPAQuery(entityManager);
        QFechaConvocatoria qFechaConvocatoria = QFechaConvocatoria.fechaConvocatoria;
        return query.from(qFechaConvocatoria).where(qFechaConvocatoria.id.eq(convocatoriaDetalleId)).list(qFechaConvocatoria).get(0);
    }
}
