package es.uji.apps.act.dao;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.act.models.Curso;
import es.uji.apps.act.models.QCurso;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class CursoDAO extends BaseDAODatabaseImpl
{
    public List<Curso> getCursosActivos()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCurso qCurso = QCurso.curso;

        return query.from(qCurso).where(qCurso.activo.eq(true)).orderBy(qCurso.id.desc()).list(qCurso);
    }

    public List<Curso> getCursosDiligenciasFueraPlazo()
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCurso qCurso = QCurso.curso;

        Date fechaActual = new Date();
        return query.from(qCurso).where(qCurso.fechaFinModificacion.before(fechaActual)).orderBy(qCurso.id.desc()).list(qCurso);
    }
}
