package es.uji.apps.act.dao;

import java.util.List;

import es.uji.apps.act.models.DesgloseGrupo;
import es.uji.apps.act.models.QDesgloseGrupo;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.act.models.QDesglosePregunta;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class DesgloseGrupoDAO extends BaseDAODatabaseImpl
{
    public List<DesgloseGrupo> getDesgloseGruposByActaId(Long actaId)
    {
        QDesgloseGrupo qDesgloseGrupo = QDesgloseGrupo.desgloseGrupo;
        QDesglosePregunta qDesglosePregunta = QDesglosePregunta.desglosePregunta;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qDesgloseGrupo)
                .leftJoin(qDesgloseGrupo.desglosePreguntas, qDesglosePregunta).fetch()
                .where(qDesgloseGrupo.acta.id.eq(actaId));

        return query.list(qDesgloseGrupo);
    }
}
