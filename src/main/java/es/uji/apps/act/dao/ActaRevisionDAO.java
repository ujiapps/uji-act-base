package es.uji.apps.act.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.act.models.ActaRevision;
import es.uji.apps.act.models.QActa;
import es.uji.apps.act.models.QActaRevision;
import es.uji.apps.act.models.QNotificacion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ActaRevisionDAO extends BaseDAODatabaseImpl
{
    public List<ActaRevision> getActaRevisionesByActa(Long actaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QActaRevision qActaRevision = QActaRevision.actaRevision;
        QNotificacion qNotificacion = QNotificacion.notificacion;

        query.from(qActaRevision).leftJoin(qActaRevision.notificaciones, qNotificacion).fetch().
                where(qActaRevision.acta.id.eq(actaId));

        return query.list(qActaRevision);
    }

    public ActaRevision getActaRevisionById(Long actaRevisionId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QActaRevision qActaRevision = QActaRevision.actaRevision;
        QActa qActa = QActa.acta;

        List<ActaRevision> result = query.from(qActaRevision).join(qActaRevision.acta, qActa)
                .fetch().fetch().where(qActaRevision.id.eq(actaRevisionId))
                .list(qActaRevision);

        return (result.isEmpty()) ? null : result.get(0);
    }

}
