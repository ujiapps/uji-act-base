package es.uji.apps.act.dao;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.act.models.Acta;
import es.uji.apps.act.models.ActaDiligencia;
import es.uji.apps.act.models.Convocatoria;
import es.uji.apps.act.models.Curso;
import es.uji.apps.act.models.QActa;
import es.uji.apps.act.models.QActaDiligencia;
import es.uji.apps.act.models.QConvocatoria;
import es.uji.apps.act.models.QCurso;
import es.uji.apps.act.models.QPersona;
import es.uji.apps.act.models.views.ActaDiligenciaVirtual;
import es.uji.apps.act.models.views.AsignaturaExpediente;
import es.uji.apps.act.models.views.QActaDiligenciaVirtual;
import es.uji.apps.act.models.views.QAsignaturaExpediente;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ActaDiligenciaDAO extends BaseDAODatabaseImpl
{

    public List<ActaDiligenciaVirtual> getActaDiligenciaVirtualByActaId(Long actaId)
    {
        QActaDiligenciaVirtual qActaDiligenciaVirtual = QActaDiligenciaVirtual.actaDiligenciaVirtual;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qActaDiligenciaVirtual).where(qActaDiligenciaVirtual.actaId.eq(actaId)
                .and(qActaDiligenciaVirtual.motivoExclusion.isNull()));

        return query.distinct().list(qActaDiligenciaVirtual);
    }

    public ActaDiligenciaVirtual getActaDiligenciaVirtualByActaEstudianteId(Long actaEstudianteId)
    {
        QActaDiligenciaVirtual qActaDiligenciaVirtual = QActaDiligenciaVirtual.actaDiligenciaVirtual;

        JPAQuery query = new JPAQuery(entityManager);
        List<ActaDiligenciaVirtual> actaDiligenciasVirtual = query.from(qActaDiligenciaVirtual)
                .where(qActaDiligenciaVirtual.actaEstudianteId.eq(actaEstudianteId)).distinct()
                .list(qActaDiligenciaVirtual);

        return actaDiligenciasVirtual.isEmpty() ? null : actaDiligenciasVirtual.get(0);
    }

    public List<ActaDiligencia> getActaDiligenciasByActaId(Long actaId)
    {
        QActaDiligencia qActaDiligencia = QActaDiligencia.actaDiligencia;
        QPersona qPersona = QPersona.persona;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qActaDiligencia).join(qActaDiligencia.persona, qPersona).fetch()
                .where(qActaDiligencia.acta.id.eq(actaId)).orderBy(qActaDiligencia.fecha.desc());

        return query.list(qActaDiligencia);
    }

    public List<ActaDiligencia> getUltimasDiligenciasByProfesor(Long connectedUserId)
    {
        QActaDiligencia qActaDiligencia = QActaDiligencia.actaDiligencia;
        QPersona qPersona = QPersona.persona;
        JPAQuery query = new JPAQuery(entityManager);

        Date sixMonthsAgo = Date.from(ZonedDateTime.now().minusMonths(6).toInstant());

        query.from(qActaDiligencia).join(qActaDiligencia.persona, qPersona).fetch()
                .where(qActaDiligencia.persona.id.eq(connectedUserId)
                        .and(qActaDiligencia.fecha.after(sixMonthsAgo)))
                .distinct().orderBy(qActaDiligencia.fecha.desc());

        return query.list(qActaDiligencia);
    }

    public List<ActaDiligencia> getDiligenciasByProfesor(Long connectedUserId, Long cursoId,
                                                         Long convocatoriaId, Long actaId)
    {
        QActaDiligencia qActaDiligencia = QActaDiligencia.actaDiligencia;
        QPersona qPersona = QPersona.persona;

        JPAQuery query = new JPAQuery(entityManager);

        BooleanBuilder condicionesWhere = new BooleanBuilder();
        condicionesWhere.and(qActaDiligencia.persona.id.eq(connectedUserId));

        if (cursoId != null)
        {
            condicionesWhere.and(qActaDiligencia.acta.curso.id.eq(cursoId));
        }

        if (convocatoriaId != null)
        {
            condicionesWhere.and(qActaDiligencia.acta.convocatoria.id.eq(convocatoriaId));
        }

        if (actaId != null)
        {
            condicionesWhere.and(qActaDiligencia.acta.id.eq(actaId));
        }

        query.from(qActaDiligencia).join(qActaDiligencia.persona, qPersona).fetch()
                .where(condicionesWhere).distinct().orderBy(qActaDiligencia.fecha.desc());

        return query.list(qActaDiligencia);
    }

    public List<Curso> getCursosDiligenciasByProfesor(Long connectedUserId)
    {
        QCurso qCurso = QCurso.curso;
        QActaDiligencia qActaDiligencia = QActaDiligencia.actaDiligencia;
        QActa qActa = QActa.acta;

        JPAQuery query = new JPAQuery(entityManager);

        query.distinct().from(qCurso).join(qCurso.actas, qActa).join(qActa.actaDiligencias, qActaDiligencia)
                .where(qActaDiligencia.persona.id.eq(connectedUserId));

        return query.list(qCurso);
    }

    public List<Convocatoria> getConvocatoriasDiligenciasByProfesor(Long connectedUserId)
    {
        QConvocatoria qConvocatoria = QConvocatoria.convocatoria;
        QActaDiligencia qActaDiligencia = QActaDiligencia.actaDiligencia;
        QActa qActa = QActa.acta;

        JPAQuery query = new JPAQuery(entityManager);

        query.distinct().from(qConvocatoria).join(qConvocatoria.actas, qActa).join(qActa.actaDiligencias, qActaDiligencia)
                .where(qActaDiligencia.persona.id.eq(connectedUserId));

        return query.list(qConvocatoria);
    }

    public List<Acta> getActasDiligenciasByProfesor(Long connectedUserId)
    {
        QActaDiligencia qActaDiligencia = QActaDiligencia.actaDiligencia;
        QActa qActa = QActa.acta;

        JPAQuery query = new JPAQuery(entityManager);

        query.distinct().from(qActa).join(qActa.actaDiligencias, qActaDiligencia)
                .where(qActaDiligencia.persona.id.eq(connectedUserId));

        return query.list(qActa);
    }

    public List<AsignaturaExpediente> getAsignaturaExpedienteByActa(Acta acta)
    {
        QAsignaturaExpediente qAsignaturaExpediente = QAsignaturaExpediente.asignaturaExpediente;

        JPAQuery query = new JPAQuery(entityManager);
        List<String> asignaturas = acta.getCodigosAsignaturas();

        query.distinct().from(qAsignaturaExpediente)
                .where(qAsignaturaExpediente.curso.eq(acta.getCurso().getId())
                        .and(qAsignaturaExpediente.convocatoriaId.eq(acta.getConvocatoriaIdUji())
                                .and(qAsignaturaExpediente.asignaturaId.in(asignaturas))));

        return query.list(qAsignaturaExpediente);
    }
}
