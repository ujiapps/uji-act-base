package es.uji.apps.act.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.act.models.views.ActaEstudianteOrigen;
import es.uji.apps.act.models.views.QActaEstudianteOrigen;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AsignaturaPersonaDAO extends BaseDAODatabaseImpl
{
    public List<ActaEstudianteOrigen> getAsignaturaPersonaByCurso(Long cursoId, Long personaId)
    {
        QActaEstudianteOrigen qActaEstudianteOrigen = QActaEstudianteOrigen.actaEstudianteOrigen;
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qActaEstudianteOrigen).where(
                qActaEstudianteOrigen.curso.eq(cursoId).and(
                        qActaEstudianteOrigen.personaId.eq(personaId)));

        return query.list(qActaEstudianteOrigen);
    }
}
