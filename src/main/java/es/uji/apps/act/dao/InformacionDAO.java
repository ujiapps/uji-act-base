package es.uji.apps.act.dao;

import java.util.List;

import es.uji.apps.act.models.Informacion;
import es.uji.apps.act.models.QInformacion;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class InformacionDAO extends BaseDAODatabaseImpl
{
    public List<Informacion> getNotificacionesActivas()
    {
        QInformacion qInformacion = QInformacion.informacion;
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qInformacion)
                .where(qInformacion.fechaInicio.currentDate().between(qInformacion.fechaInicio,
                        qInformacion.fechaFin))
                .orderBy(qInformacion.orden.asc()).distinct().list(qInformacion);
    }

}
