package es.uji.apps.act.dao;

import java.util.List;

import es.uji.apps.act.models.Log;
import es.uji.apps.act.models.QLog;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.act.models.Informacion;
import es.uji.apps.act.models.QInformacion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class LogDAO extends BaseDAODatabaseImpl
{
    public List<Log> getLogsByActaId(Long actaId)
    {
        QLog qLog = QLog.log;
        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qLog).where(qLog.acta.id.eq(actaId)).list(qLog);
    }

}
