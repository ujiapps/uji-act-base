package es.uji.apps.act.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.act.models.views.ActaDepartamento;
import es.uji.apps.act.models.views.QActaDepartamento;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ActaDepartamentoDAO extends BaseDAODatabaseImpl {
    @InjectParam
    ActaDiligenciaFueraPlazoDAO actaDiligenciaFueraPlazoDAO;

    public List<ActaDepartamento> getListaActasDepartamentoConAlumnosByCursoAndConvocatoriaAndUserId(
            Long cursoId, Long convocatoriaId, Long connectedUserId) {
        QActaDepartamento qActaDepartamento = QActaDepartamento.actaDepartamento;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qActaDepartamento)
                .where(qActaDepartamento.cursoId.eq(cursoId)
                        .and(qActaDepartamento.personaId.eq(connectedUserId))
                        .and(qActaDepartamento.numeroAlumnos.gt(0L)
                                .or(qActaDepartamento.fechaTraspaso.isNotNull())));

        if (convocatoriaId != null) {
            query.where(qActaDepartamento.convocatoriaId.eq(convocatoriaId));
        }

        return query.distinct().list(qActaDepartamento);
    }


    public List<ActaDepartamento> getActasTraspasadasProfesorByCursoAndConvocatoria(Long cursoId,
                                                                                    Long convocatoriaId, Long connectedUserId) {
        QActaDepartamento qActaDepartamento = QActaDepartamento.actaDepartamento;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qActaDepartamento)
                .where(qActaDepartamento.cursoId.eq(cursoId)
                        .and(qActaDepartamento.personaId.eq(connectedUserId))
                        .and(qActaDepartamento.fechaTraspaso.isNotNull()));

        if (convocatoriaId != null) {
            query.where(qActaDepartamento.convocatoriaId.eq(convocatoriaId));
        }

        return query.list(qActaDepartamento);
    }

    public List<ActaDepartamento> getActasTraspasadasProfesorODirectorDepartamentoByCursoAndConvocatoria(Long cursoId,
                                                                                                         Long convocatoriaId, Long connectedUserId) {
        List<Long> actaDirectorDepartamentoIds = actaDiligenciaFueraPlazoDAO.getActaIdsByDirectorDepartamento(connectedUserId, cursoId, convocatoriaId);

        QActaDepartamento qActaDepartamento = QActaDepartamento.actaDepartamento;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qActaDepartamento)
                .where(qActaDepartamento.cursoId.eq(cursoId).and(qActaDepartamento.convocatoriaId.eq(convocatoriaId))
                        .and(qActaDepartamento.personaId.eq(connectedUserId).and(qActaDepartamento.personaTraspaso.id.eq(connectedUserId))
                                .or(qActaDepartamento.actaId.in(actaDirectorDepartamentoIds).and(qActaDepartamento.personaId.eq(qActaDepartamento.personaTraspaso.id)))
                                .or(qActaDepartamento.actaId.in(actaDirectorDepartamentoIds).and(qActaDepartamento.personaId.eq(connectedUserId))))
                        .and(qActaDepartamento.fechaTraspaso.isNotNull()));

        if (convocatoriaId != null) {
            query.where(qActaDepartamento.convocatoriaId.eq(convocatoriaId));
        }

        return query.distinct().list(qActaDepartamento);
    }

    public List<ActaDepartamento> getActasTraspasadasPorCursoConvocatoriaYCodigoAsignatura(Long cursoAcademicoId, Long convocatoriaId, String asignaturaId, Long connectedUserId) {
        QActaDepartamento qActaDepartamento = QActaDepartamento.actaDepartamento;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qActaDepartamento)
                .where(qActaDepartamento.cursoId.eq(cursoAcademicoId)
                        .and(qActaDepartamento.codigo.eq(asignaturaId).and(qActaDepartamento.convocatoriaId.eq(convocatoriaId))));

        List<Long> listaActas = new ArrayList<Long>();
        List<ActaDepartamento> listaActasDepartamento = new ArrayList<ActaDepartamento>();

        for (ActaDepartamento actaDepartamento : query.list(qActaDepartamento)) {
            if (!listaActas.contains(actaDepartamento.getActaId()) && actaDepartamento.getActaId() != null) {
                listaActas.add(actaDepartamento.getActaId());
                ActaDepartamento ad = clonaActa(actaDepartamento, connectedUserId);
                listaActasDepartamento.add(ad);
            }
        }

        return listaActasDepartamento;
    }

    private ActaDepartamento clonaActa(ActaDepartamento actaDepartamento, Long connectedUserId) {
        ActaDepartamento ad = new ActaDepartamento();
        ad.setCodigo(actaDepartamento.getCodigo());
        ad.setFechaAlta(actaDepartamento.getFechaAlta());
        ad.setActaId(actaDepartamento.getActaId());
        ad.setDepartamentoId(actaDepartamento.getDepartamentoId());
        ad.setId(actaDepartamento.getId());
        ad.setConvocatoriaId(actaDepartamento.getConvocatoriaId());
        ad.setConvocatoriaNombre(actaDepartamento.getConvocatoriaNombre());
        ad.setFechaTraspaso(actaDepartamento.getFechaTraspaso());
        ad.setReferencia(actaDepartamento.getReferencia());
        ad.setDepartamentoRecibeActaId(actaDepartamento.getDepartamentoRecibeActaId());
        ad.setTipoEstudioId(actaDepartamento.getTipoEstudioId());
        ad.setTipoEstudioNombre(actaDepartamento.getTipoEstudioNombre());
        ad.setNumeroAlumnos(actaDepartamento.getNumeroAlumnos());
        ad.setNumeroAlumnosTotal(actaDepartamento.getNumeroAlumnosTotal());
        ad.setFechaFirmaDigital(actaDepartamento.getFechaFirmaDigital());
        ad.setDepartamentoNombre(actaDepartamento.getDepartamentoNombre());
        ad.setNombreDepartamentoRecibeActa(actaDepartamento.getNombreDepartamentoRecibeActa());
        ad.setFechaFinModificacion(actaDepartamento.getFechaFinModificacion());
        ad.setPersonaId(connectedUserId);

        return ad;
    }
}
