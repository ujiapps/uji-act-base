package es.uji.apps.act.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.act.models.Acta;
import es.uji.apps.act.models.QActa;
import es.uji.apps.act.models.views.ActaPersona;
import es.uji.apps.act.models.views.QActaPersona;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ActaPersonaDAO extends BaseDAODatabaseImpl
{

    public List<ActaPersona> getListaActasPersonasByCurso(Long cursoId, Long connectedUserId)
    {
        QActaPersona qActaPersona = QActaPersona.actaPersona;
        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qActaPersona)
                .where(qActaPersona.cursoId.eq(cursoId).and(qActaPersona.perId.eq(connectedUserId)))
                .list(qActaPersona);
    }

    public ActaPersona getActaPersonaByCursoAndCodigosAndPersona(Long cursoId, String asiCodigos,
            Long personaId)
    {
        QActaPersona qActaPersona = QActaPersona.actaPersona;
        JPAQuery query = new JPAQuery(entityManager);
        List<ActaPersona> results = query.from(qActaPersona).where(qActaPersona.cursoId.eq(cursoId)
                .and(qActaPersona.perId.eq(personaId)).and(qActaPersona.asiCodigos.eq(asiCodigos)))
                .list(qActaPersona);

        return (results.isEmpty()) ? null : results.get(0);
    }

    public ActaPersona getActaPersonaByCursoAndAsignaturasAndPersona(Long cursoId,
            String asiCodigos, Long personaId)
    {
        QActaPersona qActaPersona = QActaPersona.actaPersona;
        JPAQuery query = new JPAQuery(entityManager);
        List<ActaPersona> results = query.from(qActaPersona).where(qActaPersona.cursoId.eq(cursoId)
                .and(qActaPersona.asiCodigos.eq(asiCodigos)).and(qActaPersona.perId.eq(personaId)))
                .distinct().list(qActaPersona);

        return (results.isEmpty()) ? null : results.get(0);
    }

}
