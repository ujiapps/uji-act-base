package es.uji.apps.act.dao;

import java.text.MessageFormat;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.act.models.QPersona;
import es.uji.apps.act.models.views.QResponsableAsignatura;
import es.uji.apps.act.models.views.ResponsableAsignatura;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class ResponsableAsignaturaDAO extends BaseDAODatabaseImpl {
    public List<ResponsableAsignatura> getReponsablesAsignatura(Long cursoId,
                                                                String codigoAsignatura) {
        QResponsableAsignatura qResponsableAsignatura = QResponsableAsignatura.responsableAsignatura;
        QPersona qPersona = QPersona.persona;

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qResponsableAsignatura).join(qResponsableAsignatura.persona, qPersona)
                .where(qResponsableAsignatura.curso.eq(cursoId)
                        .and(qResponsableAsignatura.asignaturaId.eq(codigoAsignatura))
                        .and(qResponsableAsignatura.administrador.eq("N")))
                .distinct().list(qResponsableAsignatura);
    }

    public List<ResponsableAsignatura> getReponsablesPuedenTraspasarAsignatura(Long cursoId,
                                                                               String codigoAsignatura) {
        QResponsableAsignatura qResponsableAsignatura = QResponsableAsignatura.responsableAsignatura;
        QPersona qPersona = QPersona.persona;

        JPAQuery query = new JPAQuery(entityManager);
        return query.from(qResponsableAsignatura).join(qResponsableAsignatura.persona, qPersona)
                .fetch()
                .where(qResponsableAsignatura.curso.eq(cursoId)
                        .and(qResponsableAsignatura.asignaturaId.eq(codigoAsignatura))
                        .and(qResponsableAsignatura.administrador.eq("N"))
                        .and(qResponsableAsignatura.puedeTraspasar.eq("S")))
                .distinct().list(qResponsableAsignatura);
    }
}
