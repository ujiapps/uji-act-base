package es.uji.apps.act.dao;

import java.util.List;

import es.uji.apps.act.models.QActa;
import es.uji.apps.act.models.QActaAsignatura;
import es.uji.apps.act.models.views.AsignaturaCursada;
import es.uji.apps.act.models.views.QAsignaturaCursada;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class AsignaturaCursadaDAO extends BaseDAODatabaseImpl
{
    public AsignaturaCursada getAsignaturaCursada(Long personaId, String asignaturaId,
                                                  Long cursoAcademicoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QAsignaturaCursada qAsignaturaCursada = QAsignaturaCursada.asignaturaCursada;

        List<AsignaturaCursada> result = query.from(qAsignaturaCursada)
                .where(qAsignaturaCursada.personaId.eq(personaId)
                        .and(qAsignaturaCursada.asignaturaId.eq(asignaturaId))
                        .and(qAsignaturaCursada.curso.eq(cursoAcademicoId)))
                .distinct().list(qAsignaturaCursada);

        return (result.isEmpty()) ? null : result.get(0);
    }

    public List<AsignaturaCursada> getAsignaturasCursadasByActaId(Long actaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QAsignaturaCursada qAsignaturaCursada = QAsignaturaCursada.asignaturaCursada;
        QActaAsignatura qActaAsignatura = QActaAsignatura.actaAsignatura;
        QActa qActa = QActa.acta;

        return query.from(qAsignaturaCursada, qActaAsignatura, qActa)
                .where(qActa.id.eq(qActaAsignatura.acta.id)
                        .and(qAsignaturaCursada.asignaturaId.eq(qActaAsignatura.asignatura.id))
                        .and(qAsignaturaCursada.curso.eq(qActa.curso.id)).and(qActa.id.eq(actaId)))
                .distinct().list(qAsignaturaCursada);
    }

    public AsignaturaCursada getAsignaturaCursadasByActaIdAndAlumno(Long actaId, Long alumnoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QAsignaturaCursada qAsignaturaCursada = QAsignaturaCursada.asignaturaCursada;
        QActaAsignatura qActaAsignatura = QActaAsignatura.actaAsignatura;
        QActa qActa = QActa.acta;

        List<AsignaturaCursada> result = query.from(qAsignaturaCursada, qActaAsignatura, qActa)
                .where(qActa.id.eq(qActaAsignatura.acta.id)
                        .and(qAsignaturaCursada.asignaturaId.eq(qActaAsignatura.asignatura.id)
                                .and(qAsignaturaCursada.curso.eq(qActa.curso.id)
                                        .and(qActa.id.eq(actaId)
                                                .and(qAsignaturaCursada.personaId.eq(alumnoId))))))
                .distinct().list(qAsignaturaCursada);

        return (result.isEmpty()) ? null : result.get(0);
    }

}
