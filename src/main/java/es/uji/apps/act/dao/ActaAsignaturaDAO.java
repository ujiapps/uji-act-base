package es.uji.apps.act.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.act.models.ActaAsignatura;
import es.uji.apps.act.models.QActa;
import es.uji.apps.act.models.QActaAsignatura;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ActaAsignaturaDAO extends BaseDAODatabaseImpl
{
    public List<ActaAsignatura> getActaAsignaturasByActa(Long actaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QActaAsignatura qActaAsignatura = QActaAsignatura.actaAsignatura;
        query.from(qActaAsignatura).where(qActaAsignatura.acta.id.eq(actaId));

        return query.list(qActaAsignatura);
    }

    public ActaAsignatura getActaAsignaturaById(Long actaAsignaturaId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QActaAsignatura qActaAsignatura = QActaAsignatura.actaAsignatura;
        QActa qActa = QActa.acta;


        List<ActaAsignatura> result = query.from(qActaAsignatura).join(qActaAsignatura.acta, qActa)
                .fetch().fetch().where(qActaAsignatura.id.eq(actaAsignaturaId))
                .list(qActaAsignatura);

        return (result.isEmpty()) ? null : result.get(0);
    }

}
