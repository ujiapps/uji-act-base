package es.uji.apps.act.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.act.models.PDIActivo;
import es.uji.apps.act.models.QPDIActivo;
import es.uji.apps.act.models.QPersona;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class PDIDAO extends BaseDAODatabaseImpl {

    public List<PDIActivo> findPdiActivo(String queryString) {
        QPDIActivo qpdiActivo = QPDIActivo.pDIActivo;
        QPersona qPersona = QPersona.persona;

        JPAQuery query = new JPAQuery(entityManager);

        return query.from(qpdiActivo)
                .where(qpdiActivo.nombre.lower().like("%" + queryString.toLowerCase() + "%"))
                .distinct().list(qpdiActivo);
    }
}
