package es.uji.apps.act.services.rest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.NotFoundException;
import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.act.exceptions.ActaNoEditableException;
import es.uji.apps.act.exceptions.InconsistenciaDeDatosException;
import es.uji.apps.act.exceptions.NoAutorizadoException;
import es.uji.apps.act.models.ActaRevision;
import es.uji.apps.act.models.Notificacion;
import es.uji.apps.act.models.enums.Acceso;
import es.uji.apps.act.services.ActaRevisionService;
import es.uji.apps.act.services.AuthService;
import es.uji.apps.act.services.NotificacionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;

@Path("notificaciones")
public class NotificacionResource extends CoreBaseService
{
    @InjectParam
    private AuthService authService;

    @InjectParam
    private NotificacionService notificacionService;

    @PathParam("actaId")
    Long actaId;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getNotificaciones() throws NotFoundException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        if (actaId == null) {
            throw new NotFoundException("Es necessari un actaId per obtindre les notificacions");
        }
        List<Notificacion> listaNotificaciones = notificacionService.getNotificacionesByActa(actaId, connectedUserId);

        return UIEntity.toUI(listaNotificaciones);
    }

}
