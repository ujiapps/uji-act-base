package es.uji.apps.act.services.rest;

import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.act.exceptions.ActaNoEditableException;
import es.uji.apps.act.exceptions.NoAutorizadoException;
import es.uji.apps.act.models.views.ResponsableAsignatura;
import es.uji.apps.act.services.ResponsableAsignaturaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;

@Path("responsables-asignatura")
public class ResponsablesAsignaturaResource extends CoreBaseService {
    @InjectParam
    private ResponsableAsignaturaService responsableAsignaturaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getResponsablesAsignatura(@QueryParam("cursoId") Long cursoId,
                                                    @QueryParam("codigoAsignatura") String codigoAsignatura)
            throws RegistroNoEncontradoException, ActaNoEditableException, NoAutorizadoException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<ResponsableAsignatura> listaResponsables;

        listaResponsables =
                responsableAsignaturaService
                        .getReponsablesAsignatura(cursoId, codigoAsignatura, connectedUserId);

        return UIEntity.toUI(listaResponsables);
    }
}
