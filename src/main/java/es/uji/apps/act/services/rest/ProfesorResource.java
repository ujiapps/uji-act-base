package es.uji.apps.act.services.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.act.exceptions.*;
import es.uji.apps.act.models.Acta;
import es.uji.apps.act.models.ActaDiligencia;
import es.uji.apps.act.models.Convocatoria;
import es.uji.apps.act.models.Curso;
import es.uji.apps.act.models.enums.Acceso;
import es.uji.apps.act.models.enums.Calificacion;
import es.uji.apps.act.models.views.ActaDiligenciaVirtual;
import es.uji.apps.act.models.views.AsignaturaCursada;
import es.uji.apps.act.services.*;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;

@Path("profesor")
public class ProfesorResource extends CoreBaseService
{
    @InjectParam
    private AuthService authService;

    @InjectParam
    private ActaDiligenciaService actaDiligenciaService;

    @GET
    @Path("diligencias")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getActaDiligencias(@QueryParam("cursoId") Long cursoId,
            @QueryParam("convocatoriaId") Long convocatoriaId, @QueryParam("actaId") Long actaId)
            throws NoAutorizadoException, ActaNoEditableException, RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<ActaDiligencia> listaProfesorDiligencias = actaDiligenciaService
                .getDiligenciasByProfesor(connectedUserId, cursoId, convocatoriaId, actaId);
        return componerListaProfesorDiligencias(listaProfesorDiligencias);
    }

    private List<UIEntity> componerListaProfesorDiligencias(
            List<ActaDiligencia> listaProfesorDiligencias)
    {
        List<UIEntity> listaUI = new ArrayList<>();

        for (ActaDiligencia profesorDiligencia : listaProfesorDiligencias)
        {
            UIEntity ui = creaProfesorDiligencia(profesorDiligencia);
            listaUI.add(ui);
        }

        return listaUI;
    }

    @GET
    @Path("diligencias/cursos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getActaDiligenciasCursos()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<Curso> listaCursos = actaDiligenciaService
                .getCursosDiligenciasByProfesor(connectedUserId);
        return UIEntity.toUI(listaCursos);
    }

    @GET
    @Path("diligencias/resumen")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getResumenDiligenciasUltimosSeisMeses()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<ActaDiligencia> listaProfesorDiligencias = actaDiligenciaService
                .getUltimasDiligenciasByProfesor(connectedUserId);
        return creaResumenDiligencias(listaProfesorDiligencias);
    }

    private UIEntity creaResumenDiligencias(List<ActaDiligencia> listaProfesorDiligencias) {
        UIEntity ui = new UIEntity();

        ui.put("diligencias", listaProfesorDiligencias.size());
        ui.put("actasAfectadas", listaProfesorDiligencias.stream().map(d -> d.getActa().getDescripcion()).distinct().count());

        return ui;
    }

    @GET
    @Path("diligencias/convocatorias")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getActaDiligenciasConvocatorias()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<Convocatoria> listaConvocatorias = actaDiligenciaService
                .getConvocatoriasDiligenciasByProfesor(connectedUserId);
        return UIEntity.toUI(listaConvocatorias);
    }

    @GET
    @Path("diligencias/actas")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getActasDiligencias()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<Acta> listaActas = actaDiligenciaService
                .getActasDiligenciasByProfesor(connectedUserId);
        return UIEntity.toUI(listaActas);
    }

    private UIEntity creaProfesorDiligencia(ActaDiligencia profesorDiligencia)
    {
        UIEntity ui = UIEntity.toUI(profesorDiligencia);
        ui.put("alumnoNombre",
                profesorDiligencia.getActaEstudiante().getPersona().getNombreCompleto());
        ui.put("identificacion",
                profesorDiligencia.getActaEstudiante().getPersona().getIdentificacion());
        ui.put("convocatoria", profesorDiligencia.getActa().getConvocatoria().getNombre());
        ui.put("cursoAcademico", profesorDiligencia.getActa().getCurso().getId());
        return ui;
    }

}
