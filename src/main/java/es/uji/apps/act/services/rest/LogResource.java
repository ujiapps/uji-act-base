package es.uji.apps.act.services.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.NotFoundException;
import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.act.exceptions.ActaNoEncontradaException;
import es.uji.apps.act.exceptions.NoAutorizadoException;
import es.uji.apps.act.models.Informacion;
import es.uji.apps.act.models.Log;
import es.uji.apps.act.services.InformacionService;
import es.uji.apps.act.services.LogService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("logs")
public class LogResource extends CoreBaseService
{
    @InjectParam
    private LogService logService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getLogs(@QueryParam("actaId") Long actaId)
            throws NoAutorizadoException, ActaNoEncontradaException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        if (actaId == null)
        {
            throw new ActaNoEncontradaException();
        }

        List<Log> logs = logService.getLogsByActaId(actaId, connectedUserId);

        return UIEntity.toUI(logs);
    }

}
