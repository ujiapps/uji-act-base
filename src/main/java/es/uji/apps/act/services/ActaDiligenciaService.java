package es.uji.apps.act.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.act.dao.ActaConvocatoriaDAO;
import es.uji.apps.act.dao.ActaDiligenciaDAO;
import es.uji.apps.act.dao.AsignaturaCursadaDAO;
import es.uji.apps.act.exceptions.NotaNoValidaException;
import es.uji.apps.act.exceptions.TraspasarDiligenciaException;
import es.uji.apps.act.models.*;
import es.uji.apps.act.models.enums.Calificacion;
import es.uji.apps.act.models.ui.UIDiligencia;
import es.uji.apps.act.models.views.ActaDiligenciaVirtual;
import es.uji.apps.act.models.views.AsignaturaCursada;
import es.uji.apps.act.models.views.AsignaturaExpediente;
import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.rest.CoreBaseService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ActaDiligenciaService extends CoreBaseService {
    @InjectParam
    private ActaDiligenciaDAO actaDiligenciaDAO;

    @InjectParam
    private ActaConvocatoriaDAO actaConvocatoriaDAO;

    @InjectParam
    private AsignaturaCursadaDAO asignaturaCursadaDAO;

    @InjectParam
    private CorreoService correoService;

    @InjectParam
    private ResponsableAsignaturaService responsableAsignaturaService;

    @InjectParam
    private AvisosService avisosService;

    public List<ActaDiligenciaVirtual> getActaDiligenciaVirtualByActaId(Long actaId,
                                                                        Long connectedUserId) {
        return actaDiligenciaDAO.getActaDiligenciaVirtualByActaId(actaId);
    }

    public ActaDiligenciaVirtual getActaDiligenciaVirtualByActaEstudianteId(Long actaEstudianteId,
                                                                            Long connectedUserId) {
        return actaDiligenciaDAO.getActaDiligenciaVirtualByActaEstudianteId(actaEstudianteId);
    }

    @Transactional
    public void traspasarDiligencias(Acta acta, List<ActaDiligenciaVirtual> actaDiligencias,
                                     Long connectedUserId, String ip)
            throws NotaNoValidaException, IOException, TraspasarDiligenciaException, MessageNotSentException {
        List<AsignaturaExpediente> asignaturasExpediente = getAsignaturaExpedienteByActa(acta);

        checkNumeroMatriculas(acta.getId(), actaDiligencias);

        if (!acta.isDiligenciable()) {
            throw new TraspasarDiligenciaException(
                    "No es pot crear la diligencia perque el curs acadèmic està tancat. Has de crear una Diligencia extraordinaria");
        }

        for (ActaDiligenciaVirtual actaDiligenciaVirtual : actaDiligencias) {
            actaDiligenciaVirtual.checkNotaAcordeConCalificacion();
            AsignaturaCursada asignaturaCursada = asignaturaCursadaDAO.getAsignaturaCursada(
                    actaDiligenciaVirtual.getAlumnoId(), actaDiligenciaVirtual.getAsignaturaId(),
                    actaDiligenciaVirtual.getCursoId());
            if (asignaturaCursada == null) {
                throw new TraspasarDiligenciaException(
                        "No es pot crear la diligencia perque el alumne "
                                + actaDiligenciaVirtual.getAlumnoNombre()
                                + " ja no està matriculat a la assignatura.");
            }

            if (!asignaturaCursada.isExpedienteAbierto()) {
                throw new TraspasarDiligenciaException(
                        "No es pot crear la diligencia perque el alumne "
                                + actaDiligenciaVirtual.getAlumnoNombre()
                                + " te el expedient tancat.");
            }

            AsignaturaExpediente asignaturaExpediente = asignaturasExpediente
                    .stream().filter(ae -> ae.getPersonaId().equals(actaDiligenciaVirtual.getAlumnoId())).findFirst().orElse(null);

            if (asignaturaExpediente != null && asignaturaExpediente.isAprobadoConvocatoriaAnterior()) {
                throw new TraspasarDiligenciaException(
                        "No es pot crear la diligencia perque el alumne "
                                + actaDiligenciaVirtual.getAlumnoNombre()
                                + " ja ha superat l'assignatura.");
            }

            if (asignaturaExpediente != null && asignaturaExpediente.isCalificadoConvocatoriaPosterior()) {
                throw new TraspasarDiligenciaException(
                        "No es pot crear la diligencia perque el alumne "
                                + actaDiligenciaVirtual.getAlumnoNombre()
                                + " té una qualificació en una convocatòria posterior.");
            }

        }
        actaDiligencias.forEach(ad -> addActaDiligencia(ad, connectedUserId, ip));
        actaDiligenciaDAO.flush();

        RemoteTraspasarDiligenciaProcedure remoteTraspasarDiligenciaProcedure = new RemoteTraspasarDiligenciaProcedure();
        remoteTraspasarDiligenciaProcedure.init();
        remoteTraspasarDiligenciaProcedure.execute(acta.getId(), connectedUserId);

        List<UIDiligencia> uiDiligencias = actaDiligencias.stream().map(ad -> getUIDiligencia(ad)).collect(Collectors.toList());
        List<String> emailResponsables = responsableAsignaturaService.getReponsablesAsignaturaPuedenTraspasar(acta.getCurso().getId(), acta.getCodigosAsignaturas().get(0))
                .stream().map(ra -> ra.getEmail()).collect(Collectors.toList());

        avisosService.notificaDiligenciasAlumnos(uiDiligencias);
        avisosService.notificaDiligenciasResponsables(uiDiligencias, emailResponsables);
    }

    private UIDiligencia getUIDiligencia(ActaDiligenciaVirtual actaDiligencia) {
        UIDiligencia uiDiligencia = new UIDiligencia();
        uiDiligencia.setAsignatura(actaDiligencia.getAsignaturaId());
        uiDiligencia.setAlumno(actaDiligencia.getAlumnoNombre());
        uiDiligencia.setEmail(actaDiligencia.getCuenta());
        uiDiligencia.setCalificacion(actaDiligencia.getCalificacionAsString());
        uiDiligencia.setNota(actaDiligencia.getNota());
        uiDiligencia.setComentario(actaDiligencia.getComentario());
        return uiDiligencia;
    }

    private void checkNumeroMatriculas(Long actaId, List<ActaDiligenciaVirtual> actaDiligencias)
            throws TraspasarDiligenciaException {
        Long numeroMaximoMatriculas = actaConvocatoriaDAO.getActaConvocatoriaByActaId(actaId)
                .getMatriculas();
        List<ActaDiligenciaVirtual> matriculasEnActa = actaDiligenciaDAO
                .getActaDiligenciaVirtualByActaId(actaId).stream()
                .filter(adv -> Calificacion.MATRICULA_HONOR.getId().equals(adv.getCalificacionId()))
                .collect(Collectors.toList());

        Long numeroMatriculasNuevas = 0L;
        for (ActaDiligenciaVirtual diligencia : actaDiligencias) {
            // Si la diligencia es una matrícula y no tiene matrícula en el acta
            if (diligencia.getCalificacionId().equals(Calificacion.MATRICULA_HONOR.getId())
                    && matriculasEnActa.stream()
                    .filter(adv -> adv.getAlumnoId().equals(diligencia.getAlumnoId()))
                    .count() == 0) {
                numeroMatriculasNuevas++;
            }

            // Si la diligencia no es matrícula y tiene matrícula en el acta
            if (!diligencia.getCalificacionId().equals(Calificacion.MATRICULA_HONOR.getId())
                    && matriculasEnActa.stream()
                    .filter(adv -> adv.getAlumnoId().equals(diligencia.getAlumnoId()))
                    .count() > 0) {
                numeroMatriculasNuevas--;
            }
        }

        if (matriculasEnActa.size() + numeroMatriculasNuevas > numeroMaximoMatriculas) {
            throw new TraspasarDiligenciaException(
                    "No es pot traspassar la diligència, se superaria el nombre màxim de matrícules d'honor de l'acta");
        }
    }

    public void addActaDiligencia(ActaDiligenciaVirtual actaDiligenciaVirtual, Long connectedUserId, String ip) {
        ActaDiligenciaVirtual actaDiligenciaVirtualAnterior = actaDiligenciaDAO
                .getActaDiligenciaVirtualByActaEstudianteId(
                        actaDiligenciaVirtual.getActaEstudianteId());

        Double nuevaNota = actaDiligenciaVirtual.getNota();
        if (nuevaNota != null) {
            nuevaNota = Math.floor(nuevaNota * 10) / 10.0;
        }

        ActaDiligencia actaDiligencia = new ActaDiligencia();
        actaDiligencia.setNotaAnterior(actaDiligenciaVirtualAnterior.getNota());
        actaDiligencia.setCalificacionAnteriorId(actaDiligenciaVirtualAnterior.getCalificacionId());
        actaDiligencia
                .setActaEstudiante(new ActaEstudiante(actaDiligenciaVirtual.getActaEstudianteId()));
        actaDiligencia.setNotaNueva(nuevaNota);
        actaDiligencia.setCalificacionNuevaId(actaDiligenciaVirtual.getCalificacionId() == 0 ? null
                : actaDiligenciaVirtual.getCalificacionId());
        actaDiligencia.setActa(new Acta(actaDiligenciaVirtualAnterior.getActaId()));
        actaDiligencia.setPersona(new Persona(connectedUserId));
        actaDiligencia.setFecha(new Date());
        actaDiligencia.setComentarioDiligencia(actaDiligenciaVirtual.getComentario());
        actaDiligencia.setIp(ip);
        actaDiligenciaDAO.insert(actaDiligencia);
    }

    public void addActaDiligencia(ActaDiligenciaFueraPlazo actaDiligenciaFueraPlazo, String ip, Long profesorId)
            throws TraspasarDiligenciaException, IOException {
        checkNumeroMatriculas(actaDiligenciaFueraPlazo);
        ActaDiligencia actaDiligencia = creaActaDiligencia(actaDiligenciaFueraPlazo, ip);
        actaDiligenciaDAO.insert(actaDiligencia);

        RemoteTraspasarDiligenciaProcedure remoteTraspasarDiligenciaProcedure = new RemoteTraspasarDiligenciaProcedure();
        remoteTraspasarDiligenciaProcedure.init();
        remoteTraspasarDiligenciaProcedure.execute(actaDiligenciaFueraPlazo.getActa().getId(), profesorId);
    }

    public void checkNumeroMatriculas(ActaDiligenciaFueraPlazo actaDiligenciaFueraPlazo)
            throws TraspasarDiligenciaException {
        if (actaDiligenciaFueraPlazo.getCalificacionNuevaId() == null || !actaDiligenciaFueraPlazo.getCalificacionNuevaId().equals(Calificacion.MATRICULA_HONOR.getId())) {
            return;
        }

        Long numeroMaximoMatriculas = actaConvocatoriaDAO.getActaConvocatoriaByActaId(actaDiligenciaFueraPlazo.getActa().getId())
                .getMatriculas();

        List<ActaDiligenciaVirtual> matriculasEnActa = actaDiligenciaDAO
                .getActaDiligenciaVirtualByActaId(actaDiligenciaFueraPlazo.getActa().getId()).stream()
                .filter(adv -> Calificacion.MATRICULA_HONOR.getId().equals(adv.getCalificacionId()))
                .collect(Collectors.toList());

        if (matriculasEnActa.size() == numeroMaximoMatriculas) {
            throw new TraspasarDiligenciaException(
                    "No es pot traspassar la diligència, se superaria el nombre màxim de matrícules d'honor de l'acta");
        }
    }

    protected ActaDiligencia creaActaDiligencia(ActaDiligenciaFueraPlazo actaDiligenciaFueraPlazo, String ip) {
        Double nuevaNota = actaDiligenciaFueraPlazo.getNotaNueva();
        if (nuevaNota != null) {
            nuevaNota = Math.floor(nuevaNota * 10) / 10.0;
        }

        ActaDiligencia actaDiligencia = new ActaDiligencia();
        actaDiligencia.setNotaAnterior(actaDiligenciaFueraPlazo.getNotaAnterior());
        actaDiligencia.setCalificacionAnteriorId(actaDiligenciaFueraPlazo.getCalificacionAnteriorId());
        actaDiligencia
                .setActaEstudiante(new ActaEstudiante(actaDiligenciaFueraPlazo.getActaEstudiante().getId()));
        actaDiligencia.setNotaNueva(nuevaNota);
        actaDiligencia.setCalificacionNuevaId(actaDiligenciaFueraPlazo.getCalificacionNuevaId());
        actaDiligencia.setActa(new Acta(actaDiligenciaFueraPlazo.getActa().getId()));
        actaDiligencia.setPersona(actaDiligenciaFueraPlazo.getPersona());
        actaDiligencia.setFecha(new Date());
        actaDiligencia.setComentarioDiligencia(actaDiligenciaFueraPlazo.getComentarioDiligencia());
        actaDiligencia.setIp(ip);
        return actaDiligencia;
    }

    public List<ActaDiligencia> getActaDiligenciasByActaId(Long actaId) {
        return actaDiligenciaDAO.getActaDiligenciasByActaId(actaId);
    }

    public List<ActaDiligencia> getUltimasDiligenciasByProfesor(Long connectedUserId) {
        return actaDiligenciaDAO.getUltimasDiligenciasByProfesor(connectedUserId);
    }

    public List<ActaDiligencia> getDiligenciasByProfesor(Long connectedUserId, Long cursoId, Long convocatoriaId, Long actaId) {
        return actaDiligenciaDAO.getDiligenciasByProfesor(connectedUserId, cursoId, convocatoriaId, actaId);
    }

    public List<Curso> getCursosDiligenciasByProfesor(Long connectedUserId) {
        return actaDiligenciaDAO.getCursosDiligenciasByProfesor(connectedUserId);
    }

    public List<Convocatoria> getConvocatoriasDiligenciasByProfesor(Long connectedUserId) {
        return actaDiligenciaDAO.getConvocatoriasDiligenciasByProfesor(connectedUserId);
    }

    public List<Acta> getActasDiligenciasByProfesor(Long connectedUserId) {
        return actaDiligenciaDAO.getActasDiligenciasByProfesor(connectedUserId);
    }

    public List<AsignaturaExpediente> getAsignaturaExpedienteByActa(Acta acta) {
        return actaDiligenciaDAO.getAsignaturaExpedienteByActa(acta);
    }

    public void compruebaNotaPuedeTraspasarse(Acta acta, ActaDiligenciaFueraPlazo actaDiligenciaFueraPlazo) throws TraspasarDiligenciaException {

        List<AsignaturaExpediente> asignaturasExpediente = getAsignaturaExpedienteByActa(acta);

        AsignaturaExpediente asignaturaExpediente = asignaturasExpediente
                .stream().filter(ae -> ae.getPersonaId().equals(actaDiligenciaFueraPlazo.getActaEstudiante().getPersona().getId())).findFirst().orElse(null);
        Boolean aprobadoConvocatoriaAnterior = (asignaturaExpediente != null) ? asignaturaExpediente.isAprobadoConvocatoriaAnterior() : false;
        Boolean calificadoConvocatoriaPosterior = (asignaturaExpediente != null) ? asignaturaExpediente.isCalificadoConvocatoriaPosterior() : false;

        if (aprobadoConvocatoriaAnterior) {
            throw new TraspasarDiligenciaException("No es pot traspasar aquesta diligència perquè l'estudiant ja està aprovat en una convocatòria anterior.");
        }

        if (calificadoConvocatoriaPosterior && actaDiligenciaFueraPlazo.getNotaNueva() != null && actaDiligenciaFueraPlazo.getNotaNueva() >= 5) {
            throw new TraspasarDiligenciaException("No es pot traspasar aquesta diligència perquè l'estudiant ja està qualificat a una convocatòria posterior.");

        }

    }
}
