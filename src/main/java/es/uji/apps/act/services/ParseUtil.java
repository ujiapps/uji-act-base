package es.uji.apps.act.services;

import es.uji.apps.act.exceptions.NotaNoValidaException;

public class ParseUtil
{
    public static Double parseaNota(String notaImportada) throws NotaNoValidaException
    {
        if (notaImportada == null || notaImportada.trim().isEmpty())
        {
            return null;
        }
        Double nota;
        try
        {
            nota = Double.parseDouble(notaImportada.replace(',', '.'));
            nota = Math.floor(nota * 10) / 10.0;
        }
        catch (NumberFormatException e)
        {
            throw new NotaNoValidaException("El format de la nota no es correcte");
        }

        if (nota < 0 || nota > 10)
        {
            throw new NotaNoValidaException("La nota no ha de ser inferior a 0 ni superior a 10");
        }
        return nota;
    }
}