package es.uji.apps.act.services;

import java.io.IOException;
import java.io.Reader;
import java.util.List;
import java.util.Map;

public interface ImportExportDataService
{
    String getRecordsAsStringContent(List<String[]> records);

    List<Map<String, String>> getFileAsRecordsMapped(Reader fileReader) throws IOException;
}
