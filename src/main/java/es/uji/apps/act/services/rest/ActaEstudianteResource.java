package es.uji.apps.act.services.rest;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.act.exceptions.ActaNoEditableException;
import es.uji.apps.act.exceptions.EstudianteConMotivoExclusionException;
import es.uji.apps.act.exceptions.ExpedienteBloqueadoException;
import es.uji.apps.act.exceptions.InconsistenciaDeDatosException;
import es.uji.apps.act.exceptions.NoAutorizadoException;
import es.uji.apps.act.exceptions.NotaNoEditableException;
import es.uji.apps.act.exceptions.NotaNoValidaException;
import es.uji.apps.act.exceptions.SincronizarActaException;
import es.uji.apps.act.exceptions.TraspasarActaException;
import es.uji.apps.act.models.ActaEstudiante;
import es.uji.apps.act.models.NotaDesgloseCalculada;
import es.uji.apps.act.models.enums.Acceso;
import es.uji.apps.act.services.ActaEstudianteService;
import es.uji.apps.act.services.ActaService;
import es.uji.apps.act.services.AuthService;
import es.uji.apps.act.services.IpUtil;
import es.uji.apps.act.services.ParseService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;

@Path("actaestudiantes")
public class ActaEstudianteResource extends CoreBaseService
{
    @InjectParam
    private AuthService authService;

    @InjectParam
    private ActaEstudianteService actaEstudianteService;

    @InjectParam
    private ActaService actaService;

    @PathParam("actaId")
    Long actaId;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getActaEstudiantes()
            throws NoAutorizadoException, ActaNoEditableException, RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkPuedeAccederActa(Acceso.LECTURA, actaId, connectedUserId);

        List<ActaEstudiante> listaActaEstudiantes = actaEstudianteService
                .getActaEstudianteByActaId(actaId, connectedUserId).stream().distinct()
                .collect(Collectors.toList());

        return modelToUI(listaActaEstudiantes);
    }

    @PUT
    @Path("sincroniza")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage sincronizaActaEstudiantes()
            throws NoAutorizadoException, ActaNoEditableException, RegistroNoEncontradoException,
            IOException, SincronizarActaException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkPuedeAccederActa(Acceso.LECTURA, actaId, connectedUserId);
        actaEstudianteService.sincronizaEstudiantes(actaId, connectedUserId);
        return new ResponseMessage(true);
    }

    @PUT
    @Path("{actaEstudianteId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateActaEstudiante(@PathParam("actaEstudianteId") Long actaEstudianteId,
                                         UIEntity entity)
            throws NotaNoValidaException, EstudianteConMotivoExclusionException,
            NoAutorizadoException, ActaNoEditableException, InconsistenciaDeDatosException, NotaNoEditableException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkPuedeAccederActaEstudiante(Acceso.ESCRITURA, actaEstudianteId,
                connectedUserId);

        ActaEstudiante actaEstudiante = actaEstudianteService
                .getActaEstudianteById(actaEstudianteId, connectedUserId);

        Boolean uiMatricula = entity.getBoolean("matriculaHonor");
        if (!actaEstudiante.isMatriculaHonor().equals(uiMatricula))
        {
            authService.checkPuedeModificarMatriculaHonor(actaEstudiante, connectedUserId);
        }

        if (uiMatricula && !actaEstudiante.isMatriculaHonor())
        {
            Long actaId = actaEstudiante.getActaAsignatura().getActa().getId();
            actaService.checkSePuedePonerOtraMatriculaEnActa(actaId);
        }

        Double nota = ParseService.parsearDouble(entity.get("nota"));
        Boolean matriculaHonor = entity.getBoolean("matriculaHonor");
        Boolean noPresentado = entity.getBoolean("noPresentado");
        String comentario = entity.get("comentario");

        if (actaEstudiante.getMotivoExclusion() != null
                && !actaEstudiante.getMotivoExclusion().isEmpty())
        {
            throw new EstudianteConMotivoExclusionException();
        }

        if (!actaEstudiante.isEditable())
        {
            throw new NotaNoEditableException();
        }

        actaEstudianteService.updateNota(actaEstudiante, nota, noPresentado, matriculaHonor,
                comentario, connectedUserId, IpUtil.getClientAddres(request));

        return modelToUI(
                actaEstudianteService.getActaEstudianteById(actaEstudianteId, connectedUserId));
    }

    @PUT
    @Path("{actaEstudianteId}/borrar-traspaso")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity borrarTraspasoActaEstudiante(
            @PathParam("actaEstudianteId") Long actaEstudianteId)
            throws NoAutorizadoException, ActaNoEditableException, TraspasarActaException,
            IOException, ExpedienteBloqueadoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkPuedeAccederActaEstudiante(Acceso.ESCRITURA, actaEstudianteId,
                connectedUserId);

        ActaEstudiante actaEstudiante = actaEstudianteService
                .getActaEstudianteById(actaEstudianteId, connectedUserId);

        authService.checkPuedeTraspasarActa(actaEstudiante.getActaAsignatura().getActa(),
                connectedUserId);
        actaEstudianteService.borrarTraspaso(actaEstudiante, connectedUserId);

        return modelToUI(
                actaEstudianteService.getActaEstudianteById(actaEstudianteId, connectedUserId));
    }

    protected List<UIEntity> modelToUI(List<ActaEstudiante> actaEstudiantes)
    {
        return actaEstudiantes.stream().map(ae -> modelToUI(ae)).collect(Collectors.toList());
    }

    protected UIEntity modelToUI(ActaEstudiante actaEstudiante)
    {
        UIEntity uiEntity = UIEntity.toUI(actaEstudiante);
        uiEntity.put("editable", actaEstudiante.isEditable());
        NotaDesgloseCalculada notaDesgloseCalculada = actaEstudiante.getNotaDesgloseCalculada();
        uiEntity.put("notaCalculada", notaDesgloseCalculada.getNota());
        uiEntity.put("notaFormula", notaDesgloseCalculada.getResultadoExtendido());
        uiEntity.put("desgloseActivo",
                actaEstudiante.getActaAsignatura().getActa().isDesgloseActivo());
        uiEntity.put("calificacionId", actaEstudiante.getCalificacionId());
        return uiEntity;
    }

}
