package es.uji.apps.act.services;

import es.uji.apps.act.dao.AsignaturaPersonaDAO;
import es.uji.apps.act.models.views.ActaEstudianteOrigen;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AsignaturaPersonaService extends BaseService<ActaEstudianteOrigen>
{
    @Autowired
    public AsignaturaPersonaService(AsignaturaPersonaDAO dao)
    {
        super(dao, ActaEstudianteOrigen.class);
    }

    public List<ActaEstudianteOrigen> getAsignaturasPersonaByCursoId(Long cursoId, Long personaId)
    {
        return ((AsignaturaPersonaDAO) dao).getAsignaturaPersonaByCurso(cursoId, personaId);
    }
}