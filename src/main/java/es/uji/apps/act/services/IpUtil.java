package es.uji.apps.act.services;

import javax.servlet.http.HttpServletRequest;

public class IpUtil
{
    public static String getClientAddres(HttpServletRequest request)
    {
        String xForwardedFor = request.getHeader("X-FORWARDED-FOR");
        if (xForwardedFor == null || xForwardedFor.isEmpty())
        {
            return request.getRemoteAddr();
        }
        return xForwardedFor;
    }
}