package es.uji.apps.act.services;

import java.util.List;

import es.uji.apps.act.dao.AsignaturaCursadaDAO;
import es.uji.apps.act.models.views.AsignaturaCursada;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.act.dao.AsignaturaPersonaDAO;
import es.uji.apps.act.models.views.ActaEstudianteOrigen;

@Service
public class AsignaturaCursadaService extends BaseService<AsignaturaCursada>
{
    @Autowired
    public AsignaturaCursadaService(AsignaturaCursadaDAO dao)
    {
        super(dao, AsignaturaCursada.class);
    }

    public List<AsignaturaCursada> getAsignaturasCursadasByActaId(Long actaId)
    {
        return ((AsignaturaCursadaDAO) dao).getAsignaturasCursadasByActaId(actaId);
    }

    public AsignaturaCursada getAsignaturaCursadasByActaIdAndAlumno(Long actaId, Long alumnoId)
    {
        return ((AsignaturaCursadaDAO) dao).getAsignaturaCursadasByActaIdAndAlumno(actaId, alumnoId);
    }
}