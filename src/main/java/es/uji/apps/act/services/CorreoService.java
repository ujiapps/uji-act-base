package es.uji.apps.act.services;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import es.uji.apps.act.models.ui.UIEmail;
import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.messaging.client.MessagingClient;
import es.uji.commons.messaging.client.model.MailMessage;

@Service
public class CorreoService
{
    @Value("${uji.act.sendDebugEmailsTo}")
    private String sendDebugEmailsTo;

    public void enviaEmail(UIEmail uiEmail)
            throws MessageNotSentException
    {
        MailMessage email = new MailMessage("ACT");
        email.setTitle(uiEmail.getAsunto());
        email.setSender("no-reply@uji.es");
        email.setContent(uiEmail.getContenido());
        email.setContentType(MediaType.TEXT_PLAIN);
        email.addToRecipient(isDebugActive() ? sendDebugEmailsTo : uiEmail.getEmail());

        MessagingClient emailClient = new MessagingClient();
        emailClient.send(email);
    }

    public void enviaEmails(List<UIEmail> emails)
    {
        for (UIEmail uiEmail : emails)
        {
            try
            {
                enviaEmail(uiEmail);
            }
            catch (MessageNotSentException e)
            {
            }
            if (isDebugActive())
            {
                return;
            }
        }
    }

    private Boolean isDebugActive()
    {
        return (sendDebugEmailsTo != null) && (!sendDebugEmailsTo.trim().isEmpty());
    }
}