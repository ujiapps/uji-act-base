package es.uji.apps.act.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.act.exceptions.ActaNoEditableException;
import es.uji.apps.act.exceptions.NoAutorizadoException;
import es.uji.apps.act.models.PDIActivo;
import es.uji.apps.act.models.views.ResponsableAsignatura;
import es.uji.apps.act.services.PDIService;
import es.uji.apps.act.services.ResponsableAsignaturaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("pdi")
public class PDIResource extends CoreBaseService {
    @InjectParam
    private PDIService pdiService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getResponsablesAsignatura(@QueryParam("query") String query)
            throws RegistroNoEncontradoException, ActaNoEditableException, NoAutorizadoException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<PDIActivo> listaResponsables;

        listaResponsables = pdiService.findReponsablesAsignatura(query, connectedUserId);

        return UIEntity.toUI(listaResponsables);
    }
}
