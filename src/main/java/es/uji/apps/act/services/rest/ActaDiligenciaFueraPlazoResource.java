package es.uji.apps.act.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.act.exceptions.NoAutorizadoException;
import es.uji.apps.act.exceptions.NotaNoValidaException;
import es.uji.apps.act.exceptions.TraspasarDiligenciaException;
import es.uji.apps.act.models.Acta;
import es.uji.apps.act.models.ActaDiligenciaFueraPlazo;
import es.uji.apps.act.models.ActaEstudiante;
import es.uji.apps.act.models.Persona;
import es.uji.apps.act.models.enums.EstadoDiligenciaFueraPlazo;
import es.uji.apps.act.models.views.ActaDiligenciaVirtual;
import es.uji.apps.act.services.*;
import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Path("diligenciasFueraPlazo")
public class ActaDiligenciaFueraPlazoResource extends CoreBaseService
{
    @InjectParam
    private AuthService authService;

    @InjectParam
    private ActaDiligenciaService actaDiligenciaService;

    @InjectParam
    private ActaService actaService;

    @InjectParam
    private ActaDiligenciaFueraPlazoService actaDiligenciaFueraPlazoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getActasDiligenciasFueraPlazo()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<ActaDiligenciaFueraPlazo> actasDiligenciasFueraPlazo = actaDiligenciaFueraPlazoService.getActaDiligenciaFueraPlazoByPersonaId(connectedUserId);


        return actasDiligenciasFueraPlazo.stream().map(ad -> creaActaDiligenciaFueraPlazoUI(ad)).collect(Collectors.toList());
    }

    @GET
    @Path("moderacion")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getActasDiligenciasFueraPlazoModeracion()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<ActaDiligenciaFueraPlazo> actasDiligenciasFueraPlazo = actaDiligenciaFueraPlazoService.getActaDiligenciaFueraPlazoByModerador(connectedUserId);
        return actasDiligenciasFueraPlazo.stream().map(ad -> creaActaDiligenciaFueraPlazoUI(ad)).collect(Collectors.toList());
    }

    private UIEntity creaActaDiligenciaFueraPlazoUI(ActaDiligenciaFueraPlazo actaDiligenciaFueraPlazo)
    {
        UIEntity ui = UIEntity.toUI(actaDiligenciaFueraPlazo);
        ui.put("solicitante", actaDiligenciaFueraPlazo.getPersona().getNombreCompleto());
        ui.put("estadoTxt", actaDiligenciaFueraPlazo.getEstadoTxt());
        ui.put("caducada", actaDiligenciaFueraPlazo.isCaducada());
        ui.put("diligencia", componerMensajeDiligenciaFueraPlazo(actaDiligenciaFueraPlazo));
        return ui;
    }

    private String componerMensajeDiligenciaFueraPlazo(ActaDiligenciaFueraPlazo actaDiligenciaFueraPlazo)
    {
        Long curso = actaDiligenciaFueraPlazo.getActa().getCurso().getId();
        String acta = actaDiligenciaFueraPlazo.getActa().getCodigo();
        String convocatoria = actaDiligenciaFueraPlazo.getActa().getConvocatoria().getNombre();
        String nombreAlumno = actaDiligenciaFueraPlazo.getActaEstudiante().getPersona().getNombreCompleto();

        return String.format("%d - %s (%s) - %s", curso, acta, convocatoria, nombreAlumno);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity insertDiligenciaFueraPlazo(UIEntity entity)
            throws NotaNoValidaException, NoAutorizadoException, TraspasarDiligenciaException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        ActaDiligenciaVirtual actaDiligenciaVirtual = actaDiligenciaService.getActaDiligenciaVirtualByActaEstudianteId(entity.getLong("actaEstudianteId"), connectedUserId);

        Acta acta = actaService.getActaByIdConNotasCalculadas(actaDiligenciaVirtual.getActaId());
        authService.checkPersonaTraspasoODirectorDepartamento(acta, connectedUserId);

        ActaDiligenciaFueraPlazo actaDiligenciaFueraPlazo = getActaDiligenciaFueraPlazo(actaDiligenciaVirtual, entity, connectedUserId, IpUtil.getClientAddres(request));
        actaDiligenciaFueraPlazo.getActaEstudiante().setPersona(new Persona(actaDiligenciaVirtual.getAlumnoId()));
        actaDiligenciaService.checkNumeroMatriculas(actaDiligenciaFueraPlazo);

        actaDiligenciaService.compruebaNotaPuedeTraspasarse(acta, actaDiligenciaFueraPlazo);
        actaDiligenciaFueraPlazo = actaDiligenciaFueraPlazoService.insertActaDiligenciaFueraPlazo(actaDiligenciaFueraPlazo);

        return UIEntity.toUI(actaDiligenciaFueraPlazo);
    }

    private ActaDiligenciaFueraPlazo getActaDiligenciaFueraPlazo(ActaDiligenciaVirtual actaDiligenciaVirtual, UIEntity entity, Long personaId, String ip)
            throws NotaNoValidaException
    {
        ActaDiligenciaFueraPlazo actaDiligenciaFueraPlazo = new ActaDiligenciaFueraPlazo();
        actaDiligenciaFueraPlazo.setActa(new Acta(actaDiligenciaVirtual.getActaId()));
        actaDiligenciaFueraPlazo.setActaEstudiante(new ActaEstudiante(actaDiligenciaVirtual.getActaEstudianteId()));
        actaDiligenciaFueraPlazo.setCalificacionAnteriorId(actaDiligenciaVirtual.getCalificacionId());
        actaDiligenciaFueraPlazo.setNotaAnterior(actaDiligenciaVirtual.getNota());
        actaDiligenciaFueraPlazo.setCalificacionNuevaId(entity.getLong("calificacionId") == 0 ? null : entity.getLong("calificacionId"));
        actaDiligenciaFueraPlazo.setNotaNueva(ParseUtil.parseaNota(entity.get("nota")));
        actaDiligenciaFueraPlazo.setComentarioDiligencia(entity.get("comentario"));
        actaDiligenciaFueraPlazo.setEstado(EstadoDiligenciaFueraPlazo.PENDIENTE.getId());
        actaDiligenciaFueraPlazo.setFecha(new Date());
        actaDiligenciaFueraPlazo.setPersona(new Persona(personaId));
        actaDiligenciaFueraPlazo.setIp(ip);

        return actaDiligenciaFueraPlazo;
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{actaDiligenciaFueraPlazoId}/validar")
    public UIEntity validarActaDiligenciaFueraPlazo(@PathParam("actaDiligenciaFueraPlazoId") Long actaDiligenciaFueraPlazoId, UIEntity entity)
            throws NoAutorizadoException, MessageNotSentException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        ActaDiligenciaFueraPlazo actaDiligenciaFueraPlazo = actaDiligenciaFueraPlazoService.getActaDiligenciaFueraPlazoById(actaDiligenciaFueraPlazoId);
        actaDiligenciaFueraPlazoService.validar(actaDiligenciaFueraPlazo, connectedUserId);
        return UIEntity.toUI(actaDiligenciaFueraPlazo);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{actaDiligenciaFueraPlazoId}/denegar")
    public UIEntity denegarActaDiligenciaFueraPlazo(@PathParam("actaDiligenciaFueraPlazoId") Long actaDiligenciaFueraPlazoId, UIEntity entity)
            throws NoAutorizadoException, MessageNotSentException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        ActaDiligenciaFueraPlazo actaDiligenciaFueraPlazo = actaDiligenciaFueraPlazoService.getActaDiligenciaFueraPlazoById(actaDiligenciaFueraPlazoId);
        actaDiligenciaFueraPlazo.setComentarioDenegacion(entity.get("comentarioDenegacion"));
        actaDiligenciaFueraPlazoService.denegar(actaDiligenciaFueraPlazo, connectedUserId);
        return UIEntity.toUI(actaDiligenciaFueraPlazo);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{actaDiligenciaFueraPlazoId}/firmar")
    public UIEntity firmarActaDiligenciaFueraPlazo(@PathParam("actaDiligenciaFueraPlazoId") Long actaDiligenciaFueraPlazoId, UIEntity entity)
            throws NoAutorizadoException, IOException, TraspasarDiligenciaException, MessageNotSentException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        ActaDiligenciaFueraPlazo actaDiligenciaFueraPlazo = actaDiligenciaFueraPlazoService.getActaDiligenciaFueraPlazoById(actaDiligenciaFueraPlazoId);
        actaDiligenciaFueraPlazoService.firmar(actaDiligenciaFueraPlazo, connectedUserId, IpUtil.getClientAddres(request));
        return UIEntity.toUI(actaDiligenciaFueraPlazo);
    }

    @DELETE
    @Path("{diligenciaFueraPlazoId}")
    public Response deleteActa(@PathParam("diligenciaFueraPlazoId") Long diligenciaFueraPlazoId) throws
            NoAutorizadoException, TraspasarDiligenciaException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        ActaDiligenciaFueraPlazo actaDiligenciaFueraPlazo = actaDiligenciaFueraPlazoService.get(diligenciaFueraPlazoId, connectedUserId);
        authService.checkPuedeTraspasarActa(actaDiligenciaFueraPlazo.getActa(), connectedUserId);
        actaDiligenciaFueraPlazoService.deleteActaDiligenciaFueraPlazo(diligenciaFueraPlazoId, connectedUserId);
        return Response.ok().build();
    }

}
