package es.uji.apps.act.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.act.models.ActaDiligenciaFueraPlazo;
import es.uji.apps.act.models.ui.UIDiligencia;
import es.uji.apps.act.models.ui.UIEmail;
import es.uji.commons.messaging.client.MessageNotSentException;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;


@Service
public class AvisosService
{
    @InjectParam
    private CorreoService correoService;

    public void notificaDiligenciasAlumnos(List<UIDiligencia> diligencias)
            throws MessageNotSentException
    {
        if (diligencias.isEmpty())
        {
            return;
        }

        for (UIDiligencia diligencia : diligencias)
        {
            notificaDiligenciasAlumno(diligencia);
        }
    }

    public void notificaDiligenciasAlumno(UIDiligencia diligencia)
            throws MessageNotSentException
    {
        if (diligencia.getEmail() == null || diligencia.getEmail().isEmpty())
        {
            return;
        }
        String asunto = MessageFormat.format("Diligència assignatura {0}", diligencia.getAsignatura());
        String contenido = MessageFormat.format("S''ha fet una diligència a l''assignatura {0}\nAlumne/a: {1}\nLa nova qualificació és: {2}, {3}.\nMotiu: {4}"
                , diligencia.getAsignatura()
                , diligencia.getAlumno()
                , diligencia.getCalificacion()
                , diligencia.getNota()
                , diligencia.getComentario());
        UIEmail uiEmail = new UIEmail(diligencia.getEmail(), asunto, contenido);
        correoService.enviaEmail(uiEmail);
    }

    public void notificaDiligenciasResponsables(List<UIDiligencia> diligencias, List<String> emailReponsables)
    {
        if (diligencias.isEmpty())
        {
            return;
        }

        List<UIEmail> emails = new ArrayList<>();
        String asunto = MessageFormat.format("Diligència assignatura {0}", diligencias.get(0).getAsignatura());
        String contenido = "S'han fet les següents diligències:\n\n";

        for (UIDiligencia diligencia : diligencias)
        {
            contenido += MessageFormat.format("- Alumne {0}.\nLa nova qualificació és: {1}, {2}.\nMotiu: {3}\n\n"
                    , diligencia.getAlumno()
                    , diligencia.getCalificacion()
                    , diligencia.getNota()
                    , diligencia.getComentario());
        }

        for (String email : emailReponsables)
        {
            UIEmail uiEmail = new UIEmail(email, asunto, contenido);
            emails.add(uiEmail);
        }

        correoService.enviaEmails(emails);
    }

    public void notificaDiligenciaFueraPlazoValidadaProfesor(ActaDiligenciaFueraPlazo actaDiligenciaFueraPlazo)
            throws MessageNotSentException
    {
        String asunto = MessageFormat.format("Diligència fora de termini assignatura {0} pendent de signar", actaDiligenciaFueraPlazo.getActa().getCodigo());
        String contenido = "S'ha aprovat la diligència:\n\n";

        contenido += MessageFormat.format("Alumne {0}.\nLa nova qualificació és: {1}, {2}.\nMotiu: {3}\n\nTens 7 dies per entrar a l''apartat Diligències extres de l''aplicació per a signar-la\n\nhttp://ujiapps.uji.es/act\n"
                , actaDiligenciaFueraPlazo.getActaEstudiante().getPersona().getNombreCompleto()
                , actaDiligenciaFueraPlazo.getCalificacionNuevaAsString()
                , actaDiligenciaFueraPlazo.getNotaNueva()
                , actaDiligenciaFueraPlazo.getComentarioDiligencia());

        UIEmail uiEmail = new UIEmail(actaDiligenciaFueraPlazo.getPersona().getEmail(), asunto, contenido);
        correoService.enviaEmail(uiEmail);
    }

    public void notificaDiligenciaFueraPlazoDenegadaProfesor(ActaDiligenciaFueraPlazo actaDiligenciaFueraPlazo)
            throws MessageNotSentException
    {
        String asunto = MessageFormat.format("Diligència fora de termini assignatura {0} denegada", actaDiligenciaFueraPlazo.getActa().getCodigo());
        String contenido = "S'ha denegat la diligència:\n\n";

        contenido += MessageFormat.format("Alumne {0}.\nLa nova qualificació és: {1}, {2}.\nMotiu diligencia: {3}\n\nMotiu denegació: {4}\n\n"
                , actaDiligenciaFueraPlazo.getActaEstudiante().getPersona().getNombreCompleto()
                , actaDiligenciaFueraPlazo.getCalificacionNuevaAsString()
                , actaDiligenciaFueraPlazo.getNotaNueva()
                , actaDiligenciaFueraPlazo.getComentarioDiligencia()
                , actaDiligenciaFueraPlazo.getComentarioDenegacion());

        UIEmail uiEmail = new UIEmail(actaDiligenciaFueraPlazo.getPersona().getEmail(), asunto, contenido);
        correoService.enviaEmail(uiEmail);
    }

    public void notificaDiligenciaFueraPlazoResponsables(ActaDiligenciaFueraPlazo actaDiligenciaFueraPlazo, List<String> emailRepsponsables)
    {
        List<UIEmail> emails = new ArrayList<>();

        String asunto = MessageFormat.format("Diligència fora de termini assignatura {0}", actaDiligenciaFueraPlazo.getActa().getCodigo());
        String contenido = "Tens un diligència fora de termini pendent d'aprovar:\n\n";

        contenido += MessageFormat.format("Alumne {0}.\nLa nova qualificació és: {1}, {2}.\nMotiu: {3}\n\nEntra a l''apartat Diligències extres de l''aplicació per a aprovar-la o denegar-la\n\nhttp://ujiapps.uji.es/act\n"
                , actaDiligenciaFueraPlazo.getActaEstudiante().getPersona().getNombreCompleto()
                , actaDiligenciaFueraPlazo.getCalificacionNuevaAsString()
                , actaDiligenciaFueraPlazo.getNotaNueva()
                , actaDiligenciaFueraPlazo.getComentarioDiligencia());

        for (String email : emailRepsponsables)
        {
            UIEmail uiEmail = new UIEmail(email, asunto, contenido);
            emails.add(uiEmail);
        }

        correoService.enviaEmails(emails);
    }
}
