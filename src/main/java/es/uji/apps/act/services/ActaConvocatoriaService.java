package es.uji.apps.act.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.act.dao.ActaConvocatoriaDAO;
import es.uji.apps.act.models.views.ActaConvocatoria;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActaConvocatoriaService
{
    @InjectParam
    public ActaConvocatoriaDAO actaConvocatoriaDAO;

    public List<ActaConvocatoria> getActaConvocatoriasByCursoAndCodigo(Long cursoId, String codigo,
            Long connectedUserId)
    {
        return actaConvocatoriaDAO.getActaConvocatoriasByCursoAndCodigo(cursoId, codigo);
    }

    public ActaConvocatoria getActaConvocatoriaByActaId(Long actaId)
    {
        return actaConvocatoriaDAO.getActaConvocatoriaByActaId(actaId);
    }
}
