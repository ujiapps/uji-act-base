package es.uji.apps.act.services.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.act.models.views.ActaConvocatoria;
import es.uji.apps.act.models.views.ActaPersona;
import es.uji.apps.act.services.ActaConvocatoriaService;
import es.uji.apps.act.services.ActaPersonaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("actaconvocatorias")
public class ActaConvocatoriaResource extends CoreBaseService
{
    @InjectParam
    private ActaConvocatoriaService actaConvocatoriaService;

    @InjectParam
    private ActaPersonaService actaPersonaService;

    @PathParam("cursoId")
    Long cursoId;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getActaConvocatoriasByCodigo(@QueryParam("codigo") String codigo)
            throws ParseException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<ActaConvocatoria> actaConvocatorias = actaConvocatoriaService
                .getActaConvocatoriasByCursoAndCodigo(cursoId, codigo, connectedUserId);
        ActaPersona actaPersona = actaPersonaService
                .getActaPersonaByCursoAndAsignaturasAndPersona(cursoId, codigo, connectedUserId);

        return modelToUI(actaConvocatorias, actaPersona);
    }

    public List<UIEntity> modelToUI(List<ActaConvocatoria> actaConvocatorias,
            ActaPersona actaPersona) throws ParseException
    {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

        List<UIEntity> uiEntities = new ArrayList<>();
        for (ActaConvocatoria actaConvocatoria : actaConvocatorias)
        {
            UIEntity uiEntity = UIEntity.toUI(actaConvocatoria);
            Boolean personaAdmin = (actaPersona != null) ? Arrays.asList(new Long[]{831L, 75554L}).contains(actaPersona.getPerId()) : false;
            Boolean personaPuedeTraspasar = (actaPersona != null) ? actaPersona.getPuedeTraspasar()
                    : false;
            uiEntity.put("editable", actaConvocatoria.isEditable());
            uiEntity.put("traspasable", actaConvocatoria.isTraspasable() && personaPuedeTraspasar);
            uiEntity.put("puedePasarNotasAExpediente", personaPuedeTraspasar && actaConvocatoria.isPuedePasarNotasAExpediente());

            if (actaConvocatoria.getMuestraNotasInicio() != null)
            {
                uiEntity.put("muestraNotasHora",
                        sdf.format(actaConvocatoria.getMuestraNotasInicio()));
            }
            uiEntity.put("destraspasable",
                    actaConvocatoria.isDestraspasable() && personaPuedeTraspasar && personaAdmin);
            uiEntity.put("compartida", actaConvocatoria.getAsiCodigos().contains(",")
                    || actaConvocatoria.getAsiCodigos().contains(" i "));
            uiEntity.put("puedeGuardarNotas", actaConvocatoria.isPuedeGuardarNotas());
            uiEntity.put("puedeRecuperarNotas", actaConvocatoria.isPuedeGuardarNotas() && personaPuedeTraspasar);
            uiEntities.add(uiEntity);
        }
        return uiEntities;
    }

}
