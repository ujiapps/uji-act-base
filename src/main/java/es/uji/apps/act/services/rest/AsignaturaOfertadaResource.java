package es.uji.apps.act.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.act.models.views.AsignaturaOfertada;
import es.uji.apps.act.services.AsignaturaOfertadaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("asignaturas-ofertadas")
public class AsignaturaOfertadaResource extends CoreBaseService
{
    @InjectParam
    private AsignaturaOfertadaService asignaturaOfertadaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAsignaturasPersona(@QueryParam("cursoId") Long cursoId, @QueryParam("estudioId") Long estudioId)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<AsignaturaOfertada> asignaturasOfertadas = asignaturaOfertadaService.getAsignaturasCursadasByCurso(cursoId, estudioId);
        return UIEntity.toUI(asignaturasOfertadas);
    }
}
