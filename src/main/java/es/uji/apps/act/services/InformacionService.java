package es.uji.apps.act.services;

import java.util.List;

import es.uji.apps.act.dao.InformacionDAO;
import es.uji.apps.act.models.Convocatoria;
import es.uji.apps.act.models.Informacion;
import org.springframework.stereotype.Service;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.act.dao.ActaConvocatoriaDAO;
import es.uji.apps.act.models.views.ActaConvocatoria;

@Service
public class InformacionService
{
    @InjectParam
    public InformacionDAO informacionDAO;

    public List<Informacion> getNotificacionesActivas(Long connectedUserId)
    {
        return informacionDAO.getNotificacionesActivas();
    }
}
