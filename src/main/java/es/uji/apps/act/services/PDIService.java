package es.uji.apps.act.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.act.dao.PDIDAO;
import es.uji.apps.act.dao.ResponsableAsignaturaDAO;
import es.uji.apps.act.exceptions.ActaNoEditableException;
import es.uji.apps.act.exceptions.NoAutorizadoException;
import es.uji.apps.act.models.PDIActivo;
import es.uji.apps.act.models.enums.Acceso;
import es.uji.apps.act.models.views.ResponsableAsignatura;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PDIService {
    @InjectParam
    public PDIDAO pdidao;

    @InjectParam
    private AuthService authService;

    public List<PDIActivo> findReponsablesAsignatura(String query, Long connectedUserId) throws NoAutorizadoException {
        authService.checkIsAdmin(connectedUserId);
        return pdidao.findPdiActivo(query);
    }
}
