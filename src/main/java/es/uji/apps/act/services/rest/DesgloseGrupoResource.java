package es.uji.apps.act.services.rest;

import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.act.exceptions.ActaNoEditableException;
import es.uji.apps.act.exceptions.FormatoNoValidoException;
import es.uji.apps.act.exceptions.InconsistenciaDeDatosException;
import es.uji.apps.act.exceptions.NoAutorizadoException;
import es.uji.apps.act.models.Acta;
import es.uji.apps.act.models.DesgloseGrupo;
import es.uji.apps.act.models.enums.Acceso;
import es.uji.apps.act.services.DesgloseGrupoService;
import es.uji.apps.act.services.AuthService;
import es.uji.apps.act.services.ParseService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;

@Path("desglosegrupos")
public class DesgloseGrupoResource extends CoreBaseService
{
    @InjectParam
    private DesgloseGrupoService desgloseGrupoService;

    @InjectParam
    private AuthService authService;

    @PathParam("actaId")
    Long actaId;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getDesgloseGrupos()
            throws NoAutorizadoException, ActaNoEditableException, RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkPuedeAccederActa(Acceso.LECTURA, actaId, connectedUserId);
        List<DesgloseGrupo> listaDesgloseGrupos = desgloseGrupoService.getByActaId(actaId);
        return UIEntity.toUI(listaDesgloseGrupos);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity insertDesgloseGrupo(UIEntity entity)
            throws NoAutorizadoException, InconsistenciaDeDatosException, ActaNoEditableException,
            RegistroNoEncontradoException, FormatoNoValidoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkPuedeAccederActa(Acceso.ESCRITURA, actaId, connectedUserId);
        DesgloseGrupo desgloseGrupo = uiToModel(entity);
        desgloseGrupoService.insert(desgloseGrupo, connectedUserId);
        return UIEntity.toUI(desgloseGrupo);
    }

    @PUT
    @Path("{desgloseGrupoId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateDesgloseGrupo(@PathParam("desgloseGrupoId") Long desgloseGrupoId,
                                        UIEntity entity) throws NoAutorizadoException, InconsistenciaDeDatosException,
            ActaNoEditableException, RegistroNoEncontradoException, FormatoNoValidoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkPuedeAccederActa(Acceso.ESCRITURA, actaId, connectedUserId);
        DesgloseGrupo desgloseGrupo = uiToModel(entity);
        desgloseGrupoService.update(desgloseGrupo, connectedUserId);
        return UIEntity.toUI(desgloseGrupo);
    }

    @DELETE
    @Path("{desgloseGrupoId}")
    public Response deleteDesgloseGrupo(@PathParam("desgloseGrupoId") Long desgloseGrupoId,
                                        UIEntity entity)
            throws NoAutorizadoException, ActaNoEditableException, RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkPuedeAccederActa(Acceso.ESCRITURA, actaId, connectedUserId);
        desgloseGrupoService.delete(desgloseGrupoId, connectedUserId);
        return Response.ok().build();
    }

    private DesgloseGrupo uiToModel(UIEntity uiEntity) throws FormatoNoValidoException
    {
        DesgloseGrupo desgloseGrupo = new DesgloseGrupo();
        desgloseGrupo.setId(uiEntity.getLong("id"));
        desgloseGrupo.setActa(new Acta(actaId));

        if (uiEntity.get("etiqueta").length() > 10)
        {
            throw new FormatoNoValidoException(
                    "L'etiqueta no pot tenir una llargaria superior a 10 caràcters.");
        }

        desgloseGrupo.setEtiqueta(uiEntity.get("etiqueta"));
        desgloseGrupo.setNombre(uiEntity.get("nombre"));
        desgloseGrupo.setOrden(uiEntity.getLong("orden"));
        desgloseGrupo.setNotaMaxima(ParseService.parsearDouble(uiEntity.get("notaMaxima")));
        desgloseGrupo.setNotaMinima(ParseService.parsearDouble(uiEntity.get("notaMinima")));
        desgloseGrupo.setNotaMinimaAprobado(
                ParseService.parsearDouble(uiEntity.get("notaMinimaAprobado")));
        desgloseGrupo.setPeso(ParseService.parsearDouble(uiEntity.get("peso")));
        desgloseGrupo.setVisible(uiEntity.getBoolean("visible"));
        return desgloseGrupo;
    }

}