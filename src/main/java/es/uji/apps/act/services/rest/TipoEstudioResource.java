package es.uji.apps.act.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.act.exceptions.InconsistenciaDeDatosException;
import es.uji.apps.act.exceptions.NoAutorizadoException;
import es.uji.apps.act.models.TipoEstudio;
import es.uji.apps.act.services.AuthService;
import es.uji.apps.act.services.TipoEstudioService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("tipoEstudios")
public class TipoEstudioResource extends CoreBaseService
{
    @InjectParam
    private TipoEstudioService tipoEstudioService;

    @InjectParam
    private AuthService authService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTipoEstudios() throws NoAutorizadoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkIsAdmin(connectedUserId);
        List<TipoEstudio> listaTipoEstudios = tipoEstudioService.getAll(connectedUserId);
        return UIEntity.toUI(listaTipoEstudios);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity insertTipoEstudio(UIEntity entity)
            throws NoAutorizadoException, InconsistenciaDeDatosException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkIsAdmin(connectedUserId);
        TipoEstudio tipoEstudio = entity.toModel(TipoEstudio.class);
        tipoEstudioService.insert(tipoEstudio, connectedUserId);
        return UIEntity.toUI(tipoEstudio);
    }

    @PUT
    @Path("{tipoEstudioId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateTipoEstudio(@PathParam("tipoEstudioId") Long tipoEstudioId,
            UIEntity entity) throws NoAutorizadoException, InconsistenciaDeDatosException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkIsAdmin(connectedUserId);
        TipoEstudio tipoEstudio = entity.toModel(TipoEstudio.class);
        tipoEstudioService.update(tipoEstudio, connectedUserId);
        return UIEntity.toUI(tipoEstudio);
    }

    @DELETE
    @Path("{tipoEstudioId}")
    public Response deleteTipoEstudio(@PathParam("tipoEstudioId") Long tipoEstudioId,
            UIEntity entity) throws NoAutorizadoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkIsAdmin(connectedUserId);
        tipoEstudioService.delete(tipoEstudioId, connectedUserId);
        return Response.ok().build();
    }
}