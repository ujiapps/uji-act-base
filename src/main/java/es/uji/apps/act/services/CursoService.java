package es.uji.apps.act.services;

import es.uji.apps.act.dao.CursoDAO;
import es.uji.apps.act.models.Curso;
import es.uji.commons.rest.Role;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CursoService extends BaseService<Curso>
{
    @Autowired
    public CursoService(CursoDAO dao)
    {
        super(dao, Curso.class);
    }

    @Role({"USUARIO", "ADMIN"})
    public List<Curso> getCursosActivos(Long connectedUserId)
    {
        return ((CursoDAO) dao).getCursosActivos();
    }

    public List<Curso> getCursosDiligenciasFueraPlazo(Long connectedUserId)
    {
        return ((CursoDAO) dao).getCursosDiligenciasFueraPlazo();
    }
}