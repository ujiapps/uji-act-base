package es.uji.apps.act.services;

import java.util.List;

import es.uji.apps.act.exceptions.InconsistenciaDeDatosException;
import es.uji.commons.db.BaseDAO;

public class BaseService<T>
{
    protected BaseDAO dao;
    protected Class<T> modelClass;

    public BaseService(BaseDAO dao, Class<T> modelClass)
    {
        this.dao = dao;
        this.modelClass = modelClass;
    }

    public List<T> getAll(Long connectedUserId)
    {
        return dao.get(modelClass);
    }

    public T get(Long id, Long connectedUserId)
    {
        List<T> items = dao.get(modelClass, id);
        return (items.isEmpty()) ? null : items.get(0);
    }

    public T insert(T model, Long connectedUserId) throws InconsistenciaDeDatosException
    {
        return dao.insert(model);
    }

    public T update(T model, Long connectedUserId) throws InconsistenciaDeDatosException
    {
        return dao.update(model);
    }

    public void delete(Long id, Long connectedUserId)
    {
        dao.delete(modelClass, id);
    }
}