package es.uji.apps.act.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.act.dao.ActaPersonaDAO;
import es.uji.apps.act.models.views.ActaPersona;

public class ActaPersonaService
{
    @InjectParam
    public ActaPersonaDAO actaPersonaDAO;

    public ActaPersona getActaPersonaByCursoAndAsignaturasAndPersona(Long cursoId, String asiCodigos, Long personaId)
    {
        return actaPersonaDAO.getActaPersonaByCursoAndAsignaturasAndPersona(cursoId, asiCodigos, personaId);
    }
}
