package es.uji.apps.act.services;

import es.uji.apps.act.dao.ActaPersonaDAO;
import es.uji.apps.act.dao.ConvocatoriaDAO;
import es.uji.apps.act.dao.FechaConvocatoriaDAO;
import es.uji.apps.act.models.Convocatoria;
import es.uji.apps.act.models.FechaConvocatoria;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ConvocatoriaService extends BaseService<Convocatoria> {
    private FechaConvocatoriaDAO fechaConvocatoriaDAO;

    @Autowired
    public ConvocatoriaService(ConvocatoriaDAO dao, FechaConvocatoriaDAO fechaConvocatoriaDAO) {
        super(dao, Convocatoria.class);
        this.fechaConvocatoriaDAO = fechaConvocatoriaDAO;
    }

    public List<Convocatoria> getConvocatorias() {
        return ((ConvocatoriaDAO) dao).get(Convocatoria.class);
    }

    public List<Convocatoria> getConvocatoriasOficiales() {
        return ((ConvocatoriaDAO) dao).getConvocatoriasOficiales();
    }

    public List<FechaConvocatoria> getFechasConvocatoriasOficiales(Long cursoId) {
        return fechaConvocatoriaDAO.getConvocatoriasOficiales(cursoId);
    }

    public FechaConvocatoria updateFechasConvocatoria(Long convocatoriaDetalleId, Date fechaInicioTraspaso, Date fechaFinTraspaso, Date fechaInicioActaUnica) {
        FechaConvocatoria fechaConvocatoria = fechaConvocatoriaDAO.getConvocatoriaById(convocatoriaDetalleId);

        fechaConvocatoria.setFechaInicioTraspaso(fechaInicioTraspaso);
        fechaConvocatoria.setFechaFinTraspaso(fechaFinTraspaso);
        fechaConvocatoria.setFechaInicioActaUnica(fechaInicioActaUnica);


        return fechaConvocatoriaDAO.update(fechaConvocatoria);
    }
}