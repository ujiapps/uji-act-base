package es.uji.apps.act.services;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import org.springframework.stereotype.Service;

@Service
public class ParseService
{
    public static Double parsearDouble(String nota)
    {
        if (nota == null)
        {
            return null;
        }

        try
        {
            String notaDosDecimales = String.format("%.2f", Double.parseDouble(nota));
            NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);
            return format.parse(notaDosDecimales).doubleValue();
        }
        catch (NumberFormatException e)
        {
            return null;
        }
        catch (ParseException e)
        {
            return null;
        }
    }
}