package es.uji.apps.act.services.rest;

import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.act.models.Informacion;
import es.uji.apps.act.services.InformacionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("informaciones")
public class InformacionResource extends CoreBaseService
{
    @InjectParam
    private InformacionService informacionService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getInformaciones()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Informacion> informaciones = informacionService
                .getNotificacionesActivas(connectedUserId);

        return UIEntity.toUI(informaciones);
    }

}
