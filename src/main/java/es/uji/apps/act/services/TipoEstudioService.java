package es.uji.apps.act.services;

import es.uji.apps.act.dao.TipoEstudioDAO;
import es.uji.apps.act.models.TipoEstudio;
import es.uji.commons.rest.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TipoEstudioService extends BaseService<TipoEstudio>
{
    @Autowired
    public TipoEstudioService(TipoEstudioDAO dao)
    {
        super(dao, TipoEstudio.class);
    }
}