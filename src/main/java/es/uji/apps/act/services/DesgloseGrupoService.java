package es.uji.apps.act.services;

import es.uji.apps.act.dao.DesgloseGrupoDAO;
import es.uji.apps.act.models.DesgloseGrupo;
import es.uji.apps.act.models.DesglosePregunta;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DesgloseGrupoService extends BaseService<DesgloseGrupo>
{
    @Autowired
    public DesgloseGrupoService(DesgloseGrupoDAO dao)
    {
        super(dao, DesgloseGrupo.class);
    }

    public List<DesgloseGrupo> getByActaId(Long actaId)
    {
        return ((DesgloseGrupoDAO) dao).getDesgloseGruposByActaId(actaId);
    }

    public void importarConfiguracionDesglose(Long actaOrigenId)
    {
        List<DesgloseGrupo> grupos = getByActaId(actaOrigenId);
        for (DesgloseGrupo grupo : grupos)
        {
            // Clonado y alta de los grupos
            DesgloseGrupo desgloseGrupoNuevo = grupo.clonar();
            for (DesglosePregunta pregunta : grupo.getDesglosePreguntas())
            {
                // Clonado y alta de las preguntas
                DesglosePregunta desglosePreguntaNueva = pregunta.clonar();
                desglosePreguntaNueva.setDesgloseGrupo(desgloseGrupoNuevo);
            }
            dao.insert(grupo);
        }
        // Persistir cambios
    }
}