package es.uji.apps.act.services;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.act.dao.ActaAsignaturaDAO;
import es.uji.apps.act.dao.ActaDAO;
import es.uji.apps.act.dao.ActaEstudianteDAO;
import es.uji.apps.act.exceptions.ExpedienteBloqueadoException;
import es.uji.apps.act.exceptions.NotaNoValidaException;
import es.uji.apps.act.exceptions.SincronizarActaException;
import es.uji.apps.act.exceptions.TraspasarActaException;
import es.uji.apps.act.models.Acta;
import es.uji.apps.act.models.ActaEstudiante;
import es.uji.apps.act.models.ActaRevision;
import es.uji.apps.act.models.DesgloseGrupo;
import es.uji.apps.act.models.DesgloseNota;
import es.uji.apps.act.models.DesglosePregunta;
import es.uji.apps.act.models.FechaConvocatoria;
import es.uji.apps.act.models.NotaDesgloseCalculada;
import es.uji.apps.act.models.Persona;
import es.uji.apps.act.models.RemoteBorrarPasoNotaExpedienteProcedure;
import es.uji.apps.act.models.RemoteSincronizarActaProcedure;
import es.uji.apps.act.models.views.ActaEstudianteOrigen;
import es.uji.apps.act.models.views.AsignaturaCursada;

@Service
public class ActaEstudianteService extends BaseService<ActaEstudiante>
{
    private ActaAsignaturaDAO actaAsignaturaDAO;

    private ActaDAO actaDAO;

    private LogService logService;

    private AsignaturaCursadaService asignaturaCursadaService;

    private Map<Long, List<ActaEstudiante>> actaEstudiantesCache;

    @Autowired
    public ActaEstudianteService(ActaEstudianteDAO dao, ActaAsignaturaDAO actaAsignaturaDAO,
                                 ActaDAO actaDAO, LogService logService, AsignaturaCursadaService asignaturaCursadaService)
    {
        super(dao, ActaEstudiante.class);
        this.actaAsignaturaDAO = actaAsignaturaDAO;
        this.actaDAO = actaDAO;
        this.logService = logService;
        this.asignaturaCursadaService = asignaturaCursadaService;
        actaEstudiantesCache = new HashMap<>();
    }

    public List<ActaEstudiante> getActaEstudianteByActaId(Long actaId, Long connectedUserId)
    {
        List<ActaEstudiante> actaEstudiantes = ((ActaEstudianteDAO) dao)
                .getActaEstudiantesByActaId(actaId);
        actaEstudiantesCache.clear();
        actaEstudiantes.stream()
                .forEach(ae -> ae.setNotaDesgloseCalculada(getNotaDesgloseCalculada(ae)));
        cargaInformacionExpediente(actaId, actaEstudiantes);
        return actaEstudiantes;
    }

    protected void cargaInformacionExpediente(Long actaId, List<ActaEstudiante> estudiantes)
    {
        List<AsignaturaCursada> asignaturasCursadas = asignaturaCursadaService.getAsignaturasCursadasByActaId(actaId);

        for (ActaEstudiante actaEstudiante : estudiantes)
        {
            Optional<AsignaturaCursada> asignaturaCursada = asignaturasCursadas.stream().filter(ac -> ac.getPersonaId().equals(actaEstudiante.getPersona().getId())).findFirst();
            if (asignaturaCursada.isPresent())
            {
                actaEstudiante.setExpedienteAbierto(asignaturaCursada.get().isExpedienteAbierto());
            }
            else
            {
                actaEstudiante.setExpedienteAbierto(false);
            }
        }
    }

    public List<ActaEstudiante> getActaEstudianteByActaRevision(ActaRevision actaRevision,
                                                                Long connectedUserId)
    {
        List<ActaEstudiante> actaEstudiantes = ((ActaEstudianteDAO) dao)
                .getActaEstudiantesByActaRevision(actaRevision);
        actaEstudiantes.stream()
                .forEach(ae -> ae.setNotaDesgloseCalculada(getNotaDesgloseCalculada(ae)));
        cargaInformacionExpediente(actaRevision.getActa().getId(), actaEstudiantes);
        return actaEstudiantes;
    }

    public void calculaNotasCalculadas(Acta acta)
    {
        List<ActaEstudiante> actaEstudiantes = acta.getActaEstudiantes();
        actaEstudiantesCache.clear();
        actaEstudiantes.stream()
                .forEach(ae -> ae.setNotaDesgloseCalculada(getNotaDesgloseCalculada(ae)));
        cargaInformacionExpediente(acta.getId(), actaEstudiantes);
    }

    public ActaEstudiante getActaEstudianteById(Long actaEstudianteId, Long connectedUserId)
    {
        ActaEstudiante actaEstudiante = ((ActaEstudianteDAO) dao)
                .getActaEstudianteById(actaEstudianteId);
        actaEstudiante.setNotaDesgloseCalculada(getNotaDesgloseCalculada(actaEstudiante));

        AsignaturaCursada asignaturaCursada = asignaturaCursadaService.getAsignaturaCursadasByActaIdAndAlumno(actaEstudiante.getActaAsignatura().getActa().getId(), actaEstudiante.getPersona().getId());
        actaEstudiante.setExpedienteAbierto((asignaturaCursada != null) ? asignaturaCursada.isExpedienteAbierto() : false);
        return actaEstudiante;
    }

    public ActaEstudiante updateNota(ActaEstudiante actaEstudiante, Double nuevaNota,
                                     Boolean noPresentado, Boolean matriculaHonor, String comentario, Long connectedUserId,
                                     String ip) throws NotaNoValidaException
    {
        if (nuevaNota != null)
        {
            nuevaNota = Math.floor(nuevaNota * 10) / 10.0;
        }
        actaEstudiante.setNota(nuevaNota);
        actaEstudiante.setMatriculaHonor(matriculaHonor);
        actaEstudiante.setNoPresentado(noPresentado);
        actaEstudiante.setComentario(comentario);
        actaEstudiante.setIp(ip);
        Persona personaNota = new Persona(connectedUserId);
        actaEstudiante.setPersonaNota(personaNota);
        Date fechaActual = new Date();
        actaEstudiante.setFechaNota(fechaActual);

        Double nota = actaEstudiante.getNotaEfectiva();

        if ((nota != null && nota < 9 && actaEstudiante.isMatriculaHonor()))
        {
            throw new NotaNoValidaException(
                    "No es pot assignar una matrícula d'honor per a notes inferiors a 9");
        }

        if ((nota == null || actaEstudiante.isNoPresentado()) || (nota != null && nota < 9))
        {
            actaEstudiante.setMatriculaHonor(false);
        }

        if (nota != null && (nota < 0 || nota > 10))
        {
            throw new NotaNoValidaException("La nota no pot ser inferior a 0 ni superior a 10");
        }

        dao.update(actaEstudiante);
        logService.registraNota(actaEstudiante, connectedUserId, ip);
        return actaEstudiante;
    }

    @Transactional
    public void sincronizaEstudiantes(Long actaId, Long connectedUserId)
            throws IOException, SincronizarActaException
    {
        Acta acta = actaDAO.getActaById(actaId);
        if (acta == null || !acta.sePuedeSincronizar())
        {
            return;
        }

        RemoteSincronizarActaProcedure remoteSincronizarActaProcedure = new RemoteSincronizarActaProcedure();
        remoteSincronizarActaProcedure.init();
        remoteSincronizarActaProcedure.execute(actaId, connectedUserId);
    }

    protected Boolean seDebeActualizar(ActaEstudiante actaEstudiante,
                                       ActaEstudianteOrigen actaEstudianteOrigen)
    {
        if (!actaEstudiante.getGrupoId().equals(actaEstudianteOrigen.getGrupoId()))
        {
            return true;
        }

        String actaMotivoExclusion = actaEstudiante.getMotivoExclusion();
        String actaOrigenMotivoExclusion = actaEstudianteOrigen.getMotivoExclusion();

        if (actaMotivoExclusion == null && actaOrigenMotivoExclusion != null)
        {
            return true;
        }

        if (actaMotivoExclusion != null && !actaMotivoExclusion.equals(actaOrigenMotivoExclusion))
        {
            return true;
        }

        return false;
    }

    private NotaDesgloseCalculada getNotaDesgloseCalculada(ActaEstudiante actaEstudiante)
    {
        Double NOTA_NO_CUMPLE = 4.0;
        Acta acta = actaEstudiante.getActaAsignatura().getActa();
        if (!acta.isDesgloseActivo() || acta.getPreguntasOrdenadas().isEmpty())
        {
            return new NotaDesgloseCalculada(actaEstudiante.getNota());
        }

        if (actaEstudiante.getNumeroNotasDesgloseIntroducidas() == 0)
        {
            return new NotaDesgloseCalculada(null);
        }

        Double nota = 0.0;
        Boolean cumpleNotaMinima = true;
        List<String> resultadoGrupo = new ArrayList<>();
        String errorMsg;
        BigDecimal notaBD = BigDecimal.valueOf(nota);

        for (DesgloseGrupo grupo : acta.getDesglosesGrupos().stream()
                .sorted(Comparator.comparing(DesgloseGrupo::getOrden)).collect(Collectors.toList()))
        {
            NotaDesgloseCalculada notaGrupo = calcularNotaGrupoDesglose(actaEstudiante, grupo);
            if (notaGrupo.getNota().compareTo(grupo.getNotaMinimaAprobado()) < 0)
            {
                cumpleNotaMinima = false;
                errorMsg = MessageFormat.format(
                        "<b><span style=\"color:red\">({0}<{1} No compleix)</span></b>",
                        String.format("%.1f", notaGrupo.getNota()).replace(",", "."),
                        grupo.getNotaMinimaAprobado());
            }
            else
            {
                errorMsg = "";
            }
            notaBD = notaBD.add(BigDecimal.valueOf(notaGrupo.getNota()).multiply(BigDecimal.valueOf(grupo.getPeso())).divide(BigDecimal.valueOf(100.0), RoundingMode.HALF_DOWN));
            resultadoGrupo.add(MessageFormat.format("({0}) * {1}% {2}", notaGrupo.getResultado(),
                    grupo.getPeso(), errorMsg));
        }
        nota = notaBD.doubleValue();
        if (!cumpleNotaMinima && acta.getDesglosesGrupos().size() > 1)
        {
            nota = nota.compareTo(NOTA_NO_CUMPLE) < 0 ? nota : NOTA_NO_CUMPLE;
        }
        if (actaEstudiante.getId() == 2052613) {
            System.out.println("Redondeo: " + nota.toString());
        }

        Double notaRedondeada = Math.floor(nota * 10) / 10.0;
        return new NotaDesgloseCalculada(notaRedondeada, MessageFormat.format("{0} = {1}",
                String.join(" + ", resultadoGrupo), notaRedondeada.toString().replace(",", ".")));
    }

    private NotaDesgloseCalculada calcularNotaGrupoDesglose(ActaEstudiante actaEstudiante,
                                                            DesgloseGrupo grupo)
    {
        if (grupo.getDesglosePreguntas().isEmpty())
        {
            return new NotaDesgloseCalculada(0.0, "");
        }
        Double notaGrupo = 0.0;
        List<String> resultadoGrupo = new ArrayList<>();
        for (DesglosePregunta pregunta : grupo.getDesglosePreguntas().stream()
                .sorted(Comparator.comparing(DesglosePregunta::getOrden))
                .collect(Collectors.toList()))
        {
            Double notaPregunta;
            if (pregunta.getParcialActa() != null)
            {
                ActaEstudiante actaEstudianteParcial = getActaEstudianteCached(
                        pregunta.getParcialActa().getId(), actaEstudiante.getPersona().getId());

                if (actaEstudianteParcial != null)
                {
                    if (actaEstudianteParcial.getNota() != null)
                    {
                        notaPregunta = actaEstudianteParcial.getNota();
                    }
                    else
                    {
                        NotaDesgloseCalculada notaDesgloseCalculada = getNotaDesgloseCalculada(
                                actaEstudianteParcial);
                        notaPregunta = notaDesgloseCalculada.getNota();
                    }
                }
                else
                {
                    notaPregunta = null;
                }
            }
            else
            {
                DesgloseNota desgloseNotaOptional = actaEstudiante
                        .getDesgloseNotaByPreguntaId(pregunta.getId());

                notaPregunta = (desgloseNotaOptional != null) ? desgloseNotaOptional.getNota()
                        : null;
            }
            if (notaPregunta == null)
            {
                notaPregunta = 0.0;
            }
            if (notaGrupo != null)
            {
                notaGrupo += notaPregunta * pregunta.getPeso() / 100.0;
            }
            resultadoGrupo.add(MessageFormat.format("({0} * {1}%)",
                    notaPregunta.toString().replace(",", "."), pregunta.getPeso()));
        }

        return new NotaDesgloseCalculada(notaGrupo, String.join(" + ", resultadoGrupo));
    }

    protected ActaEstudiante getActaEstudianteCached(Long actaId, Long estudianteId)
    {
        if (!actaEstudiantesCache.containsKey(actaId))
        {
            actaEstudiantesCache.put(actaId,
                    ((ActaEstudianteDAO) dao).getActaEstudiantesByActaId(actaId));
        }

        List<ActaEstudiante> actaEstudiantes = actaEstudiantesCache.get(actaId);

        return actaEstudiantes.stream().filter(ae -> ae.getPersona().getId().equals(estudianteId))
                .findFirst().orElse(null);
    }

    public void borrarTraspaso(ActaEstudiante actaEstudiante, Long connectedUserId)
            throws TraspasarActaException, IOException, ExpedienteBloqueadoException
    {
        if (!actaEstudiante.isExpedienteAbierto())
        {
            throw new ExpedienteBloqueadoException("No es pot eliminar el traspàs perque el expedient de l'alumne està bloquejat.");
        }
        Acta acta = actaEstudiante.getActaAsignatura().getActa();
        if (!acta.isActaUnica())
        {
            throw new TraspasarActaException("No es pot eliminar el traspàs d'una acta no única");
        }
        if (acta.getFechaTraspaso() != null)
        {
            throw new TraspasarActaException(
                    "No es pot eliminar el traspàs d'una acta ja traspasada");
        }
        Date ahora = new Date();
        FechaConvocatoria fechaConvocatoria = acta.getFechaConvocatoria();
        if (fechaConvocatoria.getFechaInicioActaUnica().after(ahora))
        {
            throw new TraspasarActaException(
                    "No es permet el canvi fins després de la data d'inici de pas de notes");
        }

        RemoteBorrarPasoNotaExpedienteProcedure remoteBorrarPasoNotaExpedienteProcedure = new RemoteBorrarPasoNotaExpedienteProcedure();
        remoteBorrarPasoNotaExpedienteProcedure.init();
        remoteBorrarPasoNotaExpedienteProcedure.execute(actaEstudiante.getId());
    }
}