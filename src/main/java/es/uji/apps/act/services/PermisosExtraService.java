package es.uji.apps.act.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.act.dao.PermisosExtraDAO;
import es.uji.apps.act.exceptions.NoAutorizadoException;
import es.uji.apps.act.models.PermisosExtra;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class PermisosExtraService extends BaseService<PermisosExtra> {
    @InjectParam
    private AuthService authService;

    @InjectParam
    public PermisosExtraDAO permisosExtraDAO;

    @Autowired
    public PermisosExtraService(PermisosExtraDAO dao) {
        super(dao, PermisosExtra.class);
    }


    public List<PermisosExtra> getPermisosExtra(Long connectedUserId) throws NoAutorizadoException {
        authService.checkIsAdmin(connectedUserId);
        return permisosExtraDAO.getPermisosExtra();
    }


    public void deletePermisoExtra(Long permisoExtraId, Long connectedUserId) throws NoAutorizadoException {

        authService.checkIsAdmin(connectedUserId);
        this.delete(permisoExtraId, connectedUserId);
    }

    public List<PermisosExtra> getPermisosExtraByConnectedUserId(Long connectedUserId) {
        return permisosExtraDAO.getPermisosExtraByConnectedUserId(connectedUserId);
    }

    public PermisosExtra updateFechaFin(Long permisoExtraId, Date fechaFin, Long connecteduserId) {
        PermisosExtra permisoExtra = permisosExtraDAO.get(PermisosExtra.class, permisoExtraId).get(0);

        permisoExtra.setFechaFin(fechaFin);
        return permisosExtraDAO.update(permisoExtra);
    }
}
