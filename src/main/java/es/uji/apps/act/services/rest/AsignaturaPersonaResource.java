package es.uji.apps.act.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.act.models.views.ActaEstudianteOrigen;
import es.uji.apps.act.services.AsignaturaPersonaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("asignaturas-persona")
public class AsignaturaPersonaResource extends CoreBaseService
{
    @InjectParam
    private AsignaturaPersonaService asignaturaPersonaService;

    @PathParam("cursoId")
    Long cursoId;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAsignaturasPersona()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<ActaEstudianteOrigen> listaConvocatorias = asignaturaPersonaService
                .getAsignaturasPersonaByCursoId(cursoId, connectedUserId);
        return UIEntity.toUI(listaConvocatorias);
    }
}
