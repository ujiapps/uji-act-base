package es.uji.apps.act.services;

import es.uji.apps.act.dao.*;
import org.springframework.stereotype.Service;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.act.exceptions.ActaNoEditableException;
import es.uji.apps.act.exceptions.NoAutorizadoException;
import es.uji.apps.act.models.Acta;
import es.uji.apps.act.models.ActaEstudiante;
import es.uji.apps.act.models.ActaRevision;
import es.uji.apps.act.models.enums.Acceso;
import es.uji.apps.act.models.views.ActaPersona;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.dao.ApaDAO;

@Service
public class AuthService extends CoreBaseService
{
    @InjectParam
    public ApaDAO apaDAO;

    @InjectParam
    public ActaPersonaDAO actaPersonaDAO;

    @InjectParam
    public PermisosExtraDAO permisosExtraDAO;

    @InjectParam
    public ActaDAO actaDAO;

    @InjectParam
    public ActaEstudianteDAO actaEstudianteDAO;

    @InjectParam
    public ActaRevisionDAO actaRevisionDAO;

    @InjectParam
    public ActaDiligenciaFueraPlazoDAO actaDiligenciaFueraPlazoDAO;

    public void checkPuedeTraspasarActa(Acta acta, Long personaId)
            throws NoAutorizadoException
    {
        ActaPersona actaPersona = actaPersonaDAO.getActaPersonaByCursoAndAsignaturasAndPersona(
                acta.getCurso().getId(), acta.getCodigo(), personaId);
        Boolean isPermisoExtraActa = permisosExtraDAO.isReponsableActa(acta.getId(), personaId);

        if (!isPermisoExtraActa && (actaPersona == null || !actaPersona.puedeTraspasar))
        {
            throw new NoAutorizadoException("No tens permís de traspàs de l'acta");
        }
    }

    public void checkPersonaTraspasoODirectorDepartamento(Acta acta, Long personaId)
            throws NoAutorizadoException
    {
        Boolean isMismaPersonaTraspasoActa = acta.getPersonaIdTraspaso().equals(personaId);
        Boolean isDirectorDepartamentoActa = actaDiligenciaFueraPlazoDAO.getResponsablesByActaId(acta.getId()).stream().anyMatch(r -> r.getDirectorDepartamentoId().equals(personaId));
        Boolean isPermisoExtraActa = permisosExtraDAO.isReponsableActa(acta.getId(), personaId);

        if (!isMismaPersonaTraspasoActa && !isDirectorDepartamentoActa && !isPermisoExtraActa)
        {
            throw new NoAutorizadoException("Només el responsable de l'acta i el director de departament pot crear la diligència fora de termini");
        }
    }

    public void checkDirectorCentro(Acta acta, Long personaId) throws NoAutorizadoException
    {
        Boolean isDirectorCentroActa = actaDiligenciaFueraPlazoDAO.getResponsablesByActaId(acta.getId()).stream().anyMatch(r -> r.getDirectorCentroId().equals(personaId));
        if (!isDirectorCentroActa)
        {
            throw new NoAutorizadoException("No autoritzat. Només el director de centre pot realitzar la acció");
        }
    }

    public void checkPuedeModificarMatriculaHonor(ActaEstudiante actaEstudiante, Long personaId)
            throws NoAutorizadoException
    {
        Acta acta = actaEstudiante.getActaAsignatura().getActa();
        ActaPersona actaPersona = actaPersonaDAO.getActaPersonaByCursoAndAsignaturasAndPersona(
                acta.getCurso().getId(), acta.getCodigo(), personaId);
        if (actaPersona == null || !actaPersona.puedeTraspasar)
        {
            throw new NoAutorizadoException(
                    "No tens permís per assignar / treure matrícules d'honor");
        }
    }

    public void checkPuedeAccederActa(Acceso acceso, Long actaId, Long personaId)
            throws NoAutorizadoException, ActaNoEditableException, RegistroNoEncontradoException
    {
        Acta acta = actaDAO.getActaById(actaId);
        if (acta == null)
        {
            throw new RegistroNoEncontradoException("No s'ha trobat l'acta");
        }

        if (acceso == Acceso.ESCRITURA && !acta.isEditable())
        {
            throw new ActaNoEditableException();
        }

        if (isAdmin(personaId))
        {
            return;
        }
        ActaPersona actaPersona = actaPersonaDAO.getActaPersonaByCursoAndCodigosAndPersona(
                acta.getCurso().getId(), acta.getCodigo(), personaId);
        if (actaPersona == null)
        {
            throw new NoAutorizadoException("No tens permís per accedir a l'acta");
        }
    }

    public void checkPuedeAccederActaEstudiante(Acceso acceso, Long actaEstudianteId,
                                                Long personaId) throws NoAutorizadoException, ActaNoEditableException
    {
        ActaEstudiante actaEstudiante = actaEstudianteDAO.getActaEstudianteById(actaEstudianteId);
        if (actaEstudiante == null)
        {
            return;
        }

        Acta acta = actaEstudiante.getActaAsignatura().getActa();
        if (acceso == Acceso.ESCRITURA && !acta.isEditable())
        {
            throw new ActaNoEditableException(
                    "No es pot modificar la nota");
        }

        if (isAdmin(personaId))
        {
            return;
        }
        ActaPersona actaPersona = actaPersonaDAO.getActaPersonaByCursoAndCodigosAndPersona(
                acta.getCurso().getId(), acta.getCodigo(), personaId);
        if (actaPersona == null)
        {
            throw new NoAutorizadoException("No tens permís per accedir a l'acta");
        }
    }

    public void checkPuedeAccederActaRevision(Acceso acceso, Long actaRevisionId, Long personaId)
            throws NoAutorizadoException, ActaNoEditableException
    {
        ActaRevision actaRevision = actaRevisionDAO.getActaRevisionById(actaRevisionId);
        if (actaRevision == null)
        {
            return;
        }

        Acta acta = actaRevision.getActa();
        if (acceso == Acceso.ESCRITURA && !acta.isEditable())
        {
            throw new ActaNoEditableException();
        }

        if (isAdmin(personaId))
        {
            return;
        }
        ActaPersona actaPersona = actaPersonaDAO.getActaPersonaByCursoAndCodigosAndPersona(
                acta.getCurso().getId(), acta.getCodigo(), personaId);
        if (actaPersona == null)
        {
            throw new NoAutorizadoException("No tens permís per accedir a l'acta");
        }
    }

    public boolean isAdmin(Long personaId)
    {
        return apaDAO.hasPerfil("ACT", "ADMIN", personaId);
    }

    public void checkIsAdmin(Long personaId) throws NoAutorizadoException
    {
        if (!isAdmin(personaId))
        {
            throw new NoAutorizadoException(
                    "Només els administradors poden accedir a aquest recurs");
        }
    }

    public void checkPuedeAccederActaByCodigo(Acceso acceso, String codigoAsignatura, Long cursoId,
                                              Long personaId)
            throws RegistroNoEncontradoException, ActaNoEditableException, NoAutorizadoException
    {
        Acta acta = actaDAO.getActasByCursoAndCodigo(cursoId, codigoAsignatura);
        if (acta == null)
        {
            throw new RegistroNoEncontradoException("No s'ha trobat l'acta");
        }

        if (acceso == Acceso.ESCRITURA && !acta.isEditable())
        {
            throw new ActaNoEditableException();
        }

        if (isAdmin(personaId))
        {
            return;
        }
        ActaPersona actaPersona = actaPersonaDAO.getActaPersonaByCursoAndCodigosAndPersona(
                acta.getCurso().getId(), acta.getCodigo(), personaId);
        if (actaPersona == null)
        {
            throw new NoAutorizadoException("No tens permís per accedir a l'acta");
        }
    }
}