package es.uji.apps.act.services.rest;

import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.act.exceptions.ActaNoEditableException;
import es.uji.apps.act.exceptions.FormatoNoValidoException;
import es.uji.apps.act.exceptions.InconsistenciaDeDatosException;
import es.uji.apps.act.exceptions.NoAutorizadoException;
import es.uji.apps.act.models.Acta;
import es.uji.apps.act.models.DesgloseGrupo;
import es.uji.apps.act.models.DesglosePregunta;
import es.uji.apps.act.models.enums.Acceso;
import es.uji.apps.act.services.DesglosePreguntaService;
import es.uji.apps.act.services.AuthService;
import es.uji.apps.act.services.ParseService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;

@Path("desglosepreguntas")
public class DesglosePreguntaResource extends CoreBaseService
{
    @InjectParam
    private DesglosePreguntaService desglosePreguntaService;

    @InjectParam
    private AuthService authService;

    @PathParam("actaId")
    Long actaId;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getActaPreguntas()
            throws NoAutorizadoException, ActaNoEditableException, RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkPuedeAccederActa(Acceso.LECTURA, actaId, connectedUserId);
        List<DesglosePregunta> listaDesglosePreguntas = desglosePreguntaService
                .getDesglosePreguntasByActaId(actaId, connectedUserId);
        return UIEntity.toUI(listaDesglosePreguntas);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity insertActaPregunta(UIEntity entity)
            throws NoAutorizadoException, InconsistenciaDeDatosException, ActaNoEditableException,
            RegistroNoEncontradoException, FormatoNoValidoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkPuedeAccederActa(Acceso.ESCRITURA, actaId, connectedUserId);
        DesglosePregunta desglosePregunta = uiToModel(entity);
        desglosePreguntaService.insert(desglosePregunta, connectedUserId);
        return UIEntity.toUI(desglosePregunta);
    }

    @PUT
    @Path("{actaPreguntaId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateActaPregunta(@PathParam("actaPreguntaId") Long actaPreguntaId,
            UIEntity entity) throws NoAutorizadoException, InconsistenciaDeDatosException,
            ActaNoEditableException, RegistroNoEncontradoException, FormatoNoValidoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkPuedeAccederActa(Acceso.ESCRITURA, actaId, connectedUserId);
        DesglosePregunta desglosePregunta = uiToModel(entity);
        desglosePreguntaService.update(desglosePregunta, connectedUserId);
        desglosePregunta = desglosePreguntaService.getPreguntaConGrupo(actaPreguntaId, connectedUserId);
        return UIEntity.toUI(desglosePregunta);
    }

    @DELETE
    @Path("{actaPreguntaId}")
    public Response deleteActaPregunta(@PathParam("actaPreguntaId") Long actaPreguntaId,
            UIEntity entity)
            throws NoAutorizadoException, ActaNoEditableException, RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkPuedeAccederActa(Acceso.ESCRITURA, actaId, connectedUserId);
        desglosePreguntaService.delete(actaPreguntaId, connectedUserId);
        return Response.ok().build();
    }

    private DesglosePregunta uiToModel(UIEntity uiEntity) throws FormatoNoValidoException
    {
        DesglosePregunta desglosePregunta = new DesglosePregunta();
        desglosePregunta.setId(uiEntity.getLong("id"));
        desglosePregunta.setActa(new Acta(actaId));
        desglosePregunta.setDesgloseGrupo(new DesgloseGrupo(uiEntity.getLong("desgloseGrupoId")));
        if (uiEntity.get("parcialActaId") != null && !uiEntity.get("parcialActaId").isEmpty())
        {
            desglosePregunta.setParcialActa(new Acta(uiEntity.getLong("parcialActaId")));
        }

        if (uiEntity.get("etiqueta").length() > 10)
        {
            throw new FormatoNoValidoException(
                    "L'etiqueta no pot tenir una llargaria superior a 10 caràcters.");
        }
        desglosePregunta.setEtiqueta(uiEntity.get("etiqueta"));
        desglosePregunta.setNombre(uiEntity.get("nombre"));
        desglosePregunta.setOrden(uiEntity.getLong("orden"));
        desglosePregunta.setNotaMaxima(ParseService.parsearDouble(uiEntity.get("notaMaxima")));
        desglosePregunta.setNotaMinima(ParseService.parsearDouble(uiEntity.get("notaMinima")));
        desglosePregunta.setPeso(ParseService.parsearDouble(uiEntity.get("peso")));
        desglosePregunta.setVisible(uiEntity.getBoolean("visible"));
        return desglosePregunta;
    }

}