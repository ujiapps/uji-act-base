package es.uji.apps.act.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.act.exceptions.TraspasarActaException;
import es.uji.apps.act.models.Acta;
import es.uji.apps.act.services.ActaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("test")
public class TestResource extends CoreBaseService
{
    @InjectParam
    private ActaService actaService;

    @GET
    @Path("notas-desglosadas")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> checkNotasDesglosadas(
            @QueryParam("cursoId") Long cursoId) throws Exception
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Acta> actas = actaService.getActasTraspasadasByCurso(cursoId, connectedUserId);

        List<UIEntity> resultados = new ArrayList<>();

        for (Acta acta : actas)
        {
            UIEntity informeActa = getUIInformeActa(acta);
            Acta actaConEstudiantes = actaService
                    .getActaConEstudiantes(acta.getId(), connectedUserId);

            try
            {
                actaService.checkNotasCaluladasActa(actaConEstudiantes);
                informeActa.put("status", "ok");
            }
            catch (TraspasarActaException e)
            {
                informeActa.put("error", e.getMessage());
            }

            resultados.add(informeActa);
        }

        return resultados;
    }

    protected UIEntity getUIInformeActa(Acta acta)
    {
        UIEntity uiEntity = new UIEntity();
        uiEntity.put("actaId", acta.getId());
        uiEntity.put("curso", acta.getCurso().getId());
        uiEntity.put("codigo", acta.getCodigo());
        uiEntity.put("fechaTraspaso", acta.getFechaTraspaso());
        uiEntity.put("descripcion", acta.getDescripcion());
        uiEntity.put("referencia", acta.getReferencia());
        return uiEntity;
    }
}
