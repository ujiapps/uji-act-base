package es.uji.apps.act.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.act.exceptions.InconsistenciaDeDatosException;
import es.uji.apps.act.exceptions.NoAutorizadoException;
import es.uji.apps.act.models.ActaAsignatura;
import es.uji.apps.act.services.ActaAsignaturaService;
import es.uji.apps.act.services.ActaService;
import es.uji.apps.act.services.AuthService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;




@Path("actaAsignaturas")
public class ActaAsignaturaResource extends CoreBaseService
{
    @InjectParam
    private AuthService authService;


    @InjectParam
    private ActaAsignaturaService actaAsignaturaService;

    @InjectParam
    private ActaService actaService;

    @PathParam("actaId")
    Long actaId;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getActaAsignaturas()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<ActaAsignatura> listaActaAsignaturas = (actaId == null)
                ? actaAsignaturaService.getAll(connectedUserId)
                : actaAsignaturaService.getActaAsignaturasByActa(actaId, connectedUserId);

        return UIEntity.toUI(listaActaAsignaturas);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity insertActaAsignatura(UIEntity entity)
            throws NoAutorizadoException, InconsistenciaDeDatosException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkIsAdmin(connectedUserId);
        ActaAsignatura actaAsignatura = entity.toModel(ActaAsignatura.class);
        actaAsignaturaService.insert(actaAsignatura, connectedUserId);
        return UIEntity.toUI(actaAsignatura);
    }

    @DELETE
    @Path("{actaAsignaturaId}")
    public Response deleteActaAsignatura(@PathParam("actaAsignaturaId") Long actaAsignaturaId,
            UIEntity entity) throws NoAutorizadoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkIsAdmin(connectedUserId);
        actaAsignaturaService.delete(actaAsignaturaId, connectedUserId);
        return Response.ok().build();
    }

    @Path("{actaAsignaturaId}/actaestudiantes")
    public ActaEstudianteResource getActaAsignaturaResource(
            @InjectParam ActaEstudianteResource actaEstudianteResource)
    {
        return actaEstudianteResource;
    }
}
