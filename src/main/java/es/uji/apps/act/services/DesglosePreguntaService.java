package es.uji.apps.act.services;

import java.util.List;

import es.uji.apps.act.dao.DesglosePreguntaDAO;
import es.uji.apps.act.exceptions.InconsistenciaDeDatosException;
import es.uji.apps.act.models.DesglosePregunta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DesglosePreguntaService extends BaseService<DesglosePregunta>
{
    @Autowired
    public DesglosePreguntaService(DesglosePreguntaDAO dao)
    {
        super(dao, DesglosePregunta.class);
    }

    public List<DesglosePregunta> getDesglosePreguntasByActaId(Long actaId, Long connectedUserId)
    {
        return ((DesglosePreguntaDAO) dao).getDesglosePreguntasByActaId(actaId);
    }

    public List<DesglosePregunta> getDesglosePreguntasByParcialId(Long parcialId, Long connectedUserId)
    {
        return ((DesglosePreguntaDAO) dao).getDesglosePreguntasByParcialId(parcialId);
    }

    public DesglosePregunta update(DesglosePregunta model, Long connectedUserId)
            throws InconsistenciaDeDatosException
    {
        if (model.getParcialActa() != null && model.getActa() != null
                && model.getParcialActa().getId().equals(model.getActa().getId()))
        {
            throw new InconsistenciaDeDatosException(
                    "No es pot associar la pregunta amb el mateix parcial");
        }
        return dao.update(model);
    }

    public DesglosePregunta getPreguntaConGrupo(Long actaPreguntaId, Long connectedUserId)
    {
        return ((DesglosePreguntaDAO) dao).getPreguntaConGrupo(actaPreguntaId);
    }
}