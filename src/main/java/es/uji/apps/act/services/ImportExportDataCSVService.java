package es.uji.apps.act.services;

import java.io.IOException;
import java.io.Reader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

@Service
public class ImportExportDataCSVService implements ImportExportDataService
{
    final char DELIMITER = ';';

    public String getRecordsAsStringContent(List<String[]> records)
    {
        StringWriter writer = new StringWriter();
        CSVWriter csvWriter = new CSVWriter(writer, DELIMITER, CSVWriter.DEFAULT_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER, "\n");
        csvWriter.writeAll(records);
        return writer.toString();
    }

    public List<Map<String, String>> getFileAsRecordsMapped(Reader fileReader) throws IOException
    {
        CSVReader csvReader = new CSVReader(fileReader, DELIMITER, CSVWriter.DEFAULT_QUOTE_CHARACTER);
        List<String[]> plainRecords = csvReader.readAll();

        String[] header = plainRecords.get(0);
        plainRecords.remove(0);

        List<Map<String, String>> rows = new ArrayList<>();
        for (String[] plainRecord : plainRecords)
        {
            Map<String, String> row = new HashMap<>();
            for (int i = 0; i < plainRecord.length; i++)
            {
                try
                {
                    row.put(header[i], plainRecord[i]);
                }
                catch (Exception e)
                {
                }
            }
            rows.add(row);
        }
        return rows;
    }
}
