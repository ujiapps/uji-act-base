package es.uji.apps.act.services;

import java.util.List;

import es.uji.apps.act.dao.LogDAO;
import es.uji.apps.act.exceptions.NoAutorizadoException;
import es.uji.apps.act.models.*;
import es.uji.apps.act.models.enums.Acceso;
import es.uji.commons.sso.AccessManager;

import org.springframework.stereotype.Service;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.act.dao.InformacionDAO;

@Service
public class LogService
{
    @InjectParam
    private AuthService authService;

    @InjectParam
    public LogDAO logDAO;

    public List<Log> getLogsByActaId(Long actaId, Long connectedUserId) throws NoAutorizadoException
    {
        authService.checkIsAdmin(connectedUserId);
        return logDAO.getLogsByActaId(actaId);
    }

    public void registraNota(ActaEstudiante actaEstudiante, Long connectedUserId, String ip)
    {
        Log log = new Log();
        log.setActa(new Acta(actaEstudiante.getActaAsignatura().getActa().getId()));
        log.setActaEstudianteId(actaEstudiante.getId());
        log.setEstudiante(new Persona(actaEstudiante.getPersona().getId()));
        log.setNota(actaEstudiante.getNota());
        log.setCalificacionId(actaEstudiante.getCalificacionId());
        log.setPersona(new Persona(connectedUserId));
        log.setComentario(actaEstudiante.getComentario());
        log.setIp(ip);

        logDAO.insert(log);
    }

    public void registraNotaDesglose(DesgloseNota desgloseNota, Long connectedUserId, String ip)
    {
        Log log = new Log();
        log.setActa(new Acta(desgloseNota.getActaEstudiante().getActaAsignatura().getActa().getId()));
        log.setActaEstudianteNotaId(desgloseNota.getId());
        log.setEstudiante(new Persona(desgloseNota.getActaEstudiante().getPersona().getId()));
        log.setNota(desgloseNota.getNota());
        log.setPersona(new Persona(connectedUserId));
        log.setIp(ip);

        logDAO.insert(log);
    }
}
