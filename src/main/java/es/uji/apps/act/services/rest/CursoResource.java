package es.uji.apps.act.services.rest;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.act.exceptions.InconsistenciaDeDatosException;
import es.uji.apps.act.exceptions.NoAutorizadoException;
import es.uji.apps.act.models.Curso;
import es.uji.apps.act.services.AuthService;
import es.uji.apps.act.services.CursoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("cursos")
public class CursoResource extends CoreBaseService
{
    @InjectParam
    private CursoService cursoService;

    @InjectParam
    private AuthService authService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCursos()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Curso> listaCursos = cursoService.getAll(connectedUserId).stream().sorted(Comparator.comparingLong(Curso::getId)).collect(Collectors.toList());
        return UIEntity.toUI(listaCursos);
    }

    @GET
    @Path("activos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCursosActivos()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Curso> listaCursos = cursoService.getCursosActivos(connectedUserId);
        return UIEntity.toUI(listaCursos);
    }

    @GET
    @Path("diligenciasFueraPlazo")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCursosDiligenciasFueraPlazo()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Curso> listaCursos = cursoService.getCursosDiligenciasFueraPlazo(connectedUserId);
        return UIEntity.toUI(listaCursos);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity insertCurso(UIEntity entity)
            throws NoAutorizadoException, InconsistenciaDeDatosException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkIsAdmin(connectedUserId);
        Curso curso = entity.toModel(Curso.class);
        cursoService.insert(curso, connectedUserId);
        return UIEntity.toUI(curso);
    }

    @PUT
    @Path("{cursoId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateCurso(@PathParam("cursoId") Long cursoId, UIEntity entity)
            throws NoAutorizadoException, InconsistenciaDeDatosException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkIsAdmin(connectedUserId);
        Curso curso = entity.toModel(Curso.class);
        cursoService.update(curso, connectedUserId);
        return UIEntity.toUI(curso);
    }

    @DELETE
    @Path("{cursoId}")
    public Response deleteCurso(@PathParam("cursoId") Long cursoId, UIEntity entity)
            throws NoAutorizadoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkIsAdmin(connectedUserId);
        cursoService.delete(cursoId, connectedUserId);
        return Response.ok().build();
    }

    @Path("{cursoId}/actas")
    public ActaResource getActaResource(@InjectParam ActaResource actaResource)
    {
        return actaResource;
    }

    @Path("{cursoId}/convocatorias")
    public ConvocatoriaResource getConvocatoriaResource(
            @InjectParam ConvocatoriaResource convocatoriaResource)
    {
        return convocatoriaResource;
    }

    @Path("{cursoId}/actaconvocatorias")
    public ActaConvocatoriaResource getActaConvocatoriaResource(
            @InjectParam ActaConvocatoriaResource convocatoriaResource)
    {
        return convocatoriaResource;
    }
}