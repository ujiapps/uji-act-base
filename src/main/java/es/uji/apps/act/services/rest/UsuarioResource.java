package es.uji.apps.act.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.act.models.ActaDiligencia;
import es.uji.apps.act.models.Preferencia;
import es.uji.apps.act.services.ActaDiligenciaService;
import es.uji.apps.act.services.AuthService;
import es.uji.apps.act.services.PreferenciaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("usuario")
public class UsuarioResource extends CoreBaseService
{
    @InjectParam
    private AuthService authService;

    @InjectParam
    private ActaDiligenciaService actaDiligenciaService;

    @InjectParam
    private PreferenciaService preferenciaService;

    @GET
    @Path("roles")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getRol()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        UIEntity ui = new UIEntity();
        ui.put("admin", authService.isAdmin(connectedUserId));

        List<ActaDiligencia> listaDiligencias = actaDiligenciaService
                .getDiligenciasByProfesor(connectedUserId, null, null, null);
        ui.put("tieneDiligencias", listaDiligencias.size() > 0);
        return ui;
    }

    @GET
    @Path("preferencias")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getPreferencias()
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        UIEntity ui = new UIEntity();
        Preferencia preferencia = preferenciaService.getPreferencias(connectedUserId);

        return UIEntity.toUI(preferencia);
    }

    @POST
    @Path("preferencias")
    @Produces(MediaType.APPLICATION_JSON)
    public Response setTema(UIEntity ui)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        preferenciaService.updatePreferencia(ui, connectedUserId);
        return Response.ok().build();
    }

}