package es.uji.apps.act.services.rest;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.act.exceptions.ActaNoEditableException;
import es.uji.apps.act.exceptions.NoAutorizadoException;
import es.uji.apps.act.models.enums.Acceso;
import es.uji.apps.act.models.ui.UIActa;
import es.uji.apps.act.models.ui.UIActaEstudiante;
import es.uji.apps.act.services.ActaService;
import es.uji.apps.act.services.AuthService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.web.template.PDFTemplate;
import es.uji.commons.web.template.Template;

@Path("publicacion")
public class PublicacionResource extends CoreBaseService
{
    @InjectParam
    private ActaService actaService;

    @InjectParam
    private AuthService authService;

    @GET
    @Path("listado-notas-nombre/{actaId}")
    @Produces("application/pdf")
    public Template listadoNotasNombre(@PathParam("actaId") Long actaId,
                                       @QueryParam("asignatura") String asignatura, @QueryParam("grupo") String grupo)
            throws NoAutorizadoException, ActaNoEditableException, RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkPuedeAccederActa(Acceso.LECTURA, actaId, connectedUserId);
        SimpleDateFormat humanDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        UIActa uiActa = actaService.getUIActa(actaId, asignatura, grupo, connectedUserId);
        Template template = new PDFTemplate("act/acta-nombre", new Locale("CA"), "act");
        template.put("acta", uiActa);
        template.put("fechaActual", humanDateFormat.format(new Date()));
        template.put("mostrarNombre", true);
        template.put("asignatura", asignatura);
        template.put("grupo", grupo);
        return template;
    }

    @GET
    @Path("listado-sin-nombre/{actaId}")
    @Produces("application/pdf")
    public Template listadoNotasAnonimo(@PathParam("actaId") Long actaId,
                                        @QueryParam("asignatura") String asignatura, @QueryParam("grupo") String grupo)
            throws NoAutorizadoException, ActaNoEditableException, RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkPuedeAccederActa(Acceso.LECTURA, actaId, connectedUserId);
        SimpleDateFormat humanDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        UIActa uiActa = actaService.getUIActa(actaId, asignatura, grupo, connectedUserId);
        List<UIActaEstudiante> uiActaEstudiantes = uiActa.getEstudiantes().stream().sorted((o1, o2) -> o1.getDni().compareTo(o2.getDni())).collect(Collectors.toList());
        uiActa.setEstudiantes(uiActaEstudiantes);
        Template template = new PDFTemplate("act/acta-nombre", new Locale("CA"), "act");
        template.put("acta", uiActa);
        template.put("fechaActual", humanDateFormat.format(new Date()));
        template.put("mostrarNombre", false);
        template.put("asignatura", asignatura);
        template.put("grupo", grupo);
        return template;
    }

    @GET
    @Path("listado-firmas/{actaId}")
    @Produces("application/pdf")
    public Template listadoFirmas(@PathParam("actaId") Long actaId,
                                  @QueryParam("asignatura") String asignatura, @QueryParam("grupo") String grupo)
            throws NoAutorizadoException, ActaNoEditableException, RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkPuedeAccederActa(Acceso.LECTURA, actaId, connectedUserId);
        SimpleDateFormat humanDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        UIActa uiActa = actaService.getUIActa(actaId, asignatura, grupo, connectedUserId);
        Template template = new PDFTemplate("act/acta-firmas", new Locale("CA"), "act");
        template.put("acta", uiActa);
        template.put("fechaActual", humanDateFormat.format(new Date()));
        template.put("asignatura", asignatura);
        template.put("grupo", grupo);
        return template;
    }
}