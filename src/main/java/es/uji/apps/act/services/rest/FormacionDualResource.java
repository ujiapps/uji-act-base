package es.uji.apps.act.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.act.exceptions.ActaNoEncontradaException;
import es.uji.apps.act.exceptions.InconsistenciaDeDatosException;
import es.uji.apps.act.exceptions.NoAutorizadoException;
import es.uji.apps.act.models.FormacionDual;
import es.uji.apps.act.models.PermisosExtra;
import es.uji.apps.act.models.views.AsignaturaOfertada;
import es.uji.apps.act.services.ActaService;
import es.uji.apps.act.services.FormacionDualService;
import es.uji.apps.act.services.PermisosExtraService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Path("formacionDual")
public class FormacionDualResource extends CoreBaseService {
    @InjectParam
    private FormacionDualService formacionDualService;

    @InjectParam
    private ActaService actaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getFormacionDual()
            throws NoAutorizadoException, ActaNoEncontradaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<FormacionDual> listaFormacionDual = formacionDualService.getFormacionDualByCursoAcademicoId(connectedUserId);
        List<AsignaturaOfertada> listaAsignaturas = formacionDualService.getAsignaturasOfertadas(listaFormacionDual, connectedUserId);
        return creaEntidadFormacionDual(listaFormacionDual, listaAsignaturas);
    }

    private List<UIEntity> creaEntidadFormacionDual(List<FormacionDual> listaFormacionDual, List<AsignaturaOfertada> listaAsignaturas) {
        List<UIEntity> lista = new ArrayList<>();

        for (FormacionDual formacionDual : listaFormacionDual) {
            UIEntity entity = UIEntity.toUI(formacionDual);
            entity.put("fecha", formacionDual.getFecha());
            entity.put("cursoAcademicoId", formacionDual.getCursoAcademicoId().toString());
            entity.put("asignaturaId", formacionDual.getAsignaturaId());
            entity.put("asignatura", listaAsignaturas.stream().filter(asignatura -> asignatura.getAsignaturaId().equals(formacionDual.getAsignaturaId())).findFirst().get().getNombre());
            lista.add(entity);
        }
        return lista;
    }

    @DELETE
    @Path("{formacionDualId}")
    public Response deleteBloque(@PathParam("formacionDualId") Long formacionDualId, UIEntity entity) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        formacionDualService.delete(formacionDualId, connectedUserId);
        return Response.ok().build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity setTema(UIEntity entity) throws InconsistenciaDeDatosException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        FormacionDual formacionDual = entity.toModel(FormacionDual.class);
        formacionDual.setFecha(new Date());
        formacionDual.setCursoAcademicoId(Long.parseLong(entity.get("cursoAcademicoId")));
        formacionDual.setAsignaturaId(entity.get("asignaturaId"));

        if (formacionDual.getAsignaturaId().contains(",")) {
            formacionDual.setAsignaturaId(formacionDual.getAsignaturaId().split(",")[0]);
        }
        formacionDualService.insert(formacionDual, connectedUserId);
        return UIEntity.toUI(formacionDual);
    }

}
