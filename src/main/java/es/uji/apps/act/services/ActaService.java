package es.uji.apps.act.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.act.dao.ActaAsignaturaDAO;
import es.uji.apps.act.dao.ActaDAO;
import es.uji.apps.act.dao.ActaDepartamentoDAO;
import es.uji.apps.act.dao.ActaPersonaDAO;
import es.uji.apps.act.exceptions.*;
import es.uji.apps.act.models.*;
import es.uji.apps.act.models.enums.Acceso;
import es.uji.apps.act.models.enums.Calificacion;
import es.uji.apps.act.models.ui.UIActa;
import es.uji.apps.act.models.ui.UIActaEstudiante;
import es.uji.apps.act.models.ui.UIActaRevision;
import es.uji.apps.act.models.ui.UIEmail;
import es.uji.apps.act.models.views.*;
import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ActaService extends BaseService<Acta> {
    @InjectParam
    private ActaPersonaDAO actaPersonaDAO;

    @InjectParam
    private ActaDepartamentoDAO actaDepartamentoDAO;

    @InjectParam
    private ActaAsignaturaDAO actaAsignaturaDAO;

    @InjectParam
    private ActaEstudianteService actaEstudianteService;

    @InjectParam
    private NotificacionService notificacionService;

    @InjectParam
    private DesglosePreguntaService desglosePreguntaService;

    @InjectParam
    private AuthService authService;

    @InjectParam
    private CorreoService correoService;

    @InjectParam
    private ResponsableAsignaturaService responsableAsignaturaService;

    @Autowired
    public ActaService(ActaDAO dao) {
        super(dao, Acta.class);
    }

    public List<ActaPersona> getListaActasPersonaByCurso(Long cursoId, Long connectedUserId) {
        return actaPersonaDAO.getListaActasPersonasByCurso(cursoId, connectedUserId);
    }

    public List<Acta> getActasTraspasadasByCurso(Long cursoId, Long connectedUserId) {
        return ((ActaDAO) dao).getActasTraspasadasByCurso(cursoId);
    }

    public List<NotaCalculada> getNotasCalculadasByActa(Long actaId, Long connectedUserId) {
        return ((ActaDAO) dao).getNotasCalculadasByActa(actaId);
    }

    public List<ActaDepartamento> getListaActasDepartamentoByCursoAndConvocatoria(Long cursoId,
                                                                                  Long convocatoriaId, Long connectedUserId) {
        List<ActaDepartamento> listaActaDepartamento = actaDepartamentoDAO
                .getListaActasDepartamentoConAlumnosByCursoAndConvocatoriaAndUserId(cursoId,
                        convocatoriaId, connectedUserId);

        return listaActaDepartamento;
    }

    @Transactional
    public Acta creaActa(Long cursoId, String codigoAsignaturas, Long convocatoriaId,
                         String descripcion, Long connectedUserId) throws RegistroNoEncontradoException {
        ActaVirtual actaFutura = ((ActaDAO) dao).getActaFuturaByCursoAndCodigo(cursoId,
                codigoAsignaturas);

        if (actaFutura == null) {
            throw new RegistroNoEncontradoException("No s'ha trobat el acta");
        }

        Acta acta = new Acta();
        acta.setConvocatoria(new Convocatoria(convocatoriaId));
        acta.setCurso(new Curso(actaFutura.getCurso()));
        acta.setTipoEstudio(new TipoEstudio(actaFutura.getTipoEstudioId()));
        acta.setActaUnica(actaFutura.getActaUnica().equals("S"));
        acta.setFechaAlta(new Date());
        acta.setOficial(actaFutura.getOficial().equals("S"));
        if (descripcion == null || descripcion.isEmpty()) {
            descripcion = codigoAsignaturas;
        }
        acta.setDescripcion(descripcion);
        acta.setPersonaId(connectedUserId);
        acta.setDesgloseActivo(false);
        acta.setCodigo(codigoAsignaturas);
        dao.insert(acta);
        dao.flush();

        List<ActaVirtualAsignatura> actafuturaAsignaturas = ((ActaDAO) dao)
                .getActaFuturaAsignaturasByCursoAndCodigo(cursoId, codigoAsignaturas);
        for (ActaVirtualAsignatura actaFuturaAsignatura : actafuturaAsignaturas) {
            ActaAsignatura actaAsignatura = new ActaAsignatura();
            actaAsignatura.setActa(acta);
            actaAsignatura.setAsignatura(new Asignatura(actaFuturaAsignatura.getAsignaturaId()));
            actaAsignatura.setGrupoId(actaFuturaAsignatura.getGrupoId());
            dao.insert(actaAsignatura);
        }
        return acta;
    }

    @Transactional
    public void deleteActa(Long actaId, Long connectedUserId) throws ActaNoPermitidaException {
        Acta acta = ((ActaDAO) dao).getActaById(actaId);

        if (!acta.getConvocatoria().getId().equals(Convocatoria.EXAMEN_PARCIAL)) {
            throw new ActaNoPermitidaException(
                    "Només es poden esborrar les actes associades a un parcial");
        }

        if (acta.getFechaTraspaso() != null) {
            throw new ActaNoPermitidaException(
                    "No es pot esborrar una acta que ha estat traspassada");
        }

        List<DesglosePregunta> preguntas = desglosePreguntaService
                .getDesglosePreguntasByParcialId(actaId, connectedUserId);
        if (!preguntas.isEmpty()) {
            throw new ActaNoPermitidaException(
                    "No es pot esborrar el parcial perque està associat a una pregunta del desglossament");
        }

        dao.delete(acta);
    }

    public void enviaNotificacionEstudiantes(Long actaId, String asunto, String contenido,
                                             User user) {
        List<ActaEstudiante> listaActaEstudiantes = actaEstudianteService
                .getActaEstudianteByActaId(actaId, user.getId());
        List<String> correos = listaActaEstudiantes.stream()
                .filter(e -> (e.getMotivoExclusion() == null || e.getMotivoExclusion().isEmpty())
                        && e.getPersona().getEmail() != null)
                .map(e -> e.getPersona().getEmail()).collect(Collectors.toList());

        List<UIEmail> emails = correos.stream().map(e -> new UIEmail(e, asunto, contenido))
                .collect(Collectors.toList());
        correoService.enviaEmails(emails);
        notificacionService.registraNotificacion(actaId, Notificacion.TIPO_MANUAL, asunto,
                contenido);
    }

    public Acta getActaConEstudiantes(Long actaId, Long connectedUserId) {
        Acta acta = ((ActaDAO) dao).getActaConEstudiantes(actaId);
        actaEstudianteService.calculaNotasCalculadas(acta);
        return acta;
    }

    public UIActa getUIActa(Long actaId, String asignatura, String grupo, Long connectedUserId) {
        SimpleDateFormat humanDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat humanHourFormat = new SimpleDateFormat("HH:mm");

        Acta acta = ((ActaDAO) dao).getActaConEstudiantes(actaId, asignatura, grupo);
        actaEstudianteService.calculaNotasCalculadas(acta);
        UIActa uiActa = new UIActa();
        uiActa.setCurso(acta.getCurso().getId());
        uiActa.setNumeroAsignaturas(new Long(acta.getActasAsignaturas().size()));
        uiActa.setFirmaDigital(acta.getReferencia());

        if (acta.getConvocatoria().getId() == Convocatoria.EXAMEN_PARCIAL) {
            uiActa.setConvocatoria(acta.getDescripcion());
        } else {
            uiActa.setConvocatoria(acta.getConvocatoria().getNombre());
        }

        List<UIActaEstudiante> uiActaEstudiantes = new ArrayList<>();
        uiActa.setEstudiantes(uiActaEstudiantes);
        List<UIActaRevision> uiActaRevisiones = new ArrayList<>();
        uiActa.setRevisiones(uiActaRevisiones);
        uiActa.setCodigosAsignatura(acta.getActasAsignaturas().stream()
                .map(aa -> aa.getAsignatura().getId()).collect(Collectors.joining(", ")));
        uiActa.setGrupos(acta.getActasAsignaturas().stream().filter(aa -> aa.getGrupoId() != null)
                .map(aa -> aa.getGrupoId()).sorted().collect(Collectors.joining(", ")));

        for (ActaRevision actaRevision : acta.getActasRevisiones()) {
            UIActaRevision uiActaRevision = new UIActaRevision();
            uiActaRevision.setAsignaturas(actaRevision.getAsignaturas());
            uiActaRevision.setGrupos(actaRevision.getGrupos());
            uiActaRevision.setNombre(actaRevision.getNombre());
            uiActaRevision.setObservaciones(actaRevision.getObservaciones());
            uiActaRevision.setLugar(actaRevision.getLugar());

            if (actaRevision.getFecha() != null) {
                uiActaRevision.setFecha(humanDateFormat.format(actaRevision.getFecha()));
                uiActaRevision.setHoraInicio(humanHourFormat.format(actaRevision.getFecha()));
            }
            if (actaRevision.getFechaFin() != null) {
                uiActaRevision.setHoraFin(humanHourFormat.format(actaRevision.getFechaFin()));
            }
            uiActaRevisiones.add(uiActaRevision);
        }

        for (ActaAsignatura actaAsignatura : acta.getActasAsignaturas()) {
            uiActa.setNombreAsignatura(actaAsignatura.getAsignatura().getNombre());
            for (ActaEstudiante actaEstudiante : actaAsignatura.getActasEstudiantes()) {
                UIActaEstudiante uiActaEstudiante = new UIActaEstudiante();
                uiActaEstudiante.setAsignaturaId(actaAsignatura.getAsignatura().getId());
                uiActaEstudiante.setNombre(actaEstudiante.getPersona().getNombre());
                uiActaEstudiante.setApellidos(actaEstudiante.getPersona().getApellidos());
                uiActaEstudiante.setDni(actaEstudiante.getPersona().getIdentificacion());
                uiActaEstudiante.setDniOfuscado(actaEstudiante.getPersona().getIdentificacionOfuscado());
                uiActaEstudiante.setGrupoId(actaEstudiante.getGrupoId());
                uiActaEstudiante.setNota(
                        actaEstudiante.isNoPresentado() ? null : actaEstudiante.getNotaEfectiva());
                uiActaEstudiante.setMatriculaHonor(
                        actaEstudiante.getCalificacionId() == Calificacion.MATRICULA_HONOR.getId());
                uiActaEstudiantes.add(uiActaEstudiante);
            }
        }

        uiActa.setEstudiantes(uiActa.getEstudiantes().stream()
                .sorted(Comparator.comparing(UIActaEstudiante::getApellidos))
                .collect(Collectors.toList()));

        return uiActa;
    }

    public void traspasarActa(Acta acta, Long connectedUserId, String ip)
            throws IOException, TraspasarActaException, InconsistenciaDeDatosException, ActaNoPermitidaException {
        checkTraspasosAnteriores(acta, connectedUserId);
        checkLimiteMatriculasNoExcedido(acta);
        acta.comprobarActaTraspasable();
        acta.traspasar(connectedUserId, ip);
        checkNotasCaluladasActa(acta);
    }

    private void checkTraspasosAnteriores(Acta acta, Long personaId) throws ActaNoPermitidaException {
        ActaPersona actaPersona = actaPersonaDAO.getActaPersonaByCursoAndAsignaturasAndPersona(
                acta.getCurso().getId(), acta.getCodigo(), personaId);
        Long ordinaria2 = actaPersona.getOrdinaria2Id();
        if (ordinaria2 == acta.getConvocatoria().getId()) {
            Long ordinaria1 = actaPersona.getOrdinaria1Id();
            Acta actaAnterior = ((ActaDAO) dao).getActaByCursoAndCodigoAndConvocatoria(
                    acta.getCurso().getId(), acta.getCodigo(), ordinaria1);
            if (actaAnterior == null || actaAnterior.getFechaTraspaso() == null) {
                throw new ActaNoPermitidaException(
                        "No es pot traspassar l'acta perque las primera ordinària no està traspassada.");
            }
        }
    }

    public void checkTraspasosAnteriores(String asiCodigo, Long cursoId, Long convocatoriaId, Long personaId)
            throws TraspasarActaException {
        ActaPersona actaPersona = actaPersonaDAO.getActaPersonaByCursoAndAsignaturasAndPersona(
                cursoId, asiCodigo, personaId);
        Long ordinaria2 = actaPersona.getOrdinaria2Id();
        if (ordinaria2 == convocatoriaId) {
            Long ordinaria1 = actaPersona.getOrdinaria1Id();
            Acta actaAnterior = ((ActaDAO) dao).getActaByCursoAndCodigoAndConvocatoria(
                    cursoId, asiCodigo, ordinaria1);
            if (actaAnterior == null || actaAnterior.getFechaTraspaso() == null) {
                throw new TraspasarActaException(
                        "No es pot traspassar l'acta perque las primera ordinària no està traspassada.");
            }
        }
    }

    public void destraspasarActa(Acta acta, Long connectedUserId)
            throws IOException, TraspasarActaException {
        acta.destraspasar(connectedUserId);
    }

    public Acta getActaById(Long actaId) {
        return ((ActaDAO) dao).getActaById(actaId);
    }

    public Acta getActaByIdConNotasCalculadas(Long actaId) {
        Acta acta = ((ActaDAO) dao).getActaByIdConNotasCalculadas(actaId);
        actaEstudianteService.calculaNotasCalculadas(acta);
        return acta;
    }

    public void checkSePuedePonerOtraMatriculaEnActa(Long actaId) throws InconsistenciaDeDatosException {
        Acta acta = ((ActaDAO) dao).getActaConEstudiantes(actaId);
        actaEstudianteService.calculaNotasCalculadas(acta);
        if (acta.getNumeroMatriculas() >= acta.getNumeroMaximoMatriculas()) {
            throw new InconsistenciaDeDatosException("No es poden assignar més matrícules d'honor");
        }
    }

    public void checkLimiteMatriculasNoExcedido(Acta acta) throws InconsistenciaDeDatosException {
        actaEstudianteService.calculaNotasCalculadas(acta);
        if (acta.getNumeroMatriculas() > acta.getNumeroMaximoMatriculas()) {
            throw new InconsistenciaDeDatosException("El nombre de matrícules es més gran que el permés");
        }
    }

    public void enviaNotaEstudiantes(ActaRevision actaRevision, User user) {
        List<ActaEstudiante> listaActaEstudiantes = actaEstudianteService
                .getActaEstudianteByActaRevision(actaRevision, user.getId()).stream()
                .filter(e -> e.getMotivoExclusion() == null || e.getMotivoExclusion().isEmpty())
                .collect(Collectors.toList());

        String asunto = "";
        String contenido = "";
        List<UIEmail> emails = new ArrayList<>();
        for (ActaEstudiante actaEstudiante : listaActaEstudiantes) {
            asunto = getAsuntoEmailNotas(actaEstudiante);
            contenido = getTextoEmailNotas(actaEstudiante, actaRevision);
            emails.add(new UIEmail(actaEstudiante.getPersona().getEmail(), asunto, contenido));
        }
        correoService.enviaEmails(emails);
        notificacionService.registraNotificacion(actaRevision.getActa().getId(),
                actaRevision.getId(), Notificacion.TIPO_AUTO, asunto, contenido);
    }

    public UIEntity getEjemploMailEstudiante(ActaRevision actaRevision, Long connectedUserId) {
        List<ActaEstudiante> listaActaEstudiantes = actaEstudianteService
                .getActaEstudianteByActaRevision(actaRevision, connectedUserId).stream()
                .filter(e -> e.getMotivoExclusion() == null || e.getMotivoExclusion().isEmpty())
                .collect(Collectors.toList());

        ActaEstudiante actaEstudiante = listaActaEstudiantes.get(0);
        String contenido = getTextoEmailNotas(actaEstudiante, actaEstudiante.getActaAsignatura()
                .getActa().getActasRevisiones().stream().findFirst().get());
        String asunto = getAsuntoEmailNotas(actaEstudiante);

        UIEntity ui = new UIEntity();
        ui.put("titulo", asunto);
        ui.put("contenido", contenido);

        return ui;
    }

    private String getAsuntoEmailNotas(ActaEstudiante actaEstudiante) {
        return MessageFormat.format("Notes assignatura {0} {1} - {2} - {3}",
                actaEstudiante.getActaAsignatura().getAsignatura().getId(),
                actaEstudiante.getActaAsignatura().getAsignatura().getNombre(),
                actaEstudiante.getActaAsignatura().getActa().getCurso().getId().toString(),
                actaEstudiante.getActaAsignatura().getActa().getConvocatoria().getNombre());
    }

    private String getTextoEmailNotas(ActaEstudiante actaEstudiante, ActaRevision actaRevision) {
        SimpleDateFormat humanDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat humanTimeFormat = new SimpleDateFormat("HH:mm");

        String revisionTxt = MessageFormat.format(
                "\nRevisió:\n - {0}, dia {1} de {2} a {3} en {4}\n", actaRevision.getNombre(),
                humanDateFormat.format(actaRevision.getFecha()),
                humanTimeFormat.format(actaRevision.getFecha()),
                humanTimeFormat.format(actaRevision.getFechaFin()), actaRevision.getLugar());

        Calificacion calificacion = Calificacion
                .getCalificacionById(actaEstudiante.getCalificacionId());

        String calificacionTxt;
        if (actaEstudiante.isNoPresentado() || actaEstudiante.getNotaEfectiva() == null) {
            calificacionTxt = "\nLa teva qualificació és no presentat";
        } else {
            calificacionTxt = MessageFormat.format(
                    "\nLa teva nota és un {0}, amb qualificació {1}.",
                    actaEstudiante.getNotaEfectiva(),
                    (calificacion != null) ? calificacion.getNombre() : "No presentat");
        }

        String publicacionTxt = "";
        if (actaRevision.getMuestraNotasInicio() != null && actaRevision.getMuestraNotasFin() != null) {
            publicacionTxt = MessageFormat.format(
                    "Les notes estaran disponibles al tauler virtual del {0} al {1}.",
                    humanDateFormat.format(actaRevision.getMuestraNotasInicio()),
                    humanDateFormat.format(actaRevision.getMuestraNotasFin()));
        }

        String fechaEnvio = MessageFormat.format("Data d''enviament: {0}",
                humanDateFormat.format(new Date()));
        return MessageFormat.format("{0}\n{1}\n{2}\n{3}\n{4}", getAsuntoEmailNotas(actaEstudiante),
                calificacionTxt, revisionTxt, publicacionTxt, fechaEnvio);
    }

    public List<ActaDepartamento> getActasTraspasadasProfesorByCursoAndConvocatoria(Long cursoId,
                                                                                    Long convocatoriaId, Long connectedUserId) {
        return actaDepartamentoDAO.getActasTraspasadasProfesorByCursoAndConvocatoria(cursoId,
                convocatoriaId, connectedUserId);
    }

    public List<ActaDepartamento> getActasTraspasadasProfesorODirectorDepartamentoByCursoAndConvocatoria(Long cursoId,
                                                                                                         Long convocatoriaId, Long connectedUserId) {
        return actaDepartamentoDAO.getActasTraspasadasProfesorODirectorDepartamentoByCursoAndConvocatoria(cursoId,
                convocatoriaId, connectedUserId);
    }

    public List<Acta> getActasByActaEstudianteIds(List<Long> actaEstudianteIds) {
        return ((ActaDAO) dao).getActasByActaEstudianteIds(actaEstudianteIds);
    }

    public void pasarNotasExpediente(Acta acta, List<Long> listaActaEstudianteIds, Long connectedUserId)
            throws IOException,
            PasarNotasExpedienteException, NoAutorizadoException, TraspasarActaException {
        acta.comprobarSePuedePasarNotasExpediente();
        for (Long actaEstudianteId : listaActaEstudianteIds) {
            acta.pasarNotasExpediente(actaEstudianteId, connectedUserId);
        }
    }

    @Transactional
    public void toggleDesgloseActa(Long actaId, Boolean permiteDetalle, Long connectedUserId)
            throws NoAutorizadoException, ActaNoEditableException, RegistroNoEncontradoException {
        authService.checkPuedeAccederActa(Acceso.ESCRITURA, actaId, connectedUserId);
        ((ActaDAO) dao).setPermiteDetalle(actaId, permiteDetalle);
    }

    public List<Acta> getActasParcialesByCursoAndCodigo(Long cursoId, String codigo,
                                                        Long connectedUserId) {
        return ((ActaDAO) dao).getActasParcialesByCursoAndCodigo(cursoId, codigo);
    }

    public void checkNotasCaluladasActa(Acta acta) throws TraspasarActaException {
        Double TOLERANCE = 0.000001;

        List<NotaCalculada> notasCalculadas = ((ActaDAO) dao)
                .getNotasCalculadasByActa(acta.getId());

        for (ActaEstudiante actaEstudiante : acta.getActaEstudiantes()) {
            NotaCalculada notaCalculada = notasCalculadas.stream()
                    .filter(nc -> nc.getEstudianteId().equals(actaEstudiante.getId())).findFirst()
                    .orElse(null);

            Double notaJava = actaEstudiante.isNoPresentado() ? null
                    : actaEstudiante.getNotaDesgloseCalculada().getNota();
            Double notaPL = notaCalculada.getNotaCalculada();
            if (!sonIguales(notaJava, notaPL, TOLERANCE)) {
                throw new TraspasarActaException(MessageFormat.format(
                        "Inconsistència de dades alumne {0} nota JAVA {1} nota PL {2}",
                        actaEstudiante.getPersona().getNombreCompleto(), notaJava, notaPL));
            }
        }
    }

    public void notificarResponsablesAsignatura(Long cursoId, Convocatoria convocatoria,
                                                List<String> codigos, String mensaje, User connectedUser)
            throws MessageNotSentException {
        class Tuple {
            private String email;
            private String asignatura;

            public Tuple(String email, String asignatura) {
                this.email = email;
                this.asignatura = asignatura;
            }

            public String getEmail() {
                return email;
            }

            public String getAsignatura() {
                return asignatura;
            }
        }

        StringBuilder report = new StringBuilder(
                "S'ha enviat un email als responsables d'aquestes assignatures:\n\n");
        List<Tuple> items = new ArrayList<>();
        List<String> responsables = new ArrayList<>();
        for (String codigo : codigos) {
            List<ResponsableAsignatura> responsablesAsignatura = responsableAsignaturaService
                    .getReponsablesAsignaturaPuedenTraspasar(cursoId, codigo);
            responsables.clear();
            for (ResponsableAsignatura responsableAsignatura : responsablesAsignatura) {
                responsables.add(responsableAsignatura.getPersona().getNombreCompleto() + " ("
                        + responsableAsignatura.getPersona().getEmail() + ")");
                items.add(new Tuple(responsableAsignatura.getEmail(), codigo));
            }
            report.append(codigo + " : " + String.join(",", responsables) + "\n");
        }

        Map<String, List<Tuple>> tuplasPorEmail = items.stream()
                .collect(Collectors.groupingBy(Tuple::getEmail));
        List<UIEmail> emails = new ArrayList<>();
        for (Map.Entry<String, List<Tuple>> entry : tuplasPorEmail.entrySet()) {
            String asunto = MessageFormat.format("Notificació convocatoria {0} curs {1}",
                    convocatoria.getNombre(), cursoId.toString());
            String asignaturas = entry.getValue().stream().map(ui -> ui.getAsignatura())
                    .collect(Collectors.joining(", "));
            String contenido = MessageFormat.format("{0}\n\nAssignatura(es): {1}", mensaje,
                    asignaturas);
            UIEmail uiEmail = new UIEmail(entry.getKey(), asunto, contenido);
            emails.add(uiEmail);
            // report.append(MessageFormat.format("{0}: {1}\n", entry.getKey(), asignaturas));
        }
        correoService.enviaEmails(emails);
        correoService.enviaEmail(new UIEmail(connectedUser.getName() + "@uji.es",
                "Resum de notificacions enviades", report.toString()));
    }

    private Boolean sonIguales(Double nota1, Double nota2, Double tolerance) {
        if ((nota1 == null) && (nota2 == null)) {
            return true;
        }
        if (nota1 == null || nota2 == null) {
            return false;
        }
        return Math.abs(nota1 - nota2) < tolerance;
    }

    public List<Acta> getListActasByCursoAndConvocatoria(Long cursoId, Long convocatoriaId,
                                                         Long connectedUserId) {
        return ((ActaDAO) dao).getListActasByCursoAndConvocatoria(cursoId, convocatoriaId);
    }

    public void checkNumeroEstudiantesActa(Long cursoId, String codigo, Long connectedUserId)
            throws ActaSinEstudiantesException {
        ActaPersona actaPersona = actaPersonaDAO
                .getActaPersonaByCursoAndAsignaturasAndPersona(cursoId, codigo, connectedUserId);

        Long numeroEstudiantes = actaPersona.getNumeroEstudiantes();

        if (numeroEstudiantes == null || numeroEstudiantes.equals(0L)) {
            throw new ActaSinEstudiantesException();
        }
    }

    public List<CursoMoodle> getCursosMoodleByCursoAcademicoYAsignatura(Long actaId, Long connectedUserId)
            throws RegistroNoEncontradoException, ActaNoEditableException, NoAutorizadoException {
        authService.checkPuedeAccederActa(Acceso.ESCRITURA, actaId, connectedUserId);

        Acta acta = ((ActaDAO) dao).getActaById(actaId);
        List<String> listaActasAsignaturas = actaAsignaturaDAO.getActaAsignaturasByActa(actaId).stream().map(a -> a.getAsignatura().getId()).collect(Collectors.toList());

        return ((ActaDAO) dao).getCursosMoodleByCursoAcademicoYAsignatura(acta.getCurso().getId(), listaActasAsignaturas, connectedUserId);
    }

    public Acta getActaByCursoAndCodigoAndConvocatoria(long cursoAcademicoId, String asignaturaId, long personaId) {
        return ((ActaDAO) dao).getActaByCursoAndCodigoAndConvocatoria(cursoAcademicoId, asignaturaId, personaId);
    }

    public List<ActaDepartamento> getActasTraspasadasPorCursoConvocatoriaYCodigoAsignatura(Long cursoAcademicoId, Long convocatoriaId, String asignaturaId, Long connectedUserId) {
        return actaDepartamentoDAO
                .getActasTraspasadasPorCursoConvocatoriaYCodigoAsignatura(cursoAcademicoId, convocatoriaId, asignaturaId, connectedUserId);

    }

    public List<Acta> getListCodigosActasByCurso(Long cursoId, Long convocatoriaId, Long connectedUserId) {
        List<Acta> listaActas = ((ActaDAO) dao).getActasByCurso(cursoId);

        HashSet<String> codigoYaRegistrado = new HashSet<>();

        if (convocatoriaId != null) {
            listaActas.removeIf(a -> a.getConvocatoria().getId() != convocatoriaId);
        }

        listaActas.removeIf(a -> !codigoYaRegistrado.add(a.getDescripcion()));

        return listaActas;
    }

    public List<Acta> getListCodigosActasByCursoAndQuery(Long cursoId, String query, Long connectedUserId) {
        List<Acta> listaActas = ((ActaDAO) dao).getActasByCursoAndQuery(cursoId, query);

        HashSet<String> codigoYaRegistrado = new HashSet<>();

        listaActas.removeIf(a -> !codigoYaRegistrado.add(a.getDescripcion()));

        return listaActas;
    }

}
