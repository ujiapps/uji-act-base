package es.uji.apps.act.services;

import es.uji.apps.act.dao.ActaRevisionDAO;
import es.uji.apps.act.exceptions.InconsistenciaDeDatosException;
import es.uji.apps.act.models.ActaRevision;
import es.uji.apps.act.models.views.ActaConvocatoria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.List;

@Service
public class ActaRevisionService extends BaseService<ActaRevision>
{
    private ActaConvocatoriaService actaConvocatoriaService;

    @Autowired
    public ActaRevisionService(ActaRevisionDAO dao, ActaConvocatoriaService actaConvocatoriaService)
    {
        super(dao, ActaRevision.class);
        this.actaConvocatoriaService = actaConvocatoriaService;
    }

    public List<ActaRevision> getActaRevisionesByActa(Long actaId, Long connectedUserId)
    {
        return ((ActaRevisionDAO) dao).getActaRevisionesByActa(actaId);
    }

    @Override
    public ActaRevision insert(ActaRevision actaRevision, Long connectedUserId)
            throws InconsistenciaDeDatosException
    {
        checkFechasActaRevision(actaRevision);
        vaciaAsignaturasYGruposSiRevisionConjunta(actaRevision);
        return dao.insert(actaRevision);
    }

    @Override
    public ActaRevision update(ActaRevision actaRevision, Long connectedUserId)
            throws InconsistenciaDeDatosException
    {
        checkFechasActaRevision(actaRevision);
        vaciaAsignaturasYGruposSiRevisionConjunta(actaRevision);
        return dao.update(actaRevision);
    }

    private void checkFechasActaRevision(ActaRevision actaRevision)
            throws InconsistenciaDeDatosException
    {
        if (actaRevision.getFecha() == null && actaRevision.getFechaFin() == null)
        {
            return;
        }
        if ((actaRevision.getFecha() == null && actaRevision.getFechaFin() != null)
                || (actaRevision.getFecha() != null && actaRevision.getFechaFin() == null))
        {
            throw new InconsistenciaDeDatosException("S'han de definir les dues dates");
        }
        if (actaRevision.getFecha().after(actaRevision.getFechaFin()))
        {
            throw new InconsistenciaDeDatosException(
                    "L''hora d'inici no pot ser posterior a l''hora de fi");
        }
        ActaConvocatoria actaConvocatoria = actaConvocatoriaService
                .getActaConvocatoriaByActaId(actaRevision.getActa().getId());

        if (actaConvocatoria.getFechaExamen() != null && (actaRevision.getFecha()
                .before(DateUtil.addDays(actaConvocatoria.getFechaExamen(), 1))))
        {
            SimpleDateFormat humanDateFormat = new SimpleDateFormat("dd/MM/yyyy");
            throw new InconsistenciaDeDatosException(MessageFormat.format(
                    "El dia de la revisió ha de ser posterior a la data de l''examen {0}",
                    humanDateFormat.format(actaConvocatoria.getFechaExamen())));
        }

        if (actaRevision.getMuestraNotasInicio() == null && actaRevision.getMuestraNotasFin() == null)
        {
            return;
        }
        if ((actaRevision.getMuestraNotasInicio() == null && actaRevision.getMuestraNotasFin() != null)
                || (actaRevision.getMuestraNotasInicio() != null && actaRevision.getMuestraNotasFin() == null))
        {
            throw new InconsistenciaDeDatosException("S'han de definir les dues dates de publicació");
        }
        if (actaRevision.getMuestraNotasInicio().after(actaRevision.getMuestraNotasFin()))
        {
            throw new InconsistenciaDeDatosException(
                    "La data d'inici no pot ser posterior a la data de fi");
        }
    }

    private void vaciaAsignaturasYGruposSiRevisionConjunta(ActaRevision actaRevision)
    {
        if (actaRevision.isConjunta())
        {
            actaRevision.setAsignaturas("");
            actaRevision.setGrupos("");
        }
    }
}