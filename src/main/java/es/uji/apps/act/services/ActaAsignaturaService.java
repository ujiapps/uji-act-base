package es.uji.apps.act.services;

import es.uji.apps.act.dao.ActaAsignaturaDAO;
import es.uji.apps.act.models.ActaAsignatura;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActaAsignaturaService extends BaseService<ActaAsignatura> {
    @Autowired
    public ActaAsignaturaService(ActaAsignaturaDAO dao) {
        super(dao, ActaAsignatura.class);
    }

    public List<ActaAsignatura> getActaAsignaturasByActa(Long actaId, Long connectedUserId) {
        return ((ActaAsignaturaDAO) dao).getActaAsignaturasByActa(actaId);
    }


    public ActaAsignatura getActaAsignaturaById(Long actaAsignaturaId) {
        return ((ActaAsignaturaDAO) dao).getActaAsignaturaById(actaAsignaturaId);
    }

}