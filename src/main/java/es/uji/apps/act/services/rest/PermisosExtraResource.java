package es.uji.apps.act.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.act.exceptions.ActaNoEncontradaException;
import es.uji.apps.act.exceptions.InconsistenciaDeDatosException;
import es.uji.apps.act.exceptions.NoAutorizadoException;
import es.uji.apps.act.models.Acta;
import es.uji.apps.act.models.FechaConvocatoria;
import es.uji.apps.act.models.PermisosExtra;
import es.uji.apps.act.services.ActaService;
import es.uji.apps.act.services.PermisosExtraService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Path("permisosExtra")
public class PermisosExtraResource extends CoreBaseService {
    @InjectParam
    private PermisosExtraService permisosExtraService;

    @InjectParam
    private ActaService actaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPermisosExtra()
            throws NoAutorizadoException, ActaNoEncontradaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<PermisosExtra> permisosExtra = permisosExtraService.getPermisosExtra(connectedUserId);

        return UIEntity.toUI(permisosExtra);
    }

    @PUT
    @Path("{permisoExtraId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateFechaPermisoExtra(@PathParam("permisoExtraId") Long permisoExtraId,
                                            UIEntity entity) throws ParseException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy H:mm");
        Date fechaFin = null;

        if (entity.get("fechaFin") != null) {
            fechaFin = dateTimeFormat.parse(entity.get("fechaFin"));
        }

        PermisosExtra permisosExtra = permisosExtraService.updateFechaFin(permisoExtraId, fechaFin, connectedUserId);
        return UIEntity.toUI(permisosExtra);
    }

    @DELETE
    @Path("{permisoExtraId}")
    public Response deleteBloque(@PathParam("permisoExtraId") Long permisoExtraId, UIEntity entity) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        permisosExtraService.delete(permisoExtraId, connectedUserId);
        return Response.ok().build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity setTema(UIEntity entity) throws InconsistenciaDeDatosException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        PermisosExtra permisosExtra = entity.toModel(PermisosExtra.class);
        permisosExtra.setFecha(new Date());
        // Fecha fin catorce días a partir de hoy
        permisosExtra.setFechaFin(new Date((new Date()).getTime() + (1000 * 60 * 60 * 24 * 14)));
        if (permisosExtra.getAsignaturaId().contains(",")) {
            permisosExtra.setAsignaturaId(permisosExtra.getAsignaturaId().split(",")[0]);
        }
        permisosExtraService.insert(permisosExtra, connectedUserId);
        return UIEntity.toUI(permisosExtra);
    }

}
