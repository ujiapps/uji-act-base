package es.uji.apps.act.services;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.multipart.FormDataMultiPart;

import es.uji.apps.act.exceptions.NoAutorizadoException;

@Service
public class ImportRemoteDataMoodleService implements ImportRemoteDataService
{
    final char DELIMITER = ';';

    @Value("${uji.moodle.saltKey}")
    private String saltKey;

    @Value("${uji.moodle.privateKey}")
    private String privateKey;

    @Value("${uji.moodle.authToken}")
    private String authToken;

    @Value("${uji.moodle.url}")
    private String url;

    public static byte[] hexStringToByteArray(String s)
    {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2)
        {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    @Override
    public String getNotasDeMoodle(Long cursoId, Long connectedUserId)
            throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, InvalidKeySpecException, NoAutorizadoException
    {
        WebResource moodleClient = Client.create().resource(url);

        FormDataMultiPart multiPart = new FormDataMultiPart();
        // multiPart.field("courseidnumber", cursoId.toString());
        multiPart.field("courseidnumber", cursoId.toString());

        multiPart.field("teacherperid", connectedUserId.toString());

        ClientResponse response = moodleClient.type(MediaType.MULTIPART_FORM_DATA)
                .header("X-UJI-AuthToken", authToken).post(ClientResponse.class, multiPart);

        String result = response.getEntity(String.class);

        if (response.getStatus() != 200)
        {
            throw new NoAutorizadoException();
        }

        return decryptCSV(result);

    }

    private String decryptCSV(String result)
            throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException
    {
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        KeySpec spec = new PBEKeySpec(privateKey.toCharArray(), saltKey.getBytes(), 1000, 128);
        SecretKey tmp = factory.generateSecret(spec);
        SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        byte[] arrayDeBytes = decodeHexString(result.substring(0, 32));
        IvParameterSpec ivspec = new IvParameterSpec(arrayDeBytes, 0, cipher.getBlockSize());
        cipher.init(Cipher.DECRYPT_MODE, secretKey, ivspec);
        String encryptedBase64CSV = result.substring(32);
        System.out.println(encryptedBase64CSV);
        return new String(cipher.doFinal(Base64.getDecoder().decode(encryptedBase64CSV)));
    }

    /**
     * Convierte una cadena hexadecimal a un array the bytes.
     * <p>
     * Este método lo hemos adaptado de https://www.baeldung.com/java-byte-arrays-hex-strings.
     *
     * @param hexString La cadena hexadecimal.
     * @return array de bytes con la cadena.
     */
    public static byte[] decodeHexString(String hexString)
    {
        if (hexString.length() % 2 == 1)
        {
            throw new IllegalArgumentException(
                    "Invalid hexadecimal String supplied.");
        }
        byte[] bytes = new byte[hexString.length() / 2];
        for (int i = 0; i < hexString.length(); i += 2)
        {
            bytes[i / 2] = hexToByte(hexString.substring(i, i + 2));
        }
        return bytes;
    }

    public static byte hexToByte(String hexString)
    {
        int firstDigit = toDigit(hexString.charAt(0));
        int secondDigit = toDigit(hexString.charAt(1));
        return (byte) ((firstDigit << 4) + secondDigit);
    }

    private static int toDigit(char hexChar)
    {
        int digit = Character.digit(hexChar, 16);
        if (digit == -1)
        {
            throw new IllegalArgumentException(
                    "Invalid Hexadecimal Character: " + hexChar);
        }
        return digit;
    }

}
