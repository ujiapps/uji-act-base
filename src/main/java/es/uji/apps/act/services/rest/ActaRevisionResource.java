package es.uji.apps.act.services.rest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.act.exceptions.ActaNoEditableException;
import es.uji.apps.act.exceptions.InconsistenciaDeDatosException;
import es.uji.apps.act.exceptions.NoAutorizadoException;
import es.uji.apps.act.models.ActaRevision;
import es.uji.apps.act.models.enums.Acceso;
import es.uji.apps.act.services.ActaRevisionService;
import es.uji.apps.act.services.ActaService;
import es.uji.apps.act.services.AuthService;
import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.User;

@Path("actarevisiones")
public class ActaRevisionResource extends CoreBaseService
{
    @InjectParam
    private AuthService authService;

    @InjectParam
    private ActaRevisionService actaRevisionService;

    @InjectParam
    private ActaService actaService;

    @PathParam("actaId")
    Long actaId;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getActaRevisiones() throws ParseException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<ActaRevision> listaActaRevisiones = (actaId == null)
                ? actaRevisionService.getAll(connectedUserId)
                : actaRevisionService.getActaRevisionesByActa(actaId, connectedUserId);

        return creaListaActaRevisionesUI(listaActaRevisiones);
    }

    private List<UIEntity> creaListaActaRevisionesUI(List<ActaRevision> listaActaRevisiones)
            throws ParseException
    {
        List<UIEntity> listaUIs = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();

        for (ActaRevision actaRevision : listaActaRevisiones)
        {
            UIEntity ui = UIEntity.toUI(actaRevision);
            calendar.setTime(actaRevision.getFecha());
            ui.put("fechaInicio", calendar.getTime());
            ui.put("enviados", actaRevision.getNotificaciones().size());

            listaUIs.add(ui);
        }
        return listaUIs;
    }

    @POST
    @Path("{actaRevisionId}/envianotas")
    public Response enviaNotaEstudiantes(@PathParam("actaRevisionId") Long actaRevisionId)
            throws NoAutorizadoException, ActaNoEditableException, RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        User user = AccessManager.getConnectedUser(request);
        ActaRevision actaRevision = actaRevisionService.get(actaRevisionId, connectedUserId);
        authService.checkPuedeAccederActa(Acceso.LECTURA, actaRevision.getActa().getId(), connectedUserId);
        actaService.enviaNotaEstudiantes(actaRevision, user);
        return Response.ok().build();
    }

    @GET
    @Path("{actaRevisionId}/envianotas-preview")
    public UIEntity previewEmailNotificacionEstudiantes(@PathParam("actaRevisionId") Long actaRevisionId)
            throws NoAutorizadoException, ActaNoEditableException, RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        ActaRevision actaRevision = actaRevisionService.get(actaRevisionId, connectedUserId);
        authService.checkPuedeAccederActa(Acceso.LECTURA, actaId, connectedUserId);
        return actaService.getEjemploMailEstudiante(actaRevision, connectedUserId);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity insertActaRevision(UIEntity entity)
            throws NoAutorizadoException, ActaNoEditableException, ParseException,
            InconsistenciaDeDatosException, RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkPuedeAccederActa(Acceso.ESCRITURA, entity.getLong("actaId"),
                connectedUserId);
        ActaRevision actaRevision = revisionUIToActaRevision(entity);
        actaRevisionService.insert(actaRevision, connectedUserId);
        return UIEntity.toUI(actaRevision);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{actaRevisionId}")
    public UIEntity updateActaRevision(@PathParam("actaRevisionId") Long actaRevisionId,
            UIEntity entity) throws NoAutorizadoException, ActaNoEditableException, ParseException,
            InconsistenciaDeDatosException, RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkPuedeAccederActa(Acceso.ESCRITURA, entity.getLong("actaId"),
                connectedUserId);
        ActaRevision actaRevision = revisionUIToActaRevision(entity);
        actaRevisionService.update(actaRevision, connectedUserId);
        return UIEntity.toUI(actaRevision);
    }

    private ActaRevision revisionUIToActaRevision(UIEntity entity) throws ParseException
    {
        ActaRevision actaRevision = entity.toModel(ActaRevision.class);

        DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy H:mm");

        actaRevision.setFecha(dateTimeFormat.parse(entity.get("fecha")));
        actaRevision.setFechaFin(dateTimeFormat.parse(entity.get("fechaFin")));

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        if (entity.get("muestraNotasInicio") != null)
        {
            actaRevision.setMuestraNotasInicio(dateFormat.parse(entity.get("muestraNotasInicio")));
        }

        if (entity.get("muestraNotasFin") != null) {
            actaRevision.setMuestraNotasFin(dateFormat.parse(entity.get("muestraNotasFin")));
        }

        return actaRevision;
    }

    @DELETE
    @Path("{actaRevisionId}")
    public Response deleteActaRevision(@PathParam("actaRevisionId") Long actaRevisionId,
            UIEntity entity) throws NoAutorizadoException, ActaNoEditableException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkPuedeAccederActaRevision(Acceso.ESCRITURA, actaRevisionId,
                connectedUserId);
        actaRevisionService.delete(actaRevisionId, connectedUserId);
        return Response.ok().build();
    }

}
