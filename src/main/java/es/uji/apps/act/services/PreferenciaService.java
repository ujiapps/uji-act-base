package es.uji.apps.act.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.act.dao.PreferenciaDAO;
import es.uji.apps.act.models.Preferencia;
import es.uji.commons.rest.UIEntity;
import org.springframework.stereotype.Service;

@Service
public class PreferenciaService
{
    @InjectParam
    public PreferenciaDAO preferenciaDAO;

    public void updatePreferencia(UIEntity uiPreferencia, Long connectedUserId)
    {
        Preferencia preferencia = preferenciaDAO.getPreferencia(connectedUserId);

        if (preferencia == null)
        {
            preferencia = new Preferencia();
            preferencia.setPerId(connectedUserId);
            preferencia.setTema(uiPreferencia.get("tema"));
            preferenciaDAO.insert(preferencia);
        }
        else
        {
            preferencia.setTema(uiPreferencia.get("tema"));
            preferenciaDAO.update(preferencia);
        }
    }

    public Preferencia getPreferencias(Long connectedUserId)
    {
        return preferenciaDAO.getPreferencia(connectedUserId);
    }
}