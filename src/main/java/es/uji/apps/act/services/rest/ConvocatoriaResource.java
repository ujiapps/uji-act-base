package es.uji.apps.act.services.rest;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.act.exceptions.ActaNoEditableException;
import es.uji.apps.act.exceptions.InconsistenciaDeDatosException;
import es.uji.apps.act.exceptions.NoAutorizadoException;
import es.uji.apps.act.models.Convocatoria;
import es.uji.apps.act.models.DesgloseNota;
import es.uji.apps.act.models.FechaConvocatoria;
import es.uji.apps.act.models.enums.Acceso;
import es.uji.apps.act.services.ActaService;
import es.uji.apps.act.services.AuthService;
import es.uji.apps.act.services.ConvocatoriaService;
import es.uji.apps.act.services.IpUtil;
import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.User;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Path("convocatorias")
public class ConvocatoriaResource extends CoreBaseService {
    @InjectParam
    private ConvocatoriaService convocatoriaService;

    @InjectParam
    private AuthService authService;

    @InjectParam
    private ActaService actaService;

    @PathParam("cursoId")
    Long cursoId;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getConvocatoriasByCursoYCodigo() {
        List<Convocatoria> listaConvocatorias = convocatoriaService.getConvocatorias();
        return UIEntity.toUI(listaConvocatorias);
    }

    @GET
    @Path("oficiales")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getConvocatoriasOficiales() {
        List<Convocatoria> listaConvocatorias = convocatoriaService.getConvocatoriasOficiales();
        return UIEntity.toUI(listaConvocatorias);
    }

    @GET
    @Path("detalle")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getConvocatoriasDetalle(@QueryParam("cursoId") Long cursoId) {
        if (cursoId == null) {
            return UIEntity.toUI(Collections.emptyList());
        }

        List<FechaConvocatoria> listaConvocatorias = convocatoriaService
                .getFechasConvocatoriasOficiales(cursoId);
        return toConvocatoriasDetalleUI(listaConvocatorias);
    }

    @PUT
    @Path("detalle/{convocatoriaDetalleI}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateFechaConvocatoria(@PathParam("convocatoriaDetalleI") Long convocatoriaDetalleId,
                                            UIEntity entity) throws ParseException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy H:mm");
        Date fechaInicioTraspaso = null;
        Date fechaFinTraspaso = null;
        Date fechaInicioActaUnica = null;

        if (entity.get("fechaInicioTraspaso") != null) {
            fechaInicioTraspaso = dateTimeFormat.parse(entity.get("fechaInicioTraspaso"));
        }

        if (entity.get("fechaFinTraspaso") != null) {
            fechaFinTraspaso = dateTimeFormat.parse(entity.get("fechaFinTraspaso"));
        }

        if (entity.get("fechaInicioActaUnica") != null) {
            fechaInicioActaUnica = dateTimeFormat.parse(entity.get("fechaInicioActaUnica"));
        }

        FechaConvocatoria fechaConvocatoria = convocatoriaService.updateFechasConvocatoria(convocatoriaDetalleId, fechaInicioTraspaso, fechaFinTraspaso, fechaInicioActaUnica);
        return toConvocatoriaDetalleUI(fechaConvocatoria);
    }


    @POST
    @Path("{convocatoriaId}/enviarnotificacion")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage notificar(@PathParam("convocatoriaId") Long convocatoriaId, UIEntity uiEntity)
            throws NoAutorizadoException, MessageNotSentException {
        User user = AccessManager.getConnectedUser(request);
        authService.checkIsAdmin(user.getId());
        List<String> codigos = uiEntity.getArray("codigos");
        String mensaje = uiEntity.get("mensaje");
        Convocatoria convocatoria = convocatoriaService.get(convocatoriaId, user.getId());
        actaService.notificarResponsablesAsignatura(cursoId, convocatoria, codigos, mensaje, user);
        return new ResponseMessage(true);
    }

    private List<UIEntity> toConvocatoriasDetalleUI(
            List<FechaConvocatoria> listaFechasConvocatorias) {
        List<UIEntity> convocatoriasDetalle = new ArrayList<>();

        for (FechaConvocatoria fechaConvocatoria : listaFechasConvocatorias) {
            convocatoriasDetalle.add(toConvocatoriaDetalleUI(fechaConvocatoria));
        }
        return convocatoriasDetalle;
    }

    private UIEntity toConvocatoriaDetalleUI(FechaConvocatoria fechaConvocatoria) {
        UIEntity ui = new UIEntity();

        if (fechaConvocatoria == null) {
            return ui;
        }

        ui.put("id", fechaConvocatoria.getId());
        ui.put("convocatoriaId", fechaConvocatoria.getConvocatoria().getId());
        ui.put("nombre", fechaConvocatoria.getConvocatoria().getNombre());
        ui.put("tipoEstudio", fechaConvocatoria.getTipoEstudio().getNombre());
        ui.put("tipoEstudioId", fechaConvocatoria.getTipoEstudio().getId());
        ui.put("fechaInicioTraspaso", fechaConvocatoria.getFechaInicioTraspaso());
        ui.put("fechaFinTraspaso", fechaConvocatoria.getFechaFinTraspaso());
        ui.put("fechaInicioActaUnica", fechaConvocatoria.getFechaInicioActaUnica());
        ui.put("extraordinaria", fechaConvocatoria.getConvocatoria().isExtraordinaria());
        ui.put("semestre", fechaConvocatoria.getConvocatoria().getSemestre());
        return ui;
    }
}
