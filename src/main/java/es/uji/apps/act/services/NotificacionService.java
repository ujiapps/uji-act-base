package es.uji.apps.act.services;


import org.springframework.stereotype.Service;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.act.dao.NotificacionDAO;
import es.uji.apps.act.models.Acta;
import es.uji.apps.act.models.ActaRevision;
import es.uji.apps.act.models.Notificacion;

import java.util.List;

@Service
public class NotificacionService
{
    @InjectParam
    private AuthService authService;

    @InjectParam
    public NotificacionDAO notificacionDAO;


    public void registraNotificacion(Long actaId, Long actaRevisionId, String tipo, String asunto, String mensaje)
    {
        Notificacion notificacion = new Notificacion();
        notificacion.setActa(new Acta(actaId));
        notificacion.setTipo(tipo);
        notificacion.setAsunto(asunto);
        notificacion.setMensaje(mensaje);
        if (actaRevisionId != null)
        {
            notificacion.setActaRevision(new ActaRevision(actaRevisionId));
        }

        notificacionDAO.insert(notificacion);
    }

    public void registraNotificacion(Long actaId, String tipo, String asunto, String mensaje)
    {
        Notificacion notificacion = new Notificacion();
        notificacion.setActa(new Acta(actaId));
        notificacion.setTipo(tipo);
        notificacion.setAsunto(asunto);
        notificacion.setMensaje(mensaje);

        notificacionDAO.insert(notificacion);
    }

    public List<Notificacion> getNotificacionesByActa(Long actaId, Long connectedUserId)
    {
        return notificacionDAO.getNotificacionesByActa(actaId);
    }
}
