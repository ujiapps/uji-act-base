package es.uji.apps.act.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.act.exceptions.ActaNoEditableException;
import es.uji.apps.act.exceptions.NoAutorizadoException;
import es.uji.apps.act.exceptions.NotaNoValidaException;
import es.uji.apps.act.exceptions.TraspasarDiligenciaException;
import es.uji.apps.act.models.Acta;
import es.uji.apps.act.models.ActaDiligencia;
import es.uji.apps.act.models.enums.Acceso;
import es.uji.apps.act.models.enums.Calificacion;
import es.uji.apps.act.models.views.ActaDiligenciaVirtual;
import es.uji.apps.act.models.views.AsignaturaCursada;
import es.uji.apps.act.models.views.AsignaturaExpediente;
import es.uji.apps.act.services.*;
import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Path("actadiligencias")
public class ActaDiligenciaResource extends CoreBaseService
{
    @InjectParam
    private AuthService authService;

    @InjectParam
    private ActaDiligenciaService actaDiligenciaService;

    @InjectParam
    private AsignaturaCursadaService asignaturaCursadaService;

    @InjectParam
    private ActaService actaService;

    @PathParam("actaId")
    Long actaId;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getActaDiligencias()
            throws NoAutorizadoException, ActaNoEditableException, RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkPuedeAccederActa(Acceso.LECTURA, actaId, connectedUserId);

        return getUIActaDiligencias(connectedUserId);
    }

    @GET
    @Path("fueraplazo")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getActaDiligenciasFueraPlazo()
            throws NoAutorizadoException, ActaNoEditableException, RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Acta acta = actaService.getActaById(actaId);
        authService.checkPersonaTraspasoODirectorDepartamento(acta, connectedUserId);

        return getUIActaDiligencias(connectedUserId);
    }

    private List<UIEntity> getUIActaDiligencias(Long connectedUserId)
    {
        Acta acta = actaService.getActaByIdConNotasCalculadas(actaId);

        List<ActaDiligenciaVirtual> listaActaEstudiantes = actaDiligenciaService
                .getActaDiligenciaVirtualByActaId(actaId, connectedUserId);

        List<ActaDiligencia> listaActaDiligenciasEstudiantes = actaDiligenciaService
                .getActaDiligenciasByActaId(actaId);

        List<AsignaturaCursada> asignaturasCursadas = asignaturaCursadaService
                .getAsignaturasCursadasByActaId(actaId);

        List<AsignaturaExpediente> asignaturasExpediente = actaDiligenciaService
                .getAsignaturaExpedienteByActa(acta);

        return componerListaActaDiligenciasUI(acta, listaActaEstudiantes, listaActaDiligenciasEstudiantes,
                asignaturasCursadas, asignaturasExpediente);
    }

    private List<UIEntity> componerListaActaDiligenciasUI(
            Acta acta,
            List<ActaDiligenciaVirtual> listaActaEstudiantes,
            List<ActaDiligencia> listaActaDiligenciasEstudiantes,
            List<AsignaturaCursada> asignaturasCursadas,
            List<AsignaturaExpediente> asignaturasExpedientes)
    {
        List<UIEntity> listaUI = new ArrayList<>();

        for (ActaDiligenciaVirtual actaDiligenciaEstudiante : listaActaEstudiantes)
        {
            UIEntity ui = creaActaDiligenciasEstudiante(actaDiligenciaEstudiante);
            List<UIEntity> listaDiligenciasUI = creaListaDiligenciasEstudianteUI(
                    actaDiligenciaEstudiante.getActaEstudianteId(),
                    listaActaDiligenciasEstudiantes);
            ui.put("diligencias", listaDiligenciasUI);
            ui.put("noPresentado", actaDiligenciaEstudiante.getNoPresentado());
            Boolean isExpedienteAbierto = isExpedienteAbierto(actaDiligenciaEstudiante, asignaturasCursadas);
            ui.put("expedienteAbierto", isExpedienteAbierto);

            AsignaturaExpediente asignaturaExpediente = asignaturasExpedientes
                    .stream().filter(ae -> ae.getPersonaId().equals(actaDiligenciaEstudiante.getAlumnoId())).findFirst().orElse(null);
            Boolean aprobadoConvocatoriaAnterior = (asignaturaExpediente != null) ? asignaturaExpediente.isAprobadoConvocatoriaAnterior() : false;
            Boolean calificadoConvocatoriaPosterior = (asignaturaExpediente != null) ? asignaturaExpediente.isCalificadoConvocatoriaPosterior() : false;
            ui.put("aprobadoConvocatoriaAnterior", aprobadoConvocatoriaAnterior);
            ui.put("calificadoConvocatoriaPosterior", calificadoConvocatoriaPosterior);
            ui.put("editable", isExpedienteAbierto && acta.isDiligenciable() && !aprobadoConvocatoriaAnterior && !calificadoConvocatoriaPosterior);
            ui.put("editableFueraPlazo", isExpedienteAbierto && acta.isDiligenciableFueraPlazo() && !aprobadoConvocatoriaAnterior);

            listaUI.add(ui);
        }

        return listaUI;
    }

    private Boolean isExpedienteAbierto(ActaDiligenciaVirtual actaDiligenciaEstudiante,
                                        List<AsignaturaCursada> asignaturasCursadas)
    {
        Optional<AsignaturaCursada> asignaturaCursada = asignaturasCursadas.stream()
                .filter(ac -> ac.getPersonaId().equals(actaDiligenciaEstudiante.getAlumnoId()))
                .findFirst();
        return asignaturaCursada.isPresent() ? asignaturaCursada.get().isExpedienteAbierto()
                : false;
    }

    private UIEntity creaActaDiligenciasEstudiante(ActaDiligenciaVirtual actaDiligenciaEstudiante)
    {
        UIEntity ui = UIEntity.toUI(actaDiligenciaEstudiante);

        ui.put("nota", (actaDiligenciaEstudiante.getCalificacionId() == null) ? ""
                : actaDiligenciaEstudiante.getNota());

        ui.put("comentario", "");
        return ui;
    }

    private List<UIEntity> creaListaDiligenciasEstudianteUI(Long actaEstudianteId,
                                                            List<ActaDiligencia> listaActaDiligenciasEstudiantes)
    {
        List<ActaDiligencia> listaDiligenciasEstudiante = listaActaDiligenciasEstudiantes.stream()
                .filter(a -> a.getActaEstudiante().getId().equals(actaEstudianteId))
                .collect(Collectors.toList());

        return modelToUI(listaDiligenciasEstudiante);
    }

    protected UIEntity modelToUI(ActaDiligencia actaDiligencia)
    {
        UIEntity entity = UIEntity.toUI(actaDiligencia);

        entity.put("personaNombre", actaDiligencia.getPersona().getNombreCompleto());

        Calificacion calificacion = Calificacion
                .getCalificacionById(actaDiligencia.getCalificacionAnteriorId());
        entity.put("calificacionAnterior",
                (calificacion != null) ? calificacion.getNombre() : "No presentat");

        Calificacion calificacionNueva = Calificacion
                .getCalificacionById(actaDiligencia.getCalificacionNuevaId());
        entity.put("calificacionNueva",
                (calificacionNueva != null) ? calificacionNueva.getNombre() : "No presentat");

        entity.put("comentarioTraspaso", actaDiligencia.getComentarioTraspaso());

        return entity;
    }

    protected List<UIEntity> modelToUI(List<ActaDiligencia> actaDiligencias)
    {
        return actaDiligencias.stream().map(ad -> modelToUI(ad)).collect(Collectors.toList());
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage guardarActaDiligencias(UIEntity uiEntity)
            throws NoAutorizadoException, NotaNoValidaException,
            IOException, TraspasarDiligenciaException, MessageNotSentException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<ActaDiligenciaVirtual> actaDiligencias = uiEntity.getRelations().get("diligencias")
                .stream().map(e -> e.toModel(ActaDiligenciaVirtual.class))
                .collect(Collectors.toList());

        List<Long> actaEstudianteIds = actaDiligencias.stream().map(ad -> ad.getActaEstudianteId())
                .distinct().collect(Collectors.toList());

        List<Acta> actas = actaService.getActasByActaEstudianteIds(actaEstudianteIds);
        if (actas.size() > 1)
        {
            throw new NoAutorizadoException(
                    "S'ha detectat més d'una acta en l'operació de traspàs");
        }

        Acta acta = actas.get(0);
        if (!acta.getId().equals(actaId))
        {
            throw new NoAutorizadoException("Les diligències no pertanyen a l'acta");
        }

        authService.checkPuedeTraspasarActa(acta, connectedUserId);
        actaDiligenciaService.traspasarDiligencias(acta, actaDiligencias, connectedUserId, IpUtil.getClientAddres(request));

        return new ResponseMessage(true);
    }

}
