package es.uji.apps.act.services;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.act.dao.ActaDiligenciaFueraPlazoDAO;
import es.uji.apps.act.exceptions.NoAutorizadoException;
import es.uji.apps.act.exceptions.NotaNoValidaException;
import es.uji.apps.act.exceptions.TraspasarDiligenciaException;
import es.uji.apps.act.models.ActaDiligenciaFueraPlazo;
import es.uji.apps.act.models.enums.EstadoDiligenciaFueraPlazo;
import es.uji.apps.act.models.ui.UIDiligencia;
import es.uji.apps.act.models.views.ActaDiligenciaFueraPlazoModeracion;
import es.uji.commons.messaging.client.MessageNotSentException;

@Service
public class ActaDiligenciaFueraPlazoService extends BaseService<ActaDiligenciaFueraPlazo>
{
    @InjectParam
    ActaDiligenciaService actaDiligenciaService;

    @InjectParam
    AuthService authService;

    @InjectParam
    AvisosService avisosService;

    @InjectParam
    ResponsableAsignaturaService responsableAsignaturaService;

    @Autowired
    public ActaDiligenciaFueraPlazoService(ActaDiligenciaFueraPlazoDAO dao)
    {
        super(dao, ActaDiligenciaFueraPlazo.class);
    }

    public ActaDiligenciaFueraPlazo getActaDiligenciaFueraPlazoById(Long actaDiligenciaFueraPlazoId)
    {
        return ((ActaDiligenciaFueraPlazoDAO) dao).getActaDiligenciaFueraPlazoById(actaDiligenciaFueraPlazoId);
    }

    public List<ActaDiligenciaFueraPlazo> getActaDiligenciaFueraPlazoByPersonaId(Long personaId)
    {
        return ((ActaDiligenciaFueraPlazoDAO) dao).getActaDiligenciaFueraPlazoByPersonaId(personaId);
    }

    public List<ActaDiligenciaFueraPlazo> getActaDiligenciaFueraPlazoByModerador(Long personaId)
    {
        return ((ActaDiligenciaFueraPlazoDAO) dao).getActaDiligenciaFueraPlazoByModerador(personaId);
    }

    public ActaDiligenciaFueraPlazo insertActaDiligenciaFueraPlazo(ActaDiligenciaFueraPlazo actaDiligenciaFueraPlazo)
            throws TraspasarDiligenciaException, NotaNoValidaException
    {
        if (actaDiligenciaFueraPlazo.getComentarioDiligencia() == null || actaDiligenciaFueraPlazo.getComentarioDiligencia().isEmpty())
        {
            throw new TraspasarDiligenciaException("Cal introduir un comentari");
        }
        actaDiligenciaFueraPlazo.checkNotaAcordeConCalificacion();
        actaDiligenciaFueraPlazo = dao.insert(actaDiligenciaFueraPlazo);
        actaDiligenciaFueraPlazo = getActaDiligenciaFueraPlazoById(actaDiligenciaFueraPlazo.getId());

        List<String> emailResponsables = ((ActaDiligenciaFueraPlazoDAO) dao).getEmailsDirectoresCentroByActaId(actaDiligenciaFueraPlazo.getActa().getId());
        avisosService.notificaDiligenciaFueraPlazoResponsables(actaDiligenciaFueraPlazo, emailResponsables);
        return actaDiligenciaFueraPlazo;
    }

    public ActaDiligenciaFueraPlazo validar(ActaDiligenciaFueraPlazo actaDiligenciaFueraPlazo, Long connectedUserId)
            throws NoAutorizadoException, MessageNotSentException
    {
        authService.checkDirectorCentro(actaDiligenciaFueraPlazo.getActa(), connectedUserId);
        actaDiligenciaFueraPlazo.setEstado(EstadoDiligenciaFueraPlazo.APROBADA.getId());
        actaDiligenciaFueraPlazo.setFechaModeracion(new Date());
        actaDiligenciaFueraPlazo = dao.update(actaDiligenciaFueraPlazo);
        // Notificar al profesor
        avisosService.notificaDiligenciaFueraPlazoValidadaProfesor(actaDiligenciaFueraPlazo);
        return actaDiligenciaFueraPlazo;
    }

    public ActaDiligenciaFueraPlazo denegar(ActaDiligenciaFueraPlazo actaDiligenciaFueraPlazo, Long connectedUserId)
            throws NoAutorizadoException, MessageNotSentException
    {
        authService.checkDirectorCentro(actaDiligenciaFueraPlazo.getActa(), connectedUserId);
        actaDiligenciaFueraPlazo.setEstado(EstadoDiligenciaFueraPlazo.DENEGADA.getId());
        actaDiligenciaFueraPlazo.setFechaModeracion(new Date());
        actaDiligenciaFueraPlazo = dao.update(actaDiligenciaFueraPlazo);
        // Notificar al profesor
        avisosService.notificaDiligenciaFueraPlazoDenegadaProfesor(actaDiligenciaFueraPlazo);
        return actaDiligenciaFueraPlazo;
    }

    public ActaDiligenciaFueraPlazo firmar(ActaDiligenciaFueraPlazo actaDiligenciaFueraPlazo, Long connectedUserId, String ip)
            throws NoAutorizadoException, IOException, TraspasarDiligenciaException, MessageNotSentException
    {
        if (!actaDiligenciaFueraPlazo.getPersona().getId().equals(connectedUserId))
        {
            throw new NoAutorizadoException();
        }

        if (actaDiligenciaFueraPlazo.isCaducada())
        {
            throw new NoAutorizadoException("Ha passat el termini de signatura de la diligència");
        }

        actaDiligenciaService.addActaDiligencia(actaDiligenciaFueraPlazo, ip, connectedUserId);

        actaDiligenciaFueraPlazo.setEstado(EstadoDiligenciaFueraPlazo.FIRMADA.getId());
        actaDiligenciaFueraPlazo.setFechaFirma(new Date());
        dao.update(actaDiligenciaFueraPlazo);

        UIDiligencia uiDiligencia = getUIDiligencia(actaDiligenciaFueraPlazo);
        List<String> emailResponsables = responsableAsignaturaService.getReponsablesAsignaturaPuedenTraspasar(actaDiligenciaFueraPlazo.getActa().getCurso().getId(), actaDiligenciaFueraPlazo.getActa().getCodigosAsignaturas().get(0))
                .stream().map(ra -> ra.getEmail()).collect(Collectors.toList());

        avisosService.notificaDiligenciasResponsables(Arrays.asList(uiDiligencia), emailResponsables);
        avisosService.notificaDiligenciasAlumno(uiDiligencia);

        return actaDiligenciaFueraPlazo;
    }

    private UIDiligencia getUIDiligencia(ActaDiligenciaFueraPlazo actaDiligenciaFueraPlazo)
    {
        UIDiligencia uiDiligencia = new UIDiligencia();
        uiDiligencia.setAsignatura(actaDiligenciaFueraPlazo.getActa().getCodigo());
        uiDiligencia.setAlumno(actaDiligenciaFueraPlazo.getActaEstudiante().getPersona().getNombreCompleto());
        uiDiligencia.setEmail(actaDiligenciaFueraPlazo.getActaEstudiante().getPersona().getEmail());
        uiDiligencia.setCalificacion(actaDiligenciaFueraPlazo.getCalificacionNuevaAsString());
        uiDiligencia.setNota(actaDiligenciaFueraPlazo.getNotaNueva());
        uiDiligencia.setComentario(actaDiligenciaFueraPlazo.getComentarioDiligencia());
        return uiDiligencia;
    }

    public List<ActaDiligenciaFueraPlazoModeracion> getResponsablesByActaId(Long actaId)
    {
        return ((ActaDiligenciaFueraPlazoDAO) dao).getResponsablesByActaId(actaId);
    }

    @Transactional
    public void deleteActaDiligenciaFueraPlazo(Long actaDiligenciaFueraPlazoId, Long connectedUserId)
            throws TraspasarDiligenciaException, NoAutorizadoException
    {
        ActaDiligenciaFueraPlazo diligenciaFueraPlazo = dao.get(ActaDiligenciaFueraPlazo.class, actaDiligenciaFueraPlazoId).get(0);

        if (!authService.isAdmin(connectedUserId) && !diligenciaFueraPlazo.getPersona().getId().equals(connectedUserId))
        {
            throw new NoAutorizadoException("No tens permís per esborrar la diligència");
        }

        if (!diligenciaFueraPlazo.getEstado().equals(EstadoDiligenciaFueraPlazo.PENDIENTE.getId()))
        {
            throw new TraspasarDiligenciaException("Només es pot esborrar una diligència en estat pendent");
        }
        dao.delete(diligenciaFueraPlazo);
    }
}