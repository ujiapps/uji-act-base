package es.uji.apps.act.services;

import es.uji.apps.act.dao.AsignaturaOfertadaDAO;
import es.uji.apps.act.models.views.AsignaturaOfertada;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AsignaturaOfertadaService extends BaseService<AsignaturaOfertada>
{
    @Autowired
    public AsignaturaOfertadaService(AsignaturaOfertadaDAO dao)
    {
        super(dao, AsignaturaOfertada.class);
    }

    public List<AsignaturaOfertada> getAsignaturasCursadasByCurso(Long cursoId, Long estudioId)
    {
        return ((AsignaturaOfertadaDAO) dao).getAsignaturasOfertadasByCurso(cursoId, estudioId);
    }
}