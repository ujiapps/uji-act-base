package es.uji.apps.act.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.act.dao.FormacionDualDAO;
import es.uji.apps.act.exceptions.NoAutorizadoException;
import es.uji.apps.act.models.FormacionDual;
import es.uji.apps.act.models.views.AsignaturaOfertada;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class FormacionDualService extends BaseService<FormacionDual> {
    @InjectParam
    private AuthService authService;

    @InjectParam
    public FormacionDualDAO formacionDualDAO;

    @Autowired
    public FormacionDualService(FormacionDualDAO dao) {
        super(dao, FormacionDual.class);
    }


    public List<FormacionDual> getFormacionDualByCursoAcademicoId(Long connectedUserId) throws NoAutorizadoException {
        authService.checkIsAdmin(connectedUserId);
        return formacionDualDAO.getFormacionDual(connectedUserId);
    }


    public void deleteFormacionDual(Long formacionDualId, Long connectedUserId) throws NoAutorizadoException {

        authService.checkIsAdmin(connectedUserId);
        this.delete(formacionDualId, connectedUserId);
    }

    public List<FormacionDual> getFormacionDualByConnectedUserId(Long connectedUserId) {
        return formacionDualDAO.getFormacionDualByConnectedUserId(connectedUserId);
    }

    public List<AsignaturaOfertada> getAsignaturasOfertadas(List<FormacionDual> listaFormacionDual, Long connectedUserId) {
        List<String> listaCodigosAsignaturas = listaFormacionDual.stream().map(FormacionDual::getAsignaturaId).collect(Collectors.toList());
        return formacionDualDAO.getAsignaturasOfertadas(listaCodigosAsignaturas, connectedUserId);
    }
}
