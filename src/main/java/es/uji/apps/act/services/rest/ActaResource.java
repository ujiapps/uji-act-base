package es.uji.apps.act.services.rest;

import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.BodyPartEntity;
import com.sun.jersey.multipart.FormDataMultiPart;
import es.uji.apps.act.exceptions.*;
import es.uji.apps.act.models.Acta;
import es.uji.apps.act.models.CursoMoodle;
import es.uji.apps.act.models.PermisosExtra;
import es.uji.apps.act.models.enums.Acceso;
import es.uji.apps.act.models.ui.UINotaImport;
import es.uji.apps.act.models.views.ActaDepartamento;
import es.uji.apps.act.models.views.ActaPersona;
import es.uji.apps.act.services.*;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.User;
import org.apache.avalon.framework.parameters.ParameterException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;

@Path("actas")
public class ActaResource extends CoreBaseService {
    @InjectParam
    private AuthService authService;

    @InjectParam
    private PermisosExtraService permisosExtraService;

    @InjectParam
    private ActaService actaService;

    @InjectParam
    private ImportExportService importExportService;

    @InjectParam
    private ActaAsignaturaService actaAsignaturaService;

    @InjectParam
    private ActaRevisionService actaRevisionService;

    @InjectParam
    private ImportExportDataService importExportDataService;

    @InjectParam
    private ImportRemoteDataService importRemoteDataService;

    @InjectParam
    private DesgloseGrupoService desgloseGrupoService;

    @PathParam("cursoId")
    Long cursoId;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getActasPersonaByCurso() {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<ActaPersona> listaActas = actaService.getListaActasPersonaByCurso(cursoId,
                connectedUserId);
        return UIEntity.toUI(listaActas);
    }

    @GET
    @Path("admin")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getActasDepartamentoByCurso(
            @QueryParam("convocatoriaId") Long convocatoriaId) throws ParameterException {
        if (cursoId == null || convocatoriaId == null) {
            throw new ParameterException("Has d'introduir el curs i la convocatòria");
        }
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<ActaDepartamento> listaActas = actaService
                .getListaActasDepartamentoByCursoAndConvocatoria(cursoId, convocatoriaId,
                        connectedUserId);

        return listaActas.stream().map(a -> modelToUI(a)).collect(Collectors.toList());
    }

    @GET
    @Path("{actaId}/cursos-moodle")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCursosMoodle(
            @PathParam("actaId") Long actaId)
            throws ParameterException, NoAutorizadoException, ActaNoEditableException, RegistroNoEncontradoException {
        if (actaId == null) {
            throw new ParameterException("Has d'introduir el acta per obtindre els cursos");
        }

        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<CursoMoodle> listaCursos = actaService
                .getCursosMoodleByCursoAcademicoYAsignatura(actaId,
                        connectedUserId);

        if (listaCursos.size() == 0) {
            throw new RegistroNoEncontradoException("No s'ha trobat cap curs en l'AulaVirtual a on figures com a professorat d'aquesta acta.");
        }
        return listaCursos.stream().map(a -> UIEntity.toUI(a)).collect(Collectors.toList());
    }

    @GET
    @Path("{actaId}/cursos-moodle/{cursoId}/importar")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> importarNotasMoodle(
            @PathParam("actaId") Long actaId, @PathParam("cursoId") Long cursoId)
            throws ParameterException, NoAutorizadoException, ActaNoEditableException, RegistroNoEncontradoException, ImportacionException, IOException, NoSuchPaddingException, InvalidKeyException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, InvalidKeySpecException {
        if (actaId == null || cursoId == null) {
            throw new ParameterException("Has d'introduir el acta i el curs per obtindre les notes");
        }

        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkPuedeAccederActa(Acceso.ESCRITURA, actaId, connectedUserId);

        Acta acta = actaService.getActaConEstudiantes(actaId, connectedUserId);
        String notasCSV = importRemoteDataService.getNotasDeMoodle(cursoId, connectedUserId);

        Reader csv = new StringReader(notasCSV);
        List<Map<String, String>> records = importExportDataService.getFileAsRecordsMapped(csv);

        return UIEntity.toUI(importExportService.importarPrevio(acta, records));
    }

    private UIEntity modelToUI(ActaDepartamento actaDepartamento) {
        UIEntity uiEntity = UIEntity.toUI(actaDepartamento);
        uiEntity.put("editable", actaDepartamento.getFechaFinModificacion().after(new Date()));
        return uiEntity;
    }

    private UIEntity modelToUIPermisoExtra(Acta acta) {
        UIEntity uiEntity = new UIEntity();

        uiEntity.put("id", acta.getId());
        uiEntity.put("cursoId", acta.getCurso().getId());
        uiEntity.put("codigo", acta.getCodigo());

        return uiEntity;
    }

    private UIEntity modelToUIFormacionDual(Acta acta) {
        UIEntity uiEntity = new UIEntity();

        uiEntity.put("asignaturaId", acta.getDescripcion());
        uiEntity.put("curso", acta.getCurso().getId());

        return uiEntity;
    }

    @GET
    @Path("log")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getActasByCursoYConvocatoria(
            @QueryParam("convocatoriaId") Long convocatoriaId) throws ParameterException {
        if (cursoId == null || convocatoriaId == null) {
            throw new ParameterException("Has d'introduir el curs i la convocatòria");
        }
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Acta> listaActas = actaService.getListActasByCursoAndConvocatoria(cursoId,
                convocatoriaId, connectedUserId);
        return UIEntity.toUI(listaActas);
    }

    static class DistinctByKey<T> {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        Function<T, Object> keyExtractor;

        public DistinctByKey(Function<T, Object> ke) {
            this.keyExtractor = ke;
        }

        public boolean filter(T t) {
            return seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
        }
    }

    @GET
    @Path("traspasadas")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getActasTraspasadasByCursoAndConvocatoria(
            @QueryParam("convocatoriaId") Long convocatoriaId) throws ParameterException {
        if (cursoId == null || convocatoriaId == null) {
            throw new ParameterException("Has d'introduir el curs i la convocatòria");
        }
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<ActaDepartamento> listaActas = actaService.getActasTraspasadasProfesorByCursoAndConvocatoria(
                cursoId, convocatoriaId, connectedUserId);

        return listaActas.stream().map(a -> modelToUI(a)).filter(new DistinctByKey<UIEntity>(a -> a.get("actaId"))::filter)
                .collect(Collectors.toList());
    }

    @GET
    @Path("permisos-extra")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getActasPermisosExtraByCurso(@QueryParam("convocatoriaId") Long convocatoriaId) throws ParameterException {
        if (cursoId == null) {
            throw new ParameterException("Has d'introduir el curs");
        }
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Acta> listaActas = actaService.getListCodigosActasByCurso(
                cursoId, convocatoriaId, connectedUserId);

        return listaActas.stream().map(this::modelToUIPermisoExtra).collect(Collectors.toList());
    }

    @GET
    @Path("formacion-dual")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getActasFormacionDualByCurso(@QueryParam("query") String query) throws ParameterException {
        if (cursoId == null) {
            throw new ParameterException("Has d'introduir el curs");
        }
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Acta> listaActas = actaService.getListCodigosActasByCursoAndQuery(
                cursoId, query, connectedUserId);

        return listaActas.stream().map(this::modelToUIFormacionDual).collect(Collectors.toList());
    }

    @GET
    @Path("traspasadasProfesor")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getActasTraspasadasProfesorByCursoAndConvocatoria(
            @QueryParam("convocatoriaId") Long convocatoriaId) throws ParameterException {
        if (cursoId == null || convocatoriaId == null) {
            throw new ParameterException("Has d'introduir el curs i la convocatòria");
        }
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<ActaDepartamento> listaActas = actaService.getActasTraspasadasProfesorODirectorDepartamentoByCursoAndConvocatoria(cursoId, convocatoriaId, connectedUserId);

        List<PermisosExtra> listaPermisosExtra = permisosExtraService.getPermisosExtraByConnectedUserId(connectedUserId);


        List<ActaDepartamento> listaActasExtras = new ArrayList<ActaDepartamento>();
        for (PermisosExtra permisoExtra : listaPermisosExtra) {
            List<ActaDepartamento> lista = actaService.getActasTraspasadasPorCursoConvocatoriaYCodigoAsignatura(permisoExtra.getCursoAcademicoId(), convocatoriaId, permisoExtra.getAsignaturaId(), connectedUserId);
            listaActasExtras.addAll(lista);
        }

        List<UIEntity> result = new ArrayList<UIEntity>();
        result.addAll(listaActas.stream().map(a -> modelToUI(a)).collect(Collectors.toList()));
        result.addAll(listaActasExtras.stream().map(a -> modelToUI(a)).collect(Collectors.toList()));

        return result;
    }

    @GET
    @Path("parciales")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getActasTraspasadasByCursoAndConvocatoria(
            @QueryParam("codigo") String codigo) throws ParameterException {
        if (cursoId == null || codigo == null) {
            throw new ParameterException("Has d'introduir el curs i el codi");
        }
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Acta> listaActas = actaService.getActasParcialesByCursoAndCodigo(cursoId, codigo,
                connectedUserId);
        return UIEntity.toUI(listaActas);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity insertActa(UIEntity entity)
            throws RegistroNoEncontradoException, ActaSinEstudiantesException, TraspasarActaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Acta acta;
        Long actaId = entity.getLong("actaId");
        Long cursoId = entity.getLong("cursoId");
        String codigo = entity.get("codigo");
        Long convocatoriaId = entity.getLong("convocatoriaId");
        String descripcion = (entity.get("descripcion").isEmpty()) ? entity.get("codigo")
                : entity.get("descripcion");

        actaService.checkNumeroEstudiantesActa(cursoId, codigo, connectedUserId);

        actaService.checkTraspasosAnteriores(codigo, cursoId, convocatoriaId, connectedUserId);

        if (actaId == null) {
            acta = actaService.creaActa(cursoId, codigo, convocatoriaId, descripcion,
                    connectedUserId);
        } else {
            acta = actaService.get(actaId, connectedUserId);
        }
        return UIEntity.toUI(acta);
    }

    @GET
    @Path("{actaId}/exportar-csv")
    public Response csvExport(@PathParam("actaId") Long actaId,
                              @QueryParam("asignatura") String asignatura, @QueryParam("grupo") String grupo)
            throws NoAutorizadoException, ActaNoEditableException, RegistroNoEncontradoException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkPuedeAccederActa(Acceso.LECTURA, actaId, connectedUserId);

        Acta acta = actaService.getActaConEstudiantes(actaId, connectedUserId);
        Response.ResponseBuilder response = Response.ok();
        response.type(MediaType.TEXT_PLAIN);
        response.header("Content-Disposition",
                "attachment; filename = " + acta.getNombreFichero(asignatura, grupo) + ".csv");

        List<String[]> records = importExportService.exportar(acta, asignatura, grupo);
        response.entity(importExportDataService.getRecordsAsStringContent(records));
        return response.build();
    }

    @POST
    @Path("{actaId}/importar-previo")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public List<UIEntity> importarPrevio(FormDataMultiPart multiPart,
                                         @PathParam("actaId") Long actaId) throws RegistroNoEncontradoException,
            ActaNoEditableException, NoAutorizadoException, IOException, ImportacionException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkPuedeAccederActa(Acceso.ESCRITURA, actaId, connectedUserId);
        Acta acta = actaService.getActaConEstudiantes(actaId, connectedUserId);
        Reader csv = getFileReader(multiPart);
        List<Map<String, String>> records = importExportDataService.getFileAsRecordsMapped(csv);

        return UIEntity.toUI(importExportService.importarPrevio(acta, records));
    }

    @POST
    @Path("{actaId}/importar")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage importar(@PathParam("actaId") Long actaId, UIEntity data)
            throws RegistroNoEncontradoException, ActaNoEditableException, NoAutorizadoException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkPuedeAccederActa(Acceso.ESCRITURA, actaId, connectedUserId);
        List<UINotaImport> notas = getNotas(data);
        Acta acta = actaService.getActaConEstudiantes(actaId, connectedUserId);

        importExportService.importar(acta, notas, connectedUserId, IpUtil.getClientAddres(request));
        return new ResponseMessage(true);
    }

    private List<UINotaImport> getNotas(UIEntity data) {
        if (data.getRelations().isEmpty()) {
            return new ArrayList<>();
        }
        return data.getRelations().get("data").stream().map(ui -> ui.toModel(UINotaImport.class))
                .collect(Collectors.toList());
    }

    private Reader getFileReader(FormDataMultiPart multiPart) {
        for (BodyPart bodyPart : multiPart.getBodyParts()) {
            String mimeType = bodyPart.getHeaders().getFirst("Content-Type");
            if (mimeType != null && !mimeType.isEmpty()) {
                BodyPartEntity bodyPartEntity = (BodyPartEntity) bodyPart.getEntity();
                InputStream inputStream = bodyPartEntity.getInputStream();

                return new InputStreamReader(inputStream);
            }
        }
        return null;
    }

    @POST
    @Path("{actaId}/notificacion")
    public Response emailNotificacionEstudiantes(@PathParam("actaId") Long actaId, UIEntity entity)
            throws NoAutorizadoException, ActaNoEditableException, RegistroNoEncontradoException {
        User user = AccessManager.getConnectedUser(request);
        authService.checkPuedeAccederActa(Acceso.LECTURA, actaId, user.getId());
        String asunto = entity.get("asunto");
        String contenido = entity.get("contenido");
        actaService.enviaNotificacionEstudiantes(actaId, asunto, contenido, user);
        return Response.ok().build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{actaId}/desglose")
    public Response toggleDesgloseActa(@PathParam("actaId") Long actaId, UIEntity entity)
            throws NoAutorizadoException, ActaNoEditableException, RegistroNoEncontradoException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkPuedeAccederActa(Acceso.ESCRITURA, actaId, connectedUserId);
        Boolean permiteDetalle = new Boolean(entity.get("desgloseActivo"));
        actaService.toggleDesgloseActa(actaId, permiteDetalle, connectedUserId);
        return Response.ok().build();
    }

    @POST
    @Path("{actaId}/traspasar")
    public Response traspasarActa(@PathParam("actaId") Long actaId) throws IOException,
            TraspasarActaException, NoAutorizadoException, InconsistenciaDeDatosException, ActaNoEditableException, RegistroNoEncontradoException, ActaNoPermitidaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Acta acta = actaService.getActaByIdConNotasCalculadas(actaId);
        authService.checkPuedeTraspasarActa(acta, connectedUserId);
        actaService.traspasarActa(acta, connectedUserId, IpUtil.getClientAddres(request));
        return Response.ok().build();
    }

    @POST
    @Path("{actaId}/pasarnotasexpediente")
    public Response pasarNotasExpediente(@PathParam("actaId") Long actaId, UIEntity ui)
            throws IOException, TraspasarActaException, NoAutorizadoException,
            ActaNoEditableException, PasarNotasExpedienteException, RegistroNoEncontradoException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Acta acta = actaService.getActaById(actaId);
        List<Long> listaActaEstudianteIds = ui.getArray("actaEstudianteIds").stream().map(id -> ParamUtils.parseLong(id)).collect(Collectors.toList());
        authService.checkPuedeTraspasarActa(acta, connectedUserId);
        actaService.pasarNotasExpediente(acta, listaActaEstudianteIds, connectedUserId);
        return Response.ok().build();
    }

    @DELETE
    @Path("{actaId}")
    public Response deleteActa(@PathParam("actaId") Long actaId) throws ActaNoPermitidaException,
            NoAutorizadoException, ActaNoEditableException, RegistroNoEncontradoException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkPuedeAccederActa(Acceso.ESCRITURA, actaId, connectedUserId);
        actaService.deleteActa(actaId, connectedUserId);
        return Response.ok().build();
    }

    @PUT
    @Path("{actaId}/destraspasar")
    public Response destraspasarActa(@PathParam("actaId") Long actaId) throws ActaNoPermitidaException,
            NoAutorizadoException, ActaNoEditableException, RegistroNoEncontradoException, IOException, TraspasarActaException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkIsAdmin(connectedUserId);
        Acta acta = actaService.getActaById(actaId);
        actaService.destraspasarActa(acta, connectedUserId);
        return Response.ok().build();
    }

    @POST
    @Path("{actaId}/importar-desglose")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage importarDesglose(@PathParam("actaId") Long actaId, UIEntity data)
            throws RegistroNoEncontradoException, ActaNoEditableException, NoAutorizadoException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkPuedeAccederActa(Acceso.ESCRITURA, actaId, connectedUserId);
        //desgloseGrupoService.importarConfiguracionDesglose(actaId);
        return new ResponseMessage(true);
    }

    @Path("{actaId}/actaasignaturas")
    public ActaAsignaturaResource getActaAsignaturaResource(
            @InjectParam ActaAsignaturaResource actaAsignaturaResource) {
        return actaAsignaturaResource;
    }

    @Path("{actaId}/actaestudiantes")
    public ActaEstudianteResource getActaEstudianteResource(
            @InjectParam ActaEstudianteResource actaEstudianteResource) {
        return actaEstudianteResource;
    }

    @Path("{actaId}/actadiligencias")
    public ActaDiligenciaResource getActaEstudianteResource(
            @InjectParam ActaDiligenciaResource actaDiligenciaResource) {
        return actaDiligenciaResource;
    }

    @Path("{actaId}/actarevisiones")
    public ActaRevisionResource getActaRevisionResource(
            @InjectParam ActaRevisionResource actaRevisionResource) {
        return actaRevisionResource;
    }

    @Path("{actaId}/notificaciones")
    public NotificacionResource getActaRevisionResource(
            @InjectParam NotificacionResource notificacionResource) {
        return notificacionResource;
    }

    @Path("{actaId}/desglosegrupos")
    public DesgloseGrupoResource getActaGrupoResource(
            @InjectParam DesgloseGrupoResource desgloseGrupoResource) {
        return desgloseGrupoResource;
    }

    @Path("{actaId}/desglosepreguntas")
    public DesglosePreguntaResource getActaPreguntaResource(
            @InjectParam DesglosePreguntaResource desglosePreguntaResource) {
        return desglosePreguntaResource;
    }

    @Path("{actaId}/desglosenotas")
    public DesgloseNotaResource getActaPreguntaResource(
            @InjectParam DesgloseNotaResource desgloseNotaResource) {
        return desgloseNotaResource;
    }
}
