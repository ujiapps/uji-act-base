package es.uji.apps.act.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.act.dao.ActaDAO;
import es.uji.apps.act.dao.ActaEstudianteDAO;
import es.uji.apps.act.dao.DesgloseNotaDAO;
import es.uji.apps.act.dao.DesglosePreguntaDAO;
import es.uji.apps.act.exceptions.InconsistenciaDeDatosException;
import es.uji.apps.act.models.Acta;
import es.uji.apps.act.models.ActaEstudiante;
import es.uji.apps.act.models.DesgloseNota;
import es.uji.apps.act.models.DesglosePregunta;
import es.uji.apps.act.models.Persona;
import es.uji.apps.act.models.views.AsignaturaCursada;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

@Service
public class DesgloseNotaService extends BaseService<DesgloseNota>
{
    @Autowired
    public DesgloseNotaService(DesgloseNotaDAO dao)
    {
        super(dao, DesgloseNota.class);
    }

    @InjectParam
    private ActaDAO actaDAO;

    @InjectParam
    private DesglosePreguntaDAO desglosePreguntaDAO;

    @InjectParam
    private ActaEstudianteDAO actaEstudianteDAO;

    @InjectParam
    private ActaEstudianteService actaEstudianteService;

    @InjectParam
    private AsignaturaCursadaService asignaturaCursadaService;

    @InjectParam
    private LogService logService;

    public List<DesgloseNota> getDesgloseNotasByActaId(Long actaId, Long connectedUserId)
    {
        List<DesgloseNota> desgloseNotas = ((DesgloseNotaDAO) dao).getDesgloseNotasByActaId(actaId);
        List<Long> actaParcialesIds = desgloseNotas.stream()
                .filter(dn -> dn.getDesglosePregunta().getParcialActa() != null)
                .map(dn -> dn.getDesglosePregunta().getParcialActa().getId()).distinct()
                .collect(Collectors.toList());

        cargarNotasCalculadas(actaId, desgloseNotas);
        cargarNotasParciales(actaParcialesIds, desgloseNotas);
        cargaInformacionExpediente(actaId, desgloseNotas);

        return desgloseNotas;
    }

    protected void cargaInformacionExpediente(Long actaId, List<DesgloseNota> desgloseNotas)
    {
        List<AsignaturaCursada> asignaturasCursadas = asignaturaCursadaService.getAsignaturasCursadasByActaId(actaId);

        for (DesgloseNota desgloseNota : desgloseNotas)
        {
            Optional<AsignaturaCursada> asignaturaCursada = asignaturasCursadas.stream().filter(ac -> ac.getPersonaId().equals(desgloseNota.getActaEstudiante().getPersona().getId())).findFirst();
            if (asignaturaCursada.isPresent())
            {
                desgloseNota.getActaEstudiante().setExpedienteAbierto(asignaturaCursada.get().isExpedienteAbierto());
            }
            else
            {
                desgloseNota.getActaEstudiante().setExpedienteAbierto(false);
            }
        }
    }

    private void cargarNotasParciales(List<Long> actaParcialesIds, List<DesgloseNota> desgloseNotas)
    {
        for (Long actaParcialId : actaParcialesIds)
        {
            List<ActaEstudiante> actaEstudiantesParcial = actaEstudianteService
                    .getActaEstudianteByActaId(actaParcialId, 0L);

            for (ActaEstudiante actaEstudiante : actaEstudiantesParcial)
            {
                Optional<DesgloseNota> desgloseNota = desgloseNotas
                        .stream().filter(
                                dn -> dn.getDesglosePregunta().getParcialActa() != null
                                        && dn.getDesglosePregunta().getParcialActa().getId()
                                        .equals(actaEstudiante.getActaAsignatura().getActa()
                                                .getId())
                                        && dn.getActaEstudiante().getPersona().getId()
                                        .equals(actaEstudiante.getPersona().getId()))
                        .findFirst();
                if (desgloseNota.isPresent())
                {
                    Double notaEfectiva = actaEstudiante.getNotaEfectiva();
                    desgloseNota.get().setNota(notaEfectiva);
                }
            }
        }
    }

    private void cargarNotasCalculadas(Long actaId, List<DesgloseNota> desgloseNotas)
    {
        List<ActaEstudiante> actaEstudiantes = actaEstudianteService
                .getActaEstudianteByActaId(actaId, 0L);

        for (ActaEstudiante actaEstudiante : actaEstudiantes)
        {
            desgloseNotas.stream()
                    .filter(dn -> dn.getActaEstudiante().getPersona().getId()
                            .equals(actaEstudiante.getPersona().getId()))
                    .forEach(dn -> dn.setActaEstudiante(actaEstudiante));
        }
    }

    public DesgloseNota getDesgloseNotaById(Long desgloseNotaId)
            throws RegistroNoEncontradoException
    {
        return ((DesgloseNotaDAO) dao).getDesgloseNotaById(desgloseNotaId);
    }

    public DesgloseNota update(DesgloseNota desgloseNota, Long connectedUserId, String ip)
            throws InconsistenciaDeDatosException
    {
        DesglosePregunta desglosePregunta = desglosePreguntaDAO
                .getDesglosePreguntaByDesgloseNotaId(desgloseNota.getId());
        if (desgloseNota.getNota() != null
                && (desgloseNota.getNota().compareTo(desglosePregunta.getNotaMinima()) < 0
                || desgloseNota.getNota().compareTo(desglosePregunta.getNotaMaxima()) > 0))
        {
            String notaMin = desglosePregunta.getNotaMinima().toString();
            String notaMax = desglosePregunta.getNotaMaxima().toString();
            throw new InconsistenciaDeDatosException(
                    "La nota de la pregunta " + desglosePregunta.getEtiqueta()
                            + " ha d'estar dins del rang " + notaMin + " a  " + notaMax);
        }

        desgloseNota.setIp(ip);
        Persona personaNota = new Persona(connectedUserId);
        desgloseNota.setPersonaNota(personaNota);

        Date fechaActual = new Date();
        desgloseNota.setFechaNota(fechaActual);
        dao.update(desgloseNota);
        logService.registraNotaDesglose(desgloseNota, connectedUserId, ip);

        return desgloseNota;
    }

    public void sincronizaDesgloseNotas(Long actaId)
    {
        Acta acta = actaDAO.getActaById(actaId);
        if (acta == null || !acta.sePuedeSincronizar())
        {
            return;
        }

        List<DesglosePregunta> preguntas = desglosePreguntaDAO.getDesglosePreguntasByActaId(actaId);
        List<ActaEstudiante> estudiantes = actaEstudianteDAO.getActaEstudiantesByActaId(actaId);
        List<DesgloseNota> notas = ((DesgloseNotaDAO) dao).getDesgloseNotasByActaId(actaId);

        // Notas no creadas
        for (DesglosePregunta pregunta : preguntas)
        {
            for (ActaEstudiante estudiante : estudiantes)
            {
                Optional<DesgloseNota> nota = notas.stream()
                        .filter(n -> n.getActaEstudiante().getId().equals(estudiante.getId())
                                && n.getDesglosePregunta().getId().equals(pregunta.getId()))
                        .findFirst();
                if (!nota.isPresent())
                {
                    DesgloseNota nuevaNota = new DesgloseNota();
                    nuevaNota.setActaEstudiante(estudiante);
                    nuevaNota.setDesglosePregunta(pregunta);
                    dao.insert(nuevaNota);
                }
            }
        }
    }
}