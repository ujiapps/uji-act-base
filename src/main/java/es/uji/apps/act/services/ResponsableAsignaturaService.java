package es.uji.apps.act.services;

import java.util.List;

import es.uji.apps.act.dao.ResponsableAsignaturaDAO;
import es.uji.apps.act.exceptions.ActaNoEditableException;
import es.uji.apps.act.exceptions.NoAutorizadoException;
import es.uji.apps.act.models.Acta;
import es.uji.apps.act.models.enums.Acceso;
import es.uji.apps.act.models.views.ResponsableAsignatura;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;

import org.springframework.stereotype.Service;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.act.dao.InformacionDAO;
import es.uji.apps.act.models.Informacion;

@Service
public class ResponsableAsignaturaService {
    @InjectParam
    public ResponsableAsignaturaDAO responsableAsignaturaDAO;

    @InjectParam
    private AuthService authService;

    public List<ResponsableAsignatura> getReponsablesAsignatura(Long cursoId,
                                                                String codigoAsignatura, Long connectedUserId)
            throws NoAutorizadoException, ActaNoEditableException, RegistroNoEncontradoException {
        codigoAsignatura = codigoAsignatura.split("-")[0];
        authService.checkPuedeAccederActaByCodigo(Acceso.LECTURA, codigoAsignatura, cursoId,
                connectedUserId);
        return responsableAsignaturaDAO.getReponsablesAsignatura(cursoId, codigoAsignatura);
    }

    public List<ResponsableAsignatura> getReponsablesAsignaturaPuedenTraspasar(Long cursoId,
                                                                               String codigoAsignatura) {
        String codigo = getCodigoAsignatura(codigoAsignatura);
        return responsableAsignaturaDAO.getReponsablesPuedenTraspasarAsignatura(cursoId, codigo);
    }

    private String getCodigoAsignatura(String codigoComun) {
        String str = codigoComun.replace(" i ", ", ");
        return str.split(",")[0];
    }
}
