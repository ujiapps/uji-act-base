package es.uji.apps.act.services;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.act.dao.ActaEstudianteDAO;
import es.uji.apps.act.dao.DesgloseNotaDAO;
import es.uji.apps.act.exceptions.ImportacionException;
import es.uji.apps.act.models.*;
import es.uji.apps.act.models.enums.Calificacion;
import es.uji.apps.act.models.ui.UINotaImport;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;


public class ImportExportService {
    @InjectParam
    private ActaEstudianteDAO actaEstudianteDAO;

    @InjectParam
    private DesgloseNotaDAO desgloseNotaDAO;

    @InjectParam
    private LogService logService;

    private final char LABEL_DELIMITER = '_';

    private final String CABECERA_PER_ID = "PERSONA_ID";
    private final String CABECERA_IDENTIFICACION = "DNI";
    private final String CABECERA_IDENTIFICACION_OFUSCADA = "DNI_OFUSCADO";

    private final String CABECERA_NOMBRE = "NOMBRE";
    private final String CABECERA_NOM = "NOM";
    private final String CABECERA_NOTA = "NOTA";

    private final Long ESTADO_ERROR = 0L;
    private final Long ESTADO_WARNING = 1L;
    private final Long ESTADO_OK = 2L;
    private final Long ESTADO_UNCHANGED = 3L;

    public List<String[]> exportar(Acta acta, String asignatura, String grupo) {
        String empty = "";
        List<ActaEstudiante> actaEstudiantes = acta
                .getActaEstudiantes()
                .stream()
                .filter(ae -> !ae.isExcluido())
                .filter(ae -> ae.getGrupoId().equals(grupo) || grupo == null || empty.equals(grupo))
                .filter(ae -> ae.getActaAsignatura().getAsignatura().getId().equals(asignatura) || asignatura == null || empty.equals(asignatura))
                .sorted(Comparator.comparing(ae -> ae.getPersona().getNombreCompleto().toLowerCase(Locale.ROOT)))
                .collect(Collectors.toList());

        List<String[]> records = new ArrayList<>();
        List<String> record = getCabeceras(acta);

        String[] recordArray = new String[record.size()];
        record.toArray(recordArray);
        records.add(recordArray);
        for (ActaEstudiante actaEstudiante : actaEstudiantes) {
            record.clear();
            record.add(actaEstudiante.getPersona().getId().toString());
            record.add(actaEstudiante.getPersona().getIdentificacion());
            record.add(actaEstudiante.getPersona().getIdentificacionOfuscado());
            record.add(actaEstudiante.getPersona().getNombreCompleto());
            record.add(actaEstudiante.getNota() == null ? "" : formatNota(actaEstudiante.getNota()));

            if (acta.isDesgloseActivo()) {
                for (DesglosePregunta pregunta : acta.getPreguntasOrdenadas()) {
                    DesgloseNota desgloseNota = actaEstudiante.getDesgloseNotaByPreguntaId(pregunta.getId());
                    record.add((desgloseNota == null || desgloseNota.getNota() == null) ? "" : formatNota(desgloseNota.getNota()));
                }
            }

            recordArray = new String[record.size()];
            record.toArray(recordArray);
            records.add(recordArray);
        }

        return records;
    }

    private String formatNota(Double nota) {
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
        otherSymbols.setDecimalSeparator(',');
        otherSymbols.setGroupingSeparator('.');
        DecimalFormat numberformat = new DecimalFormat("##.#", otherSymbols);
        return numberformat.format(nota);
    }

    private List<String> getCabeceras(Acta acta) {
        List<String> cabecera = new ArrayList<>();
        cabecera.add(CABECERA_PER_ID);
        cabecera.add(CABECERA_IDENTIFICACION);
        cabecera.add(CABECERA_IDENTIFICACION_OFUSCADA);
        cabecera.add(CABECERA_NOMBRE);
        cabecera.add(CABECERA_NOTA);

        if (acta.isDesgloseActivo()) {
            for (DesglosePregunta pregunta : acta.getPreguntasOrdenadas()) {
                String etiquetaPregunta = pregunta.getEtiqueta();
                String etiquetaGrupo = pregunta.getDesgloseGrupo().getEtiqueta();
                cabecera.add(getKey(etiquetaGrupo, etiquetaPregunta));
            }
        }
        return cabecera;
    }

    private String getKey(String etiquetaGrupo, String etiquetaPregunta) {
        return MessageFormat.format("{0}{1}{2}", etiquetaGrupo, LABEL_DELIMITER, etiquetaPregunta);
    }

    public List<UINotaImport> importarPrevio(Acta acta, List<Map<String, String>> records)
            throws ImportacionException {
        List<ActaEstudiante> actaEstudiantes = acta.getActaEstudiantes();
        List<UINotaImport> notas = new ArrayList<>();

        checkCabecera(records.get(0));
        checkDesgloseValido(acta);

        for (Map<String, String> record : records) {
            String personaId = record.get(CABECERA_PER_ID);
            String dni = record.get(CABECERA_IDENTIFICACION);
            if ((personaId != null && !personaId.isEmpty()) || (dni != null && !dni.isEmpty())) {
                notas.addAll(getUINotasImport(record, actaEstudiantes));
            }
        }

        Map<Long, List<UINotaImport>> notasPorPersonaId = notas.stream()
                .filter(nota -> nota.getPersonaId() != null)
                .collect(Collectors.groupingBy(UINotaImport::getPersonaId));
        for (Map.Entry<Long, List<UINotaImport>> entry : notasPorPersonaId.entrySet()) {
            Long minEstado = entry.getValue().stream().map(ui -> ui.getEstado()).min(Double::compare).get();
            entry.getValue().stream().forEach(e -> e.setEstadoAlumno(minEstado));
        }
        return notas;
    }

    private List<UINotaImport> getUINotasImport(Map<String, String> record, List<ActaEstudiante> actaEstudiantes) {
        Long personaId = (record.get(CABECERA_PER_ID) != null) ?
                Long.parseLong(record.get(CABECERA_PER_ID)) :
                null;
        String identificacion = record.get(CABECERA_IDENTIFICACION);
        String nombre = getCabeceraNombre(record);

        ActaEstudiante actaEstudiante = (personaId != null) ?
                getActaEstudianteByPersonaId(actaEstudiantes, personaId) :
                getActaEstudianteByIdentificacion(actaEstudiantes, identificacion);

        List<UINotaImport> notas = new ArrayList<>();
        notas.add(getUINotaFinalImport(record, actaEstudiante));

        if (actaEstudiante != null && actaEstudiante.getActaAsignatura().getActa()
                .isDesgloseActivo()) {
            for (Map.Entry<String, String> entry : record.entrySet()) {
                if (isDesgloseColumn(entry.getKey())) {
                    String key = entry.getKey();
                    String nota = entry.getValue();

                    DesgloseNota desgloseNota = getDesgloseNotaByKey(actaEstudiante, key);
                    if (desgloseNota != null) {
                        notas.add(getUINotaImportDesglose(personaId, nombre, nota, desgloseNota));
                    }
                }
            }
        }
        return notas;
    }

    private String getCabeceraNombre(Map<String, String> record) {
        return record.containsKey(CABECERA_NOMBRE) ? record.get(CABECERA_NOMBRE) : record.get(CABECERA_NOM);
    }

    private boolean isDesgloseColumn(String key) {
        return (!key.equals(CABECERA_PER_ID) && !key.equals(CABECERA_IDENTIFICACION) && !key
                .equals(CABECERA_NOMBRE) && !key.equals(CABECERA_NOTA));
    }

    private UINotaImport getUINotaFinalImport(Map<String, String> record, ActaEstudiante actaEstudiante) {
        String identificacion = record.get(CABECERA_IDENTIFICACION);
        String nombre = getCabeceraNombre(record);
        String notaImportada = record.get(CABECERA_NOTA);

        UINotaImport notaImport = new UINotaImport();
        notaImport.setIdentificacion(identificacion);
        notaImport.setNombre(nombre);
        notaImport.setEstado(ESTADO_OK);
        notaImport.setMensaje("Vas a introduir aquesta nota");
        notaImport.setNotaTxt(notaImportada);

        if (actaEstudiante == null) {
            notaImport.setMensaje("No es troba en l\'acta");
            notaImport.setEstado(ESTADO_ERROR);
            return notaImport;
        }
        notaImport.setPersonaId(actaEstudiante.getPersona().getId());
        notaImport.setNombre(actaEstudiante.getPersona().getNombreCompleto());

        if (actaEstudiante.isExcluido()) {
            notaImport.setMensaje("L'alumne està exclòs de l'acta");
            notaImport.setEstado(ESTADO_ERROR);
            return notaImport;
        }

        try {
            notaImport.setId(actaEstudiante.getId());
            notaImport.setActaEstudianteId(actaEstudiante.getId());
            notaImport.setNotaAnterior(actaEstudiante.getNota());
            notaImport.setCalificacionIdAnterior(actaEstudiante.getCalificacionId());

            Double notaParseada = ParseUtil.parseaNota(notaImportada);
            Double notaActual = actaEstudiante.getNota();
            notaImport.setCalificacionId(Calificacion.getCalificacionIdByNota(notaParseada));
            notaImport.setNota(notaParseada);

            actualizaEstadoByNotas(notaImport, notaParseada, notaActual);
        } catch (Exception e) {
            notaImport.setMensaje(e.getMessage());
            notaImport.setEstado(ESTADO_ERROR);
        }

        if (!actaEstudiante.isEditable()) {
            notaImport.setMensaje("No es pot editar la nota");
            notaImport.setEstado(ESTADO_ERROR);
            return notaImport;
        }

        return notaImport;
    }

    private UINotaImport getUINotaImportDesglose(Long personaId, String nombre, String nota, DesgloseNota desgloseNota) {
        UINotaImport notaImport = new UINotaImport();
        notaImport.setPersonaId(personaId);
        notaImport.setNombre(nombre);
        notaImport.setEstado(ESTADO_OK);
        notaImport.setMensaje("Vas a introduir aquesta nota");
        notaImport.setNotaTxt(nota);

        if (desgloseNota.getActaEstudiante() == null) {
            notaImport.setMensaje("No es troba en l\'acta");
            notaImport.setEstado(ESTADO_ERROR);
            return notaImport;
        }

        notaImport.setNombre(desgloseNota.getActaEstudiante().getPersona().getNombreCompleto());

        if (desgloseNota.getActaEstudiante().isExcluido()) {
            notaImport.setMensaje("L'alumne està exclòs de l'acta");
            notaImport.setEstado(ESTADO_ERROR);
            return notaImport;
        }

        try {
            notaImport.setId(desgloseNota.getId());
            notaImport.setDesgloseNotaId(desgloseNota.getId());
            notaImport.setActaEstudianteId(desgloseNota.getActaEstudiante().getId());
            notaImport.setDesglosePreguntaId(desgloseNota.getDesglosePregunta().getId());
            notaImport.setDesglosePreguntaEtiqueta(desgloseNota.getDesglosePregunta().getEtiqueta());
            notaImport.setDesgloseGrupoId(desgloseNota.getDesglosePregunta().getDesgloseGrupo().getId());
            notaImport.setDesgloseGrupoEtiqueta(desgloseNota.getDesglosePregunta().getDesgloseGrupo().getEtiqueta());
            notaImport.setNotaAnterior(desgloseNota.getNota());

            Double notaDesgloseParseada = ParseUtil.parseaNota(nota);
            Double notaDesgloseActual = desgloseNota.getNota();
            notaImport.setNota(notaDesgloseParseada);
            actualizaEstadoByNotas(notaImport, notaDesgloseParseada, notaDesgloseActual);
        } catch (Exception e) {
            notaImport.setMensaje(e.getMessage());
            notaImport.setEstado(ESTADO_ERROR);
        }

        if (!desgloseNota.getActaEstudiante().isEditable()) {
            notaImport.setMensaje("No es pot editar la nota");
            notaImport.setEstado(ESTADO_ERROR);
            return notaImport;
        }

        return notaImport;
    }

    private void actualizaEstadoByNotas(UINotaImport notaImport, Double notaNueva, Double notaActual) {
        if ((notaActual != null) && (notaNueva == null)) {
            notaImport.setEstado(ESTADO_WARNING);
            notaImport.setMensaje("Vas a esborrar aquesta nota");
        } else if ((notaActual != null) && !notaActual.equals(notaNueva)) {
            notaImport.setEstado(ESTADO_WARNING);
            notaImport.setMensaje("Vas a canviar aquesta nota");
        } else if ((notaNueva == null && notaActual == null) || (notaNueva != null && notaNueva.equals(notaActual))) {
            notaImport.setEstado(ESTADO_UNCHANGED);
            notaImport.setMensaje("Sense Canvis");
        }
    }

    private void checkCabecera(Map<String, String> record) throws ImportacionException {
        if (!record.containsKey(CABECERA_NOTA) || (!record.containsKey(CABECERA_PER_ID) && !record
                .containsKey(CABECERA_IDENTIFICACION))) {
            throw new ImportacionException(
                    "El format de fitxer no és correcte, ha de tenir com a mínim les columnes obligatòries DNI o PERSONA_ID i NOTA. El separador de columnes ha de ser ';'");
        }
    }

    private void checkDesgloseValido(Acta acta) throws ImportacionException {
        if (acta.isDesgloseActivo() && acta.getDesglosesGrupos().isEmpty()) {
            throw new ImportacionException(
                    "Error importació, està activat el desglossament però no hi ha definit cap grup");
        }
    }

    private ActaEstudiante getActaEstudianteByPersonaId(List<ActaEstudiante> actaEstudiantes,
                                                        Long personaId) {
        return actaEstudiantes.stream().filter(ae -> personaId.equals(ae.getPersona().getId()))
                .findFirst().orElse(null);
    }

    private ActaEstudiante getActaEstudianteByIdentificacion(List<ActaEstudiante> actaEstudiantes,
                                                             String identificacion) {
        return actaEstudiantes.stream()
                .filter(ae -> identificacion.equals(ae.getPersona().getIdentificacion()))
                .findFirst().orElse(null);
    }

    private DesgloseNota getDesgloseNotaByKey(ActaEstudiante actaEstudiante, String key) {
        String[] parts = key.split(MessageFormat.format("({0})", LABEL_DELIMITER));
        String etiquetaGrupo = parts[0];
        String etiquetaPregunta = parts[1];
        return actaEstudiante.getDesgloseNotas().stream()
                .filter(dn -> dn.getDesglosePregunta().getEtiqueta().equals(etiquetaPregunta) && dn
                        .getDesglosePregunta().getDesgloseGrupo().getEtiqueta()
                        .equals(etiquetaGrupo)).findFirst().orElse(null);
    }

    public void importar(Acta acta, List<UINotaImport> notas, Long connectedUserId, String ip) {
        List<ActaEstudiante> actaEstudiantes = acta.getActaEstudiantes();
        for (UINotaImport nota : notas) {
            ActaEstudiante actaEstudiante = actaEstudiantes.stream().filter(ae -> ae.getId().equals(nota.getActaEstudianteId())).findFirst().orElse(null);
            if (actaEstudiante != null && actaEstudiante.isEditable()) {
                if (nota.getDesgloseNotaId() != null) {
                    DesgloseNota desgloseNota = actaEstudiante.getDesgloseNotas().stream().filter(dn -> dn.getId().equals(nota.getDesgloseNotaId())).findFirst().orElse(null);
                    if (acta.isDesgloseActivo() && desgloseNota != null) {
                        desgloseNota.setNota(nota.getNota());
                        desgloseNota.setIp(ip);
                        Persona personaNota = new Persona(connectedUserId);
                        desgloseNota.setPersonaNota(personaNota);
                        desgloseNotaDAO.update(desgloseNota);
                        logService.registraNotaDesglose(desgloseNota, connectedUserId, ip);
                    }
                } else {
                    if (actaEstudiante.isMatriculaHonor() && Calificacion.getCalificacionIdByNota(nota.getNota()) != Calificacion.SOBRESALIENTE.getId()) {
                        actaEstudiante.setMatriculaHonor(false);
                    }

                    actaEstudiante.setNota(nota.getNota());
                    actaEstudiante.setIp(ip);
                    Persona personaNota = new Persona(connectedUserId);
                    actaEstudiante.setPersonaNota(personaNota);
                    Date fechaActual = new Date();
                    actaEstudiante.setFechaNota(fechaActual);
                    actaEstudianteDAO.update(actaEstudiante);
                    logService.registraNota(actaEstudiante, connectedUserId, ip);
                }
            }
        }
    }
}