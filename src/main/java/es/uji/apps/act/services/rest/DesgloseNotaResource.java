package es.uji.apps.act.services.rest;

import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.act.exceptions.ActaNoEditableException;
import es.uji.apps.act.exceptions.InconsistenciaDeDatosException;
import es.uji.apps.act.exceptions.NoAutorizadoException;
import es.uji.apps.act.models.DesgloseNota;
import es.uji.apps.act.models.NotaDesgloseCalculada;
import es.uji.apps.act.models.enums.Acceso;
import es.uji.apps.act.services.DesgloseNotaService;
import es.uji.apps.act.services.AuthService;
import es.uji.apps.act.services.IpUtil;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;

@Path("desglosenotas")
public class DesgloseNotaResource extends CoreBaseService
{
    @InjectParam
    private DesgloseNotaService desgloseNotaService;

    @InjectParam
    private AuthService authService;

    @PathParam("actaId")
    Long actaId;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getDesgloseNotas()
            throws NoAutorizadoException, ActaNoEditableException, RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkPuedeAccederActa(Acceso.LECTURA, actaId, connectedUserId);
        List<DesgloseNota> listaDesgloseNota = desgloseNotaService.getDesgloseNotasByActaId(actaId,
                connectedUserId);
        return modelToUI(listaDesgloseNota);
    }

    private List<UIEntity> modelToUI(List<DesgloseNota> listaDesgloseNota)
    {
        return listaDesgloseNota.stream().map(d -> modelToUI(d)).collect(Collectors.toList());
    }

    private UIEntity modelToUI(DesgloseNota desgloseNota)
    {
        UIEntity entity = UIEntity.toUI(desgloseNota);
        entity.put("editable", desgloseNota.isEditable());
        entity.put("alumno", desgloseNota.getActaEstudiante().getPersona().getNombreCompleto());
        entity.put("identificacionAlumno",
                desgloseNota.getActaEstudiante().getPersona().getIdentificacion());
        entity.put("alumnoId",
                desgloseNota.getActaEstudiante().getPersona().getId());
        entity.put("asignaturaId",
                desgloseNota.getActaEstudiante().getActaAsignatura().getAsignatura());
        entity.put("motivoExclusion", desgloseNota.getActaEstudiante().getMotivoExclusion());
        entity.put("grupoId", desgloseNota.getActaEstudiante().getGrupoId());
        entity.put("agrupacion",
                desgloseNota.getDesglosePregunta().getDesgloseGrupo().getEtiqueta());
        entity.put("pregunta", desgloseNota.getDesglosePregunta().getEtiqueta());
        entity.put("orden", desgloseNota.getOrden());
        entity.put("expedienteAbierto", desgloseNota.getActaEstudiante().isExpedienteAbierto());

        NotaDesgloseCalculada notaDesgloseCalculada = desgloseNota.getActaEstudiante()
                .getNotaDesgloseCalculada();
        if (notaDesgloseCalculada != null)
        {
            entity.put("ts", System.currentTimeMillis());
            entity.put("notaFinal", notaDesgloseCalculada.getNota());
            entity.put("notaFormula", notaDesgloseCalculada.getResultado());
        }
        return entity;
    }

    @PUT
    @Path("{desgloseNotaId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateDesgloseNota(@PathParam("desgloseNotaId") Long desgloseNotaId,
            UIEntity entity) throws NoAutorizadoException, InconsistenciaDeDatosException,
            ActaNoEditableException, RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        authService.checkPuedeAccederActa(Acceso.ESCRITURA, actaId, connectedUserId);
        DesgloseNota desgloseNota = desgloseNotaService.getDesgloseNotaById(desgloseNotaId);
        if (entity.get("nota") == null)
        {
            desgloseNota.setNota(null);
        }
        else
        {
            desgloseNota.setNota(Double.parseDouble(entity.get("nota")));
        }
        desgloseNotaService.update(desgloseNota, connectedUserId, IpUtil.getClientAddres(request));
        return modelToUI(desgloseNota);
    }

}