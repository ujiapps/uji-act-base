CREATE TABLE UJI_ACTAS.ACT_ACTAS
  (
    id                     NUMBER NOT NULL ,
    curso_academico_id     NUMBER NOT NULL ,
    convocatoria_id        NUMBER NOT NULL ,
    tipo_Estudio_id        VARCHAR2 (10 CHAR) NOT NULL ,
    acta_unica             NUMBER NOT NULL ,
    fecha_alta             DATE NOT NULL ,
    oficial                NUMBER NOT NULL ,
    descripcion            VARCHAR2 (1000 CHAR) NOT NULL ,
    per_id                 NUMBER NOT NULL ,
    ubicacion_id           NUMBER ,
    fecha_traspaso         DATE ,
    persona_id_traspaso    NUMBER ,
    fecha_firma_digital    DATE ,
    referencia             VARCHAR2 (1000 CHAR) ,
    permite_detalle_notas  NUMBER NOT NULL ,
    muestra_notas_inicio   DATE ,
    muestra_notas_fin      DATE ,
    revision_conjunta      NUMBER NOT NULL ,
    revision_Fecha         DATE ,
    revision_lugar         VARCHAR2 (1000 CHAR) ,
    revision_observaciones VARCHAR2 (4000 CHAR) ,
    codigo                 VARCHAR2 (100 BYTE) NOT NULL
  ) ;
CREATE INDEX UJI_ACTAS.ACT_ACTAS_CURS_ACA_IDX ON UJI_ACTAS.ACT_ACTAS
  (
    curso_academico_id ASC
  ) ;
CREATE INDEX UJI_ACTAS.ACT_ACTAS_CONV_IDX ON UJI_ACTAS.ACT_ACTAS
  (
    convocatoria_id ASC
  ) ;
CREATE INDEX UJI_ACTAS.ACT_ACTAS_TIPO_EST_IDX ON UJI_ACTAS.ACT_ACTAS
  (
    tipo_Estudio_id ASC
  ) ;
CREATE UNIQUE INDEX UJI_ACTAS.ACT_ACTAS_PK ON UJI_ACTAS.ACT_ACTAS
  (
    id ASC
  )
  ;
CREATE UNIQUE INDEX UJI_ACTAS.ACT_ACTAS_UK ON UJI_ACTAS.ACT_ACTAS
  (
    descripcion ASC , convocatoria_id ASC , curso_academico_id ASC , codigo ASC
  )
  ;
  ALTER TABLE UJI_ACTAS.ACT_ACTAS ADD CONSTRAINT ACT_ACTAS_PK PRIMARY KEY ( id ) ;
  ALTER TABLE UJI_ACTAS.ACT_ACTAS ADD CONSTRAINT ACT_ACTAS_UK UNIQUE ( descripcion , convocatoria_id , curso_academico_id , codigo ) ;
  CREATE TABLE UJI_ACTAS.ACT_ACTAS_ASIGNATURAS
    (
      id                     NUMBER NOT NULL ,
      acta_id                NUMBER NOT NULL ,
      asignatura_id          VARCHAR2 (10 CHAR) NOT NULL ,
      grupo_id               VARCHAR2 (10 CHAR) ,
      revision_Fecha         DATE ,
      revision_lugar         VARCHAR2 (1000 CHAR) ,
      revision_observaciones VARCHAR2 (4000 CHAR)
    ) ;
  CREATE INDEX UJI_ACTAS.ACT_ACTAS_ASI_ACT_IDX ON UJI_ACTAS.ACT_ACTAS_ASIGNATURAS
    (
      acta_id ASC
    ) ;
  CREATE INDEX UJI_ACTAS.ACT_ACTAS_ASI_ASI_IDX ON UJI_ACTAS.ACT_ACTAS_ASIGNATURAS
    (
      asignatura_id ASC
    ) ;
CREATE UNIQUE INDEX UJI_ACTAS.ACT_ACTAS_ASIGNATURAS_PK ON UJI_ACTAS.ACT_ACTAS_ASIGNATURAS
  (
    id ASC
  )
  ;
  ALTER TABLE UJI_ACTAS.ACT_ACTAS_ASIGNATURAS ADD CONSTRAINT ACT_ACTAS_ASIGNATURAS_PK PRIMARY KEY ( id ) ;
  CREATE TABLE UJI_ACTAS.ACT_ACTAS_ESTUDIANTES
    (
      id                  NUMBER NOT NULL ,
      acta_Asignatura_id  NUMBER NOT NULL ,
      persona_id          NUMBER NOT NULL ,
      grupo_id            VARCHAR2 (10 CHAR) NOT NULL ,
      nota                NUMBER ,
      calificacion_id     NUMBER ,
      no_presentado       NUMBER NOT NULL ,
      matricula_honor     NUMBER NOT NULL ,
      fecha_traspaso      DATE ,
      comentario_traspaso VARCHAR2 (1000 CHAR)
    ) ;
  CREATE INDEX UJI_ACTAS.ACT_ACTAS_ESTUDIANTES__IDX ON UJI_ACTAS.ACT_ACTAS_ESTUDIANTES
    (
      acta_Asignatura_id ASC
    ) ;
CREATE UNIQUE INDEX UJI_ACTAS.ACT_ACTAS_ESTUDIANTES_PK ON UJI_ACTAS.ACT_ACTAS_ESTUDIANTES
  (
    id ASC
  )
  ;
CREATE UNIQUE INDEX UJI_ACTAS.ACT_ACTAS_ESTUDIANTES_UK ON UJI_ACTAS.ACT_ACTAS_ESTUDIANTES
  (
    persona_id ASC , acta_Asignatura_id ASC
  )
  ;
  ALTER TABLE UJI_ACTAS.ACT_ACTAS_ESTUDIANTES ADD CONSTRAINT ACT_ACTAS_ESTUDIANTES_PK PRIMARY KEY ( id ) ;
  ALTER TABLE UJI_ACTAS.ACT_ACTAS_ESTUDIANTES ADD CONSTRAINT ACT_ACTAS_ESTUDIANTES_UK UNIQUE ( persona_id , acta_Asignatura_id ) ;
  CREATE TABLE UJI_ACTAS.ACT_CONVOCATORIAS
    (
      id             NUMBER NOT NULL ,
      nombre         VARCHAR2 (1000 CHAR) NOT NULL ,
      extraordinaria NUMBER NOT NULL ,
      orden          NUMBER NOT NULL ,
      ordinaria      NUMBER ,
      semestre       VARCHAR2 (1 CHAR) ,
      acta_unica     NUMBER NOT NULL ,
      certificado_ca VARCHAR2 (200) NOT NULL ,
      certificado_Es VARCHAR2 (200) NOT NULL ,
      certificado_Uk VARCHAR2 (200) NOT NULL
    ) ;
CREATE UNIQUE INDEX UJI_ACTAS.ACT_CONVOCATORIAS_PK ON UJI_ACTAS.ACT_CONVOCATORIAS
  (
    id ASC
  )
  ;
  ALTER TABLE UJI_ACTAS.ACT_CONVOCATORIAS ADD CONSTRAINT ACT_CONVOCATORIAS_PK PRIMARY KEY ( id ) ;
  CREATE TABLE UJI_ACTAS.ACT_CURSOS_ACADEMICOS
    (
      ID     NUMBER NOT NULL ,
      ACTIVO NUMBER NOT NULL
    ) ;
CREATE UNIQUE INDEX UJI_ACTAS.ACT_EXT_CURSOS_ACADEMICOS_PK ON UJI_ACTAS.ACT_CURSOS_ACADEMICOS
  (
    ID ASC
  )
  ;
  ALTER TABLE UJI_ACTAS.ACT_CURSOS_ACADEMICOS ADD CONSTRAINT ACT_EXT_CURSOS_ACADEMICOS_PK PRIMARY KEY ( ID ) ;
  CREATE TABLE UJI_ACTAS.ACT_EXT_PROFESORES
    (
      ID                 NUMBER NOT NULL ,
      CURSO_ACADEMICO_ID NUMBER NOT NULL ,
      ESTUDIO_ID         NUMBER NOT NULL ,
      ASIGNATURA_ID      VARCHAR2 (10 CHAR) NOT NULL ,
      GRUPO_ID           VARCHAR2 (10 CHAR) ,
      PERSONA_ID         NUMBER NOT NULL ,
      UBICACION_ID       NUMBER ,
      AREA_ID            NUMBER ,
      PUEDE_TRASPASAR    NUMBER NOT NULL
    ) ;
CREATE UNIQUE INDEX UJI_ACTAS.ACT_EXT_PROFESORES_PK ON UJI_ACTAS.ACT_EXT_PROFESORES
  (
    ID ASC
  )
  ;
  ALTER TABLE UJI_ACTAS.ACT_EXT_PROFESORES ADD CONSTRAINT ACT_EXT_PROFESORES_PK PRIMARY KEY ( ID ) ;
  CREATE TABLE UJI_ACTAS.ACT_FECHAS_CONVOCATORIAS
    (
      id                    NUMBER NOT NULL ,
      tipo_Estudio_id       VARCHAR2 (10 CHAR) NOT NULL ,
      curso_academico_id    NUMBER NOT NULL ,
      convocatoria_id       NUMBER NOT NULL ,
      fecha_inicio_traspaso DATE ,
      fecha_fin_traspaso    DATE ,
      diligencias_dias      NUMBER
    ) ;
  CREATE INDEX UJI_ACTAS.ACT_FE_CONV_CONV_IDX ON UJI_ACTAS.ACT_FECHAS_CONVOCATORIAS
    (
      convocatoria_id ASC
    ) ;
  CREATE INDEX UJI_ACTAS.ACT_FE_CONV_TIP_EST_IDX ON UJI_ACTAS.ACT_FECHAS_CONVOCATORIAS
    (
      tipo_Estudio_id ASC
    ) ;
  CREATE INDEX UJI_ACTAS.ACT_FE_CONV_CA_IDX ON UJI_ACTAS.ACT_FECHAS_CONVOCATORIAS
    (
      curso_academico_id ASC
    ) ;
CREATE UNIQUE INDEX UJI_ACTAS.ACT_FECHAS_CONVOCATORIAS_PK ON UJI_ACTAS.ACT_FECHAS_CONVOCATORIAS
  (
    id ASC
  )
  ;
  ALTER TABLE UJI_ACTAS.ACT_FECHAS_CONVOCATORIAS ADD CONSTRAINT ACT_FECHAS_CONVOCATORIAS_PK PRIMARY KEY ( id ) ;
  CREATE TABLE UJI_ACTAS.ACT_FECHAS_CONVOCATORIAS_EST
    (
      id                      NUMBER NOT NULL ,
      fechas_convocatorias_id NUMBER NOT NULL ,
      estudio_id              NUMBER NOT NULL ,
      fecha_inicio_traspaso   DATE ,
      fecha_fin_traspaso      DATE ,
      diligencias_dias        NUMBER
    ) ;
  CREATE INDEX UJI_ACTAS.ACT_FECHAS_CONV_EST_CONV_IDX ON UJI_ACTAS.ACT_FECHAS_CONVOCATORIAS_EST
    (
      fechas_convocatorias_id ASC
    ) ;
CREATE UNIQUE INDEX UJI_ACTAS.ACT_FECHAS_CONVOC_EST_PK ON UJI_ACTAS.ACT_FECHAS_CONVOCATORIAS_EST
  (
    id ASC
  )
  ;
  ALTER TABLE UJI_ACTAS.ACT_FECHAS_CONVOCATORIAS_EST ADD CONSTRAINT ACT_FECHAS_CONVOC_EST_PK PRIMARY KEY ( id ) ;
  CREATE TABLE UJI_ACTAS.ACT_TIPOS_ESTUDIOS
    (
      id      VARCHAR2 (10 CHAR) NOT NULL ,
      nombre  VARCHAR2 (200) NOT NULL ,
      oficial NUMBER NOT NULL
    ) ;
CREATE UNIQUE INDEX UJI_ACTAS.ACT_TIPOS_ESTUDIOS_PK ON UJI_ACTAS.ACT_TIPOS_ESTUDIOS
  (
    id ASC
  )
  ;
  ALTER TABLE UJI_ACTAS.ACT_TIPOS_ESTUDIOS ADD CONSTRAINT ACT_TIPOS_ESTUDIOS_PK PRIMARY KEY ( id ) ;
  CREATE TABLE UJI_ACTAS.ACT_VM_ACTAS
    (
      ID              VARCHAR2 (500 BYTE) ,
      CURSO_ACA       NUMBER (4) ,
      ASI_CODIGOS     VARCHAR2 (100 BYTE) ,
      ASI_NOMBRE      VARCHAR2 (200 BYTE) ,
      PER_ID          NUMBER (8) ,
      TIPO_ESTUDIO_ID CHAR (1 BYTE) ,
      OFICIAL         CHAR (1 BYTE) ,
      ACTA_UNICA      VARCHAR2 (1 BYTE) ,
      EXTRAORDINARIA  NUMBER ,
      ORDINARIA_1     NUMBER ,
      ORDINARIA_2     NUMBER
    ) ;
  CREATE INDEX UJI_ACTAS.ACT_VM_ACTAS_USU_IDX ON UJI_ACTAS.ACT_VM_ACTAS
    (
      CURSO_ACA ASC ,
      PER_ID ASC
    ) ;
CREATE OR REPLACE VIEW UJI_ACTAS.ACT_EXT_ASIGNATURAS
  (
    ID, NOMBRE, TIPO, CURSO_ACADEMICO_INICIO, CURSO_ACADEMICO_FIN, GRUPO_ID, ASIGANTURA_COMPARTIDA, CODIGO_GENERACION, ACTA_UNICA, TIPO_SEMESTRE
  )
AS
  SELECT id,
    nombre_ca nombre,
    DECODE (tipo, '12C', DECODE (LENGTH (id), 6, 'G', '12C'), tipo) tipo,
    2011 curso_academico_inicio,
    NULL curso_academico_fin,
    NULL grupo_id,
    NULL asigantura_compartida,
    NULL codigo_generacion,
    NULL acta_unica,
    NULL tipo_semestre
  FROM exp_v_Asi_todas
  WHERE LENGTH(id) <= 6 ;
CREATE OR REPLACE VIEW UJI_ACTAS.ACT_EXT_LISTA_ACTAS ( CURSO_ACA, CODIGO, NOMBRE, GRUPO_ID, TIPO_ESTUDIO_ID, OFICIAL, ACTA_UNICA, CARACTER, EEP, EP, TIPO, SEMESTRE, EXTRAORDINARIA, ORDINARIA_1, ORDINARIA_2 )
AS
  SELECT DISTINCT x.curso_aca,
    codigo,
    nombre,
    grupo_id,
    'G' tipo_Estudio_id,
    'S' oficial,
    DECODE ( DECODE ( DECODE(tipo, 'A', DECODE(semestre, 1, 1, 2, 2, NULL), 'S', DECODE(semestre, 1, 1, 2, 2, NULL), NULL), NULL, 0, 1 ) + DECODE(to_number(DECODE(caracter, 'PF', '', 'LC', '', DECODE(tipo, 'A', '3', 'S', '3', ''))), NULL, 0, 1), 1, 'S', 'N' ) acta_unica,
    caracter,
    eep,
    ep,
    tipo,
    semestre,
    5 extraordinaria,
    DECODE(tipo, 'A', DECODE(semestre, 1, 1, 2, 2, NULL), 'S', DECODE(semestre, 1, 1, 2, 2, NULL), NULL) ordinaria_1,
    to_number(DECODE(caracter, 'PF', '', 'LC', '', DECODE(tipo, 'A', '3', 'S', '3', ''))) ordinaria_2
  FROM
    (SELECT ac.curso_aca,
      ac.asi_id codigo,
      a.nombre nombre,
      '' grupo_id,
      tipo,
      caracter,
      eep,
      ep,
      semestre
    FROM pod_asi_cursos ac,
      gra_pod.pod_grupos g,
      pod_asignaturas a
    WHERE ac.curso_Aca >= 2015
    AND cur_tit_id BETWEEN 201 AND 9999
    AND ac.asi_id = a.id
    AND a.id NOT LIKE 'AT%'
    AND a.id NOT LIKE 'OT%'
    AND g.id          <> 'W'
    AND ac.cur_id     <> 7
    AND ac.curso_Aca   = g.curso_aca
    AND ac.asi_id      = g.asi_id
    AND ac.asi_id NOT IN
      (SELECT asi_id
      FROM pod_comunes c,
        pod_grp_comunes g
      WHERE g.id    = gco_id
      AND curso_aca = ac.curso_aca
      )
    UNION ALL
    SELECT curso_aca,
      codigo,
      MIN(DECODE(orden, 1, asignatura_nombre, NULL)) asignatura_nombre,
      '' grupo_id,
      tipo,
      MIN(caracter) caracter,
      eep,
      ep,
      semestre
    FROM
      (SELECT curso_aca,
        codigo,
        asignatura_id,
        asignatura_nombre,
        caracter,
        tipo,
        eep,
        ep,
        semestre,
        row_number() over(partition BY curso_aca, codigo, semestre order by asignatura_id) orden
      FROM
        (SELECT DISTINCT ac.curso_aca,
          g.nombre codigo,
          ac.asi_id asignatura_id,
          a.nombre asignatura_nombre,
          caracter,
          tipo,
          eep,
          ep,
          semestre
        FROM pod_comunes c,
          pod_grp_comunes g,
          pod_asi_cursos ac,
          pod_asignaturas a,
          pod_grupos gr
        WHERE g.id      = gco_id
        AND g.curso_aca = ac.curso_aca
        AND c.asi_id    = ac.asi_id
        AND ac.cur_tit_id BETWEEN 201 AND 9999
        AND ac.curso_Aca >= 2015
        AND gr.id        <> 'W'
        AND ac.cur_id    <> 7
        AND ac.curso_Aca  = gr.curso_aca
        AND ac.asi_id     = gr.asi_id
        AND ac.asi_id     = a.id
        )
      )
    GROUP BY curso_aca,
      codigo,
      --caracter,
      tipo,
      eep,
      ep,
      semestre
    ) x ;
CREATE OR REPLACE VIEW UJI_ACTAS.ACT_EXT_LISTA_ACTAS_ASI ( CURSO_ACA, CODIGO, ASI_ID, NOMBRE, GRUPO_ID )
AS
  SELECT x.curso_aca,
    codigo,
    asi_id,
    nombre,
    grupo_id
  FROM
    (SELECT ac.curso_aca,
      a.id codigo,
      ac.asi_id,
      a.nombre,
      '' grupo_id
    FROM pod_asi_cursos ac,
      pod_grupos g,
      pod_asignaturas a
    WHERE ac.curso_Aca >= 2015
    AND cur_tit_id BETWEEN 201 AND 9999
    AND ac.asi_id = a.id
    AND a.id NOT LIKE 'AT%'
    AND a.id NOT LIKE 'OT%'
    AND ac.curso_Aca   = g.curso_aca
    AND ac.asi_id      = g.asi_id
    AND ac.asi_id NOT IN
      (SELECT asi_id
      FROM pod_comunes c,
        pod_grp_comunes g
      WHERE g.id    = gco_id
      AND curso_aca = ac.curso_aca
      )
    UNION ALL
    SELECT DISTINCT ac.curso_aca,
      g.nombre codigo,
      ac.asi_id,
      a.nombre,
      '' grupo_id
    FROM pod_comunes c,
      pod_grp_comunes g,
      pod_asi_cursos ac,
      pod_asignaturas a,
      pod_grupos gr
    WHERE g.id      = gco_id
    AND g.curso_aca = ac.curso_aca
    AND c.asi_id    = ac.asi_id
    AND ac.cur_tit_id BETWEEN 201 AND 9999
    AND ac.curso_Aca >= 2015
    AND ac.curso_Aca  = gr.curso_aca
    AND ac.asi_id     = gr.asi_id
    AND ac.asi_id     = a.id
    ) x ;
CREATE OR REPLACE VIEW UJI_ACTAS.ACT_EXT_PERSONAS ( ID, NOMBRE, APELLIDOS, CUENTA )
AS
  SELECT id,
    nombre,
    trim(apellido1
    ||' '
    ||apellido2) apellidos,
    busca_cuenta(id) cuenta
  FROM per_personas ;
CREATE OR REPLACE VIEW UJI_ACTAS.ACT_EXT_PERSONAS_ASIGNATURAS ( CURSO_ACA, ASI_ID, PER_ID, GRUPO_ID, PUEDE_TRASPASAR )
AS
  SELECT CURSO_ACA,
    ASI_ID,
    PER_ID,
    GRUPO_ID,
    PUEDE_TRASPASAR
  FROM
    ( -- profesorado
    SELECT DISTINCT ac.curso_aca,
      ac.asi_id,
      cdo_per_id per_id,
      '' grupo_id,
      'N' puede_traspasar
    FROM pod_asignaturas a,
      pod_asi_cursos ac,
      pod_subgrupo_pdi p
    WHERE a.id        = ac.asi_id
    AND ac.curso_aca >= 2015
    AND cur_tit_id BETWEEN 201 AND 9999
    AND curso_aca = sgr_grp_curso_aca
    AND ac.asi_id = sgr_grp_asi_id
    UNION ALL

    /* administradores */
    SELECT DISTINCT ac.curso_aca,
      ac.asi_id,
      p.id per_id,
      '' grupo_id,
      'S' puede_traspasar
    FROM pod_asignaturas a,
      pod_asi_cursos ac,
      per_personas p
      /*
      uji_actas.act_administradores */
    WHERE a.id        = ac.asi_id
    AND ac.curso_aca >= 2015
    AND cur_tit_id BETWEEN 201 AND 9999
    AND p.id IN (65394)
    UNION ALL

    /* permisos extra */
    SELECT DISTINCT ac.curso_aca,
      ac.asi_id,
      p.id per_id,
      '' grupo_id,
      'N' puede_traspasar
    FROM pod_asignaturas a,
      pod_asi_cursos ac,
      per_personas p
      /* uji_act.act_permisos_Extra */
    WHERE a.id        = ac.asi_id
    AND ac.curso_aca >= 2015
    AND cur_tit_id BETWEEN 201 AND 9999
    AND p.id IN (65394)
    UNION ALL

    /* vicedecanos y vicedirectores */
    SELECT DISTINCT ac.curso_aca,
      ac.asi_id,
      c.per_id per_id,
      '' grupo_id,
      'S' puede_traspasar
    FROM pod_asignaturas a,
      pod_asi_cursos ac,
      GRH_CARGOS_PER c
    WHERE a.id        = ac.asi_id
    AND ac.curso_aca >= 2015
    AND cur_tit_id BETWEEN 201 AND 9999
    AND ac.asi_id NOT LIKE 'AT%'
    AND ac.asi_id NOT LIKE 'OT%'
    AND crg_id  IN (108, 192, 193, 257, 258, 151, 298)
    AND ( f_fin IS NULL
    OR f_fin    >= sysdate)
    AND tit_id BETWEEN 201 AND 9999
    AND cur_tit_id = c.tit_id
    UNION ALL

    /* admin departamentos */
    SELECT DISTINCT a.curso_aca,
      aa.id asi_id,
      u.per_id,
      '' grupo_id,
      'S' puede_traspasar
    FROM grh_vw_contrataciones_ult u,
      est_ubic_estructurales ue,
      pod_v_estructuras e,
      pod_asignaturas_area a,
      pod_asignaturas aa
    WHERE e.area_id  = a.uest_id
    AND ue.id        = u.ubicacion_id
    AND ue.id        = e.dept_id
    AND aa.id        = a.asi_id
    AND u.act_id     = 'PAS'
    AND a.curso_aca >= 2015
    AND ctnes_id    IN
      (SELECT ctnes_id
      FROM grh_vw_contrataciones
      WHERE expfis_per_id   = u.per_id
      AND pcont_f_fin      IS NULL
      AND ctnes_cper_act_id = u.act_id
      AND grh_vw_plazas_plz_nombre LIKE 'Administr%'
      )
    ) ;
CREATE OR REPLACE VIEW UJI_ACTAS.ACT_VW_ACTAS ( ID, CURSO_ACA, ASI_CODIGOS, ASI_NOMBRE, PER_ID, TIPO_ESTUDIO_ID, OFICIAL, ACTA_UNICA, EXTRAORDINARIA, ORDINARIA_1, ORDINARIA_2 )
AS
  SELECT DISTINCT l.curso_aca
    ||'.'
    || l.codigo
    ||'.'
    || l.nombre
    ||'.'
    || pa.per_id AS id,
    l.curso_aca,
    l.codigo AS asi_codigos,
    l.nombre asi_nombre,
    pa.per_id AS per_id,
    tipo_Estudio_id,
    oficial,
    acta_unica,
    extraordinaria,
    ordinaria_1,
    ordinaria_2
  FROM act_ext_lista_actas l,
    act_ext_lista_actas_asi la,
    act_ext_personas_asignaturas pa
  WHERE l.codigo   = la.codigo
  AND l.curso_aca  = la.curso_aca
  AND la.curso_aca = pa.curso_aca
  AND la.asi_id    = pa.asi_id
  AND per_id       = 65394 ;
CREATE OR REPLACE VIEW UJI_ACTAS.ACT_VW_ACTAS_CONVOCATORIAS ( ID, CURSO_ACA, ASI_CODIGOS, CONVOCATORIA_ID, CONVOCATORIA_NOMBRE, SEMESTRE_ID, ORDEN, ORDINARIA, ACTA_ID, FECHA_ALTA, DESCRIPCION )
AS
  SELECT curso_Aca
    || '.'
    || a.asi_codigos
    || '.'
    || c.id
    || '.'
    || c.nombre id,
    curso_Aca,
    a.asi_codigos,
    c.id convocatoria_id,
    c.nombre convocatoria_nombre,
    c.semestre,
    orden,
    ordinaria,
    act.id acta_id,
    act.fecha_alta,
    descripcion
  FROM act_vw_actas a,
    act_convocatorias c,
    act_Actas act
  WHERE ( a.extraordinaria = c.id
  OR ordinaria_1           = c.id
  OR ordinaria_2           = c.id)
  AND curso_aca            = curso_academico_id(+)
  AND a.asi_codigos        = act.codigo(+)
  AND c.id                 = act.convocatoria_id(+)
  UNION ALL
  SELECT curso_academico_id
    || '.'
    || codigo
    || '.'
    || convocatoria_id
    || '.'
    || c.nombre
    || '.'
    || a.descripcion id,
    curso_academico_id,
    codigo,
    convocatoria_id,
    c.nombre convocatoria_nombre,
    c.semestre,
    orden,
    ordinaria,
    a.id acta_id,
    fecha_alta,
    descripcion
  FROM act_Actas a,
    act_convocatorias c
  WHERE convocatoria_id = 99
  AND convocatoria_id   = c.id ;
CREATE OR REPLACE VIEW UJI_ACTAS.ACT_VW_ACTAS_ESTUDIANTES ( ID, ACTA_ID, CURSO_ACADEMICO_ID, CONVOCATORIA_ID, ASIGNATURA_ID, GRUPO_ID, PERSONA_ID )
AS
  SELECT a.id
    ||'.'
    ||curso_academico_id
    ||'.'
    ||convocatoria_id
    ||'.'
    ||asignatura_id
    ||'.'
    ||mat_Exp_per_id id,
    a.id acta_id,
    curso_academico_id,
    convocatoria_id,
    asignatura_id,
    grp_id,
    mat_exp_per_id
  FROM uji_actas.act_actas a,
    uji_actas.act_actas_asignaturas aa,
    exp_asi_cursadas ac
  WHERE a.id               = aa.acta_id
  AND a.curso_Academico_id = mat_curso_aca
  AND aa.asignatura_id     = asi_id
  AND NOT EXISTS
    (SELECT 1
    FROM exp_v_notas
    WHERE per_id  = ac.mat_exp_per_id
    AND asi_id    = ac.asi_id
    AND curso_aca = ac.mat_curso_aca
    AND cal_id   <> 'S'
    )
  AND NOT ( NVL (cco_id, 0)   = 1
  OR NVL (cco_id_recurso, 0)  = 1
  OR NVL (cco_id_p_resol, 0)  = 1
  OR NVL (cco_id_p_alzada, 0) = 1)
  AND a.fecha_traspaso       IS NULL
  UNION ALL
  SELECT a.id
    ||'.'
    ||curso_academico_id
    ||'.'
    ||convocatoria_id
    ||'.'
    ||asignatura_id
    ||'_'
    ||per_id id,
    a.id acta_id,
    curso_academico_id,
    convocatoria_id,
    asignatura_id,
    ae.grupo_id,
    persona_id
  FROM uji_actas.act_actas a,
    uji_actas.act_actas_asignaturas aa,
    uji_Actas.act_actas_estudiantes ae
  WHERE a.id            = aa.acta_id
  AND aa.id             = acta_asignatura_id
  AND a.fecha_traspaso IS NOT NULL ;
  ALTER TABLE UJI_ACTAS.ACT_ACTAS ADD CONSTRAINT ACT_ACTAS_ACT_TIP_EST_FK FOREIGN KEY ( tipo_Estudio_id ) REFERENCES UJI_ACTAS.ACT_TIPOS_ESTUDIOS ( id ) ;
  ALTER TABLE UJI_ACTAS.ACT_ACTAS_ASIGNATURAS ADD CONSTRAINT ACT_ACTAS_ASI_ASI_FK FOREIGN KEY ( acta_id ) REFERENCES UJI_ACTAS.ACT_ACTAS ( id ) ;
  ALTER TABLE UJI_ACTAS.ACT_ACTAS ADD CONSTRAINT ACT_ACTAS_CONV_FK FOREIGN KEY ( convocatoria_id ) REFERENCES UJI_ACTAS.ACT_CONVOCATORIAS ( id ) ;
  ALTER TABLE UJI_ACTAS.ACT_ACTAS ADD CONSTRAINT ACT_ACTAS_CURS_ACA_FK FOREIGN KEY ( curso_academico_id ) REFERENCES UJI_ACTAS.ACT_CURSOS_ACADEMICOS ( ID ) ;
  ALTER TABLE UJI_ACTAS.ACT_ACTAS_ESTUDIANTES ADD CONSTRAINT ACT_ACTAS_EST_ACT_ASI_FK FOREIGN KEY ( acta_Asignatura_id ) REFERENCES UJI_ACTAS.ACT_ACTAS_ASIGNATURAS ( id ) ;
  ALTER TABLE UJI_ACTAS.ACT_FECHAS_CONVOCATORIAS_EST ADD CONSTRAINT ACT_FECHAS_CONV_EST_CONV_FK FOREIGN KEY ( fechas_convocatorias_id ) REFERENCES UJI_ACTAS.ACT_FECHAS_CONVOCATORIAS ( id ) ;
  ALTER TABLE UJI_ACTAS.ACT_FECHAS_CONVOCATORIAS ADD CONSTRAINT ACT_FE_CONV_CA_FK FOREIGN KEY ( curso_academico_id ) REFERENCES UJI_ACTAS.ACT_CURSOS_ACADEMICOS ( ID ) ;
  ALTER TABLE UJI_ACTAS.ACT_FECHAS_CONVOCATORIAS ADD CONSTRAINT ACT_FE_CONV_CONV_FK FOREIGN KEY ( convocatoria_id ) REFERENCES UJI_ACTAS.ACT_CONVOCATORIAS ( id ) ;
  ALTER TABLE UJI_ACTAS.ACT_FECHAS_CONVOCATORIAS ADD CONSTRAINT ACT_FE_CONV_TIP_EST_FK FOREIGN KEY ( tipo_Estudio_id ) REFERENCES UJI_ACTAS.ACT_TIPOS_ESTUDIOS ( id ) ;
