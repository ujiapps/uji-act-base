
		 
ALTER TABLE UJI_ACTAS.ACT_ACTAS
   ADD(revision_fecha_fin DATE);

ALTER TABLE UJI_ACTAS.ACT_ACTAS_ASIGNATURAS
   ADD(revision_fecha_fin DATE);


CREATE OR REPLACE VIEW UJI_ACTAS.ACT_VW_ACTAS ( ID, CURSO_ACA, ASI_CODIGOS, ASI_NOMBRE, PER_ID, TIPO_ESTUDIO_ID, OFICIAL, ACTA_UNICA, EXTRAORDINARIA, ORDINARIA_1, ORDINARIA_2 )
AS
  SELECT DISTINCT l.curso_aca
    || '.'
    || l.codigo
    || '.'
    || l.nombre
    || '.'
    || pa.per_id AS id,
    l.curso_aca,
    l.codigo AS asi_codigos,
    l.nombre asi_nombre,
    pa.per_id AS per_id,
    tipo_Estudio_id,
    oficial,
    acta_unica,
    extraordinaria,
    ordinaria_1,
    ordinaria_2
  FROM act_ext_lista_actas l,
    act_ext_lista_actas_asi la,
    act_ext_personas_asignaturas pa
  WHERE l.codigo   = la.codigo
  AND l.curso_aca  = la.curso_aca
  AND la.curso_aca = pa.curso_aca
  AND la.asi_id    = pa.asi_id
    --AND             per_id = 65394
    ;

	
CREATE OR REPLACE VIEW UJI_ACTAS.ACT_VW_ACTAS_CONVOCATORIAS ( ID, CURSO_ACA, ASI_CODIGOS, CONVOCATORIA_ID, CONVOCATORIA_NOMBRE, SEMESTRE_ID, ORDEN, ORDINARIA, EXTRAORDINARIA, ACTA_ID, FECHA_ALTA, DESCRIPCION, FECHA_TRASPASO, REVISION_CONJUNTA, REVISION_FECHA, REVISION_FECHA_FIN, REVISION_LUGAR, REVISION_OBSERVACIONES, MUESTRA_NOTAS_INICIO, MUESTRA_NOTAS_FIN )
AS
  SELECT x."ID",
    x."CURSO_ACA",
    x."ASI_CODIGOS",
    x."CONVOCATORIA_ID",
    x."CONVOCATORIA_NOMBRE",
    x."SEMESTRE",
    x."ORDEN",
    x."ORDINARIA",
    x.extraordinaria,
    act.id acta_id,
    act.fecha_alta,
    act.descripcion,
    act.fecha_traspaso,
    act.revision_conjunta,
    act.revision_fecha,
    act.revision_fecha_fin,
    act.revision_lugar,
    act.revision_observaciones,
    act.muestra_notas_inicio,
    act.muestra_notas_fin
  FROM
    (SELECT curso_Aca
      || '.'
      || a.asi_codigos
      || '.'
      || c.id
      || '.'
      || c.nombre id,
      curso_Aca,
      a.asi_codigos,
      c.id convocatoria_id,
      c.nombre convocatoria_nombre,
      c.semestre,
      orden,
      ordinaria,
      c.extraordinaria
    FROM act_vw_actas a,
      act_convocatorias c
    WHERE ( a.extraordinaria = c.id
    OR a.ordinaria_1         = c.id
    OR a.ordinaria_2         = c.id)
    ) x,
    act_actas act
  WHERE x.curso_aca     = curso_academico_id(+)
  AND x.asi_codigos     = act.codigo(+)
  AND x.convocatoria_id = act.convocatoria_id(+)
  UNION
  SELECT curso_academico_id
    || '.'
    || codigo
    || '.'
    || convocatoria_id
    || '.'
    || c.nombre
    || '.'
    || a.descripcion id,
    curso_academico_id,
    codigo,
    convocatoria_id,
    c.nombre convocatoria_nombre,
    c.semestre,
    orden,
    ordinaria,
    extraordinaria,
    a.id acta_id,
    fecha_alta,
    descripcion,
    a.fecha_traspaso,
    a.revision_conjunta,
    a.revision_fecha,
    a.revision_Fecha_fin,
    a.revision_lugar,
    a.revision_observaciones,
    a.muestra_notas_inicio,
    a.muestra_notas_fin
  FROM act_Actas a,
    act_convocatorias c
  WHERE convocatoria_id = 99
  AND convocatoria_id   = c.id ;


update  UJI_ACTAS.ACT_CONVOCATORIAS
set dias_para_modificar = 20;

commit;

ALTER TABLE UJI_ACTAS.ACT_CONVOCATORIAS
MODIFY(DIAS_PARA_MODIFICAR  NOT NULL);

CREATE OR REPLACE FORCE VIEW UJI_ACTAS.ACT_VW_ACTAS_ESTUDIANTES
(
   ID,
   ACTA_ID,
   CURSO_ACADEMICO_ID,
   CONVOCATORIA_ID,
   ASIGNATURA_ID,
   GRUPO_ID,
   PERSONA_ID,
   MOTIVO_EXCLUSION
)
   BEQUEATH DEFINER AS
   SELECT a.id || '.' || curso_academico_id || '.' || convocatoria_id || '.' || asignatura_id || '.' || mat_Exp_per_id
             id,
          a.id                                                                                      acta_id,
          curso_academico_id,
          convocatoria_id,
          asignatura_id,
          grp_id,
          mat_exp_per_id,
          uji_actas.pack_actas.motivo_exclusion(a.curso_academico_id, ac.asi_id, ac.mat_exp_per_id) motivo_exclusion
   FROM uji_actas.act_actas                      a,
        uji_actas.act_actas_asignaturas          aa,
        uji_actas.act_Ext_asignaturas_cursadas   ac,
        uji_Actas.act_convocatorias              c
   WHERE     a.id = aa.acta_id
         AND a.curso_Academico_id = mat_curso_aca
         and a.convocatoria_id = c.id
         and (   c.extraordinaria = 0
              or ac.mat_Exp_per_id in (select asc_mat_Exp_per_id
                                       from gra_Exp.exp_sol_convocatorias
                                       where     asc_mat_curso_aca = a.curso_academico_id
                                             and asc_asi_id = aa.asignatura_id
                                             and tipo in ('1', '13')
                                             and acta = 'S'))
         AND aa.asignatura_id = asi_id
         AND NOT EXISTS
                (SELECT 1
                 FROM uji_actas.act_Ext_asignaturas_notas
                 WHERE     per_id = ac.mat_exp_per_id
                       AND asi_id = ac.asi_id
                       AND curso_aca = ac.mat_curso_aca
                       AND cal_id <> 'S')
         AND NOT (   NVL(cco_id, 0) = 1
                  OR NVL(cco_id_recurso, 0) = 1
                  OR NVL(cco_id_p_resol, 0) = 1
                  OR NVL(cco_id_p_alzada, 0) = 1)
         AND a.fecha_traspaso IS NULL
   UNION ALL
   SELECT a.id || '.' || curso_academico_id || '.' || convocatoria_id || '.' || asignatura_id || '_' || per_id id,
          a.id                                                                                                 acta_id,
          curso_academico_id,
          convocatoria_id,
          asignatura_id,
          ae.grupo_id,
          persona_id,
          motivo_exclusion
   FROM uji_actas.act_actas a, uji_actas.act_actas_asignaturas aa, uji_Actas.act_actas_estudiantes ae
   WHERE     a.id = aa.acta_id
         AND aa.id = acta_asignatura_id
         AND a.fecha_traspaso IS NOT NULL;

		 
		 		 