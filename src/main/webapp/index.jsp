<%@page import="es.uji.commons.sso.User" %>
<%@page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Actes acadèmiques</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="//static.uji.es/js/extjs/ext-6.2.1/build/classic/theme-triton/resources/theme-triton-all.css">
    <link rel="stylesheet" type="text/css" href="//static.uji.es/js/extjs/ext-6.2.1/build/packages/font-awesome/resources/font-awesome-all.css">
    <link rel="stylesheet" type="text/css" href="/act/css/estilos.css">
    <style type="text/css">
        a.infoEstudiante {
            color: #222;
            text-decoration: none;
        }

        a.infoEstudiante:hover {
            color: #222;
            text-decoration: underline;
        }
        .infoacta .x-form-display-field-default {
            font-size: 14px;
            font-weight: bold;
            color: darkgreen;
        }
    </style>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <script type="text/javascript" src="//static.uji.es/js/extjs/ext-6.2.1/build/ext-all.js"></script>
    <%
        User user = (User) session.getAttribute(User.SESSION_USER);

        String login = user.getName();
        String activeSession = user.getActiveSession();

        ServletContext context = getServletContext();
        String appVersion = context.getInitParameter("appVersion");
    %>

    <script type="text/javascript">
        var login = '<%=user.getName()%>';
        var perId = <%=user.getId()%>;
        var appversion = '<%=appVersion%>';
    </script>
    <script type="text/javascript" src="/act/app/Application.js"></script>
</head>
<body>
    <div id="landing-loading">
        <img src="img/gears.gif"/>
        <p>Carregant aplicació...</p>
    </div>
</body>
</html>
