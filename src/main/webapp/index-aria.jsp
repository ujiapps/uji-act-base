<%@page import="es.uji.commons.sso.User" %>
<%@page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Actes acadèmiques</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="//static.uji.es/js/extjs/ext-6.2.1/build/packages/font-awesome/resources/font-awesome-all.css">
    <link rel="stylesheet" type="text/css" href="/act/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="//static.uji.es/js/extjs/ext-6.2.1/build/classic/theme-aria/resources/theme-aria-all.css">
    <style type="text/css">
        a.infoEstudiante {
            color: white;
            text-decoration: none;
        }

        a.infoEstudiante:hover {
            color: white;
            text-decoration: underline;
        }
        .infoacta .x-form-display-field-default {
            font-size: 14px;
            font-weight: bold;
            color: white;
        }
    </style>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <script type="text/javascript" src="//static.uji.es/js/extjs/ext-6.2.1/build/ext-all.js"></script>
    <script type="text/javascript" src="//static.uji.es/js/extjs/ext-6.2.1/build/classic/theme-aria/theme-aria.js"></script>
    <%
        User user = (User) session.getAttribute(User.SESSION_USER);

        String login = user.getName();
        String activeSession = user.getActiveSession();

        ServletContext context = getServletContext();
        String appVersion = context.getInitParameter("appVersion");
    %>

    <script type="text/javascript">
        var login = '<%=user.getName()%>';
        var perId = <%=user.getId()%>;
        var appversion = '<%=appVersion%>';
    </script>
    <script type="text/javascript" src="/act/app/Application.js"></script>
</head>
<body>
    <div id="landing-loading">
        <img src="img/gears.gif"/>
        <p>Carregant aplicació...</p>
    </div>
</body>
</html>
