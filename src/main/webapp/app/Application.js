Ext.Loader.setPath('Ext.ux', '//static.uji.es/js/extjs/ext-6.2.1/packages/ux/classic/src');
Ext.Loader.setPath('Ext.ux.uji', '//static.uji.es/js/extjs/uji-commons-extjs-widgets/src/ux/uji');
//Ext.Loader.setPath('Ext.ux.uji', '//localhost/uji-commons-extjs-widgets/src/ux/uji');

Ext.Loader.setPath('act', 'app');

Ext.override(Ext.data.proxy.Ajax,
    {
        timeout: 120000
    });

var appLang = 'ca';

Ext.define("Ext.locale.ca.picker.Date",
    {
        override: "Ext.picker.Date",
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado']
    });


function setTema(tema) {
    var pathname = window.location.pathname;
    if ((tema === 'triton' || tema === '') && (pathname !== '/act/index.jsp' && pathname !== '/act' && pathname !== '/act/')) {
        return window.location = '/act/index.jsp';
    }

    if (tema === 'aria' && pathname !== '/act/index-aria.jsp') {
        return window.location = '/act/index-aria.jsp';
    }
}

Ext.require('Ext.ux.TabCloseMenu');
Ext.require('Ext.ux.form.SearchField');
Ext.require('Ext.ux.uji.ApplicationViewport');
Ext.require('Ext.ux.uji.grid.ForeignColumn');
Ext.require('Ext.ux.uji.data.Store');
Ext.require('Ext.ux.uji.data.Model');
Ext.require('Ext.ux.uji.data.identifier.None');
Ext.require('Ext.ux.uji.combo.Combo');
Ext.require('act.view.dashboard.PanelDashboard');
Ext.require('act.store.Calificaciones');

Ext.ariaWarn = Ext.emptyFn;

Ext.define('Ext.ux.uji.AppConfig',
    {
        alias: 'widget.AppConfig',
        singleton: true,
        appName: 'act'
    });

Ext.application(
    {
        extend: 'Ext.app.Application',
        name: 'act',
        title: 'act',

        requires: ['act.view.dashboard.PanelDashboard', 'act.view.curso.Main', 'act.view.tipoEstudio.Main', 'act.view.acta.Main', 'act.view.actaAdmin.Main', 'act.view.permisosExtra.Main', 'act.view.actaLog.Main',
            'act.view.actaEstudiante.Main', 'act.view.actaAsignatura.Main', 'act.model.Acta', 'act.model.ActaEstudiante', 'act.model.ActaAsignatura', 'act.model.ActaPersona','act.model.CursoMoodle',
            'act.model.ActaDepartamento', 'act.model.ActaConvocatoria', 'act.model.Rol', 'act.model.ResumenDiligencias', 'act.model.ActaDiligenciaPendiente', 'act.model.Curso', 'act.model.Curso', 'act.model.Grupo', 'act.model.Convocatoria',
            'act.model.TipoEstudio', 'act.model.Lookup', 'act.view.diligencias.Main', 'act.view.diligenciasExtra.Main', 'act.model.ActaDiligencia', 'act.model.Diligencia', 'act.model.ConvocatoriaDetalle',
            'act.model.Revision', 'act.model.GrupoDesglose', 'act.model.PreguntaDesglose', 'act.model.NotaDesglose', 'act.model.Informacion', 'act.model.Notificacion', 'act.model.Log', 'act.model.PermisosExtra', 'act.model.PDIActivo',
            'act.model.ResponsableAsignatura', 'act.model.Import', 'act.view.misDiligencias.Main', 'act.model.ProfesorDiligencia', 'act.model.DiligenciaFueraPlazo', 'act.view.preferencias.Main'],
        stores: ['act.store.Calificaciones'],

        launch: function () {
            Ext.Ajax.request(
                {
                    url: '/act/rest/usuario/preferencias',
                    method: 'GET',
                    success: function (response) {
                        var responseObject = Ext.JSON.decode(response.responseText);
                        var tema = responseObject.data.tema;

                        setTema(tema);
                        Ext.apply(Ext.util.Format,
                            {
                                decimalSeparator: '.'
                            });

                        var viewport = Ext.create('Ext.ux.uji.ApplicationViewport',
                            {
                                codigoAplicacion: 'ACT',
                                tituloAplicacion: 'Actes acadèmiques',
                                dashboard: true
                            });
                        document.getElementById('landing-loading').style.display = 'none';
                    }
                })
        }
    })

