Ext.define('act.model.ProfesorDiligencia',
{
    extend : 'Ext.ux.uji.data.Model',
    fields : [
    {
        name : 'id',
        type : 'number'
    },
    {
        name : 'fecha',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s'
    },
    {
        name : 'actaEstudianteId',
        type : 'number'
    },
    {
        name : 'actaId',
        type : 'number'
    },
    {
        name : 'cursoAcademico',
        type : 'number'
    },
    {
        name : 'convocatoria',
        type : 'string'
    },
    {
        name : 'ip',
        type : 'string'
    },
    {
        name : 'notaAnterior',
        type : 'number',
        allowNull : true
    },
    {
        name : 'calificacionAnteriorId',
        type : 'number'
    },
    {
        name : 'notaNueva',
        type : 'number',
        allowNull : true
    },
    {
        name : 'calificacionNuevaId',
        type : 'number'
    },
    {
        name : 'comentarioDiligencia',
        type : 'string'
    },
    {
        type : 'auto',
        name : '_persona'
    },
    {
        type : 'auto',
        name : '_acta'
    },
    {
        name : 'alumnoNombre',
        type : 'string'
    },
    {
        name : 'identificacion',
        type : 'string'
    },
    {
        name : 'acta',
        type : 'string',
        persist : false,
        convert : function(v, record)
        {
            var data = record.get('_acta');
            return (data) ? data.descripcion : null;
        }
    } ],
    sorters : [
    {
        property : 'fecha',
        direction : 'desc'
    } ]
});