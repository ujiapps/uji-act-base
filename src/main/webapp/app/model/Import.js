Ext.define('act.model.Import',
{
    extend : 'Ext.data.Model',
    fields : [ 'id', 'nombre', 'dni', 'nota', 'notaTxt', 'notaAnterior', 'calificacionId', 'calificacionIdAnterior', 'mensaje', 'desgloseGrupoEtiqueta', 'desgloseGrupoId', 'desgloseNotaId',
        'desglosePreguntaEtiqueta', 'desglosePreguntaId', 'actaEstudianteId', 'desglosePreguntaId',
            {
                name : 'estadoAlumno',
                type : 'number'
            },
            {
                name : 'estado',
                type : 'number'
            } ]
});