Ext.define('act.model.ActaDiligenciaPendiente',
    {
        extend: 'Ext.ux.uji.data.Model',

        idProperty: 'actaEstudianteId',
        fields: [
            {
                name: 'actaEstudianteId',
                type: 'number'
            },
            {
                name: 'alumnoId',
                type: 'number'
            },
            {
                name: 'estado',
                type: 'number'
            },
            {
                name: 'estadoTxt',
                type: 'string'
            },
            {
                name: 'cursoId',
                type: 'number'
            },
            {
                name: 'editable',
                type: 'boolean'
            },
            {
                name: 'asignaturaId',
                type: 'string'
            },
            {
                name: 'identificacion',
                type: 'string'
            },
            {
                name: 'nota',
                type: 'number',
                allowNull: true
            },
            {
                name: 'calificacionId',
                type: 'number'
            },
            {
                name: 'alumnoNombre',
                type: 'string'
            },
            {
                name: 'noPresentado',
                type: 'boolean'
            },
            {
                name: 'comentario',
                type: 'string'
            },
            {
                name: 'diligencias',
                type: 'auto'
            },
            {
                name: 'tieneDiligencias',
                type: 'boolean'
            },
            {
                name: 'expedienteAbierto',
                type: 'boolean'
            },
            {
                name: 'aprobadoConvocatoriaAnterior',
                type: 'boolean'
            },
            {
                name: 'calificadoConvocatoriaPosterior',
                type: 'boolean'
            }],
        validators:
            {
                calificacionId: function (val, record) {
                    if (record.get('nota') === null && record.get('calificacionId') === 0) {
                        return true;
                    }
                    else if (record.get('nota') !== null && record.get('nota') < 5 && record.get('calificacionId') === 1) {
                        return true;
                    }
                    else if (record.get('nota') >= 5 && record.get('nota') < 7 && record.get('calificacionId') === 2) {
                        return true;
                    }
                    else if (record.get('nota') >= 7 && record.get('nota') < 9 && record.get('calificacionId') === 3) {
                        return true;
                    }
                    else if (record.get('nota') >= 9 && [4, 5].indexOf(record.get('calificacionId')) !== -1) {
                        return true;
                    }
                    return false;
                }
            }
    });