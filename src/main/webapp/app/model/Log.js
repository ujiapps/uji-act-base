Ext.define('act.model.Log',
{
    extend : 'Ext.data.Model',
    fields : [ 'id', 'nota', 'personaId', 'estudianteId', 'calificacionId', 'actaId', 'comentario',
    {
        name : 'fecha',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s'
    },
    {
        name : 'nombreCompletoEditor',
        type : 'string',
        persist : false,
        convert : function(v, record)
        {
            var data = record.get('_persona');
            return (data) ? data.apellidos + ', ' + data.nombre : null;
        }
    },
    {
        name : 'nombreCompletoAlumno',
        type : 'string',
        persist : false,
        convert : function(v, record)
        {
            var data = record.get('_estudiante');
            return (data) ? data.apellidos + ', ' + data.nombre : null;
        }
    }, ]
});