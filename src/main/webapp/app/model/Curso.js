Ext.define('act.model.Curso',
{
    extend : 'Ext.ux.uji.data.Model',

    fields : [
    {
        name : 'id',
        type : 'number'
    },
    {
        name : 'activo',
        type : 'boolean',
        useNull : false
    } ]
});