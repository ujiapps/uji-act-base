Ext.define('act.model.Informacion',
{
    extend : 'Ext.data.Model',
    fields : [ 'id', 'orden', 'texto', {
        name : 'fechaInicio',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s'
    }, {
        name : 'fechaFin',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s'
    } ]
});