Ext.define('act.model.Notificacion',
{
    extend : 'Ext.data.Model',
    fields : [ 'id', 'asunto', 'actaId', 'mensaje', 'tipo',
    {
        name : 'fecha',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s'
    },
        {
            type: 'auto',
            name: '_actaRevision'
        },
        {
            name: 'grupos',
            type: 'string',
            persist: false,
            convert: function (v, record) {
                var data = record.get('_actaRevision');
                return (data) ? data.grupos : null;
            }
        },
        {
            name: 'asignaturas',
            type: 'string',
            persist: false,
            convert: function (v, record) {
                var data = record.get('_actaRevision');
                return (data) ? data.asignaturas : null;
            }
        }]
});