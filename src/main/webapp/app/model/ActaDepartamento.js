Ext.define('act.model.ActaDepartamento',
    {
        extend: 'Ext.ux.uji.data.Model',

        fields: [
            {
                name: 'id',
                type: 'number'
            },
            {
                name: 'cursoId',
                type: 'number',
                useNull: false
            },
            {
                name: 'numeroAlumnos',
                type: 'number'
            },
            {
                name: 'editable',
                type: 'boolean'
            },
            {
                name: 'convocatoriaId',
                type: 'number',
                useNull: false
            },
            {
                name: 'convocatoriaNombre',
                type: 'string'
            },
            {
                name: 'perId',
                type: 'number'
            },
            {
                name: 'codigo',
                type: 'string',
                useNull: false
            },
            {
                name: 'personaNombre',
                type: 'string'
            },
            {
                name: 'fechaAlta',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s',
                useNull: false
            },
            {
                name: 'fechaTraspaso',
                width: 120,
                type: 'date',
                dateFormat: 'd/m/Y H:i:s',
                useNull: false
            },
            {
                name: 'fechaFirmaDigital',
                width: 120,
                type: 'date',
                dateFormat: 'd/m/Y H:i:s',
                useNull: false
            },
            {
                name: 'departamentoId',
                type: 'number'
            },
            {
                name: 'departamentoRecibeActaId',
                type: 'number'
            },
            {
                name: 'departamentoNombre',
                type: 'string',
                useNull: true
            },
            {
                name: 'nombreDepartamentoRecibeActa',
                type: 'string',
                useNull: true
            },
            {
                name: 'referencia',
                type: 'string',
                useNull: true
            },
            {
                name: 'tipoEstudioId',
                type: 'string'
            },
            {
                name: 'tipoEstudioNombre',
                type: 'string'
            },
            {
                type: 'auto',
                name: '_personaTraspaso'
            },
            {
                name: 'nombreCompletoPersonaTraspaso',
                type: 'string',
                persist: false,
                convert: function (v, record) {
                    var data = record.get('_personaTraspaso');
                    return (data) ? data.apellidos + ', ' + data.nombre : null;
                }
            }]
    });