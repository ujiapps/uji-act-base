Ext.define('act.model.enums.Calificacion',
{
    statics :
    {
        SUSPENSO :
        {
            id : 1,
            nombre : 'Suspens'
        },
        APROBADO :
        {
            id : 2,
            nombre : 'Aprovat'
        },
        NOTABLE :
        {
            id : 3,
            nombre : 'Notable'
        },
        SOBRESALIENTE :
        {
            id : 4,
            nombre : 'Excel·lent'
        },
        MATRICULA_HONOR :
        {
            id : 5,
            nombre : 'Matricula Honor'
        }
    }
});
