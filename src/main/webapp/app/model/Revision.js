Ext.define('act.model.Revision',
    {
        extend: 'Ext.ux.uji.data.Model',

        fields: [
            {
                name: 'id',
                type: 'number'
            },
            {
                name: 'actaId',
                type: 'number'
            },
            {
                name: 'nombre',
                type: 'string'
            },
            {
                name: 'fecha',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'fechaInicio',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'fechaFin',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'lugar',
                type: 'string'
            },
            {
                name: 'grupos',
                type: 'string'
            },
            {
                name: 'asignaturas',
                type: 'string'
            },
            {
                name: 'enviados',
                type: 'number'
            },
            {
                name: 'conjunta',
                type: 'boolean'
            },
            {
                name: 'observaciones',
                type: 'string'
            },
            {
                name: 'muestraNotasInicio',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'muestraNotasFin',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'hora',
                type: 'string',
                calculate: function (data) {
                    return data.fechaInicio ? Ext.Date.format(new Date(data.fechaInicio), 'G:i') : '';
                }
            },
            {
                name: 'horaFin',
                type: 'string',
                calculate: function (data) {
                    return data.fechaFin ? Ext.Date.format(new Date(data.fechaFin), 'G:i') : '';
                }
            }]
    });