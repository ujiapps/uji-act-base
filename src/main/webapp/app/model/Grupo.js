Ext.define('act.model.Grupo',
{
    extend : 'Ext.ux.uji.data.Model',

    fields : [
    {
        name : 'id',
        type : 'string'
    },
    {
        name : 'nombre',
        type : 'string'
    } ]
});