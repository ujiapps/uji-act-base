Ext.define('act.model.Rol',
{
    extend : 'Ext.ux.uji.data.Model',

    fields : [
    {
        name : 'admin',
        type : 'boolean',
        useNull : false
    } ]
});