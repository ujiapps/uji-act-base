Ext.define('act.model.ActaAsignatura',
{
    extend : 'Ext.ux.uji.data.Model',

    fields : [
    {
        name : 'id',
        type : 'number'
    },
    {
        name : 'actaId',
        type : 'number',
        useNull : false
    },
    {
        name : 'asignaturaId',
        type : 'string',
        useNull : false
    },
    {
        name : 'grupoId',
        type : 'string',
        useNull : false
    } ]
});