Ext.define('act.model.DiligenciaFueraPlazo',
    {
        extend: 'Ext.ux.uji.data.Model',
        fields: [
            {
                name: 'actaEstudianteId',
                type: 'number'
            },
            {
                name: 'estado',
                type: 'number'
            },
            {
                name: 'estadoTxt',
                type: 'string'
            },
            {
                name: 'diligencia',
                type: 'string'
            },
            {
                name: 'caducada',
                type: 'boolean'
            },
            {
                name: 'solicitante',
                type: 'string'
            },
            {
                name: 'personaId',
                type: 'number'
            },
            {
                name: 'fecha',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'notaAnterior',
                type: 'number',
                allowNull: true,
                sortType: function (value) {
                    if (!value)
                        return -1;
                    return value;
                }
            },
            {
                name: 'calificacionAnterior',
                type: 'string'
            },
            {
                name: 'notaNueva',
                type: 'number',
                allowNull: true,
                sortType: function (value) {
                    if (!value)
                        return -1;
                    return value;
                }
            },
            {
                name: 'calificacionNueva',
                type: 'string'
            },
            {
                name: 'comentarioDiligencia',
                type: 'string'
            },
            {
                name: 'ip',
                type: 'string'
            },
            {
                name: 'fechaModeracion',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'fechaFirma',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'comentarioValidacion',
                type: 'string'
            }]
    });