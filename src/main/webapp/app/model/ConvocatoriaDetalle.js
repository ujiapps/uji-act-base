Ext.define('act.model.ConvocatoriaDetalle',
{
    extend : 'Ext.ux.uji.data.Model',

    fields : [
    {
        name : 'id',
        type : 'number'
    },
    {
        name : 'convocatoriaId',
        type : 'number'
    },
    {
        name : 'tipoEstudioId',
        type : 'string'
    },
    {
        name : 'nombre',
        type : 'string'
    },
    {
        name : 'tipoEstudio',
        type : 'string'
    },
    {
        name : 'fechaInicioTraspaso',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s'
    },
    {
        name : 'fechaFinTraspaso',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s'
    },
    {
        name : 'fechaInicioActaUnica',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s'
    } ]
});