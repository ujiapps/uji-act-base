Ext.define('act.model.ActaPersona',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'cursoId',
        type : 'number',
        useNull : false
    },
    {
        name : 'asiCodigos',
        type : 'string',
        useNull : false
    },
    {
        name : 'asiNombre',
        type : 'string',
        useNull : false
    },
    {
        name : 'perId',
        type : 'number',
        useNull : false
    },
    {
        name : 'extraordinariaId',
        type : 'number',
        useNull : false
    },
    {
        name : 'ordinaria1Id',
        type : 'number',
        useNull : false
    },
    {
        name : 'ordinaria2Id',
        type : 'number',
        useNull : true
    },
    {
        name : 'descripcion',
        type : 'string',
        persist : false,
        convert : function(v, record)
        {
            return record.get('asiCodigos') + ' ' + record.get('asiNombre');
        }
    } ]
});