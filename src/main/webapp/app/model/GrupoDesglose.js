Ext.define('act.model.GrupoDesglose',
{
    extend : 'Ext.ux.uji.data.Model',

    fields : [
    {
        name : 'id',
        type : 'number'
    },
    {
        name : 'actaId',
        type : 'number'
    },
    {
        name : 'nombre',
        type : 'string'
    },
    {
        name : 'etiqueta',
        type : 'string'
    },
    {
        name : 'orden',
        type : 'number'
    },
    {
        name : 'visible',
        defaultValue: true,
        type : 'boolean'
    },
    {
        name : 'peso',
        type : 'number'
    },
    {
        name : 'notaMaxima',
        type : 'string',
        useNull : true
    },
    {
        name : 'notaMinima',
        type : 'string',
        useNull : true
    },
    {
        name : 'notaMinimaAprobado',
        type : 'string',
        useNull : true
    } ]
});