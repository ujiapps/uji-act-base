Ext.define('act.model.ResponsableAsignatura',
{
    extend : 'Ext.ux.uji.data.Model',
    idProperty: 'personaId',

    fields : [
    {
        name : 'personaId',
        type : 'number'
    },
    {
        name : 'curso',
        type : 'string'
    },
    {
        name : 'asignaturaId',
        type : 'string'
    },
    {
        name : 'puedeTraspasar',
        type : 'string'
    },
    {
        type : 'auto',
        name : '_persona'
    },
    {
        name : 'nombreCompleto',
        type : 'string',
        persist : false,
        convert : function(v, record)
        {
            var data = record.get('_persona');
            return (data) ? data.nombre + ' ' + data.apellidos : null;
        }
    }, ]
});