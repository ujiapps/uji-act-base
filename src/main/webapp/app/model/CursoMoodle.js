Ext.define('act.model.CursoMoodle',
{
    extend : 'Ext.ux.uji.data.Model',

    fields : [
    {
        name : 'id',
        type : 'number'
    },
        {
            name: 'nombre',
            type: 'string'
        },
    ]
});