Ext.define('act.model.ActaConvocatoria',
    {
        extend: 'Ext.ux.uji.data.Model',

        fields: [
            {
                name: 'id',
                type: 'string'
            },
            {
                name: 'cursoAcademicoId',
                type: 'number'
            },
            {
                name: 'asiCodigos',
                type: 'string'
            },
            {
                name: 'convocatoriaId',
                type: 'number'
            },
            {
                name: 'convocatoriaNombre',
                type: 'string'
            },
            {
                name: 'semestreId',
                type: 'string'
            },
            {
                name: 'orden',
                type: 'number'
            },
            {
                name: 'ordinariaId',
                type: 'number'
            },
            {
                name: 'extraordinaria',
                type: 'boolean'
            },
            {
                name: 'actaId',
                type: 'number',
                allowNull: true
            },
            {
                name: 'fechaAlta',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'descripcion',
                type: 'string'
            },
            {
                name: 'compartida',
                type: 'boolean'
            },
            {
                name: 'actaUnica',
                type: 'boolean'
            },
            {
                name: 'editable',
                type: 'boolean'
            },
            {
                name: 'traspasable',
                type: 'boolean'
            },
            {
                name: 'puedePasarNotasAExpediente',
                type: 'boolean'
            },
            {
                name: 'destraspasable',
                type: 'boolean'
            },
            {
                name: 'puedeGuardarNotas',
                type: 'boolean'
            },
            {
                name: 'puedeRecuperarNotas',
                type: 'boolean'
            },
            {
                name: 'desgloseActivo',
                type: 'boolean'
            },
            {
                name: 'fechaRevision',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'fechaFinRevision',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'fechaTraspaso',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'fechaInicioTraspaso',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'fechaFinTraspaso',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'referencia',
                type: 'string'
            },
            {
                name: 'fechaLimiteModificacion',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'lugarRevision',
                type: 'string'
            },
            {
                name: 'matriculas',
                type: 'number'
            },
            {
                name: 'observacionesRevision',
                type: 'string'
            },
            {
                name: 'muestraNotasInicio',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'muestraNotasFin',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'hora',
                type: 'string',
                calculate: function (data) {
                    return data.fechaRevision ? Ext.Date.format(new Date(data.fechaRevision), 'H:i') : '';
                }
            },
            {
                name: 'horaFin',
                type: 'string',
                calculate: function (data) {
                    return data.fechaFinRevision ? Ext.Date.format(new Date(data.fechaFinRevision), 'H:i') : '';
                }
            }]
    });