Ext.define('act.model.Convocatoria',
{
    extend : 'Ext.ux.uji.data.Model',

    fields : [
    {
        name : 'id',
        type : 'string'
    },
    {
        name : 'nombre',
        type : 'string'
    },
    {
        name : 'extraordinaria',
        type : 'boolean'
    },
    {
        name : 'orden',
        type : 'number'
    },
    {
        name : 'ordinaria',
        type : 'boolean'
    },
    {
        name : 'semestre',
        type : 'string'
    },
    {
        name : 'actaUnica',
        type : 'boolean'
    } ]
});