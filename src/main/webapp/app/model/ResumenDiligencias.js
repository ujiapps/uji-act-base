Ext.define('act.model.ResumenDiligencias',
{
    extend : 'Ext.ux.uji.data.Model',

    fields : [
    {
        name : 'diligencias',
        type : 'number'
    },
    {
        name : 'actasAfectadas',
        type : 'number'
    } ]
});