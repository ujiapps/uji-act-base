Ext.define('act.model.Diligencia',
    {
        extend: 'Ext.ux.uji.data.Model',
        fields: [
            {
                name: 'personaNombre',
                type: 'string'
            },
            {
                name: 'notaAnterior',
                type: 'number',
                allowNull: true,
                sortType: function (value) {
                    if (!value)
                        return -1;
                    return value;
                }
            },
            {
                name: 'asignaturaId',
                type: 'string'
            },
            {
                name: 'grupoId',
                type: 'string'
            },
            {
                name: 'calificacionAnterior',
                type: 'string'
            },
            {
                name: 'fecha',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                name: 'comentarioDiligencia',
                type: 'string'
            }]
    });