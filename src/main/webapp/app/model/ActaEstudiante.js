Ext.define('act.model.ActaEstudiante',
    {
        extend: 'Ext.ux.uji.data.Model',

        fields: [
            {
                name: 'id',
                type: 'number'
            },
            {
                name: 'actaAsignaturaId',
                type: 'number',
                useNull: false
            },
            {
                name: 'personaId',
                type: 'number',
                useNull: false
            },
            {
                name: 'grupoId',
                type: 'string',
                useNull: false
            },
            {
                name: 'nota',
                type: 'number',
                allowNull: true,
                sortType: function (value) {
                    if (!value) return -1;
                    return value;
                }
            },
            {
                name: 'notaCalculada',
                type: 'number',
                allowNull: true
            },
            {
                name: 'calificacionId',
                type: 'number'
            },
            {
                name: 'noPresentado',
                type: 'boolean'
            },
            {
                name: 'matriculaHonor',
                type: 'boolean'
            },
            {
                name: 'fechaTraspaso',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s',
                useNull: true
            },
            {
                name: 'comentario',
                type: 'string',
                useNull: true
            },
            {
                name: 'comentarioTraspaso',
                type: 'string',
                useNull: true
            },
            {
              name: 'personaIdNota',
              type: 'number'
            },
            {
                name: 'fechaNota',
                type: 'string',
            },
            {
                name: 'motivoExclusion',
                type: 'string',
                useNull: true
            },
            {
                name: 'editable',
                type: 'boolean'
            },
            {
                name: 'editableFueraPlazo',
                type: 'boolean',
                default: true
            },
            {
                name: 'desgloseActivo',
                type: 'boolean'
            },
            {
                name: 'expedienteAbierto',
                type: 'boolean'
            },
            {
                type: 'auto',
                name: '_persona'
            },
            {
                type: 'auto',
                name: '_personaNota'
            },
            {
                name: 'aExpediente',
                persist: false,
                type: 'boolean'
            },
            {
                name: 'personaNota',
                type: 'string',
                persist: false,
                convert: function (v, record) {
                    var data = record.get('_personaNota');
                    return (data) ? data.apellidos + ', ' + data.nombre : null;
                }
            },
            {
                name: 'nombreCompletoAlumno',
                type: 'string',
                persist: false,
                convert: function (v, record) {
                    var data = record.get('_persona');
                    return (data) ? data.apellidos + ', ' + data.nombre : null;
                }
            },
            {
                name: 'nombreCompletoAlumnoAscii',
                type: 'string',
                persist: false,
                convert: function (v, record) {
                    var data = record.get('_persona');
                    var nombreCompleto = data.apellidos + ', ' + data.nombre;

                    var diacritics = [/[\300-\306]/g, /[\340-\346]/g, // A, a
                        /[\310-\313]/g, /[\350-\353]/g, // E, e
                        /[\314-\317]/g, /[\354-\357]/g, // I, i
                        /[\322-\330]/g, /[\362-\370]/g, // O, o
                        /[\331-\334]/g, /[\371-\374]/g, // U, u
                        /[\321]/g, /[\361]/g, // N, n
                        /[\307]/g, /[\347]/g, // C, c
                    ];
                    var chars = ['A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u', 'N', 'n', 'C', 'c'];

                    for (var i = 0; i < diacritics.length; i++) {
                        nombreCompleto = nombreCompleto.replace(diacritics[i], chars[i]);
                    }

                    return nombreCompleto;
                }
            },
            {
                name: 'identificacionAlumno',
                type: 'string',
                persist: false,
                convert: function (v, record) {
                    var data = record.get('_persona');
                    return (data) ? data.identificacionOfuscado : null;
                }
            },
            {
                name: 'asignaturaId',
                type: 'string',
                persist: false,
                convert: function (v, record) {
                    var data = record.get('_actaAsignatura');
                    return (data) ? data.asignatura : null;
                }
            }]
    });