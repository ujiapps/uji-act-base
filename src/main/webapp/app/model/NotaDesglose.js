Ext.define('act.model.NotaDesglose', {
    extend: 'Ext.ux.uji.data.Model',

    fields: [{
        name: 'id',
        type: 'number'
    }, {
        name: 'actaEstudianteId',
        type: 'number'
    }, {
        name: 'alumnoId',
        type: 'number'
    }, {
        name: 'desglosePreguntaId',
        type: 'number'
    }, {
        name: 'asignaturaId',
        type: 'string'
    }, {
        name: 'alumno',
        type: 'string'
    }, {
        name: 'grupoId',
        type: 'string'
    }, {
        name: 'motivoExclusion',
        type: 'string'
    }, {
        name: 'pregunta',
        type: 'string'
    }, {
        name: 'agrupacion',
        type: 'string'
    }, {
        name: 'editable',
        type: 'boolean'
    }, {
        name: 'expedienteAbierto',
        type: 'boolean'
    }, {
        name: 'nota',
        type: 'number',
        allowNull: true
    }, {
        name: '_personaNota',
        type: 'auto'
    }, {
        name: 'personaNota',
        type: 'string',
        persist: false,
        convert: function (v, record) {
            var data = record.get('_personaNota');
            return (data) ? data.apellidos + ', ' + data.nombre : null;
        }
    },
        {
            name: 'nombreCompletoAlumnoAscii',
            type: 'string',
            persist: false,
            convert: function (v, record) {
                var nombreCompleto = record.get('alumno');

                var diacritics = [/[\300-\306]/g, /[\340-\346]/g, // A, a
                    /[\310-\313]/g, /[\350-\353]/g, // E, e
                    /[\314-\317]/g, /[\354-\357]/g, // I, i
                    /[\322-\330]/g, /[\362-\370]/g, // O, o
                    /[\331-\334]/g, /[\371-\374]/g, // U, u
                    /[\321]/g, /[\361]/g, // N, n
                    /[\307]/g, /[\347]/g, // C, c
                ];
                var chars = ['A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u', 'N', 'n', 'C', 'c'];

                for (var i = 0; i < diacritics.length; i++) {
                    nombreCompleto = nombreCompleto.replace(diacritics[i], chars[i]);
                }

                return nombreCompleto.toLowerCase();
            }
        },]
});