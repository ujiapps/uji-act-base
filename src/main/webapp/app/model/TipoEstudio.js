Ext.define('act.model.TipoEstudio',
{
    extend : 'Ext.ux.uji.data.Model',

    fields : [
    {
        name : 'id',
        type : 'string'
    },
    {
        name : 'nombre',
        type : 'string',
        useNull : false
    },
    {
        name : 'oficial',
        type : 'boolean',
        useNull : true
    } ]
});