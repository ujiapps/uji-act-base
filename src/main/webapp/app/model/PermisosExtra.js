Ext.define('act.model.PermisosExtra',
    {
        extend: 'Ext.ux.uji.data.Model',

        idProperty: "id",

        fields: [
            {
                name: 'id',
                type: 'number'
            },
            {
                name: 'cursoAcademicoId',
                type: 'number'
            },
            {
                name: 'asignaturaId',
                type: 'string'
            },
            {
                name: 'personaId',
                type: 'number'
            },
            {
                name: 'fecha',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s'
            },
            {
                type: 'auto',
                name: '_persona'
            },
            {
                name: 'nombreCompleto',
                type: 'string',
                persist: false,
                convert: function (v, record) {
                    var data = record.get('_persona');
                    return (data) ? data.apellidos + ', ' + data.nombre : null;
                }
            }]
    });