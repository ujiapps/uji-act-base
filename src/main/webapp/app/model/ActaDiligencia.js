Ext.define('act.model.ActaDiligencia',
    {
        extend: 'Ext.ux.uji.data.Model',

        idProperty: 'actaEstudianteId',
        fields: [
            {
                name: 'actaEstudianteId',
                type: 'number'
            },
            {
                name: 'alumnoId',
                type: 'number'
            },
            {
                name: 'cursoId',
                type: 'number'
            },
            {
                name: 'editable',
                type: 'boolean'
            },
            {
                name: 'editableFueraPlazo',
                type: 'boolean'
            },
            {
                name: 'asignaturaId',
                type: 'string'
            },
            {
                name: 'identificacion',
                type: 'string'
            },
            {
                name: 'identificacionOfuscado',
                type: 'string'
            },
            {
                name: 'nota',
                type: 'number',
                allowNull: true
            },
            {
                name: 'calificacionId',
                type: 'number'
            },
            {
                name: 'alumnoNombre',
                type: 'string'
            },
            {
                name: 'noPresentado',
                type: 'boolean'
            },
            {
                name: 'comentarioDiligencia',
                type: 'string'
            },
            {
                name: 'comentarioTraspaso',
                type: 'string'
            },
            {
                name: 'diligencias',
                type: 'auto'
            },
            {
                name: 'fechaNota',
                type: 'string',
            },

            {
                name: 'tieneDiligencias',
                type: 'boolean'
            },
            {
                name: 'expedienteAbierto',
                type: 'boolean'
            },
            {
                name: 'aprobadoConvocatoriaAnterior',
                type: 'boolean'
            },
            {
                type: 'auto',
                name: '_personaNota'
            },
            {
                name: 'calificadoConvocatoriaPosterior',
                type: 'boolean'
            },
            {
                name: 'personaNota',
                type: 'string',
                persist: false,
                convert: function (v, record) {
                    var data = record.get('_personaNota');
                    return (data) ? data.apellidos + ', ' + data.nombre : null;
                }
            },
            {
                name: 'fechaTraspaso',
                type: 'date',
                dateFormat: 'd/m/Y H:i:s',
                useNull: true
            }],
        validators:
            {
                calificacionId: function (val, record) {
                    if (record.get('nota') === null && record.get('calificacionId') === 0) {
                        return true;
                    } else if (record.get('nota') !== null && record.get('nota') < 5 && record.get('calificacionId') === 1) {
                        return true;
                    } else if (record.get('nota') >= 5 && record.get('nota') < 7 && record.get('calificacionId') === 2) {
                        return true;
                    } else if (record.get('nota') >= 7 && record.get('nota') < 9 && record.get('calificacionId') === 3) {
                        return true;
                    } else if (record.get('nota') >= 9 && [4, 5].indexOf(record.get('calificacionId')) !== -1) {
                        return true;
                    }
                    return false;
                }
            }
    });