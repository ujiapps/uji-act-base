Ext.define('act.model.Acta',
{
    extend : 'Ext.ux.uji.data.Model',

    fields : [
    {
        name : 'id',
        type : 'number'
    },
    {
        name : 'cursoId',
        type : 'number',
        useNull : false
    },
    {
        name : 'codigo',
        type : 'string',
        useNull : false
    },
    {
        name : 'numeroAlumnos',
        type : 'number'
    },
    {
        name : 'convocatoriaId',
        type : 'number',
        useNull : false
    },
    {
        name : 'tipoEstudioId',
        type : 'number',
        useNull : false
    },
    {
        name : 'actaUnica',
        type : 'boolean',
        useNull : false
    },
    {
        name : 'fechaAlta',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s',
        useNull : false
    },
    {
        name : 'oficial',
        type : 'boolean',
        useNull : false
    },
    {
        name : 'descripcion',
        type : 'string',
        useNull : true
    },
    {
        name : 'personaId',
        type : 'number',
        useNull : false
    },
    {
        name : 'ubicacionId',
        type : 'string',
        useNull : false
    },
    {
        name : 'fechaTraspaso',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s',
        useNull : true
    },
    {
        name : 'personaIdTraspaso',
        type : 'number',
        useNull : true
    },
    {
        name : 'fechaFirmaDigital',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s',
        useNull : true
    },
    {
        name : 'referencia',
        type : 'string',
        useNull : true
    },
    {
        name : 'desgloseActivo',
        type : 'boolean',
        useNull : true
    },
    {
        name : 'muestraNotasInicio',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s',
        useNull : true
    },
    {
        name : 'muestraNotasfin',
        type : 'date',
        dateFormat : 'd/m/Y H:i:s',
        useNull : true
    } ]
});