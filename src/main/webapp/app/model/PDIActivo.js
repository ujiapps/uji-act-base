Ext.define('act.model.PDIActivo',
{
    extend : 'Ext.ux.uji.data.Model',
    idProperty: 'id',

    fields : [
    {
        name : 'id',
        type : 'number'
    },
    {
        name : 'nombre',
        type : 'string',
    }, ]
});