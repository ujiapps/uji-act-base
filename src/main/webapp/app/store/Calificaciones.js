Ext.define('act.store.Calificaciones',
{
    extend : 'Ext.data.Store',
    requires : [ 'act.model.enums.Calificacion' ],
    alias : 'store.calificaciones',
    model : 'act.model.Lookup',

    proxy :
    {
        type : 'memory'
    },

    data : [
    {
        id : 0,
        nombre : 'No presentat'
    },
    {
        id : 1,
        nombre : 'Suspens'
    },
    {
        id : 2,
        nombre : 'Aprovat'
    },
    {
        id : 3,
        nombre : 'Notable'
    },
    {
        id : 4,
        nombre : 'Excel·lent'
    },
    {
        id : 5,
        nombre : 'Matricula Honor'
    } ]
});