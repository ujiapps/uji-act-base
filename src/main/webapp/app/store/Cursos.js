Ext.define('act.store.Cursos',
    {
        extend: 'Ext.data.Store',
        alias: 'store.cursos',
        model: 'act.model.Curso',
        sorters: [{property: 'id', direction: 'DESC'}],
        proxy:
            {
                type: 'rest',
                url: '/act/rest/cursos',
                reader:
                    {
                        type: 'json',
                        rootProperty: 'data'
                    },
                writer:
                    {
                        type: 'json',
                        writeAllFields: true
                    }
            },
        autoLoad: true
    });
