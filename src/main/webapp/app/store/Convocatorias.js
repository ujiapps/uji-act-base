Ext.define('act.store.Convocatorias',
{
    extend : 'Ext.data.Store',
    alias : 'store.convocatorias',
    model : 'act.model.Convocatoria',

    proxy :
    {
        type : 'rest',
        url : '/act/rest/convocatorias',
        reader :
        {
            type : 'json',
            rootProperty : 'data'
        },
        writer :
        {
            type : 'json',
            writeAllFields : true
        }
    },
    sorters : [ 'orden' ],
    autoLoad : true
});
