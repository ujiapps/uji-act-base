Ext.define('act.store.ConvocatoriasOficiales',
{
    extend : 'Ext.data.Store',
    alias : 'store.convocatoriasOficiales',
    model : 'act.model.Convocatoria',

    proxy :
    {
        type : 'rest',
        url : '/act/rest/convocatorias/oficiales',
        reader :
        {
            type : 'json',
            rootProperty : 'data'
        },
        writer :
        {
            type : 'json',
            writeAllFields : true
        }
    },
    sorters : [ 'orden' ],
    autoLoad : true
});
