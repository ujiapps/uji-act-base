Ext.define('act.store.Temas',
{
    extend : 'Ext.data.Store',
    alias : 'store.temas',
    model : 'act.model.Lookup',
    proxy :
    {
        type : 'memory'
    },
    data : [
    {
        id : 'triton',
        nombre : 'Per defecte'
    },
    {
        id : 'aria',
        nombre : 'Alt contrast'
    } ]
});