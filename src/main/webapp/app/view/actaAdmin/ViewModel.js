Ext.define('act.view.actaAdmin.ViewModel',
{
    extend : 'Ext.app.ViewModel',
    alias : 'viewmodel.actaAdminViewModel',
    requires : [ 'act.model.ActaAsignatura', 'act.model.ActaEstudiante', 'act.model.ActaPersona', 'act.model.Convocatoria' ],
    formulas :
    {
        cursoYconvocatoriaSeleccionados : function(get)
        {
            return get('comboConvocatoria.selection') && get('comboCurso.selection');
        },
        isAdmin : function(get)
        {
            var roles = get('rolesStore').first();
            return roles.get('admin');
        }

    },
    stores :
    {
        cursosStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'act.model.Curso',
            url : '/act/rest/cursos',
            autoLoad : true
        }),
        convocatoriasStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'act.model.Convocatoria',
            url : '/act/rest/convocatorias',
            autoLoad : true
        }),
        convocatoriasDetalleStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'act.model.ConvocatoriaDetalle',
            url : '/act/rest/convocatorias/detalle',
            autoLoad : false
        }),
        actasStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'act.model.Acta',
            url : '/act/rest/actas',
            autoLoad : false,
            queryModel : 'local'
        }),
        rolesStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'act.model.Rol',
            url : '/act/rest/usuario/roles',
            autoLoad : true
        }),
        actasDepartamentosStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'act.model.ActaDepartamento',
            url : '/act/rest/actas',
            autoLoad : false,
            queryModel : 'local',
            sorters : [ 'codigo' ]
        })

    }
});
