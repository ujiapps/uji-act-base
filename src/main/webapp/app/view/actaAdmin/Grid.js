Ext.define('act.view.actaAdmin.Grid',
{
    extend : 'Ext.ux.uji.grid.Panel',
    allowEdit : false,

    alias : 'widget.actaAdminGrid',
    flex : 1,
    scrollable : true,
    controller : 'actaAdminGridController',
    bind :
    {
        store : '{actasDepartamentosStore}'
    },

    emptyText : 'Selecciona un curs i una convocatoria',

    border : 0,
    bbar : [
    {
        xtype : 'button',
        text : 'Recarregar',
        action : 'reload',
        ref : 'reload',
        iconCls : 'fa fa-refresh',
        handler : 'onReload'
    },
    {
        xtype : 'label',
        text : '',
        name : 'elementCount',
        ref : 'elementCount'
    }, '->',
    {
        xtype : 'button',
        text : 'Enviar notificació a les actes visualitzades',
        iconCls : 'fa fa-envelope',
        handler : 'onEnviarNotificacion',
        bind :
        {
            hidden : '{!isAdmin}'
        }
    } ],
    columns : [
    {
        text : 'Id',
        dataIndex : 'id',
        hidden : true
    },
    {
        dataIndex : 'codigo',
        text : 'Assignatures',
        flex : 1
    },
    {
        dataIndex : 'tipoEstudioNombre',
        text : 'Tipus estudi',
        hidden : true
    },
    {
        dataIndex : 'convocatoriaNombre',
        text : 'Convocatoria',
        flex : 1
    },
    {
        dataIndex : 'numeroAlumnos',
        text : 'Alumnes',
        align : 'center',
        hidden : true
    },
    {
        xtype : 'datecolumn',
        format : 'd/m/Y',
        dataIndex : 'fechaAlta',
        text : 'Data generació',
        width : 120,
        align : 'center'
    },
    {
        xtype : 'datecolumn',
        format : 'd/m/Y',
        dataIndex : 'fechaTraspaso',
        text : 'Data traspàs',
        width : 120,
        align : 'center'
    },
    {
        dataIndex : 'nombreCompletoPersonaTraspaso',
        text : 'Traspassat per',
        width : 150,
        align : 'center'
    },
    {
        xtype : 'actioncolumn',
        dataIndex : 'actaId',
        align : 'right',
        width : 50,
        items : [
        {
            iconCls : 'x-fa fa-remove',
            tooltip : 'Mostrar acta',
            text : 'Mostrar acta',
            handler : 'onMostrarActa',
            getClass : function(actaId, cell, record)
            {
                return actaId !== null && record.get('fechaTraspaso') !== null ? 'x-fa fa-file-pdf-o' : '';
            }
        } ]
    } ],
    listeners :
    {
        updateElementCount : 'updateElementCount',
        render : 'updateElementCount',
        filterchange : 'updateElementCount'
    }
});