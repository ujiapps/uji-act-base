Ext.define('act.view.actaAdmin.ComboCurso',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.actaAdminComboCurso',
    fieldLabel : 'Curs',
    showClearIcon : true,
    width : 180,
    labelWidth : 60,
    allowBlank : true,
    displayField : 'id',

    bind :
    {
        store : '{cursosStore}'
    },
    listeners :
    {
        change : function(combo, cursoId)
        {
            combo.up('actaAdminMain').fireEvent('cursoSelected', cursoId);
        }
    }

});