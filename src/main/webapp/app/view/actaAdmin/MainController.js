Ext.define('act.view.actaAdmin.MainController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.actaAdminMainController',

    onLoad : function()
    {
        var vm = this.getViewModel();
        var actasDepartamentosStore = vm.get('actasDepartamentosStore');

        actasDepartamentosStore.removeAll();
    },

    getValoresUnicos : function(store, id, name)
    {
        var items = [];

        var data = store.getData();
        var snapshot = data.getSource();
        var unfilteredCollection = snapshot || data;
        var allRecords = unfilteredCollection.getRange();

        var map = {};

        allRecords = allRecords.filter(function(item)
        {
            var id = item.get(id);
            return isNaN(id) ? id !== null : id > 0;
        });

        allRecords.map(function(item)
        {
            if (!map.hasOwnProperty(item.get(id)))
            {
                map[item.get(id)] = item.get(name);
            }
        });

        return Object.keys(map).map(function(id)
        {
            return {
                id : id,
                nombre : map[id]
            };
        });
    },

    onCursoSelected: function(cursoId) {
        var view = this.getView();
        var vm = this.getViewModel();
        var store = vm.getStore('convocatoriasDetalleStore');

        store.load({
            params: {
                cursoId: cursoId
            }
        });
    },

    onCursoConvocatoriaSelected : function()
    {
        var view = this.getView();
        var cursoId = view.down('actaAdminComboCurso').getValue();
        var convocatoriaId = view.down('actaAdminComboConvocatoria').getValue();

        if (cursoId == null)
        {
            return;
        }

        var store = this.getStore('actasDepartamentosStore');
        store.getProxy().url = "/act/rest/cursos/" + cursoId + '/actas/admin';

        if (!(cursoId && convocatoriaId))
        {
            return store.removeAll();
        }

        var comboDepartamento = view.down('actaAdminComboDepartamento');
        var comboTipoEstudio = view.down('actaAdminComboTipoEstudio');

        var selectedDepartamento = comboDepartamento.getValue();
        var selectedTipoEstudio = comboTipoEstudio.getValue();

        store.load(
        {
            params :
            {
                'convocatoriaId' : convocatoriaId
            },
            callback : function()
            {
                var nombreDepartamentos = this.getValoresUnicos(store, 'departamentoRecibeActaId', 'nombreDepartamentoRecibeActa');
                comboDepartamento.clearValue();
                comboDepartamento.getStore().loadData(nombreDepartamentos);

                if (selectedDepartamento)
                {
                    comboDepartamento.setValue(selectedDepartamento);
                }

                var codigosTipoEstudio = this.getValoresUnicos(store, 'tipoEstudioId', 'tipoEstudioNombre');
                comboTipoEstudio.clearValue();
                comboTipoEstudio.getStore().loadData(codigosTipoEstudio);

                if (selectedTipoEstudio)
                {
                    comboTipoEstudio.setValue(selectedTipoEstudio);
                }
                view.down('actaAdminGrid').fireEvent('updateElementCount');
            },
            scope : this
        });
    }
});
