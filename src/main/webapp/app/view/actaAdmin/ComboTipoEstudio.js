Ext.define('act.view.actaAdmin.ComboTipoEstudio',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.actaAdminComboTipoEstudio',
    fieldLabel : 'Tipus estudi',
    showClearIcon : true,
    margin : '0 10 0 10',
    width : 250,
    labelWidth : 100,
    allowBlank : true,
    displayField : 'nombre',
    store : Ext.create('Ext.data.Store',
    {
        model : 'act.model.Lookup',
        autoLoad : true,
        sorters : [ 'nombre' ],
        proxy :
        {
            type : 'memory'
        }
    }),
    listeners :
    {
        change : function(combo, recordId)
        {
            combo.up('actaAdminPanelFiltros').fireEvent('filtroEstudioSelected', recordId);
        }
    }
});