Ext.define('act.view.actaAdmin.ComboActasTraspasadas',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.actaAdminComboActasTraspasadas',
    fieldLabel : 'Actes traspassades',
    showClearIcon : true,
    width : 250,
    flex : 1,
    padding : '0 10 0 20',
    labelWidth : 130,
    allowBlank : true,
    displayField : 'value',
    defaultValue: 1,
    emptyText: 'Mostrar totes',
    store : Ext.create('Ext.data.Store',
    {
        fields : [ 'id', 'value' ],
        data : [
        {
            id : 1,
            value : 'Traspassades'
        },
        {
            id : 2,
            value : 'Sense traspassar'
        } ]
    }),
    listeners :
    {
        change : function(combo, recordId)
        {
            combo.up('actaAdminPanelFiltros').fireEvent('filtroActaTraspasadaSelected', recordId);
        }
    }

});