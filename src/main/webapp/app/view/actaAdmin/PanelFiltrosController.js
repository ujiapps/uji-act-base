Ext.define('act.view.actaAdmin.PanelFiltrosController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.actaAdminPanelFiltrosController',

    onFiltroDepartamentoSelected : function(departamentoId)
    {
        var panel = this.getView();
        var grid = panel.up('actaAdminMain').down('actaAdminGrid');

        grid.getStore().removeFilter('departamentoRecibeActaId');
        if (departamentoId)
        {
            var filter = new Ext.util.Filter(
            {
                id : 'departamentoRecibeActaId',
                property : 'departamentoRecibeActaId',
                value : departamentoId
            });

            grid.getStore().addFilter(filter);
        }
    },

    onFiltroEstudioSelected : function(tipoEstudioId)
    {
        var panel = this.getView();
        var grid = panel.up('actaAdminMain').down('actaAdminGrid');

        grid.getStore().removeFilter('tipoEstudioId');
        if (tipoEstudioId)
        {
            var filter = new Ext.util.Filter(
            {
                id : 'tipoEstudioId',
                property : 'tipoEstudioId',
                value : tipoEstudioId
            });

            grid.getStore().addFilter(filter);
        }
    },

    onFiltroActaGeneradaSelected : function(estado)
    {
        var panel = this.getView();
        var grid = panel.up('actaAdminMain').down('actaAdminGrid');

        grid.getStore().removeFilter('generadas');
        if (estado)
        {
            var generadasFilter = new Ext.util.Filter(
            {
                id : 'generadas',
                filterFn : function(item)
                {
                    return estado === 1 ? item.get('fechaAlta') !== null : item.get('fechaAlta') === null;
                }
            });

            grid.store.addFilter(generadasFilter);
        }
    },

    onFiltroActaTraspasadaSelected : function(estado)
    {
        var panel = this.getView();
        var grid = panel.up('actaAdminMain').down('actaAdminGrid');

        grid.getStore().removeFilter('traspasadas');
        if (estado)
        {
            var generadasFilter = new Ext.util.Filter(
            {
                id : 'traspasadas',
                filterFn : function(item)
                {
                    return estado === 1 ? item.get('fechaTraspaso') !== null : item.get('fechaTraspaso') === null;
                }
            });

            grid.store.addFilter(generadasFilter);
        }
    },

    onFiltroFueraDePlazo : function(checkbox, fueradeplazo)
    {
        var panel = this.getView();
        var grid = panel.up('actaAdminMain').down('actaAdminGrid');
        var vm = this.getViewModel();
        var comboTraspasadas = panel.up('actaAdminMain').down('actaAdminComboActasTraspasadas');

        var convocatoriasStore = vm.getStore('convocatoriasStore');
        var convocatoriasDetalleStore = vm.getStore('convocatoriasDetalleStore');

        grid.getStore().removeFilter('fueraplazo');
        if (fueradeplazo)
        {
            comboTraspasadas.setValue(2);
            var fueradePlazoFilter = new Ext.util.Filter(
            {
                id : 'fueraplazo',
                filterFn : function(acta)
                {
                    var convocatoriaId = acta.get('convocatoriaId');
                    var tipoEstudioId = acta.get('tipoEstudioId');
                    var convocatoria = convocatoriasStore.getById(convocatoriaId);

                    var convocatoriaDetalleIdx = convocatoriasDetalleStore.findBy(function(cd) {
                        return cd.get('convocatoriaId') === convocatoriaId && cd.get("tipoEstudioId") === tipoEstudioId;
                    })

                    if (convocatoriaDetalleIdx) {
                        var convocatoriaDetalle = convocatoriasDetalleStore.getAt(convocatoriaDetalleIdx);

                        if (convocatoriaDetalle.get("fechaFinTraspaso") < new Date()) {
                            return true;
                        }
                    }
                    return false;
                }
            });

            grid.store.addFilter(fueradePlazoFilter);
        } else {
            comboTraspasadas.setValue(0);
        }
    }

});
