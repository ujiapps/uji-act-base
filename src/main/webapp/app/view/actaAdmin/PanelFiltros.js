Ext.define('act.view.actaAdmin.PanelFiltros',
{
    extend : 'Ext.panel.Panel',

    alias : 'widget.actaAdminPanelFiltros',
    border : 0,
    requires : [ 'act.view.actaAdmin.PanelFiltrosController', 'act.view.actaAdmin.ComboDepartamento', 'act.view.actaAdmin.ComboActasGeneradas', 'act.view.actaAdmin.ComboTipoEstudio',
            'act.view.actaAdmin.ComboActasTraspasadas' ],
    controller : 'actaAdminPanelFiltrosController',

    layout : 'anchor',
    items : [
    {
        xtype : 'fieldset',
        title : 'Filtres',
        items : [
        {
            xtype : 'fieldcontainer',
            layout : 'hbox',
            items : [
            {
                xtype : 'actaAdminComboDepartamento',
                flex : 2
            },
            {
                xtype : 'actaAdminComboActasGeneradas',
                flex : 1
            } ]
        },
        {
            xtype : 'fieldcontainer',
            layout : 'hbox',
            items : [
            {
                xtype : 'actaAdminComboTipoEstudio',
                flex : 1
            },
            {
                xtype : 'checkbox',
                align : 'center',
                boxLabel : 'Nomès actes fora de termini',
                labelSeparator : '',
                hideLabel : true,
                handler : 'onFiltroFueraDePlazo',
                flex : 1
            },
            {
                xtype : 'actaAdminComboActasTraspasadas',
                flex : 1
            } ]
        } ]

    } ],
    listeners :
    {
        filtroDepartamentoSelected : 'onFiltroDepartamentoSelected',
        filtroEstudioSelected : 'onFiltroEstudioSelected',
        filtroActaGeneradaSelected : 'onFiltroActaGeneradaSelected',
        filtroActaTraspasadaSelected : 'onFiltroActaTraspasadaSelected'
    }
});
