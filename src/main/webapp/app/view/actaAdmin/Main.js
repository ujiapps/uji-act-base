Ext.define('act.view.actaAdmin.Main',
{
    extend : 'Ext.panel.Panel',

    alias : 'widget.actaAdminMain',
    title : 'Control d\'Actes',

    requires : [ 'act.view.actaAdmin.PanelFiltros', 'act.view.actaAdmin.Grid', 'act.view.actaAdmin.MainController', 'act.view.actaAdmin.GridController', 'act.view.actaAdmin.ViewModel',
            'act.view.actaAdmin.ComboCurso', 'act.view.actaAdmin.ComboConvocatoria', 'act.view.actaAdmin.FormEnvioRecordatorio' ],
    controller : 'actaAdminMainController',

    cls : 'no-border',
    margin : 5,
    viewModel :
    {
        type : 'actaAdminViewModel'
    },
    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },

    items : [
    {
        xtype : 'panel',
        border : 0,
        padding : 10,
        items : [
        {
            xtype : 'container',
            flex : 1,
            layout :
            {
                type : 'hbox',
                align : 'right'
            },
            items : [

            {
                xtype : 'actaAdminComboCurso',
                reference : 'comboCurso'
            },
            {
                xtype : 'box',
                width : 50
            },
            {
                xtype : 'actaAdminComboConvocatoria',
                reference : 'comboConvocatoria',
                bind :
                {
                    disabled : '{!comboCurso.selection}'
                }
            } ]
        } ]
    },
    {
        xtype : 'actaAdminPanelFiltros',
        bind :
        {
            disabled : '{!cursoYconvocatoriaSeleccionados}'
        }
    },
    {
        xtype : 'actaAdminGrid',
        bind :
        {
            disabled : '{!cursoYconvocatoriaSeleccionados}'
        }
    } ],
    listeners :
    {
        render : 'onLoad',
        cursoSelected: 'onCursoSelected',
        convocatoriaSelected : 'onCursoConvocatoriaSelected'
    }
});
