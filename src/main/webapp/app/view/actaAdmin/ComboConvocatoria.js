Ext.define('act.view.actaAdmin.ComboConvocatoria',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.actaAdminComboConvocatoria',
    fieldLabel : 'Convocatoria',
    showClearIcon : true,
    width : 450,
    labelWidth : 100,
    displayField : 'nombre',
    allowBlank: true,
    bind: {
        store: '{convocatoriasStore}'
    },

    listeners :
    {
        change : function(combo, recordId)
        {
            combo.up('actaAdminMain').fireEvent('convocatoriaSelected', recordId);
        }
    }

});