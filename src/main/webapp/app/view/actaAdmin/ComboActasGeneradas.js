Ext.define('act.view.actaAdmin.ComboActasGeneradas',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.actaAdminComboActasGeneradas',
    fieldLabel : 'Actas generadas',
    showClearIcon : true,
    width : 250,
    padding : '0 10 0 20',
    flex : 1,
    labelWidth : 130,
    allowBlank : true,
    displayField : 'value',
    emptyText : 'Mostrar totes',
    store : Ext.create('Ext.data.Store',
    {
        fields : [ 'id', 'value' ],
        data : [
        {
            id : 1,
            value : 'Generades'
        },
        {
            id : 2,
            value : 'Sense generar'
        } ]
    }),
    listeners :
    {
        change : function(combo, recordId)
        {
            combo.up('actaAdminPanelFiltros').fireEvent('filtroActaGeneradaSelected', recordId);
        }
    }

});