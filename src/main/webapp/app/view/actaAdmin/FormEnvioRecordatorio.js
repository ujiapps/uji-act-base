Ext.define('act.view.actaAdmin.FormEnvioRecordatorio',
{
    extend : 'Ext.window.Window',
    alias : 'widget.formEnvioRecordatorio',

    title : 'Enviament de recordatori',
    width : 640,
    height : 420,
    modal : true,
    bodyPadding : 10,
    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },

    requires : [ 'act.view.actaAdmin.FormEnvioRecordatorioController' ],
    controller : 'formEnvioRecordatorioController',

    items : [
    {
        xtype : 'panel',
        layout : 'anchor',
        border : 0,
        items : [
        {
            xtype : 'box',
            padding : 10,
            html : '<span style="font-size: 16px;">S\'enviarà un e-mail de notificació a tots els responsables de les actes que tens en pantalla amb el texte següent:</span>'
        },
        {
            xtype : 'textareafield',
            name: 'cuerpoNotificacion',
            padding : 10,
            width: '98%',
            height: 240,
            value : 'Et recordem que la data de traspàs d\'aquesta acta finalitza aviat.'
        } ]
    } ],
    bbar : [ '->', '->',
    {
        xtype : 'button',
        text : 'Enviar notificació',
        handler : 'enviaEmailNotificacion'
    },
    {
        xtype : 'panel',
        html : '<a style="text-decoration: none; color: #222;" href="#">Cancel·lar</a>',
        listeners :
        {
            render : function(component)
            {
                component.getEl().on('click', 'onClose');
            }
        }
    } ]
});