Ext.define('act.view.actaAdmin.GridController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.actaAdminGridController',

    updateElementCount : function()
    {
        var grid = this.getView();
        var count = grid.getStore().getCount();
        var element = grid.down('[name=elementCount]');
        if (element)
        {
            element.setText(count + ' elements');
        }
    },

    onMostrarActa : function(view, rowIndex, colIndex, item, event, acta)
    {
        if (acta.get('referencia'))
        {
            var referencia = acta.get('referencia');
            var idioma = 'CA';
            return window.open('https://e-ujier.uji.es/cocoon/4/2/' + referencia + '/CA/documento.pdf');
        }

        return window.open('/act/rest/publicacion/listado-notas-nombre/' + acta.get('actaId'));
    },

    onEnviarNotificacion : function()
    {
        var main = this.getView();
        var departamento = main.up('actaAdminMain').down('actaAdminComboDepartamento').getValue();
        var tipoEstudio = main.up('actaAdminMain').down('actaAdminComboTipoEstudio').getValue();
        var generadas = main.up('actaAdminMain').down('actaAdminComboActasGeneradas').getValue();
        var traspasadas = main.up('actaAdminMain').down('actaAdminComboActasTraspasadas').getValue();

        if (!departamento && !tipoEstudio && !generadas && !traspasadas) {
            return Ext.Msg.alert('Error', "Es necessari sel·leccionar al menys un filtre per a fer una notificació als docents");
        }

        var view = this.getView();
        var modal =
            {
                xtype : 'formEnvioRecordatorio'
            };

        view.add(modal).show();
    },
    onReload : function()
    {
        var grid = this.getView();
        var main = grid.up('actaAdminMain');
        var comboDepartamento = main.down('actaAdminComboDepartamento');
        var selectedDepartamento = comboDepartamento.getValue();
        var comboTipoEstudio = main.down('actaAdminComboTipoEstudio');
        var selectedTipoEstudio = comboTipoEstudio.getValue();

        grid.getStore().reload(
            {
                callback : function()
                {
                    comboDepartamento.setValue(selectedDepartamento);
                    comboTipoEstudio.setValue(selectedTipoEstudio);
                }
            });
    }



});
