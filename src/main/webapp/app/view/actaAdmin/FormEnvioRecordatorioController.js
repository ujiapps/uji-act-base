Ext.define('act.view.actaAdmin.FormEnvioRecordatorioController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.formEnvioRecordatorioController',

    onClose : function()
    {
        var win = Ext.WindowManager.getActive();
        if (win)
        {
            win.destroy();
        }
    },

    enviaEmailNotificacion: function()
    {
        var view = this.getView();
        var vm = this.getViewModel();
        var cursoId = view.up('actaAdminMain').down('actaAdminComboCurso').getValue();
        var convocatoriaId = view.up('actaAdminMain').down('actaAdminComboConvocatoria').getValue();
        var grid = view.up('actaAdminMain').down('actaAdminGrid');
        var mensaje = view.down('textareafield[name=cuerpoNotificacion]').getValue();
        var allRecords = grid.getStore().getData().getRange();

        var codigos = allRecords.map(function(item) {
            return item.get('codigo');
        });

        Ext.Ajax.request(
        {
            url : '/act/rest/cursos/' + cursoId + '/convocatorias/' + convocatoriaId + '/enviarnotificacion',
            jsonData: {
                codigos: codigos,
                mensaje: mensaje
            },
            method : 'POST',
            success : function(data)
            {
                view.setLoading(false);
                return Ext.Msg.alert('Fet', "S'han enviat les notificacions. Rebràs un correu resumint els enviaments de missatges que s'han fet als corresponents responsables d'acta.");
                this.onClose();
            },
            failure : function()
            {
                view.setLoading(false);
            },
            scope: this
        });
    }
});
