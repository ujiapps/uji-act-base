Ext.define('act.view.acta.Grid',
{
    extend : 'Ext.ux.uji.grid.Panel',

    alias : 'widget.actaGrid',

    store : Ext.create('Ext.ux.uji.data.Store',
    {
        model : 'act.model.Acta',
        url : '/act/rest/actas'
    }),

    title : 'Acta',

    columns : [
    {
        text : 'ID',
        dataIndex : 'id',
        hidden : true
    },
    {
        dataIndex : 'cursoId',
        text : 'Curso'
    },
    {
        dataIndex : 'convocatoriaId',
        text : 'Convocatoria'
    },
    {
        dataIndex : 'tipoEstudioId',
        text : 'Tipus'
    },
    {
        dataIndex : 'actaUnica',
        text : 'Unica'
    },
    {
        xtype : 'datecolumn',
        format: 'd/m/Y',
        dataIndex : 'fechaAlta',
        text : 'F. Alta'
    },
    {
        dataIndex : 'oficial',
        text : 'Oficial'
    },
    {
        dataIndex : 'descripcion',
        text : 'Desc.'
    },
    {
        dataIndex : 'personaId',
        text : 'Persona'
    },
    {
        dataIndex : 'ubicacionId',
        text : 'Ubicació'
    },
    {
        xtype : 'datecolumn',
        format: 'd/m/Y',
        dataIndex : 'fechaTraspaso',
        text : 'Data traspas'
    },
    {
        dataIndex : 'personaIdTraspaso',
        text : 'Persona traspas'
    },
    {
        xtype : 'datecolumn',
        format: 'd/m/Y',
        dataIndex : 'fechaFirmaDigital',
        text : 'Data firma'
    },
    {
        dataIndex : 'referencia',
        text : 'Referencia'
    },
    {
        dataIndex : 'desgloseActivo',
        text : 'Permet detall'
    },
    {
        dataIndex : 'muestraNotasInicio',
        text : 'Notes inici'
    },
    {
        dataIndex : 'muestraNotasFin',
        text : 'Notes fi'
    },
    {
        dataIndex : 'revisionConjunta',
        text : 'Revisió'
    },
    {
        xtype : 'datecolumn',
        format: 'd/m/Y',
        dataIndex : 'fechaRevision',
        text : 'Data revisió'
    },
    {
        dataIndex : 'lugarRevision',
        text : 'Lloc revisió'
    },
    {
        dataIndex : 'observacionesRevision',
        text : 'Observacions'
    } ]
});