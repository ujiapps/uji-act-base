Ext.define('act.view.acta.Main',
{
    extend : 'Ext.panel.Panel',

    alias : 'widget.actaMain',
    title : 'Gestió: Acta',
    requires : [ 'act.view.acta.Grid' ],

    items : [
    {
        xtype : 'actaGrid'
    } ]
});
