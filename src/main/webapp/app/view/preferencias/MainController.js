Ext.define('act.view.preferencias.MainController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.preferenciasController',

    temaSelected : function()
    {
        var view = this.getView();
        var tema = view.down('preferenciasComboTema').getValue();

        view.setLoading(true);
        Ext.Ajax.request(
        {
            url : '/act/rest/usuario/preferencias',
            method : 'POST',
            jsonData :
            {
                tema : tema
            },
            success : function()
            {
                var pathname = window.location.pathname;
                if (tema === 'triton' && pathname !== '/act/index.jsp')
                {
                    return window.location = '/act/index.jsp';
                }

                if (tema === 'aria' && pathname !== '/act/index-aria.jsp')
                {
                    return window.location = '/act/index-aria.jsp';
                }
                view.setLoading(false);

            },
            failure : function(response)
            {
                view.setLoading(false);
            }
        });
    }
});
