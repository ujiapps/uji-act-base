Ext.define('act.view.preferencias.Main',
{
    extend : 'Ext.panel.Panel',
    alias : 'widget.panelPreferencias',
    name : 'panelPreferencias',
    requires : [ 'act.view.preferencias.MainController', 'act.store.Temas', 'act.view.preferencias.ComboTema'],
    controller : 'preferenciasController',
    title : 'Preferències',
    frame : true,
    scrollable : true,
    items : [
    {
        xtype : 'panel',
        title : 'Visualització',
        padding : 10,
        items : [
            {
                xtype: 'panel',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'preferenciasComboTema',
                    },
                    {
                        xtype: 'button',
                        margin: 10,
                        text: 'Aplicar',
                        handler: 'temaSelected'
                    }]
            }]
    } ]
});
