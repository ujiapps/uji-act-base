Ext.define('act.view.preferencias.ComboTema',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.preferenciasComboTema',
    padding : 10,
    width : 400,
    labelWidth : 120,
    fieldLabel : 'Tema visual',
    displayField : 'nombre',
    valueField : 'id',
    queryMode : 'local',
    value : 'triton',
    store : Ext.create('act.store.Temas')
});