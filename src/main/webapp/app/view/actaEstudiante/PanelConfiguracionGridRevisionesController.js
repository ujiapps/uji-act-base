Ext.define('act.view.actaEstudiante.PanelConfiguracionGridRevisionesController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    requires : [ 'act.view.actaEstudiante.FormRevision' ],
    alias : 'controller.panelConfiguracionGridRevisionesController',

    getAsignaturasStore : function()
    {
        var vm = this.getViewModel();

        var actaEstudiantesStore = this.getStore('actaEstudiantesStore');
        var asignaturas = this.getValoresUnicos(actaEstudiantesStore, 'asignaturaId');
        var asignaturasStore = Ext.create('Ext.data.Store',
        {
            fields : [ 'id', 'nombre' ],
            sorters : [ 'nombre' ],
            data : asignaturas
        });

        return asignaturasStore;
    },

    getGruposStore : function()
    {
        var vm = this.getViewModel();

        var actaEstudiantesStore = this.getStore('actaEstudiantesStore');
        var grupos = this.getValoresUnicos(actaEstudiantesStore, 'grupoId');
        var gruposStore = Ext.create('Ext.data.Store',
        {
            fields : [ 'id', 'nombre' ],
            sorters : [ 'nombre' ],
            data : grupos
        });

        return gruposStore;
    },

    onAdd : function()
    {
        var view = this.getView().up('actaEstudianteMain')
        var vm = this.getViewModel();
        var actaId = vm.get('selectedConvocatoria').get('actaId');
        var nombreRevision = vm.get('selectedConvocatoria').get('descripcion');

        var modal =
        {
            xtype : 'formRevision',
            viewModel :
            {
                data :
                {
                    title : 'Nova revisió',
                    revision : Ext.create('act.model.Revision',
                    {
                        conjunta : true,
                        actaId : actaId,
                        nombre : nombreRevision
                    }),
                    asignaturasStore : this.getAsignaturasStore(),
                    gruposStore : this.getGruposStore()
                }
            }
        };

        view.add(modal).show();
    },

    getValoresUnicos : function(store, field)
    {
        var items = [];

        var data = store.getData();
        var snapshot = data.getSource();
        var unfilteredCollection = snapshot || data;
        var allRecords = unfilteredCollection.getRange();

        Ext.Array.map(allRecords, function(rec)
        {
            Ext.Array.include(items, rec.get(field));
        });

        return Ext.Array.map(items, function(item)
        {
            var data =
            {
                id : item,
                nombre : item
            };
            return data;
        });
    },

    onEdit : function()
    {
        var view = this.getView().up('actaEstudianteMain');
        var grid = this.getView();
        var vm = this.getViewModel();
        var revision = grid.getSelectionModel().getSelection()[0];

        if (!revision)
            return;

        var modal =
        {
            xtype : 'formRevision',
            viewModel :
            {
                data :
                {
                    title : 'Edició de revisió',
                    revision : revision,
                    asignaturasStore : this.getAsignaturasStore(),
                    gruposStore : this.getGruposStore()
                }
            }
        };

        view.add(modal).show();
    },

    onEnviarNotaActaRevision : function(view, rowIndex, colIndex, item, event, revision)
    {
        if (!revision)
            return;

        var notificacionesStore = this.getStore('notificacionesStore');
        Ext.Msg.confirm('Enviar notes', 'Esteu segur/a de voler enviar les notes als alumnes?', function(result)
        {
            if (result === 'yes')
            {
                Ext.Ajax.request(
                {
                    url : '/act/rest/actas/' + revision.get('actaId') + '/actarevisiones/' + revision.get('id') + '/envianotas',
                    method : 'POST',
                    success : function()
                    {
                        notificacionesStore.reload();
                        Ext.Msg.alert('Informació', "Les notes s'han enviat correctament");
                    },
                    failure : function()
                    {
                        Ext.Msg.alert('Error', "S'ha produït un error en fer l'enviament");
                    }
                });
            }
        });
    },

    muestraPreviewEnvioNotaAlumnos : function(view, rowIndex, colIndex, item, event, revision)
    {
        if (!revision)
            return;

        view.setLoading(true);

        Ext.Ajax.request(
            {
                url : '/act/rest/actas/' + revision.get('actaId') + '/actarevisiones/' + revision.get('id') + '/envianotas-preview',
                method : 'GET',
                success : function(response)
                {
                    var responseObject = Ext.JSON.decode(response.responseText);
                    view.setLoading(false);
                    var modal =
                        {
                            xtype : 'formEnvioNotas',
                            viewModel :
                                {
                                    data :
                                        {
                                            titulo : responseObject.data.titulo,
                                            contenido : responseObject.data.contenido.replace(/\n/g, '<br />'),
                                            revisionId: revision.get('id'),
                                            actaId: revision.get('actaId')
                                        }
                                }
                        };

                    view.up('actaEstudianteMain').add(modal).show();
                },
                failure : function()
                {
                    view.setLoading(false);
                }
            });

    }

});
