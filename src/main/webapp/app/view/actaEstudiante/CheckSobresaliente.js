Ext.define('act.view.actaEstudiante.CheckSobresaliente',
{
    extend : 'Ext.form.field.Checkbox',
    alias : 'widget.checkSobresaliente',
    fieldLabel : 'Notes >=9',
    labelWidth : 120,
    labelAlign: 'right',
    labelTextAlign: 'right',
    listeners :
    {
        change : function(check, value)
        {
            check.up('grid').fireEvent('sobresalienteSelected', value);
        }
    }

});