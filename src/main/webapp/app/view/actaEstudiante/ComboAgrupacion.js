Ext.define('act.view.actaEstudiante.ComboAgrupacion',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.comboAgrupacion',
    fieldLabel : 'Agrupació:',
    showClearIcon : true,
    allowBlank: true,
    width : 180,
    labelWidth : 70,

    store : Ext.create('Ext.data.Store',
    {
        model : 'act.model.Lookup',
        autoLoad : true,
        sorters : [ 'nombre' ],
        proxy :
        {
            type : 'memory'
        }
    }),

    listeners :
    {
        change : function(combo, recordId)
        {
            combo.up('grid').fireEvent('agrupacionSelected', recordId);
        }
    }

});