Ext.define('act.view.actaEstudiante.GridResumenActaUnica',
{
    extend : 'Ext.grid.Panel',

    alias : 'widget.gridResumenActaUnica',
    scrollable : true,
    layout : 'fit',
    bind :
    {
        store : '{resumenActaEstudiantesStore}',
        title : 'Notes ja traspassades'
    },
    columns : [
    {
        text : 'ID',
        dataIndex : 'id',
        hidden : true
    },
    {
        dataIndex : 'index',
        width : 45,
        sortable : false,
        align : 'right',
        renderer : function(value, metaData, record, rowIndex)
        {
            return rowIndex + 1;
        }
    },
    {
        dataIndex : 'nombreCompletoAlumno',
        text : 'Alumne',
        filter : 'string',
        minWidth : 150,
        flex : 1,
        renderer : function(value, metadata, record)
        {
            metadata.tdAttr = 'data-qtip="' + record.get('motivoExclusion') + '"';
            return value
        }
    },
    {
        dataIndex : 'identificacionAlumno',
        text : 'Dni',
        align : 'center',
        filter : 'string'
    },
    {
        dataIndex : 'nota',
        text : 'Nota',
        align : 'center'
    },
    {
        dataIndex : 'calificacionId',
        text : 'Qualificació',
        align : 'center',
        renderer : function(value)
        {
            var store = Ext.getStore('act.store.Calificaciones');
            var data = store.getById(value);
            return data ? data.get('nombre') : '';
        }
    },
    {
        xtype : 'datecolumn',
        format : 'd/m/Y',
        dataIndex : 'fechaTraspaso',
        dateFormat : 'd/m/Y',
        text : 'Data',
        align : 'center'
    },
    {
        dataIndex : 'comentarioTraspaso',
        text : 'Observacions',
        flex : 1
    },
    {
        text : 'Habilitar canvi nota',
        width : 200,
        align : 'center',
        stopSelection : true,
        xtype : 'widgetcolumn',
        bind :
        {
            hidden : '{!selectedConvocatoria.puedeRecuperarNotas}'
        },
        widget :
        {
            xtype : 'button',
            _btnText : "Habilitar",
            width : 80,
            defaultBindProperty : null, //important
            handler : function(widgetColumn)
            {
                var record = widgetColumn.getWidgetRecord();
                this.up('actaEstudiantePanelResumenTraspasos').fireEvent('habilitarCambioNota', record.get('id'));
            },
            listeners :
            {
                beforerender : function(widgetColumn)
                {
                    var record = widgetColumn.getWidgetRecord();
                    widgetColumn.setText(widgetColumn._btnText); //can be mixed with the row data if needed
                }
            }
        }
    } ]
});