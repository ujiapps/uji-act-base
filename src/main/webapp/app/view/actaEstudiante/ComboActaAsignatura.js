Ext.define('act.view.actaEstudiante.ComboActaAsignatura',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.comboActaAsignatura',
    fieldLabel : 'Assignatura',
    showClearIcon : true,
    width : 220,
    labelWidth : 90,
    allowBlank: true,

    displayField : 'asignaturaId',

    bind: {
        store: 'actaAsignaturasStore'
    },

    loadData : function(actaId)
    {
        this.clearValue();
        if (actaId == null)
        {
            return;
        }
        var store = this.getStore();
        store.getProxy().url = "/act/rest/actas/" + actaId + '/actaAsignaturas';
        store.load();
    },

    listeners :
    {
        change : function(combo, recordId)
        {
            combo.up('actaEstudiantePanelFiltros').fireEvent('actaAsignaturaSelected', recordId);
        }
    }

});