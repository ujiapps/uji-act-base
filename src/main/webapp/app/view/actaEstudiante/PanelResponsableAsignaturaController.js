Ext.define('act.view.actaEstudiante.PanelResponsableAsignaturaController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.panelResponsableAsignaturaController',

    onClose : function()
    {
        var win = Ext.WindowManager.getActive();
        if (win)
        {
            win.destroy();
        }
    }
});
