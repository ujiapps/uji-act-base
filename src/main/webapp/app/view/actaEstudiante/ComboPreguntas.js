Ext.define('act.view.actaEstudiante.ComboPreguntas',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.comboPreguntas',
    fieldLabel : 'Pregunta',
    showClearIcon : true,
    allowBlank: true,
    width : 180,
    labelWidth : 60,

    store : Ext.create('Ext.data.Store',
    {
        model : 'act.model.Lookup',
        autoLoad : true,
        sorters : [ 'nombre' ],
        proxy :
        {
            type : 'memory'
        }
    }),

    listeners :
    {
        change : function(combo, recordId)
        {
            combo.up('grid').fireEvent('preguntaSelected', recordId);
        }
    }

});