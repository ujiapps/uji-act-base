Ext.define('act.view.actaEstudiante.PanelResumenTraspasosController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.panelResumenTraspasosController',

    onActivate : function()
    {
        var panel = this.getView();
        var vm = this.getViewModel();
        vm.notify();
        var gridResumen = panel.down('actaEstudianteGridResumenTraspasos');
        var store = vm.get('resumenActaEstudiantesStore');
        var gridActaUnica = panel.down('gridResumenActaUnica');
        var convocatoria = vm.get('selectedConvocatoria')
        var actaId = convocatoria.get('actaId');

        if (actaId == null)
        {
            return;
        }

        store.getProxy().url = '/act/rest/actas/' + actaId + '/actaestudiantes';

        if (gridActaUnica.isVisible())
        {
            panel.setLoading(true);
            store.load(
            {
                callback : function(data)
                {
                    store.filter(
                    {
                        property : 'fechaTraspaso',
                        filterFn : function(rec)
                        {
                            return rec.get('fechaTraspaso') != null;
                        }
                    });
                    panel.setLoading(false);
                },
                scope : this
            });

        }
        else if (gridResumen.isVisible())
        {
            panel.setLoading(true);
            store.clearFilter();
            store.load(
            {
                callback : function(data)
                {
                    this.estableceTituloResumenActa();
                    panel.setLoading(false);
                },
                scope : this
            });
        }
    },

    estableceTituloResumenActa : function()
    {
        var panel = this.getView();
        var grid = panel.down('actaEstudianteGridResumenTraspasos');
        var store = this.getStore('actaEstudiantesStore');

        var data = store.getData();

        var alumnos = data.length;
        var alumnosActa = data.items.filter(function(a)
        {
            return !a.get('motivoExclusion')
        }).length;
        var alumnosFueraActa = data.items.filter(function(a)
        {
            return a.get('motivoExclusion')
        }).length;
        grid.setTitle(alumnos + ' alumnes en total, en acta ' + alumnosActa + ', i ' + alumnosFueraActa + ' fora de l\'acta');
    },

    descargarDocumento : function()
    {
        var vm = this.getViewModel();
        var acta = vm.get('selectedConvocatoria');

        if (acta.get('referencia'))
        {
            var referencia = acta.get('referencia');
            var idioma = 'CA';
            return window.open('https://e-ujier.uji.es/cocoon/4/2/' + referencia + '/CA/documento.pdf');
        }

        return window.open('/act/rest/publicacion/listado-notas-nombre/' + acta.get('actaId'));
    },

    anularTraspaso : function()
    {
        var view = this.getView();
        var vm = this.getViewModel();
        var cursoId = view.up('actaEstudianteMain').down('actaEstudianteComboCurso').getValue();
        var actaId = vm.get('selectedConvocatoria').get('actaId');
        var storeConvocatorias = vm.getStore('convocatoriasStore');

        Ext.Msg.confirm('Esborrar', 'Esteu segur/a de voler anul·lar el traspàs d\'aquesta acta?', function(result)
        {
            if (result === 'yes')
            {
                Ext.Ajax.request(
                {
                    url : '/act/rest/cursos/' + cursoId + '/actas/' + actaId + '/destraspasar',
                    method : 'DELETE',
                    success : function()
                    {
                        storeConvocatorias.reload(
                        {
                            callback : function()
                            {
                                view.setLoading(false);
                                view.up('actaEstudianteTabs').setActiveTab(3);
                            }
                        });
                    },
                    failure : function()
                    {
                        view.setLoading(false);
                    }
                });
            }
        });
    },

    habilitarCambioNota : function(actaEstudianteId)
    {
        var panel = this.getView();
        var gridActaUnica = panel.down('gridResumenActaUnica');

        Ext.Msg.confirm('Habilitar canvi de nota', 'Aquesta acció esborrarà la nota de l\'expedient del alumne/a. Esteu segur/a de voler continuar?', function(result)
        {
            if (result === 'yes')
            {
                panel.setLoading(true);
                Ext.Ajax.request(
                {
                    url : '/act/rest/actaestudiantes/' + actaEstudianteId + '/borrar-traspaso',
                    method : 'PUT',
                    success : function()
                    {
                        gridActaUnica.getStore().reload(
                        {
                            callback : function(data)
                            {
                                gridActaUnica.getStore().filter(
                                {
                                    property : 'fechaTraspaso',
                                    filterFn : function(rec)
                                    {
                                        return rec.get('fechaTraspaso') != null;
                                    }
                                });
                                panel.setLoading(false);
                            },
                            scope : this
                        });

                    },
                    failure : function(response)
                    {
                        panel.setLoading(false);
                    }
                });
            }
        });

    }
});
