Ext.define('act.view.actaEstudiante.PanelFiltrosController',
    {
        extend: 'Ext.ux.uji.grid.PanelController',
        alias: 'controller.panelFiltrosController',

        onLoad: function () {
            var vm = this.getViewModel();
            var panel = this.getView();
            var cursosStore = vm.get('cursosStore');
            var combo = panel.down('actaEstudianteComboCurso')

            cursosStore.load(
                {
                    callback: function (records) {
                        Ext.Array.map(records, function (curso) {
                            if (curso.get('activo')) {
                                combo.setValue(curso.get('id'));
                            }
                        });
                    }
                });
        },

        onActaSelected: function (cursoId, codigo) {
            var grid = this.getView().down('actaEstudianteGridConvocatoria');
            grid.fireEvent('actaSelected', cursoId, codigo);
        },

        onCursoSelected: function (cursoId) {
            var vm = this.getViewModel();
            var store = this.getStore('actasPersonaStore');
            var view = this.getView();

            view.down('actaEstudianteComboActa').clearValue();

            if (cursoId == null) {
                return view.down('actaEstudianteComboActa').reset();
            }

            view.setLoading(true);
            store.getProxy().url = "/act/rest/cursos/" + cursoId + '/actas';
            store.load({
                callback: function () {
                    view.setLoading(false);
                },
                scope: this
            });
        }
    });
