Ext.define('act.view.actaEstudiante.GridController',
    {
        extend: 'Ext.ux.uji.grid.PanelController',
        alias: 'controller.actaEstudianteGridController',

        getValoresUnicos: function (store, field) {
            var items = [];

            var data = store.getData();
            var snapshot = data.getSource();
            var unfilteredCollection = snapshot || data;
            var allRecords = unfilteredCollection.getRange();

            Ext.Array.map(allRecords, function (rec) {
                Ext.Array.include(items, rec.get(field));
            });

            return Ext.Array.map(items, function (item) {
                var data =
                    {
                        id: item,
                        nombre: item
                    };
                return data;
            });
        },

        loadActaEstudiantes: function (store, actaId, convocatoria) {
            var grid = this.getView();
            var params;
            var url;

            store.clearFilter();
            if (convocatoria.get('fechaTraspaso')) {
                url = '/act/rest/actas/' + actaId + '/actadiligencias';
                params =
                    {
                        convocatoriaId: convocatoria.get('id')
                    };

            } else {
                url = '/act/rest/actas/' + actaId + '/actaestudiantes';
            }

            store.load(
                {
                    url: url,
                    params: params,
                    callback: function (data) {
                        var asignaturas = this.getValoresUnicos(store, 'asignaturaId');
                        var comboAsignatura = grid.down('comboAsignatura');
                        comboAsignatura.clearValue();
                        comboAsignatura.getStore().loadData(asignaturas);
                        comboAsignatura.setVisible(asignaturas.length > 1);

                        var grupos = this.getValoresUnicos(store, 'grupoId');
                        var comboGrupo = grid.down('comboGrupo')
                        comboGrupo.clearValue();
                        comboGrupo.getStore().loadData(grupos);
                        comboGrupo.setVisible(grupos.length > 1);

                        var asignaturaColumn = grid.down('[dataIndex=asignaturaId]');
                        asignaturaColumn.setVisible(asignaturas.length > 1);

                        grid.getView().refresh();

                        var matriculas = 0;
                        var alumnos = data.filter(function (rec) {
                            return !rec.get('motivoExclusion');
                        }).length;

                        var alumnosExcluidos = data.length - alumnos;


                        if (convocatoria.get('fechaTraspaso')) {
                            Ext.each(data, function (rec) {
                                if (rec.data.calificacionId === 5) {
                                    matriculas++;
                                }
                            });

                        } else {
                            Ext.each(data, function (rec) {
                                if (rec.data.matriculaHonor) {
                                    matriculas++;
                                }
                            });
                        }

                        var m = matriculas + '/' + convocatoria.get('matriculas');
                        grid.down('displayfield[name=resumenAlumnos]').setValue(alumnos);
                        grid.down('displayfield[name=resumenAlumnosExcluidos]').setValue(alumnosExcluidos);
                        grid.down('displayfield[name=resumenMatriculas]').setValue(m);
                        grid.down('panel[name=infoActa]').setHidden(false);
                    },
                    scope: this
                });

        },

        onSeleccionarAlumnos: function () {
            var vm = this.getViewModel();

            var cambio = !vm.get('todoSeleccionado');
            var storeActaEstudiantes = vm.getStore('actaEstudiantesStore');

            storeActaEstudiantes.each(function (item) {
                if (item.get('editable') && item.get('notaCalculada')) {
                    item.set('aExpediente', cambio);
                }
            });

            vm.set('todoSeleccionado', cambio);
        },

        onActivate: function () {
            var grid = this.getView();
            var vm = this.getViewModel();
            var convocatoria = vm.get('selectedConvocatoria');
            if (!convocatoria)
                return;

            var actaId = convocatoria.get('actaId');
            var store;

            grid.down('panel[name=infoActa]').setHidden(true);
            if (actaId == null) {
                return;
            }

            if (convocatoria.get('fechaTraspaso')) {
                this.loadActaEstudiantes(this.getStore('actaDiligenciasStore'), actaId, convocatoria);
            } else {
                this.loadActaEstudiantes(this.getStore('actaEstudiantesStore'), actaId, convocatoria);
            }
        },

        onGrupoSelected: function (recordId) {
            var grid = this.getView();

            if (recordId) {
                grid.getStore().addFilter(new Ext.util.Filter(
                    {
                        id: 'grupoId',
                        property: 'grupoId',
                        value: recordId
                    }));
            } else {
                grid.getStore().removeFilter('grupoId');
            }
        },

        onAsignaturaSelected: function (recordId) {
            var grid = this.getView();

            if (recordId) {
                grid.getStore().addFilter(new Ext.util.Filter(
                    {
                        id: 'asignaturaId',
                        property: 'asignaturaId',
                        value: recordId
                    }));
            } else {
                grid.getStore().removeFilter('asignaturaId');
            }
        },

        onBotonActaClick: function () {
            var grid = this.getView();
            var mainPanel = grid.up('actaEstudianteMain');
            mainPanel.fireEvent('volverASelector');
        },

        onGuardarDatos: function (checkcolumn, index, checked, record) {
            var grid = this.getView();
            grid.setLoading(true);
            var vm = this.getViewModel();
            var actaConvocatoria = vm.get('selectedConvocatoria');

            grid.getStore().sync(
                {
                    success: function () {
                        var matriculas = 0;
                        grid.getStore().each(function (rec) {
                            if (rec.get('matriculaHonor')) {
                                matriculas++;
                            }
                        });
                        vm.set('numeroMatriculasActa', matriculas + '/' + actaConvocatoria.get('matriculas'));
                        grid.setLoading(false);
                    },
                    failure: function () {
                        grid.setLoading(false);
                        record.set('matriculaHonor', !checked);
                    }
                });
        },

        onAddComentario: function (view, rowIndex, colIndex, item, event, record) {
            if (record.get('motivoExclusion')) {
                return;
            }

            var view = this.getView().up('actaEstudianteMain')
            var vm = this.getViewModel();
            var modal =
                {
                    xtype: 'formComentario',
                    viewModel:
                        {
                            data:
                                {
                                    record: record,
                                    store: vm.getStore('actaEstudiantesStore')
                                }
                        }
                };

            view.add(modal).show();
        },

        onBeforeEdit: function (editor, context, eOpts) {
            return context.record.get('editable') ? true : false;
        },

        onBeforeCheckChange: function (check, rowIndex, checked, record) {
            return record.get('editable') ? true : false;
        },

        getPreviousEditableIndex: function (grid, editor, currentIndex) {
            var newIndex = currentIndex;
            while (newIndex-- > 0) {
                var record = grid.getStore().getAt(newIndex);
                if (record.get('editable')) {
                    return newIndex;
                }
            }
            return currentIndex;
        },

        getNextEditableIndex: function (grid, editor, currentIndex) {
            var newIndex = currentIndex;
            while (newIndex++ <= grid.getStore().getCount()) {
                var record = grid.getStore().getAt(newIndex);
                if (record != null && record.get('editable')) {
                    return newIndex;
                }
            }
            return currentIndex;
        },

        navigateKeys: function (nf, evt) {
            switch (evt.getKey()) {
                case 38:
                    var grid = this.getView();
                    var editor = grid.getPlugins()[1];

                    editor.startEditByPosition(
                        {
                            row: this.getPreviousEditableIndex(grid, editor, editor.context.rowIdx),
                            column: 9
                        });
                    evt.stopEvent();
                    break;
                case 9:
                case 40:
                case 13:
                    var grid = this.getView();
                    var editor = grid.getPlugins()[1];

                    editor.startEditByPosition(
                        {
                            row: evt.shiftKey ? this.getPreviousEditableIndex(grid, editor, editor.context.rowIdx) : this.getNextEditableIndex(grid, editor, editor.context.rowIdx),
                            column: 9
                        });
                    evt.stopEvent();
                    break;
                default:
                    break;
            }
            return false;
        },

        checkNotaEditable: function (editor, context) {
            var rec = context.record;
            return rec.get('editable');
        },

        sincronizarNotas: function () {
            var grid = this.getView();
            var vm = this.getViewModel();
            var store = grid.getStore();

            if (store.getModifiedRecords().length > 0) {
                grid.setLoading(true);
                grid.getStore().sync(
                    {
                        callback: function () {
                            grid.setLoading(false);
                            vm.set('cambiosPendientesNotas', store.getModifiedRecords().length);
                        }
                    });
            }
        },

        exportarNotasCsv: function () {
            var vm = this.getViewModel();
            var view = this.getView();
            var actaId = vm.get('selectedConvocatoria').get('actaId');
            var grupo = view.down('comboGrupo').getValue();
            var asignatura = view.down('comboAsignatura').getValue();

            var params = '';
            if (grupo || asignatura) {
                var obj =
                    {
                        grupo: grupo,
                        asignatura: asignatura
                    };
                params = '?' + Ext.urlEncode(obj);
            }
            return window.open('/act/rest/actas/' + actaId + '/exportar-csv' + params);
        },

        listadoConNombre: function () {
            var view = this.getView();
            var acta = view.up('actaEstudianteMain').down('actaEstudianteGridConvocatoria').getSelectionModel().getSelection()[0];
            var grupo = view.down('comboGrupo').getValue();
            var asignatura = view.down('comboAsignatura').getValue();
            window.open(this.getUrlInforme('listado-notas-nombre', acta.get('actaId'), grupo, asignatura));
        },

        listadoSinNombre: function () {
            var view = this.getView();
            var acta = view.up('actaEstudianteMain').down('actaEstudianteGridConvocatoria').getSelectionModel().getSelection()[0];
            var grupo = view.down('comboGrupo').getValue();
            var asignatura = view.down('comboAsignatura').getValue();
            window.open(this.getUrlInforme('listado-sin-nombre', acta.get('actaId'), grupo, asignatura));
        },

        listadoFirmas: function () {
            var view = this.getView();
            var acta = view.up('actaEstudianteMain').down('actaEstudianteGridConvocatoria').getSelectionModel().getSelection()[0];
            var grupo = view.down('comboGrupo').getValue();
            var asignatura = view.down('comboAsignatura').getValue();
            window.open(this.getUrlInforme('listado-firmas', acta.get('actaId'), grupo, asignatura));
        },

        getUrlInforme: function (informe, actaId, grupo, asignatura) {
            grupo = grupo ? grupo : '';
            asignatura = asignatura ? asignatura : '';
            return '/act/rest/publicacion/' + informe + '/' + actaId + '?grupo=' + grupo + '&asignatura=' + asignatura
        },

        filtraSobresaliente: function (estudiante) {
            if (estudiante.get('noPresentado')) {
                return false;
            }

            if (estudiante.get('nota')) {
                return estudiante.get('nota') >= 9;
            }

            if (estudiante.get('notaCalculada')) {
                return estudiante.get('notaCalculada') >= 9;
            }
            return false;
        },

        onSobresalienteSelected: function (value) {
            var grid = this.getView();
            var filters = grid.getStore().getFilters();

            if (value) {
                filters.add(this.filtraSobresaliente)
            } else {
                filters.remove(this.filtraSobresaliente)
            }
        },

        timeoutFn: null,
        onEditComplete: function (editor, context, ev) {
            var grid = this.getView();
            var vm = this.getViewModel();
            var store = grid.getStore();

            vm.set('cambiosPendientesNotas', store.getModifiedRecords().length);
            vm.notify();

            if (this.timeoutFn != null) {
                clearTimeout(this.timeoutFn);
            }

            var ref = this;
            this.timeoutFn = setTimeout(function () {
                ref.sincronizarNotas();
            }, 5000);

        }

    });
