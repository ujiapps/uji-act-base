Ext.define('act.view.actaEstudiante.GridPreviewImportacionDesglose',
{
    extend : 'Ext.grid.Panel',
    alias : 'widget.gridPreviewImportacionDesglose',

    title : 'Previsualització de les notes que es poden importar des de l\'arxiu CSV',
    requires : [ 'act.view.actaEstudiante.GridPreviewImportacionController' ],
    controller : 'gridPreviewImportacionController',

    bind :
    {
        store : '{validImportStore}'
    },
    cls : 'x-oculta-fila-resumen',
    anchor : '100%',
    padding : '10 0 10 0',
    flex : 1,
    features : [
    {
        ftype : 'grouping',
        startCollapsed : false,
        groupHeaderTpl : [ '{columnName}: {name}', '<strong>{rows:this.formatRows}</strong>',
        {
            formatRows : function(rows)
            {
                var limpio = true
                rows.map(function(rec)
                {
                    if (rec.get('estado') !== 3)
                    {
                        limpio = false;
                    }
                })
                return limpio ? ' (Sense canvis)' : '';
            }
        } ]
    } ],
    columns : [
    {
        dataIndex : 'nombre',
        text : 'Alumne/a',
        hidden : true
    },
    {
        name : 'espacio',
        flex : 1
    },
    {
        xtype : 'actioncolumn',
        dataIndex : 'estado',
        sortable : false,
        menuDisabled : true,
        width : 30,
        align : 'center',
        items : [
        {
            getTip : function(value, metadata, record)
            {
                return Ext.util.Format.htmlEncode(record.get('mensaje'));
            },
            getClass : function(estado)
            {
                if (estado === 1)
                    return 'x-fa fa-warning yellowiconcolor';
                return estado === 2 ? 'x-fa fa-check greeniconcolor' : 'x-fa fa-file';
            }
        } ]
    },
    {
        dataIndex : 'mensaje',
        text : 'Missatge',
        filter : 'string',
        flex : 1
    },
    {
        dataIndex : 'desgloseGrupoEtiqueta',
        text : 'Grup',
        align : 'center',
        filter : 'string'
    },

    {
        dataIndex : 'desglosePreguntaEtiqueta',
        text : 'Pregunta',
        align : 'center',
        filter : 'string',
        renderer: function(pregunta, cell, row) {
            return row.get('desgloseGrupoEtiqueta') || row.get('desglosePreguntaEtiqueta') ? pregunta : 'Nota final';
        }
    },
    {
        dataIndex : 'nota',
        text : 'Nota',
        align : 'center',
        width : 80,
    },
    {
        dataIndex : 'notaAnterior',
        text : 'Nota ant.',
        align : 'center',
        xtype : 'numbercolumn',
        format : '0.0',
        width : 100,
    } ],
    bbar : [ '->', '->',
    {
        xtype : 'button',
        text : 'Importar',
        handler : 'onImportar'
    },
    {
        xtype : 'panel',
        html : '<a style="text-decoration: none; color: #222;" href="#">Cancel·lar</a>',
        listeners :
        {
            render : function(component)
            {
                component.getEl().on('click', 'onCancel');
            }
        }
    } ]
});