Ext.define('act.view.actaEstudiante.MainController',
    {
        extend: 'Ext.ux.uji.grid.PanelController',
        alias: 'controller.actaEstudianteMainController',

        mockupActa: function () {
            var actaId = 88511;
            var titulo = "PS1048 Treball de Final de Grau";
            var codigo = "PS1048";
            var convocatoriaIndex = 1;

            var view = this.getView();
            var selection = {};
            var panel = view.down('panel[name=panelActaEstudianteGrid]')
            var store = this.getStore('convocatoriasStore');
            store.getProxy().url = '/act/rest/cursos/2017/actaconvocatorias';
            store.load(
                {
                    params:
                        {
                            codigo: codigo
                        },
                    callback: function (data) {
                        view.setActiveItem(1);
                        view.down('actaEstudianteGridConvocatoria').getView().select(convocatoriaIndex);
                        panel.down('displayfield[name=tituloActa]').setValue(titulo);
                        view.down('actaEstudianteGrid').fireEvent('actaConvocatoriaSelected', actaId);
                        view.down('actaEstudianteComboCurso').select(2017);
                        var tab = view.down('actaEstudiantePanelImportacion');
                        setTimeout(function () {
                            view.down('actaEstudianteTabs').setActiveTab(2);
                            view.fireEvent('actaConvocatoriaSelected', actaId);
                        }, 1000);
                    },
                    scope: this
                });
        },

        openInformacionActa: function () {
            var view = this.getView();
            view.setLoading(true);
            var vm = this.getViewModel();
            var store = vm.getStore('responsablesAsignaturasStore');
            var curso = view.down('actaEstudianteComboCurso').getDisplayValue();
            var asiCodigos = view.down('actaEstudianteComboActa').getSelection().get('asiCodigos');
            var convocatoria = view.down('actaEstudianteGridConvocatoria').getSelectionModel().getSelection()[0];

            // var curso = 2017;
            // var asiCodigos = "DR1027";

            asiCodigos = asiCodigos.split(",")[0].split(" ")[0];
            store.load(
                {
                    params:
                        {
                            codigoAsignatura: asiCodigos,
                            cursoId: curso
                        },
                    callback: function (data) {
                        var responsablesStore = Ext.create('Ext.ux.uji.data.Store',
                            {
                                model: 'act.model.ResponsableAsignatura'
                            });

                        responsablesStore.loadData(data);
                        var modal =
                            {
                                xtype: 'panelResponsableAsignatura',
                                viewModel:
                                    {
                                        data:
                                            {
                                                store: store,
                                                asiCodigos: convocatoria.get('asiCodigos'),
                                                actaUnica: convocatoria.get('actaUnica'),
                                                fechaInicioTraspaso: convocatoria.get('fechaInicioTraspaso'),
                                                fechaFinTraspaso: convocatoria.get('fechaFinTraspaso'),
                                                fechaInicioActaUnica: convocatoria.get('fechaInicioActaUnica')
                                            }
                                    }
                            };

                        view.add(modal).show();
                        view.setLoading(false);
                    },
                    failure: function () {
                        view.setLoading(false);
                    },

                    scope: this
                });
        },

        onActaConvocatoriaSelected: function (actaId) {
            var view = this.getView();
            var vm = this.getViewModel();
            var panel = view.down('panel[name=panelActaEstudianteGrid]')
            var curso = view.down('actaEstudianteComboCurso').getDisplayValue();
            var acta = view.down('actaEstudianteComboActa').getDisplayValue();
            var recConvocatoria = view.down('actaEstudianteGridConvocatoria').getSelectionModel().getSelection()[0];

            var comboAgrupacion = view.down('ujicombo[name=grupos]');
            comboAgrupacion.clearValue();

            panel.down('displayfield[name=tituloActa]').setValue(acta + '<br/><small>' + curso + ' - ' + recConvocatoria.get('convocatoriaNombre') + '</small>');
            view.setActiveItem(1);
            if (recConvocatoria.get('fechaTraspaso')) {
                view.down('actaEstudianteTabs').setActiveItem(1);
            } else {
                view.down('actaEstudianteTabs').setActiveItem(0);
            }

            view.down('actaEstudianteGrid').fireEvent('actaConvocatoriaSelected', actaId);
            vm.set('csvUploaded', false);
        },

        onVolverASelector: function () {
            var view = this.getView();
            view.setLoading(true);
            view.setActiveItem(0);
            var gridConvocatoria = view.down('actaEstudianteGridConvocatoria');
            gridConvocatoria.getSelectionModel().deselectAll();
            gridConvocatoria.getStore().reload(
                {
                    callback: function () {
                        view.setLoading(false);
                    }
                });
        },

        getTotalNotas: function (store) {
            var numeroNotas = 0;

            store.each(function (record) {
                if (record.get('nota') !== null || record.get('notaCalculada') !== null || record.get('noPresentado') === true) {
                    numeroNotas += 1;
                }
            });
            return numeroNotas;
        },

        pasarNotasExpediente: function () {
            var view = this.getView();
            var vm = this.getViewModel();
            var cursoId = view.down('actaEstudianteComboCurso').getValue();
            var actaId = vm.get('selectedConvocatoria').get('actaId');
            var storeActaEstudiantes = vm.getStore('actaEstudiantesStore');
            var numeroNotas = this.getTotalNotas(storeActaEstudiantes);
            var dirty = false;
            var msg = '';
            var actaEstudianteIds = [];

            storeActaEstudiantes.each(function (item) {
                if (item.dirty) {
                    dirty = true;
                }
                if (item.get('aExpediente')) {
                    actaEstudianteIds.push(item.get('id'));
                }
            });

            if (dirty) {
                return Ext.Msg.alert('Error', "Encara no s'han sincronitzat totes les notes amb el servidor. Deixa passar uns segons o polsa el botó de 'Desar notes'.");
            }

            if (numeroNotas === 0) {
                msg = 'PRECAUCIÓ: No heu introduït cap nota en l\'acta. '
            }

            if (actaEstudianteIds.length === 0) {
                return Ext.Msg.alert('Error', "Cal sel·leccionar al menys un alumne/a per a passar les notes al expedient.");
            }

            Ext.Msg.confirm('Passar notes al expedient', msg + 'Esteu segur/a de voler passar les notes sel·leccionades al expedient?', function (result) {
                if (result === 'yes') {
                    view.setLoading(true);
                    Ext.Ajax.request(
                        {
                            url: '/act/rest/cursos/' + cursoId + '/actas/' + actaId + '/pasarnotasexpediente',
                            jsonData: {actaEstudianteIds: actaEstudianteIds},
                            method: 'POST',
                            success: function () {
                                view.down('actaEstudianteGrid').getSelectionModel().deselectAll();
                                storeActaEstudiantes.reload();
                                view.setLoading(false);
                            },
                            failure: function (response) {
                                view.setLoading(false);
                            }
                        });
                }
            });
        },

        traspasarActa: function () {
            var view = this.getView();
            var vm = this.getViewModel();
            var cursoId = view.down('actaEstudianteComboCurso').getValue();
            var actaId = vm.get('selectedConvocatoria').get('actaId');
            var storeConvocatorias = vm.getStore('convocatoriasStore');
            var storeActaEstudiantes = vm.getStore('actaEstudiantesStore');

            var numeroNotas = this.getTotalNotas(storeActaEstudiantes);

            var msg = '';

            var dirty = false;
            storeActaEstudiantes.each(function (item) {
                if (item.dirty) {
                    dirty = true;
                }
            });

            if (dirty) {
                return Ext.Msg.alert('Error', "Encara no s'han sincronitzat totes les notes amb el servidor. Deixa passar uns segons o polsa el botó de 'Desar notes'.");
            }

            if (numeroNotas === 0) {
                msg = 'PRECAUCIÓ: No heu introduït cap nota en l\'acta. '
            }

            Ext.Msg.confirm('Traspassar', msg + 'Esteu segur/a de voler traspassar aquesta acta?', function (result) {
                if (result === 'yes') {
                    view.setLoading(true);
                    Ext.Ajax.request(
                        {
                            url: '/act/rest/cursos/' + cursoId + '/actas/' + actaId + '/traspasar',
                            method: 'POST',
                            success: function () {
                                storeConvocatorias.reload(
                                    {
                                        callback: function () {
                                            var resumen = view.down('actaEstudiantePanelResumenTraspasos');
                                            setTimeout(function () {
                                                view.setLoading(false);
                                                view.down('actaEstudianteTabs').setActiveTab(resumen);
                                            }, 2000);
                                        }
                                    });
                            },
                            failure: function (response) {
                                view.setLoading(false);
                            }
                        });
                }
            });
        }
    });
