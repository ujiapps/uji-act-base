Ext.define('act.view.actaEstudiante.ViewModel',
    {
        extend: 'Ext.app.ViewModel',
        alias: 'viewmodel.actaEstudianteViewModel',
        requires: ['act.model.ActaAsignatura', 'act.model.CursoMoodle', 'act.model.ActaEstudiante', 'act.model.ActaPersona', 'act.model.Revision', 'act.model.GrupoDesglose', 'act.model.PreguntaDesglose',
            'act.model.NotaDesglose', 'act.model.ResponsableAsignatura'],
        data:
            {
                countGrupoDesglose: 0,
                cambiosPendientesNotasDesglosadas: 0,
                cambiosPendientesNotas: 0,
                csvUploaded: false,
                todoSeleccionado: false
            },
        formulas:
            {
                selectedConvocatoria:
                    {
                        bind:
                            {
                                bindTo: '{gridConvocatoria.selection}',
                                deep: true
                            },
                        get: function (sel) {
                            return sel;
                        },
                        set: function (convocatoria) {
                            this.set('selectedConvocatoria', convocatoria);
                        }
                    },

                mostrarResumenTraspasoTemporalActaUnica:
                    {
                        bind:
                            {
                                bindTo: '{gridConvocatoria.selection}',
                                deep: true
                            },
                        get: function (acta) {
                            if (!acta)
                                return;
                            return acta.data.actaUnica !== 'false' && !acta.data.fechaTraspaso;
                        }
                    },

                tituloFechaRevision:
                    {
                        bind: '{selectedConvocatoria}',
                        get: function (selectedConvocatoria) {
                            if (selectedConvocatoria && selectedConvocatoria.get && selectedConvocatoria.get('compartida')) {
                                return 'Revisió tots el alumnes';
                            }
                            return 'Revisió';
                        }
                    },

                informacionTraspaso:
                    {
                        bind:
                            {
                                bindTo: '{gridConvocatoria.selection}'
                            },
                        get: function (convocatoria) {
                            if (!convocatoria || !convocatoria.get('fechaTraspaso')) {
                                return 'L\'acta encara no s\'ha traspassat';
                            }

                            return 'Data de traspas: ' + Ext.Date.format(convocatoria.get('fechaTraspaso'), 'd/m/Y');
                        }
                    },
                ocultarGridConvocatorias: function (get) {
                    return !get('comboActa.selection') || !get('comboCurso.selection');
                }
            },
        stores:
            {
                cursosStore: Ext.create('Ext.ux.uji.data.Store',
                    {
                        model: 'act.model.Curso',
                        url: '/act/rest/cursos',
                        autoLoad: false
                    }),
                cursosMoodleStore: Ext.create('Ext.ux.uji.data.Store',
                    {
                        model: 'act.model.CursoMoodle',
                        url: '/act/rest/cursos-moodle',
                        autoLoad: false
                    }),

                actaAsignaturasStore: Ext.create('Ext.ux.uji.data.Store',
                    {
                        model: 'act.model.ActaAsignatura',
                        url: '/act/rest/actaAsignaturas',
                        autoLoad: false
                    }),
                convocatoriasStore: Ext.create('Ext.ux.uji.data.Store',
                    {
                        model: 'act.model.ActaConvocatoria',
                        url: '/act/rest/actaconvocatorias',
                        autoLoad: false,
                        sorters: ['orden']
                    }),
                actaEstudiantesStore: Ext.create('Ext.ux.uji.data.Store',
                    {
                        model: 'act.model.ActaEstudiante',
                        url: '/act/rest/actaestudiantes',
                        sorters: [{
                            sorterFn: function (data1, data2) {
                                return data1.get('nombreCompletoAlumno').localeCompare(data2.get('nombreCompletoAlumno'));
                            }
                        }],
                        autoLoad: false
                    }),
                resumenActaEstudiantesStore: Ext.create('Ext.ux.uji.data.Store',
                    {
                        model: 'act.model.ActaEstudiante',
                        url: '/act/rest/actaestudiantes',
                        sorters: [{
                            sorterFn: function (data1, data2) {
                                return data1.get('nombreCompletoAlumno').localeCompare(data2.get('nombreCompletoAlumno'));
                            }
                        }],
                        filters: [
                            {
                                property: 'motivoExclusion',
                                value: '',
                                exactMatch: true
                            }],
                        autoLoad: false
                    }),
                revisionesStore: Ext.create('Ext.ux.uji.data.Store',
                    {
                        model: 'act.model.Revision',
                        url: '/act/rest/actarevisiones',
                        autoLoad: false
                    }),
                notificacionesStore: Ext.create('Ext.ux.uji.data.Store',
                    {
                        model: 'act.model.Notificacion',
                        url: '/act/rest/actanotificaciones',
                        sorters: ['fecha'],
                        autoLoad: false
                    }),
                actasPersonaStore: Ext.create('Ext.ux.uji.data.Store',
                    {
                        model: 'act.model.ActaPersona',
                        url: '/act/rest/actas',
                        autoLoad: false,
                        queryModel: 'local',
                        sorters: ['descripcion']
                    }),
                gruposDesgloseStore: Ext.create('Ext.ux.uji.data.Store',
                    {
                        model: 'act.model.GrupoDesglose',
                        url: '/act/rest/actaGrupos',
                        autoLoad: false,
                        queryModel: 'local',
                        sorters: ['orden', 'nombre']
                    }),
                preguntasDesgloseStore: Ext.create('Ext.ux.uji.data.Store',
                    {
                        model: 'act.model.PreguntaDesglose',
                        url: '/act/rest/actaPreguntas',
                        autoLoad: false,
                        queryModel: 'local',
                        groupField: 'agrupacion',
                        sorters: ['orden', 'nombre']
                    }),
                responsablesAsignaturasStore: Ext.create('Ext.ux.uji.data.Store',
                    {
                        model: 'act.model.ResponsableAsignatura',
                        url: '/act/rest/responsables-asignatura',
                        autoLoad: false,
                        queryModel: 'local',
                        sorters: [
                            {
                                property: 'puedeTraspasar',
                                direction: 'DESC'
                            }, 'nombreCompleto']
                    }),

                desgloseNotasStore: Ext.create('Ext.ux.uji.data.Store',
                    {
                        model: 'act.model.NotaDesglose',
                        url: '/act/rest/actaNotas',
                        autoLoad: false,
                        queryModel: 'local',
                        sorters: ['orden', 'agrupacion', 'pregunta'],
                        groupField: 'nombreCompletoAlumnoAscii'
                    }),
                calificacionesStore: Ext.create('act.store.Calificaciones'),
                actasParcialesStore: Ext.create('Ext.ux.uji.data.Store',
                    {
                        model: 'act.model.Acta',
                        url: '/act/rest/cursos/2016/actas/parciales?codigo=EQ1025',
                    }),
                importStore: Ext.create('Ext.ux.uji.data.Store',
                    {
                        model: 'act.model.Import',
                        url: '/act/rest/cursos/2016/actas/import',
                        groupField: 'nombre'
                    }),
                invalidImportStore:
                    {
                        model: 'act.model.Import',
                        url: '/act/rest/cursos/2016/actas/import',
                        filters: [
                            {
                                property: 'estado',
                                value: 0
                            }]
                    },
                actaDiligenciasStore: Ext.create('Ext.ux.uji.data.Store',
                    {
                        model: 'act.model.ActaDiligencia',
                        url: '/act/rest/actadiligencias',
                        sorters: [{
                            sorterFn: function (data1, data2) {
                                return data1.get('alumnoNombre').localeCompare(data2.get('alumnoNombre'));
                            }
                        }],
                        autoLoad: false,
                        autoSync: false
                    }),
                validImportStore:
                    {
                        model: 'act.model.Import',
                        url: '/act/rest/cursos/2016/actas/import',
                        grouper:
                            {
                                property: 'nombre',
                                sorterFn: function (r1, r2) {
                                    return r1.get('estadoAlumno') - r2.get('estadoAlumno');
                                }
                            },
                        filters: [
                            {
                                filterFn: function (item) {
                                    return item.get("estado") > 0;
                                }
                            }]
                    },
                onlyChangesImporStore:
                    {
                        model: 'act.model.Import',
                        url: '/act/rest/cursos/2016/actas/import',
                        filters: [
                            {
                                filterFn: function (item) {
                                    return item.get("estado") === 1 || item.get("estado") === 2;
                                }
                            }]
                    }
            }
    });
