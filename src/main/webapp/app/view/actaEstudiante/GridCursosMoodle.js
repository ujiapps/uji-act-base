Ext.define('act.view.actaEstudiante.GridCursosMoodle',
    {
        extend: 'Ext.window.Window',
        alias: 'widget.gridCursosMoodle',

        title: 'Sel·lecció del curs d\'AulaVirtual per importar notes',
        width: 720,
        height: 420,
        modal: true,
        bodyPadding: 10,
        layout:
            {
                type: 'vbox',
                align: 'stretch'
            },

        requires: ['act.view.actaEstudiante.GridCursosMoodleController'],
        controller: 'gridCursosMoodleController',

        items: [
            {
                xtype: 'box',
                padding: 10,
                html: '<span style="font-size: 16px;">Hem trobat els següents cursos en AulaVirtual associats a aquesta acta:</span>'
            },
            {
                xtype: 'grid',
                anchor: '100%',
                padding: '10 0 10 0',
                flex: 1,
                bind:
                    {
                        store: '{store}'
                    },
                columns: [
                    {
                        dataIndex: 'nombre',
                        text: 'Nom',
                        filter: 'string',
                        flex: 1
                    },
                    {
                        dataIndex: 'id',
                        text: 'URL',
                        xtype: 'templatecolumn',
                        tpl: '<a href="http://aulavirtual.uji.es/course/view.php?idnumber={id}" target="_blank" style="color: #222; text-decoration: none; font-weight: bold;"><i class="fa fa-link" /> URL</a>'
                    }],
            }],
        bbar: ['->', '->',
            {
                xtype: 'button',
                text: 'Importar notes',
                handler: 'onImportarMoodle'
            },
            {
                xtype: 'panel',
                html: '<a style="text-decoration: none; color: #222;" href="#">Cancel·lar</a>',
                listeners:
                    {
                        render: function (component) {
                            component.getEl().on('click', 'onCancel');
                        }
                    }
            }]
    })
;