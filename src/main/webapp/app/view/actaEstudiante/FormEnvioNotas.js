Ext.define('act.view.actaEstudiante.FormEnvioNotas',
{
    extend : 'Ext.window.Window',
    alias : 'widget.formEnvioNotas',

    title : 'Enviament de notes',
    width : 640,
    height : 420,
    modal : true,
    bodyPadding : 10,
    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },

    requires : [ 'act.view.actaEstudiante.FormEnvioNotasController' ],
    controller : 'formEnvioNotasController',

    items : [
    {
        xtype : 'panel',
        layout : 'anchor',
        border : 0,
        items : [
        {
            xtype : 'box',
            padding : 10,
            html : '<span style="font-size: 16px;">S\'enviarà un e-mail amb la següent informació (com exemple) a tots els alumnes d\'aquesta revisió:</span>'
        },
        {
            xtype : 'panel',
            height: 240,
            scrollable : true,
            bodyStyle :
            {
                'background-color' : '#FFEFBB'
            },
            items : [
            {
                xtype : 'box',
                padding : '10 10 0 10',
                bind :
                {
                    html : '<strong style="font-size: 14px;">{titulo}</strong>'
                }
            },
            {
                xtype : 'box',
                padding : '4 10 10 10',
                bind :
                {
                    html : '{contenido}'
                }
            } ]
        } ]
    } ],
    bbar : [ '->', '->',
    {
        xtype : 'button',
        text : 'Enviar text per correu als alumnes',
        handler : 'enviaNotaAlumnos',
        bind :
        {
            disabled : '{!selectedConvocatoria.editable}'
        }
    },
    {
        xtype : 'panel',
        html : '<a style="text-decoration: none; color: #222;" href="#">Cancel·lar</a>',
        listeners :
        {
            render : function(component)
            {
                component.getEl().on('click', 'onClose');
            }
        }
    } ]
});