Ext.define('act.view.actaEstudiante.GridDesgloseNotasController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.gridDesgloseNotasController',

    getValoresUnicos : function(store, field)
    {
        var items = [];

        var data = store.getData();
        var snapshot = data.getSource();
        var unfilteredCollection = snapshot || data;
        var allRecords = unfilteredCollection.getRange();

        Ext.Array.map(allRecords, function(rec) {
            Ext.Array.include(items, rec.get(field));
        });

        return Ext.Array.map(items, function(item)
        {
            var data =
                {
                    id : item,
                    nombre : item
                };
            return data;
        });
    },

    onLoad : function()
    {
        var grid = this.getView();
        var vm = this.getViewModel();
        var store = this.getStore('desgloseNotasStore');
        var actaId = vm.get('selectedConvocatoria').get('actaId');
        if (actaId == null)
        {
            return;
        }

        store.clearFilter();
        store.getProxy().url = '/act/rest/actas/' + actaId + '/desglosenotas';
        store.load(
        {
            callback : function(data)
            {
                var asignaturas = this.getValoresUnicos(store, 'asignaturaId');
                var comboAsignatura = grid.down('comboAsignatura');
                comboAsignatura.clearValue();
                comboAsignatura.getStore().loadData(asignaturas);
                comboAsignatura.setVisible(asignaturas.length > 1);

                var grupos = this.getValoresUnicos(store, 'grupoId');
                var comboGrupo = grid.down('comboGrupo')
                comboGrupo.clearValue();
                comboGrupo.getStore().loadData(grupos);
                comboGrupo.setVisible(grupos.length > 1);

                var preguntas = this.getValoresUnicos(store, 'pregunta');
                var comboPreguntas = grid.down('comboPreguntas')
                comboPreguntas.clearValue();
                comboPreguntas.getStore().loadData(preguntas);

                var agrupaciones = this.getValoresUnicos(store, 'agrupacion');
                var comboAgrupacion = grid.down('comboAgrupacion')
                comboAgrupacion.clearValue();
                comboAgrupacion.getStore().loadData(agrupaciones);

                var asignaturaColumn = grid.down('[dataIndex=asignaturaId]');
                asignaturaColumn.setVisible(asignaturas.length > 1);

                grid.getView().refresh();

                var matriculas = 0;
                var alumnos = data.length;
                Ext.each(data, function(rec)
                {
                    if (rec.data.matriculaHonor)
                    {
                        matriculas++;
                    }
                });
            },
            failure : function()
            {
            },
            scope : this
        });
    },

    notificaViewModel : function(store)
    {
        var vm = this.getViewModel();
        vm.set('cambiosPendientesNotasDesglosadas', store.getModifiedRecords() > 0);
        vm.notify();
    },

    onGrupoSelected : function(recordId)
    {
        var grid = this.getView();

        if (recordId)
        {
            grid.getStore().addFilter(new Ext.util.Filter(
            {
                id : 'grupoId',
                property : 'grupoId',
                value : recordId
            }));
        }
        else
        {
            grid.getStore().removeFilter('grupoId');
        }
    },

    onPreguntaSelected : function(recordId)
    {
        var grid = this.getView();

        if (recordId)
        {
            grid.getStore().addFilter(new Ext.util.Filter(
            {
                id : 'pregunta',
                property : 'pregunta',
                value : recordId
            }));
        }
        else
        {
            grid.getStore().removeFilter('pregunta');
        }
    },

    onAgrupacionSelected : function(recordId)
    {
        var grid = this.getView();

        if (recordId)
        {
            grid.getStore().addFilter(new Ext.util.Filter(
            {
                id : 'agrupacion',
                property : 'agrupacion',
                value : recordId
            }));
        }
        else
        {
            grid.getStore().removeFilter('agrupacion');
        }
    },

    onAsignaturaSelected : function(recordId)
    {
        var grid = this.getView();

        if (recordId)
        {
            grid.getStore().addFilter(new Ext.util.Filter(
            {
                id : 'asignaturaId',
                property : 'asignaturaId',
                value : recordId
            }));
        }
        else
        {
            grid.getStore().removeFilter('asignaturaId');
        }
    },

    onBeforeEdit : function(editor, context, eOpts)
    {
        return context.record.get('editable') ? true : false;
    },

    onBeforeCheckChange : function(check, rowIndex, checked, record)
    {
        return record.get('editable') ? true : false;
    },

    getPreviousEditableIndex : function(grid, editor, currentIndex)
    {
        var newIndex = currentIndex;
        while (newIndex-- > 0)
        {
            var record = grid.getStore().getAt(newIndex);
            if (record.get('editable'))
            {
                return newIndex;
            }
        }
        return currentIndex;
    },

    getNextEditableIndex : function(grid, editor, currentIndex)
    {
        var newIndex = currentIndex;
        while (newIndex++ <= grid.getStore().getCount())
        {
            var record = grid.getStore().getAt(newIndex);
            if (record != null && record.get('editable'))
            {
                return newIndex;
            }
        }
        return currentIndex;
    },

    navigateKeys : function(nf, evt)
    {
        switch (evt.getKey())
        {
        case 38:
            var grid = this.getView();
            var editor = grid.getPlugins()[1];

            editor.startEditByPosition(
            {
                row : this.getPreviousEditableIndex(grid, editor, editor.context.rowIdx),
                column : 9
            });
            evt.stopEvent();
            break;
        case 9:
        case 40:
        case 13:
            var grid = this.getView();
            var editor = grid.getPlugins()[1];

            editor.startEditByPosition(
            {
                row : evt.shiftKey ? this.getPreviousEditableIndex(grid, editor, editor.context.rowIdx) : this.getNextEditableIndex(grid, editor, editor.context.rowIdx),
                column : 9
            });
            evt.stopEvent();
            break;
        default:
            break;
        }
        return false;
    },

    checkNotaEditable : function()
    {
        var vm = this.getViewModel();
        var actaConvocatoria = vm.get('selectedConvocatoria');
        return actaConvocatoria.get('editable');
    },

    getFiltros: function() {
        var grid = this.getView();

        var comboAsignatura = grid.down('comboAsignatura');
        var asignatura = comboAsignatura.getValue();

        var comboGrupo = grid.down('comboGrupo');
        var grupo = comboGrupo.getValue();

        var comboPreguntas = grid.down('comboPreguntas')
        var pregunta = comboPreguntas.getValue();

        var comboAgrupacion = grid.down('comboAgrupacion')
        var agrupacion = comboAgrupacion.getValue();

        return {
            asignatura: asignatura,
            grupo: grupo,
            pregunta: pregunta,
            agrupacion: agrupacion
        }
    },

    restoreFiltros: function(filtros) {
        var grid = this.getView();

        var comboAsignatura = grid.down('comboAsignatura');
        var comboGrupo = grid.down('comboGrupo');
        var comboPreguntas = grid.down('comboPreguntas')
        var comboAgrupacion = grid.down('comboAgrupacion')

        comboAsignatura.setValue(filtros.asignatura);
        comboGrupo.setValue(filtros.grupo);
        comboPreguntas.setValue(filtros.pregunta);
        comboAgrupacion.setValue(filtros.agrupacion);
    },

    sincronizarNotas : function()
    {
        var grid = this.getView();
        var vm = this.getViewModel();
        var store = grid.getStore();
        var filtros = this.getFiltros();

        if (store.getModifiedRecords().length > 0)
        {
            grid.setLoading(true);
            grid.getStore().sync(
            {
                callback : function()
                {
                    vm.set('cambiosPendientesNotasDesglosadas', store.getModifiedRecords().length);
                    store.reload({
                        callback: function() {
                            this.restoreFiltros(filtros);
                            grid.setLoading(false);
                        },
                        scope: this
                    });
                },
                scope: this
            });
        }

    },

    exportarNotasDesgloseCsv: function () {
        var vm = this.getViewModel();
        var view = this.getView();
        var actaId = vm.get('selectedConvocatoria').get('actaId');
        var grupo = view.down('comboGrupo').getValue();
        //var asignatura = view.down('comboAsignatura').getValue();

        var params = '';
        if (grupo) {
            var obj = {
                grupo: grupo
//              asignatura : asignatura
            };
            params = '?' + Ext.urlEncode(obj);
        }
        return window.open('/act/rest/actas/' + actaId + '/exportar-csv' + params);
    },

    debounceTimeout : 5000,
    timeoutFn : null,
    onEditComplete : function(editor, context, ev)
    {
        var grid = this.getView();
        var vm = this.getViewModel();
        var store = grid.getStore();

        vm.set('cambiosPendientesNotasDesglosadas', store.getModifiedRecords().length);
        vm.notify();

        if (this.timeoutFn != null)
        {
            clearTimeout(this.timeoutFn);
        }

        var ref = this;
        this.timeoutFn = setTimeout(function()
        {
            ref.sincronizarNotas();
        }, this.debounceTimeout);

    }
});
