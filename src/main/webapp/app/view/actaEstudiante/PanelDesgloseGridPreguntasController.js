Ext.define('act.view.actaEstudiante.PanelDesgloseGridPreguntasController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.panelDesgloseGridPreguntasController',

    onLoad : function()
    {
        var vm = this.getViewModel();
        var actaId = vm.get('selectedConvocatoria').get('actaId');
        var preguntasDesgloseStore = vm.get('preguntasDesgloseStore');
        preguntasDesgloseStore.getProxy().url = '/act/rest/actas/' + actaId + '/desglosepreguntas';
        preguntasDesgloseStore.load(
        {
            callback : function()
            {
                var actasParcialesStore = vm.get('actasParcialesStore');
                var selectedConvocatoria = vm.get('selectedConvocatoria');
                actasParcialesStore.load(
                {
                    url : '/act/rest/cursos/' + selectedConvocatoria.get('cursoAcademicoId') + '/actas/parciales?codigo=' + selectedConvocatoria.get('asiCodigos')
                });
            }
        });

    },

    onAdd : function()
    {
        var grid = this.getView();
        var vm = this.getViewModel();
        var actaId = vm.get('selectedConvocatoria').get('actaId');

        var columnaAgrupacion = grid.down('foreigncolumn');
        columnaAgrupacion.getStore().load(
        {
            url : '/act/rest/actas/' + actaId + '/desglosegrupos',
            callback : function()
            {
                var comboGrupo = grid.down('ujicombo');
                var rec = Ext.create(grid.getStore().model.entityName,
                {
                    id : null,
                    desgloseGrupoId : comboGrupo.getValue()
                });

                grid.getStore().insert(0, rec);
                var editor = grid.plugins[0];
                editor.cancelEdit();
                editor.startEdit(rec, 1);
            }
        });
    },

    onEdit : function()
    {
        var grid = this.getView();
        var vm = this.getViewModel();
        var columnaAgrupacion = grid.down('foreigncolumn');
        var store = vm.getStore(columnaAgrupacion.bindStore.replace('{', '').replace('}', ''));
        store.reload(
        {
            callback : function()
            {

                var selection = grid.getView().getSelectionModel().getSelection()[0];
                if (selection)
                {
                    var editor = grid.plugins[0];
                    editor.cancelEdit();
                    editor.startEdit(selection);
                }
            }
        });
    },

    onToggleVisible : function()
    {
        var grid = this.getView();
        grid.getStore().sync();
    },

    onGrupoSelected : function(recordId)
    {
        var grid = this.getView();
        if (recordId)
        {
            grid.getStore().addFilter(new Ext.util.Filter(
            {
                id : 'grupoId',
                property : 'desgloseGrupoId',
                value : recordId
            }));
        }
        else
        {
            grid.getStore().removeFilter('grupoId');
        }
    },

    onBeforeEdit : function()
    {
        var vm = this.getViewModel();
        var editable = vm.get('selectedConvocatoria').get('editable');
        return editable;
    }
});
