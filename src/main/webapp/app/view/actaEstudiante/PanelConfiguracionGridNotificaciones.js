Ext.define('act.view.actaEstudiante.PanelConfiguracionGridNotificaciones',
{
    extend : 'Ext.ux.uji.grid.Panel',

    alias : 'widget.panelConfiguracionGridNotificaciones',
    title : 'Històric de notificacions ja enviades',
    padding : '0 0 10 0',
    scrollable : true,
    plugins : [],
    flex : 1,
    layout : 'fit',
    emptyText: 'No hi ha notificacions enviades',
    bind :
    {
        store : '{notificacionesStore}'
    },
    tbar : [],
    bbar : [],
    columns : [
    {
        text : 'ID',
        dataIndex : 'id',
        hidden : true
    },
    {
        dataIndex : 'tipo',
        text : 'Tipus',
        align: 'center',
        sortable : true,
        renderer: function (value)
        {
            return value === 'AUTO' ? 'Automàtica' : 'Manual';
        }
    },
    {
        dataIndex: 'asignaturas',
        text: 'Assignatures',
        flex: 1,
        renderer: function (value) {
            if (value === '' || value === null) {
                return "Totes";
            }
            return value;
        }
    },
        {
            dataIndex: 'grupos',
            text: 'Grups',
            flex: 1,
            renderer: function (value) {
                if (value === '' || value === null) {
                    return "Tots";
                }
                return value;
            }
        },
        {
            dataIndex: 'asunto',
            text: 'Assumpte',
            flex: 4
        },
    {
        dataIndex : 'fecha',
        xtype : 'datecolumn',
        width: 160,
        align: 'center',
        text : 'Data',
        format : 'd/m/Y H:i',
        sortable : false

    } ]
});
