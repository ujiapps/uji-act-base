Ext.define('act.view.actaEstudiante.FormEnvioNotasController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.formEnvioNotasController',

    onClose : function()
    {
        var win = Ext.WindowManager.getActive();
        if (win)
        {
            win.destroy();
        }
    },

    enviaNotaAlumnos : function()
    {
        var view = this.getView();
        var vm = this.getViewModel();
        var cursoId = view.up('actaEstudianteMain').down('actaEstudianteComboCurso').getValue();
        var acta = view.up('actaEstudianteMain').down('actaEstudianteGridConvocatoria').getSelectionModel().getSelection()[0];
        var notificacionesStore = vm.get('notificacionesStore');

        Ext.Ajax.request(
        {
            url : '/act/rest/actas/' + vm.get('actaId') + '/actarevisiones/' + vm.get('revisionId') + '/envianotas',
            method : 'POST',
            success : function()
            {
                view.setLoading(false);
                notificacionesStore.reload();
                this.onClose();
            },
            failure : function()
            {
                view.setLoading(false);
            },
            scope: this
        });
    }
});
