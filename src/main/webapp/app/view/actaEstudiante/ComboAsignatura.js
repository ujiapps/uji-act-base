Ext.define('act.view.actaEstudiante.ComboAsignatura',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.comboAsignatura',
    fieldLabel : 'Assignatura',
    showClearIcon : true,
    allowBlank: true,

    width : 220,
    labelWidth : 80,

    store : Ext.create('Ext.data.Store',
    {
        model : 'act.model.Lookup',
        autoLoad : true,
        sorters : [ 'nombre' ],
        proxy :
        {
            type : 'memory'
        }
    }),

    listeners :
    {
        change : function(combo, recordId)
        {
            combo.up('grid').fireEvent('asignaturaSelected', recordId);
        }
    }

});