Ext.define('act.view.actaEstudiante.PanelImportacion',
    {
        extend: 'Ext.form.Panel',

        requires: ['act.view.actaEstudiante.PanelImportacionController', 'act.view.actaEstudiante.FormErrorImportacion', 'act.view.actaEstudiante.FormErrorImportacionController',
            'act.view.actaEstudiante.GridPreviewImportacion', 'act.view.actaEstudiante.GridPreviewImportacionDesglose', 'act.view.actaEstudiante.FormErrorImportacionDesglose'],
        alias: 'widget.actaEstudiantePanelImportacion',
        cls: 'no-border',
        scrollable: true,
        controller: 'actaEstudiantePanelImportacionController',
        margin: 5,
        border: 0,
        layout:
            {
                type: 'vbox',
                align: 'stretch'
            },
        items: [
            {

                xtype: 'panel',
                bind:
                    {
                        hidden: '{csvUploaded}'
                    },
                items: [
                    {
                        border: 0,
                        cls: 'titulo',
                        bind:
                            {
                                html: 'Pots importar la informació de notes de l\'acta carregant un arxiu CSV previament emplenat ',
                            },
                        margin: '10 10 0 10',
                        height: 30
                    },
                    {
                        xtype: 'fieldcontainer',
                        padding: '0 10 10 10',
                        layout: 'hbox',
                        items: [
                            {
                                xtype: 'filefield',
                                name: 'csvfile',
                                fieldLabel: 'Arxiu CSV',
                                labelWidth: 80,
                                width: 400,
                                allowBlank: false,
                                buttonText: 'Sel·leccionar arxiu...'
                            },
                            {
                                xtype: 'button',
                                margin: '0 0 0 10',
                                text: 'Pujar arxiu',
                                handler: 'subirCSV'
                            }]
                    }]
            },
            {

                xtype: 'panel',
                margin: '10 0 0 0',
                bind:
                    {
                        hidden: '{csvUploaded}'
                    },
                items: [


                    {
                        xtype: 'fieldcontainer',
                        padding: '10 10 0 10',
                        layout: 'hbox',
                        items: [
                            {
                                border: 0,
                                cls: 'titulo',
                                bind:
                                    {
                                        html: 'També pots iniciar la importació de notes a l\'acta des-de el curs d\'Aula Virtual ',
                                    },
                                margin: '10 10 0 10',
                                height: 30
                            },
                            {
                                xtype: 'button',
                                margin: '0 0 0 10',
                                text: 'Importar d\'Aula Virtual',
                                handler: 'muestraCursosMoodle'
                            }]
                    }]
            },
            {
                xtype: 'panel',
                margin: 0,
                padding: 0,
                border: 0,
                bind:
                    {
                        hidden: '{selectedConvocatoria.desgloseActivo}'
                    },
                items: [
                    {
                        xtype: 'gridPreviewImportacion',
                        bind:
                            {
                                hidden: '{!csvUploaded}'
                            }
                    }]
            },
            {
                xtype: 'panel',
                margin: 0,
                padding: 0,
                border: 0,
                bind:
                    {
                        hidden: '{!selectedConvocatoria.desgloseActivo}'
                    },
                items: [
                    {
                        xtype: 'gridPreviewImportacionDesglose',
                        bind:
                            {
                                hidden: '{!csvUploaded}'
                            }
                    }]
            }
        ],
        listeners:
            {
                activate: 'onActivate',
                onImportarMoodle: 'onImportarMoodle'
            }
    })
;
