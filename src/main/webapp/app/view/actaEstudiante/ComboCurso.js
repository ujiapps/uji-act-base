Ext.define('act.view.actaEstudiante.ComboCurso',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.actaEstudianteComboCurso',
    fieldLabel : 'Curs',
    showClearIcon : true,
    width : 170,
    labelWidth : 40,
    requires : [ 'act.store.Cursos' ],

    displayField : 'id',

    store :
    {
        type : 'cursos'
    },

    listeners :
    {
        change : function(combo, recordId)
        {
            combo.up('actaEstudiantePanelFiltros').fireEvent('cursoSelected', recordId);
        }
    }

});