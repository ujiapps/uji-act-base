Ext.define('act.view.actaEstudiante.GridDesgloseNotas',
    {
        extend: 'Ext.ux.uji.grid.Panel',

        alias: 'widget.actaEstudianteGridDesgloseNotas',
        reference: 'gridDesgloseNotas',
        flex: 1,
        scrollable: true,
        allowEdit: false,

        bind:
            {
                store: '{desgloseNotasStore}'
            },

        requires: ['Ext.grid.filters.Filters', 'Ext.grid.plugin.CellEditing', 'act.view.actaEstudiante.GridDesgloseNotasController', 'act.view.actaEstudiante.ComboGrupo',
            'act.view.actaEstudiante.ComboAsignatura', 'act.view.actaEstudiante.FormComentario', 'act.view.actaEstudiante.CheckSobresaliente', 'act.view.actaEstudiante.ComboPreguntas',
            'act.view.actaEstudiante.ComboAgrupacion'],

        controller: 'gridDesgloseNotasController',

        viewConfig:
            {
                getRowClass: function (record, index, params, store) {
                    return record.get('editable') ? '' : 'disabled-row';
                }
            },
        selModel: 'cellmodel',

        plugins: ['gridfilters', Ext.create('Ext.grid.plugin.CellEditing',
            {
                clicksToEdit: 1,
                listeners:
                    {
                        beforeedit: 'checkNotaEditable'
                    }
            })],

        tbar: ['->',
            {
                xtype: 'comboAgrupacion',
                margin: '0 10 10 0'
            },
            {
                xtype: 'comboPreguntas',
                margin: '0 10 10 0'
            },
            {
                xtype: 'comboAsignatura',
                margin: '0 10 10 0',
                hidden: true
            },
            {
                xtype: 'comboGrupo',
                margin: '0 10 10 0',
                hidden: true
            }],

        bbar: [
            {
                xtype: 'button',
                iconCls: 'fa fa-save',
                text: 'Desar notes',
                bind:
                    {
                        disabled: '{cambiosPendientesNotasDesglosadas === 0}',
                    },
                handler: 'sincronizarNotas'
            },
            {
                xtype: 'button',
                text: 'Exportar CSV',
                iconCls: 'fa fa-file-excel-o',
                handler: 'exportarNotasDesgloseCsv'
            }],
        columns: [
            {
                text: 'ID',
                dataIndex: 'id',
                hidden: true
            },
            {
                dataIndex: 'index',
                width: 45,
                sortable: false,
                menuDisabled: true,
                align: 'right',
                renderer: function (value, metaData, record, rowIndex) {
                    return rowIndex + 1;
                }
            },
            {
                xtype: 'actioncolumn',
                dataIndex: 'motivoExclusion',
                menuDisabled: true,
                width: 30,
                align: 'center',
                items: [
                    {
                        getTip: function (value, metadata, record) {
                            var msg;
                            if (!record.get('expedienteAbierto')) {
                                msg = Ext.util.Format.htmlEncode("El expedient de l'alumne ja està tancat") + '<br/>';
                            }
                            msg += Ext.util.Format.htmlEncode(record.get('motivoExclusion'));
                            return msg;
                        },
                        getClass: function (motivoExclusion, metadata, record) {
                            if (!record.get('expedienteAbierto')) {
                                return 'x-fa fa-lock rediconcolor';
                            } else if (motivoExclusion) {
                                return 'x-fa fa-exclamation-circle rediconcolor';
                            }
                            return 'x-fa fa-check greeniconcolor';
                        }
                    }]
            },
            {
                dataIndex: 'actaAsignaturaId',
                menuDisabled: true,
                text: 'Acta Asignatura',
                hidden: true
            },
            {
                dataIndex: 'nombreCompletoAlumnoAscii',
                menuDisabled: true,
                text: 'Alumne',
                filter: 'string',
                minWidth: 150,
                flex: 1,
                renderer: function (value, metadata, record) {
                    var gridConvocatoria = this.up('actaEstudianteMain').down('actaEstudianteGridConvocatoria');
                    var selectedConvocatoria = gridConvocatoria.getSelectionModel().getSelection()[0];
                    metadata.tdAttr = 'data-qtip="' + record.get('motivoExclusion') + '"';
                    var personaId = record.get('alumnoId');
                    var asignatura = record.get('asignaturaId');
                    var curso = selectedConvocatoria.get('cursoAcademicoId');
                    var url = 'https://e-ujier.uji.es/pls/www/gri_www.euji00921?p_persona_id=' + personaId + '&p_asignatura=' + asignatura + '&p_curso=' + curso;
                    return '<a target="_blank" class="infoEstudiante" href="' + url + '">' + record.get('alumno') + '</a>';
                },
                hidden: true
            },
            {
                dataIndex: 'asignaturaId',
                menuDisabled: true,
                text: 'Assignatura',
                align: 'center',
                filter: 'string'
            },
            {
                dataIndex: 'grupoId',
                menuDisabled: true,
                text: 'Grup',
                align: 'center',
                filter: 'list'
            },
            {
                dataIndex: 'agrupacion',
                menuDisabled: true,
                text: 'Agrupació',
                align: 'center',
                filter: 'list'
            },
            {
                dataIndex: 'pregunta',
                menuDisabled: true,
                text: 'Pregunta',
                align: 'center',
                filter: 'list',
                flex: 1,
                summaryType: function (records) {
                    var formula, ts;

                    for (var i = 0; i < records.length; i++) {
                        var record = records[i];
                        if (!ts || record.get('ts') > ts) {
                            ts = record.get('ts');
                            formula = record.get('notaFormula');
                        }
                    }
                    return formula;
                },
                summaryRenderer: function (value) {
                    return 'Fòrmula: ' + value;
                }
            },
            {
                dataIndex: 'nota',
                menuDisabled: true,
                text: 'Nota',
                align: 'center',
                xtype: 'numbercolumn',
                editor:
                    {
                        field:
                            {
                                xtype: 'numberfield',
                                forceDecimalPrecision: true,
                                hideTrigger: true,
                                minValue: 0,
                                maxValue: 10,
                                keyNavEnabled: false,
                                mouseWheelEnabled: false,
                                decimalPrecision: 1,
                                decimalSeparator: '.',
                                allowBlank: true,
                                listeners:
                                    {
                                        specialkey: 'navigateKeys'
                                    }
                            }
                    },
                renderer: function (value, meta) {
                    meta.tdCls = 'editor-visible';
                    return value;
                },
                summaryType: function (records) {
                    var nota, ts;

                    for (var i = 0; i < records.length; i++) {
                        var record = records[i];
                        if (!ts || record.get('ts') > ts) {
                            ts = record.get('ts');
                            nota = record.get('notaFinal');
                        }
                    }
                    return nota;
                },
                summaryRenderer: function (value) {
                    if (value) {
                        return 'Nota final: ' + value;
                    }

                    return '';
                }
            },
            {
                xtype: 'actioncolumn',
                text: 'Info',
                dataIndex: 'comentarioTraspaso',
                menuDisabled: true,
                width: 75,
                align: 'center',
                items: [{
                    getTip: function (comentarioTraspaso, meta, rec) {
                        return rec.get("personaNota") && Ext.util.Format.htmlEncode("Nota desada per " + rec.get("personaNota") + " (" + rec.get("fechaNota") + ")");
                    },
                    getClass: function (comentarioTraspaso, cell, rec) {
                        if (!rec.get('personaNota')) return null;
                        if (!comentarioTraspaso) return 'x-fa fa-file grayiconcolor'
                        return (comentarioTraspaso === 'Correcte') ? 'x-fa fa-file greeniconcolor' : 'x-fa fa-warning rediconcolor';
                    }
                }]
            }],

        features: [
            {
                ftype: 'groupingsummary',
                showSummaryRow: true
            }],

        saveData: function () {
            this.getStore().sync();
        },

        listeners:
            {
                agrupacionSelected: 'onAgrupacionSelected',
                preguntaSelected: 'onPreguntaSelected',
                grupoSelected: 'onGrupoSelected',
                asignaturaSelected: 'onAsignaturaSelected',
                activate: 'onLoad',
                beforerowselect: 'onBeforeRowSelect',
                beforeedit: 'onBeforeEdit'
            }
    });