Ext.define('act.view.actaEstudiante.Grid',
    {
        extend: 'Ext.ux.uji.grid.Panel',

        alias: 'widget.actaEstudianteGrid',
        reference: 'gridActaEstudiante',
        flex: 1,
        scrollable: true,
        allowEdit: false,

        bind:
            {
                store: '{actaEstudiantesStore}'
            },

        requires: ['Ext.grid.filters.Filters', 'Ext.grid.plugin.CellEditing', 'act.view.actaEstudiante.GridController', 'act.view.actaEstudiante.ComboGrupo', 'act.view.actaEstudiante.ComboAsignatura',
            'act.view.actaEstudiante.FormComentario', 'act.view.actaEstudiante.CheckSobresaliente'],

        controller: 'actaEstudianteGridController',

        viewConfig:
            {
                getRowClass: function (record, index, params, store) {
                    return !record.get('editable') || record.get('desgloseActivo') ? '' : 'disabled-row';
                }
            },
        plugins: ['gridfilters', Ext.create('Ext.grid.plugin.CellEditing',
            {
                clicksToEdit: 1,
                listeners:
                    {
                        beforeedit: 'checkNotaEditable'
                    }
            })],

        selModel: 'cellmodel',

        tbar: [
            {
                xtype: 'panel',
                name: 'infoActa',
                flex: 1,
                layout: 'hbox',
                border: 0,
                items: [
                    {
                        xtype: 'displayfield',
                        name: 'resumenAlumnos',
                        renderer: function (num) {
                            var msg = '';
                            if (num > 0) {
                                msg = 'Alumnes en acta: ' + num + '.&nbsp;';
                            }
                            return msg;
                        },
                        cls: 'label-resaltado'
                    },
                    {
                        xtype: 'displayfield',
                        name: 'resumenAlumnosExcluidos',
                        renderer: function (num) {
                            var msg = '';
                            if (num > 0) {
                                msg = 'Alumnes exclosos: ' + num + '.&nbsp;';
                            }
                            return msg;
                        },
                        cls: 'label-resaltado'
                    },

                    {
                        xtype: 'displayfield',
                        name: 'resumenMatriculas',
                        renderer: function (num) {
                            var msg = '';
                            if (num) {
                                msg = 'Matrícules: ' + num + '.&nbsp;';
                            }
                            return msg;
                        },
                        bind:
                            {
                                value: '{numeroMatriculasActa}'
                            },
                        cls: 'label-resaltado'
                    },
                    {
                        xtype: 'displayfield',
                        border: 0,
                        flex: 1,
                        margin: 0,
                        padding: 0,
                        cls: 'infoacta',
                        renderer: function (convocatoria) {
                            if (!convocatoria || !convocatoria.get || !convocatoria.get('fechaTraspaso')) {
                                return '';
                            }
                            return 'Acta traspassada el ' + Ext.Date.format(convocatoria.get('fechaTraspaso'), 'd/m/Y');
                        },
                        bind:
                            {
                                value: '{selectedConvocatoria}'
                            }
                    }]
            },
            {
                xtype: 'checkSobresaliente',
                margin: '0 10 10 0'
            },
            {
                xtype: 'comboAsignatura',
                margin: '0 10 10 0'
            },
            {
                xtype: 'comboGrupo',
                margin: '0 10 10 0'
            }],

        bbar: [
            {
                xtype: 'button',
                iconCls: 'fa fa-save',
                text: 'Desar notes',
                bind:
                    {
                        disabled: '{cambiosPendientesNotas === 0}',
                    },
                handler: 'sincronizarNotas'
            },
            {
                xtype: 'button',
                text: 'Exportar CSV',
                iconCls: 'fa fa-file-excel-o',
                handler: 'exportarNotasCsv'
            }, '->',
            {
                xtype: 'button',
                text: 'Llistat amb noms',
                iconCls: 'fa fa fa-file-pdf-o',
                handler: 'listadoConNombre'
            },
            {
                xtype: 'button',
                text: 'Llistat sense noms',
                iconCls: 'fa fa-file-pdf-o',
                handler: 'listadoSinNombre'
            },
            {
                xtype: 'button',
                text: 'Llistat amb noms per signar',
                iconCls: 'fa fa-file-pdf-o',
                handler: 'listadoFirmas'
            }, '->',
            {
                xtype: 'button',
                text: 'Passar notes a l\'expedient',
                iconCls: 'fa fa-pencil-square-o',
                handler: function (grid) {
                    grid.up('actaEstudianteMain').fireEvent('pasarNotasExpediente');
                },
                bind:
                    {
                        hidden: '{!selectedConvocatoria.puedeGuardarNotas}',
                        disabled: '{!selectedConvocatoria.puedePasarNotasAExpediente}'
                    }
            },
            {
                xtype: 'button',
                iconCls: 'fa fa-check',
                width: 30,
                handler: 'onSeleccionarAlumnos',
                bind: {
                    hidden: '{!selectedConvocatoria.puedeGuardarNotas}'
                }
            }],

        columns: [
            {
                text: 'ID',
                dataIndex: 'id',
                hidden: true
            },
            {
                dataIndex: 'aExpediente',
                xtype: 'checkcolumn',
                text: '',
                width: 45,
                renderer: function (value, cell, rec) {
                    var vm = this.getView().up('actaEstudianteMain').getViewModel();
                    var selectedConvocatoria = vm.get('selectedConvocatoria');
                    return rec.get('editable') && selectedConvocatoria.get('puedeGuardarNotas') && selectedConvocatoria.get('puedePasarNotasAExpediente') ? new Ext.grid.column.Check().renderer(value) : '';
                }
            },
            {
                dataIndex: 'index',
                width: 45,
                sortable: false,
                menuDisabled: true,
                align: 'right',
                renderer: function (value, metaData, record, rowIndex) {
                    return rowIndex + 1;
                }
            },
            {
                xtype: 'actioncolumn',
                dataIndex: 'motivoExclusion',
                menuDisabled: true,
                width: 30,
                align: 'center',
                items: [
                    {
                        getTip: function (value, metadata, record) {
                            var msg = '';
                            if (!record.get('expedienteAbierto')) {
                                msg = Ext.util.Format.htmlEncode("El expedient de l'alumne ja està tancat") + '<br/>';
                            }
                            if (record.get('motivoExclusion')) {
                                msg += Ext.util.Format.htmlEncode(record.get('motivoExclusion'));
                            }
                            return msg;
                        },
                        getClass: function (motivoExclusion, metadata, record) {
                            if (!record.get('expedienteAbierto')) {
                                return 'x-fa fa-lock rediconcolor';
                            } else if (motivoExclusion) {
                                return 'x-fa fa-exclamation-circle rediconcolor';
                            }
                            return 'x-fa fa-check greeniconcolor';
                        }
                    }]
            },
            {
                dataIndex: 'actaAsignaturaId',
                text: 'Acta Asignatura',
                hidden: true
            },
            {
                dataIndex: 'nombreCompletoAlumnoAscii',
                text: 'Alumne',
                menuDisabled: true,
                filter: 'string',
                minWidth: 150,
                flex: 1,
                sorter: {
                    sorterFn: function(data1, data2) {
                        return data1.get('nombreCompletoAlumno').localeCompare(data2.get('nombreCompletoAlumno'));
                    }
                },

                renderer: function (value, metadata, record) {
                    var gridConvocatoria = this.up('actaEstudianteMain').down('actaEstudianteGridConvocatoria');
                    var selectedConvocatoria = gridConvocatoria.getSelectionModel().getSelection()[0];
                    metadata.tdAttr = 'data-qtip="' + record.get('motivoExclusion') + '"';
                    var personaId = record.get('personaId');
                    var nombreCompleto = record.get('nombreCompletoAlumno');
                    var asignatura = record.get('asignaturaId');
                    var curso = selectedConvocatoria.get('cursoAcademicoId');
                    var url = 'https://e-ujier.uji.es/pls/www/gri_www.euji00921?p_persona_id=' + personaId + '&p_asignatura=' + asignatura + '&p_curso=' + curso;
                    return '<a target="_blank" class="infoEstudiante" href="' + url + '">' + nombreCompleto + '</a>';
                }
            },
            {
                dataIndex: 'identificacionAlumno',
                text: 'Identificació',
                align: 'center',
                menuDisabled: true,
                filter: 'string'
            },
            {
                dataIndex: 'asignaturaId',
                text: 'Assignatura',
                menuDisabled: true,
                align: 'center',
                filter: 'list'
            },
            {
                dataIndex: 'grupoId',
                text: 'Grup',
                menuDisabled: true,
                align: 'center',
                filter: 'list'
            },
            {
                dataIndex: 'notaCalculada',
                text: 'Nota calc.',
                align: 'center',
                xtype: 'numbercolumn',
                format: '0.0',
                menuDisabled: true,
                bind: {
                    visible: '{selectedConvocatoria.desgloseActivo}'
                }
            },
            {
                xtype: 'checkcolumn',
                dataIndex: 'matriculaHonor',
                text: 'MH',
                width: 45,
                menuDisabled: true,
                sortable: false,
                bind: {
                    disabled: '{!selectedConvocatoria.editable}'
                },
                listeners: {
                    beforecheckchange: 'onBeforeCheckChange',
                    checkchange: 'onGuardarDatos'
                }
            },
            {
                dataIndex: 'nota',
                text: 'Nota',
                align: 'center',
                xtype: 'numbercolumn',
                width: 75,
                format: '0.0',
                menuDisabled: true,
                editor: {
                    field: {
                        xtype: 'numberfield',
                        forceDecimalPrecision: true,
                        hideTrigger: true,
                        minValue: 0,
                        maxValue: 10,
                        keyNavEnabled: false,
                        mouseWheelEnabled: false,
                        decimalPrecision: 2,
                        decimalSeparator: '.',
                        allowBlank: true,
                        listeners:
                            {
                                specialkey: 'navigateKeys'
                            }
                    }
                },
                renderer: function (value, meta, record) {
                    meta.tdCls = 'editor-visible';
                    return value === null ? null : Math.trunc(value * 10) / 10;
                }
            },
            {
                dataIndex: 'calificacionId',
                text: 'Qualificació',
                align: 'center',
                filter: 'list',
                menuDisabled: true,
                renderer: function (value, meta, record) {
                    if ((record.get('nota') === null && record.get('notaCalculada') === null) || record.get('noPresentado') === true) {
                        return "No presentat";
                    }
                    var store = Ext.getStore('act.store.Calificaciones');
                    var data = store.getById(value);
                    return data ? data.get('nombre') : '';
                }
            },
            {
                xtype: 'checkcolumn',
                dataIndex: 'noPresentado',
                text: 'NP',
                width: 45,
                menuDisabled: true,
                sortable: false,
                bind: {
                    disabled: '{!selectedConvocatoria.editable}'
                },
                listeners: {
                    beforecheckchange: 'onBeforeCheckChange',
                    checkchange: 'onGuardarDatos'
                }
            },
            {
                xtype: 'actioncolumn',
                text: 'Comentaris',
                dataIndex: 'comentario',
                width: 95,
                menuDisabled: true,
                sortable: false,
                align: 'center',
                items: [
                    {
                        handler: 'onAddComentario',
                        getTip: function (value, metadata) {
                            return value ? Ext.util.Format.htmlEncode(value) : 'Afegir comentari';
                        },
                        getClass: function (comentario) {
                            return comentario ? 'x-fa fa-file-text-o' : 'x-fa fa-plus';
                        }

                    }]
            },
            {
                xtype: 'actioncolumn',
                text: 'Info',
                dataIndex: 'comentarioTraspaso',
                menuDisabled: true,
                width: 80,
                align: 'center',
                items: [{
                    getTip: function (comentarioTraspaso, meta, rec) {
                        return rec.get("personaNota") && Ext.util.Format.htmlEncode("Nota desada per " + rec.get("personaNota") + " (" + rec.get("fechaNota") + ")");
                    },
                    getClass: function (comentarioTraspaso, cell, rec) {
                        if (!rec.get('personaNota')) return null;
                        if (!comentarioTraspaso) return 'x-fa fa-file grayiconcolor'
                        return (comentarioTraspaso === 'Correcte') ? 'x-fa fa-file greeniconcolor' : 'x-fa fa-warning rediconcolor';
                    }
                }]
            }],

        saveData: function () {
            this.getStore().sync();
        },

        listeners: {
            activate: 'onActivate',
            grupoSelected: 'onGrupoSelected',
            sobresalienteSelected: 'onSobresalienteSelected',
            asignaturaSelected: 'onAsignaturaSelected',
            botonActaClick: 'onBotonActaClick',
            beforerowselect: 'onBeforeRowSelect',
            beforeedit: 'onBeforeEdit',
            actaConvocatoriaSelected: 'onActivate'
        }
    });