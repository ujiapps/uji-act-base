Ext.define('act.view.actaEstudiante.GridConvocatoria',
{
    extend : 'Ext.grid.Panel',

    alias : 'widget.actaEstudianteGridConvocatoria',
    requires : [ 'act.view.actaEstudiante.GridConvocatoriaController' ],
    controller : 'actaEstudianteGridConvocatoriaController',
    reference : 'gridConvocatoria',

    bind :
    {
        store : '{convocatoriasStore}'
    },
    cls : 'no-border',
    padding : '0 0 10 0',
    emptyText : 'Carregant dades...',
    columns : [
    {
        text : 'ID',
        dataIndex : 'id',
        hidden : true
    },
    {
        dataIndex : 'convocatoriaNombre',
        text : 'Convocatòria',
        minWidth : 150,
        flex : 1
    },
    {
        dataIndex : 'descripcion',
        text : 'Nom',
        minWidth : 150,
        flex : 1
    },

    {
        dataIndex : 'extraordinaria',
        text : 'Extraordinària',
        width : 120,
        align : 'center',
        renderer : function(v)
        {
            return v ? 'Sí' : 'No';
        }
    },
    {
        dataIndex : 'semestreId',
        text : 'Semestre',
        width : 80,
        align : 'center'
    },
    {
        xtype : 'datecolumn',
        format : 'd/m/Y',
        dataIndex : 'fechaAlta',
        text : 'Data creació',
        width : 150,
        align : 'center'
    },
    {
        xtype : 'datecolumn',
        format : 'd/m/Y',
        dataIndex : 'fechaTraspaso',
        text : 'Data traspàs',
        width : 150,
        align : 'center'
    },
    {
        xtype : 'datecolumn',
        format : 'd/m/Y',
        dataIndex : 'fechaInicioTraspaso',
        text : 'Data Inici traspàs',
        width : 150,
        align : 'center'
    },
    {
        xtype : 'datecolumn',
        format : 'd/m/Y',
        dataIndex : 'fechaFinTraspaso',
        text : 'Data Fi traspàs',
        width : 150,
        align : 'center'
    },
    {
        xtype : 'actioncolumn',
        align : 'center',
        width : 50,
        stopSelection: false,
        items : [
        {
            iconCls : 'x-fa fa-arrow-right',
            tooltip : 'Anar a l\'acta'
        } ]
    },
    {
        xtype : 'actioncolumn',
        name: 'borrarActa',
        dataIndex : 'convocatoriaId',
        align : 'center',
        width : 50,
        items : [
        {
            iconCls : 'x-fa fa-remove',
            tooltip : 'Esborrar',
            handler: 'onDeleteConvocatoria',
            getClass : function(convocatoriaId)
            {
                return convocatoriaId === 99 && 'x-fa fa-remove';
            }
        } ]
    } ],
    bbar : [ '->',
    {
        xtype : 'button',
        iconCls : 'x-fa fa-plus',
        tooltip : 'Afegir',
        text : 'Crear un nou examen parcial',
        handler : 'onAddConvocatoria',
    } ],
    listeners :
    {
        actaSelected : 'onActaSelected',
        addConvocatoria : 'onAddConvocatoria',
        cellclick : 'onActaConvocatoriaSelected'
    }
});