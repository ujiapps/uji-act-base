Ext.define('act.view.actaEstudiante.PanelFiltros',
{
    extend : 'Ext.panel.Panel',

    alias : 'widget.actaEstudiantePanelFiltros',
    border : 0,
    padding : 10,
    requires : [ 'act.view.actaEstudiante.PanelFiltrosController', 'act.view.actaEstudiante.ComboCurso', 'act.view.actaEstudiante.ComboActa', 'act.view.actaEstudiante.GridConvocatoria',
            'act.view.actaEstudiante.ComboActaAsignatura' ],
    controller : 'panelFiltrosController',

    layout: 'anchor',
    items : [
    {
        xtype : 'actaEstudianteComboCurso',
        reference : 'comboCurso'
    },
    {
        xtype : 'actaEstudianteComboActa',
        reference : 'comboActa',
        bind :
        {
            disabled : '{!comboCurso.selection}'
        }
    },
    {
        xtype : 'actaEstudianteGridConvocatoria',
        reference : 'gridConvocatoria',
        bind :
        {
            hidden : '{ocultarGridConvocatorias}'
        }
    } ],

    listeners :
    {
        // render: 'onLoad',
        cursoSelected : 'onCursoSelected',
        actaSelected : 'onActaSelected',
        actaAsignaturaSelected : 'onActaAsignaturaSelected',
        botonGridClick : 'onBotonGridClick'
    }
});
