Ext.define('act.view.actaEstudiante.ComboGrupo',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.comboGrupo',
    fieldLabel : 'Grup',
    showClearIcon : true,
    allowBlank: true,
    width : 180,
    labelWidth : 40,

    store : Ext.create('Ext.data.Store',
    {
        model : 'act.model.Lookup',
        autoLoad : true,
        sorters : [ 'nombre' ],
        proxy :
        {
            type : 'memory'
        }
    }),

    listeners :
    {
        change : function(combo, recordId)
        {
            combo.up('grid').fireEvent('grupoSelected', recordId);
        }
    }

});