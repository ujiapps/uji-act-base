Ext.define('act.view.actaEstudiante.PanelResponsableAsignatura',
{
    extend : 'Ext.window.Window',
    alias : 'widget.panelResponsableAsignatura',

    controller : 'panelResponsableAsignaturaController',
    width : 680,
    height : 400,
    modal : true,
    bodyPadding : 10,
    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },
    bind :
    {
        title : 'Informació de l\'acta {asiCodigos}',
    },
    items : [
    {
        xtype : 'fieldcontainer',
        layout : 'hbox',
        height: 20,
        items : [
        {
            xtype : 'displayfield',
            name : 'fechaInicioTraspasoField',
            flex: 1,
            renderer : function(fechaInicioTraspaso)
            {
                return 'Data inici traspàs: ' + Ext.Date.format(new Date(fechaInicioTraspaso), 'd-m-Y');
            },
            bind :
            {
                value : '{fechaInicioTraspaso}'
            },
            cls : 'label-info-acta'
        },
        {
            xtype : 'displayfield',
            name : 'fechaFinTraspasoField',
            flex: 1,
            renderer : function(fechaFinTraspaso)
            {
                return 'Data fi traspàs: ' + Ext.Date.format(new Date(fechaFinTraspaso), 'd-m-Y');
            },
            bind :
            {
                value : '{fechaFinTraspaso}'
            },
            cls : 'label-info-acta'
        } ]
    },
    {
        xtype : 'fieldcontainer',
        layout : 'hbox',
        height: 20,
        items : [
        {

            xtype : 'displayfield',
            name : 'actaUnicaField',
            flex: 1,
            renderer : function(actaUnica)
            {
                var result = actaUnica === 'true' ? 'Sí' : 'No';
                return 'Acta única: ' + result;
            },
            bind :
            {
                value : '{actaUnica}'
            },
            cls : 'label-info-acta'
        },
        {
            xtype : 'displayfield',
            name : 'fechaInicioActaUnicaField',
            flex: 1,
            renderer : function(fechaInicioActaUnica)
            {
                return 'Data inici pas de notes a l\'expedient: ' + Ext.Date.format(new Date(fechaInicioActaUnica), 'd-m-Y');
            },
            bind :
            {
                value : '{fechaInicioTraspaso}',
                hidden : '{actaUnica === "false"}'
            },
            cls : 'label-info-acta'
        } ]
    },
    {
        xtype : 'grid',
        title : 'Informació del professorat',
        allowBlank : true,
        viewConfig :
        {
            markDirty : false
        },
        bind :
        {
            store : '{store}'
        },
        scrollable : true,
        flex : 1,
        columns : [
        {
            dataIndex : 'nombreCompleto',
            text : 'Nom',
            filter : 'string',
            flex : 1
        },
        {
            dataIndex : 'puedeTraspasar',
            text : 'Pot traspasar',
            align : 'center',
            width : 120
        } ]
    } ],
    bbar : [ '->', '->',
    {
        xtype : 'button',
        text : 'Acceptar',
        handler : 'onClose'
    } ]
});