Ext.define('act.view.actaEstudiante.PanelConfiguracionGridRevisiones', {
    extend: 'Ext.ux.uji.grid.Panel',

    alias: 'widget.panelConfiguracionGridRevisiones',
    requires: ['act.view.actaEstudiante.PanelConfiguracionGridRevisionesController'],
    border: 0,
    padding: 10,
    scrollable: true,
    plugins: [],
    controller: 'panelConfiguracionGridRevisionesController',
    flex: 1,
    layout: 'fit',
    emptyText: 'Sense revisions',
    bind:
        {
            store: '{revisionesStore}'
        },
    tbar: [
        {
            xtype: 'button',
            ref: 'add',
            iconCls: 'fa fa-plus',
            text: 'Afegir',
            handler: 'onAdd',
            bind:
                {
                    disabled: '{!selectedConvocatoria.editable}'
                }
        },
        {
            xtype: 'button',
            ref: 'edit',
            iconCls: 'fa fa-edit',
            text: 'Editar',
            handler: 'onEdit',
            bind:
                {
                    disabled: '{!selectedConvocatoria.editable}'
                }
        },
        {
            xtype: 'button',
            ref: 'remove',
            iconCls: 'fa fa-remove',
            text: 'Borrar',
            handler: 'onDelete',
            bind:
                {
                    disabled: '{!selectedConvocatoria.editable}'
                }
        }],
    bbar: [],
    columns: [
        {
            text: 'ID',
            dataIndex: 'id',
            hidden: true
        },
        {
            dataIndex: 'nombre',
            text: 'Nom',
            flex: 1,
            sortable: false
        },
        {
            dataIndex: 'asignaturas',
            text: 'Assignatures',
            flex: 1,
            sortable: false,
            renderer: function (value, meta, record) {
                if (record.get('conjunta') === true && value == '') {
                    return "Totes";
                }
                return value;
            }
        },
        {
            dataIndex: 'grupos',
            text: 'Grups',
            flex: 1,
            sortable: false,
            renderer: function (value, meta, record) {
                if (record.get('conjunta') === true && value == '') {
                    return "Tots";
                }
                return value;
            }
        },
        {
            dataIndex: 'lugar',
            text: 'Lloc',
            flex: 1,
            sortable: false
        },
        {
            dataIndex: 'fecha',
            xtype: 'datecolumn',
            text: 'Data',
            format: 'd/m/Y',
            sortable: false

        },
        {
            dataIndex: 'hora',
            text: 'Hora Inici',
            sortable: false
        },
        {
            dataIndex: 'horaFin',
            text: 'Hora fi',
            sortable: false
        },
        {
            dataIndex: 'muestraNotasInicio',
            xtype: 'datecolumn',
            text: 'Inici publicació',
            format: 'd/m/Y',
            sortable: false
        },
        {
            dataIndex: 'muestraNotasFin',
            xtype: 'datecolumn',
            text: 'Fi publicació',
            format: 'd/m/Y',
            sortable: false
        },
        {
            xtype: 'actioncolumn',
            text: 'Enviar notes',
            dataIndex: 'actaId',
            align: 'center',
            width: 120,
            stopSelection: false,
            itemConfig: {
                viewModel: true
            },
            items: [{
                tooltip: 'Enviar notes i data de revisió',
                handler: 'muestraPreviewEnvioNotaAlumnos',
                getClass: function (val, meta, rec) {
                    return rec.get('enviados') > 0 ? 'x-fa fa-check greeniconcolor' : 'x-fa fa-envelope-o rediconcolor';
                }
            }]
        },
        {
            dataIndex: 'enviados',
            text: 'Enviaments',
            sortable: false,
            align: 'center'
        }
    ],
    listeners: {
        celldblclick: 'onEdit',
        onSave: 'onSave'
    }
});
