Ext.define('act.view.actaEstudiante.GridConvocatoriaController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.actaEstudianteGridConvocatoriaController',

    onAddConvocatoria : function()
    {
        var view = this.getView().up('panel')
        var modal =
        {
            xtype : 'formNuevoParcial'
        };

        view.add(modal).show();
    },

    onDeleteConvocatoria : function(grid, rowIndex, colIndex, item, ev, record)
    {
        var grid = this.getView();

        if (record.get('convocatoriaId') !== 99) return;

        Ext.Msg.confirm('Esborrar', 'Esteu segur/a de voler esborrar la convocatòria d\'aquesta acta?', function(result)
        {
            if (result === 'yes')
            {
                Ext.Ajax.request(
                {
                    url : '/act/rest/cursos/' + record.get('cursoAcademicoId') + '/actas/' + record.get('actaId'),
                    method : 'DELETE',
                    success : function(response)
                    {
                        grid.getStore().reload();
                    },
                    scope : this
                });
            }
        });
    },

    sincronizaEstudiantes : function(actaId)
    {
        var vm = this.getViewModel();
        var storeConvocatorias = vm.getStore('convocatoriasStore');

        var grid = this.getView();
        var panel = grid.up('actaEstudiantePanelFiltros');
        Ext.Ajax.setTimeout(180000);

        Ext.Ajax.request(
        {
            url : '/act/rest/actas/' + actaId + '/actaestudiantes/sincroniza',
            method : 'PUT',
            success : function(response)
            {
                panel.setLoading(false);
                storeConvocatorias.reload(
                {
                    callback : function()
                    {
                        var mainPanel = panel.up('actaEstudianteMain')
                        mainPanel.fireEvent('actaConvocatoriaSelected', actaId);
                    }
                });
            },
            failure : function()
            {
                panel.setLoading(false);
            },
            scope : this
        });
    },

    insertaActaYSincronizaEstudiantes : function(record)
    {
        var grid = this.getView();
        var panel = grid.up('actaEstudiantePanelFiltros');
        var vm = this.getViewModel();
        var storeConvocatorias = vm.getStore('convocatoriasStore');

        var data =
        {
            cursoId : record.get('cursoAcademicoId'),
            actaId : record.get('actaId'),
            convocatoriaId : record.get('convocatoriaId'),
            descripcion : record.get('descripcion'),
            codigo : record.get('asiCodigos')
        };

        Ext.Ajax.request(
        {
            url : '/act/rest/cursos/' + data.cursoId + '/actas',
            method : 'POST',
            jsonData : data,
            success : function(response)
            {
                var responseObject = Ext.JSON.decode(response.responseText);
                storeConvocatorias.reload(
                {
                    callback : function()
                    {
                        this.sincronizaEstudiantes(responseObject.data.id);
                    },
                    scope: this
                })
            },
            failure : function()
            {
                panel.setLoading(false);
            },
            scope : this
        });
    },

    onActaConvocatoriaSelected : function(table, td, cellindex, record)
    {
        var grid = this.getView();
        var column = grid.getColumns()[cellindex];
        if (column.name === "borrarActa") return;
        var grid = this.getView();
        var panel = grid.up('actaEstudiantePanelFiltros');
        panel.setLoading(true);

        record.get('actaId') ? this.sincronizaEstudiantes(record.get('actaId')) : this.insertaActaYSincronizaEstudiantes(record);
    },

    onActaSelected : function(cursoId, codigo)
    {
        var grid = this.getView();
        var vm = this.getViewModel();
        var panel = grid.up('actaEstudiantePanelFiltros');

        panel.setLoading(true);
        var store = this.getStore('convocatoriasStore');
        store.getProxy().url = '/act/rest/cursos/' + cursoId + '/actaconvocatorias';

        store.load(
        {
            params :
            {
                codigo : codigo
            },
            callback : function()
            {
                panel.setLoading(false);
            },
            scope : this
        });
    }
});
