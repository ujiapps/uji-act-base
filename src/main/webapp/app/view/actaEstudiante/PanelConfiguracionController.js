Ext.define('act.view.actaEstudiante.PanelConfiguracionController',
    {
        extend: 'Ext.ux.uji.grid.PanelController',
        alias: 'controller.panelConfiguracionController',

        onActivateConfiguracion: function () {
            var view = this.getView();
            view.setLoading(true);
            var vm = this.getViewModel();
            var actaId = vm.get('selectedConvocatoria').get('actaId');
            var storeConvocatorias = vm.getStore('convocatoriasStore');
            var storeRevisiones = vm.getStore('revisionesStore');
            var storeNotificaciones = vm.getStore('notificacionesStore');

            storeConvocatorias.reload(
                {
                    callback: function () {
                        var convocatoria = storeConvocatorias.findRecord('actaId', actaId);
                        var revisionConjunta = convocatoria.get('revisionConjunta');

                        var store = vm.getStore('actaAsignaturasStore');
                        store.loadData([], false);
                        view.setLoading(false);
                    },
                    scope: this
                });

            storeRevisiones.getProxy().url = '/act/rest/actas/' + actaId + '/actarevisiones';
            storeRevisiones.reload();

            storeNotificaciones.getProxy().url = '/act/rest/actas/' + actaId + '/notificaciones';
            storeNotificaciones.reload();
        },

        guardaFechasNotas: function () {
            var view = this.getView();
            var cursoId = view.up('actaEstudianteMain').down('actaEstudianteComboCurso').getValue();
            var form = view.down('form[name=publicacion]');
            var data = form.getValues();

            var muestraNotasInicio = "";
            var muestraNotasFin = "";

            if (data.muestraNotasInicio && data.muestraNotasInicio && !data.muestraNotasHora) {
                return Ext.Msg.alert('Error', "Es necessari introduïr l\'hora de publicació");
            }

            if (data.muestraNotasInicio) {
                muestraNotasInicio = Ext.Date.parseDate(data.muestraNotasInicio + ' ' + data.muestraNotasHora, 'd/m/Y G:i')
            }

            if (data.muestraNotasFin) {
                muestraNotasFin = Ext.Date.parseDate(data.muestraNotasFin + ' ' + data.muestraNotasHora, 'd/m/Y G:i')
            }

            var acta =
                {
                    actaId: data.actaId,
                    muestraNotasInicio: muestraNotasInicio,
                    muestraNotasFin: muestraNotasFin
                };

            if (!form.isValid()) {
                Ext.Msg.alert('Error', 'Les dades introduïdes no són correctes');
                valid = false;
            }

            view.setLoading(true);
            var url = '/act/rest/cursos/' + cursoId + '/actas/' + acta.actaId + '/fechasNotas';
            Ext.Ajax.request(
                {
                    url: url,
                    method: 'PUT',
                    jsonData: acta,
                    success: function () {
                        view.setLoading(false);
                    },
                    failure: function () {
                        view.setLoading(false);
                    }
                });

        }
    });
