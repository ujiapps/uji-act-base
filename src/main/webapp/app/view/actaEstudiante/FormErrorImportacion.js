Ext.define('act.view.actaEstudiante.FormErrorImportacion',
{
    extend : 'Ext.window.Window',
    alias : 'widget.formErrorImportacion',

    title : 'Problemes trobats en el procés d\'importació. Aquestes dades no s\'hi importaran',
    width : 800,
    minHeight: 320,
    height: 480,
    scrollable: true,
    modal : true,
    bodyPadding : 10,
    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },

    requires : [ 'act.view.actaEstudiante.FormErrorImportacionController' ],
    controller : 'formErrorImportacionController',

    items : [
    {
        xtype : 'grid',
        allowBlank : true,
        viewConfig :
        {
            markDirty : false
        },
        bind :
        {
            store : '{store}'
        },
        anchor : '100%',
        padding : 10,
        flex : 1,
        columns : [
        {
            xtype : 'actioncolumn',
            dataIndex : 'estado',
            menuDisabled : true,
            width : 30,
            align : 'center',
            items : [
            {
                getTip : function(value, metadata, record)
                {
                    return record.get('mensaje') ? Ext.util.Format.htmlEncode(record.get('mensaje')) : '';
                },
                getClass : function(estado)
                {
                    return estado === 0 ? 'x-fa fa-exclamation-circle rediconcolor' : 'x-fa fa-check greeniconcolor';
                }

            } ]
        },
        {
            dataIndex : 'nombre',
            text : 'Alumne',
            filter : 'string',
            flex : 1
        },
        {
            dataIndex: 'personaId',
            text: 'Persona Id',
            align: 'center',
            filter : 'string'
        },
        {
            dataIndex: 'dni',
            text: 'Identificació',
            align: 'center',
            filter : 'string'
        },
        {
            dataIndex: 'notaTxt',
            text : 'Nota',
            align : 'center',
            format : '0.0',
            width : 80,
        },
        {
            dataIndex : 'mensaje',
            text : 'Raó',
            flex: 1
        } ]
    } ],
    bbar : [ '->', '->',
    {
        xtype : 'button',
        text : 'Acceptar',
        handler : 'onClose'
    } ]
});