Ext.define('act.view.actaEstudiante.Main',
{
    extend : 'Ext.panel.Panel',

    alias : 'widget.actaEstudianteMain',
    title : 'Gestió d\'Actes',

    requires : [ 'act.view.actaEstudiante.MainController', 'act.view.actaEstudiante.ViewModel', 'act.view.actaEstudiante.Tabs', 'act.view.actaEstudiante.PanelFiltros',
            'act.view.actaEstudiante.FormNuevoParcial', 'act.view.actaEstudiante.PanelResponsableAsignatura', 'act.view.actaEstudiante.PanelResponsableAsignaturaController',
            'act.view.actaEstudiante.PanelImportacion', 'act.view.actaEstudiante.GridCursosMoodle' ],
    controller : 'actaEstudianteMainController',

    cls : 'no-border',
    margin : 5,
    viewModel :
    {
        type : 'actaEstudianteViewModel'
    },
    layout :
    {
        type : 'card',
        align : 'stretch'
    },

    items : [
    {
        xtype : 'actaEstudiantePanelFiltros'
    },
    {
        xtype : 'panel',
        cls : 'no-border',
        border : 0,
        layout :
        {
            type : 'vbox',
            align : 'stretch',
            pack : 'start',
        },
        name : 'panelActaEstudianteGrid',
        tbar : [
        {
            xtype : 'button',
            iconCls : 'fa fa-arrow-left',
            text : 'Tornar',
            listeners :
            {
                click : function(element)
                {
                    this.up('actaEstudianteMain').down('actaEstudianteGrid').fireEvent('botonActaClick');
                }
            }
        },
        {
            xtype : 'displayfield',
            name : 'tituloActa',
            cls : 'tituloActa',
            flex : 1
        },
        {
            xtype : 'button',
            cls : 'infoActa',
            tooltip : 'Mostra informació del responsables de l\'acta',
            icon : '/act/img/info.png',
            handler : 'openInformacionActa'
        },
        {
            xtype : 'button',
            text : 'Traspassar i signar digitalment',
            iconCls : 'fa fa-sign-in',
            handler : 'traspasarActa',
            bind :
                {
                    disabled : '{!selectedConvocatoria.traspasable}'
                }
        } ],
        items : [
        {
            xtype : 'actaEstudianteTabs'
        } ]
    } ],
    listeners :
    {
        //render: 'mockupActa',
        pasarNotasExpediente : 'pasarNotasExpediente',
        actaConvocatoriaSelected : 'onActaConvocatoriaSelected',
        volverASelector : 'onVolverASelector'
    }
});
