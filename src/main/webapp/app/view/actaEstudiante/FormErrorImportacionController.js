Ext.define('act.view.actaEstudiante.FormErrorImportacionController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.formErrorImportacionController',

    onClose : function()
    {
        var win = Ext.WindowManager.getActive();
        if (win)
        {
            win.destroy();
        }
    }
});
