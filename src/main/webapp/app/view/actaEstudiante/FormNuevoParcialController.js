Ext.define('act.view.actaEstudiante.FormNuevoParcialController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.formNuevoParcialController',

    onClose : function()
    {
        var win = Ext.WindowManager.getActive();
        if (win)
        {
            win.destroy();
        }
    },

    onSave : function()
    {
        var view = this.getView();
        var vm = this.getViewModel();

        var combo = Ext.ComponentQuery.query('actaEstudianteComboActa')[0];
        var actaPersonaId = combo.getValue();

        var store = this.getStore('actasPersonaStore');
        var record = store.getById(actaPersonaId);
        var form = view.down('form');
        if (form.isValid())
        {
            var data = {
                descripcion: form.getValues().descripcion,
                cursoId: record.get('cursoId'),
                codigo: record.get('asiCodigos'),
                convocatoriaId: 99
            };

            Ext.Ajax.request(
            {
                url : '/act/rest/cursos/' + data.cursoId + '/actas',
                method : 'POST',
                jsonData : data,
                success : function(response)
                {
                    var grid = Ext.ComponentQuery.query('actaEstudianteGridConvocatoria')[0];
                    grid.getStore().reload();
                    this.onClose();
                },
                scope : this
            });
        }
    }
});
