Ext.define('act.view.actaEstudiante.GridResumenTraspasos',
{
    extend : 'Ext.grid.Panel',

    alias : 'widget.actaEstudianteGridResumenTraspasos',
    scrollable : true,
    bind :
    {
        store : '{resumenActaEstudiantesStore}',
        title : '{resumenTraspaso}'
    },
    bbar : [ '->',
    {
        xtype : 'button',
        text : 'Descarregar document del acta',
        iconCls : 'fa fa-download',
        handler : 'descargarDocumento'
    }, '->' ],

    columns : [
    {
        text : 'ID',
        dataIndex : 'id',
        hidden : true
    },
    {
        dataIndex : 'index',
        width : 45,
        sortable : false,
        align : 'right',
        renderer : function(value, metaData, record, rowIndex)
        {
            return rowIndex + 1;
        }
    },
    {
        dataIndex : 'nombreCompletoAlumno',
        text : 'Alumne',
        filter : 'string',
        minWidth : 150,
        flex : 1,
        renderer : function(value, metadata, record)
        {
            metadata.tdAttr = 'data-qtip="' + record.get('motivoExclusion') + '"';
            return value
        }
    },
    {
        dataIndex : 'identificacionAlumno',
        text : 'Dni',
        align : 'center',
        filter : 'string'
    },
    {
        dataIndex : 'nota',
        text : 'Nota',
        align : 'center'
    },
    {
        dataIndex : 'calificacionId',
        text : 'Qualificació',
        align : 'center',
        renderer : function(value)
        {
            var store = Ext.getStore('act.store.Calificaciones');
            var data = store.getById(value);
            return data ? data.get('nombre') : '';
        }
    },
    {
        xtype : 'datecolumn',
        format : 'd/m/Y',
        dataIndex : 'fechaTraspaso',
        dateFormat : 'd/m/Y',
        text : 'Data',
        align : 'center'
    },
    {
        dataIndex : 'comentarioTraspaso',
        text : 'Observacions',
        flex : 1
    } ]
});