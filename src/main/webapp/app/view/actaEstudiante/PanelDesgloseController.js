Ext.define('act.view.actaEstudiante.PanelDesgloseController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.panelDesgloseController',

    onLoad : function()
    {
        var view = this.getView();
        var vm = this.getViewModel();
        var actaId = vm.get('selectedConvocatoria').get('actaId');
        var desgloseActivo = vm.get('selectedConvocatoria').get('desgloseActivo');

        var gruposDesgloseStore = vm.get('gruposDesgloseStore');
        gruposDesgloseStore.on(
        {
            datachanged : this.notificaViewModel,
            scope : this
        });
        gruposDesgloseStore.getProxy().url = '/act/rest/actas/' + actaId + '/desglosegrupos';

        view.down('tabpanel').setActiveTab(0);

        if (desgloseActivo)
        {
            gruposDesgloseStore.reload();
        }
    },

    notificaViewModel : function(store)
    {
        var vm = this.getViewModel();
        vm.set('countGrupoDesglose', store.getCount());
        vm.notify();
    },

    toggleDesgloseAction : function(desgloseActivo)
    {
        var panel = this.getView();
        var vm = this.getViewModel();
        var storeActaEstudiantes = vm.get('actaEstudiantesStore');
        var actaId = vm.get('selectedConvocatoria').get('actaId');
        var gruposDesgloseStore = vm.get('gruposDesgloseStore');
        var storeConvocatorias = vm.get('convocatoriasStore');

        panel.setLoading(true);

        var data =
        {
            desgloseActivo : desgloseActivo
        };

        Ext.Ajax.request(
        {
            url : '/act/rest/actas/' + actaId + '/desglose',
            method : 'PUT',
            jsonData : data,
            scope : this,
            success : function()
            {
                if (gruposDesgloseStore)
                {
                    gruposDesgloseStore.reload();
                }
                storeConvocatorias.reload(
                {
                    callback : function()
                    {
                        storeActaEstudiantes.reload(
                        {
                            callback : function()
                            {
                                panel.setLoading(false);
                            }
                        });
                    }
                });
            },
            failure : function()
            {
                checkbox.setValue(false);
                panel.setLoading(false);
            }
        });
    },

    toggleDesgloseActivo : function(checkbox, desgloseActivo)
    {
        var panel = this.getView();
        var vm = this.getViewModel();
        var selectedConvocatoria = vm.get('selectedConvocatoria');

        if (!selectedConvocatoria || selectedConvocatoria.get('desgloseActivo') === desgloseActivo)
            return;

        var storeActaEstudiantes = vm.get('actaEstudiantesStore');

        var notas = 0;

        if (storeActaEstudiantes)
        {
            notas = storeActaEstudiantes.getData().items.filter(function(item)
            {
                return item.get('nota');
            }).length;
        }

        var ref = this;
        if (desgloseActivo && notas > 0)
        {
            Ext.Msg.confirm('Desglossament de notes', 'Voleu activar el desglossament encara que ja teniu notes introduïdes en el acta?', function(result)
            {
                if (result === 'yes')
                {
                    ref.toggleDesgloseAction(desgloseActivo);
                }
                else
                {
                    checkbox.setValue(!desgloseActivo);
                }
            })
        }
        else
        {
            this.toggleDesgloseAction(desgloseActivo);
        }
    }
});
