Ext.define('act.view.actaEstudiante.Tabs',
{
    extend : 'Ext.tab.Panel',

    alias : 'widget.actaEstudianteTabs',
    requires : [ 'act.view.actaEstudiante.Grid', 'act.view.actaEstudiante.GridDesgloseNotas', 'act.view.actaEstudiante.PanelConfiguracion', 'act.view.actaEstudiante.PanelDesglose',
            'act.view.actaEstudiante.PanelResumenTraspasos', 'act.view.actaEstudiante.PanelImportacion', 'act.view.actaEstudiante.GridActaTraspasada' ],
    border : 0,
    flex : 1,
    layout :
    {
        type : 'vbox',
        align : 'stretch',
    },

    items : [
    {
        title : 'Introducció de notes',
        xtype : 'actaEstudianteGrid',
        tabConfig: {
            hidden: true,
            bind:
                {
                    hidden: '{selectedConvocatoria.fechaTraspaso}'
                }
        }
    },
    {
        xtype : 'actaEstudiantesGridActaTraspasada',
        title : 'Acta traspassada',
        tabConfig: {
            hidden: true,
            bind:
                {
                    hidden: '{!selectedConvocatoria.fechaTraspaso}'
                }
        }
    },
    {
        title : 'Introducció de notes desglossades',
        xtype : 'actaEstudianteGridDesgloseNotas',
        bind :
        {
            disabled : '{!selectedConvocatoria.desgloseActivo}'
        }
    },
    {
        title : 'Configuració',
        xtype : 'panelConfiguracion'
    },
    {
        title : 'Detall desglossament',
        xtype : 'actaEstudiantePanelDesglose'
    },
    {
        title : 'Resum de traspasos',
        xtype : 'actaEstudiantePanelResumenTraspasos'
    },
    {
        title : 'Càrrega de dades',
        xtype : 'actaEstudiantePanelImportacion',
        bind :
        {
            disabled : '{selectedConvocatoria.fechaTraspaso}'
        }
    } ]
});
