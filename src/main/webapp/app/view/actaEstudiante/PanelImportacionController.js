Ext.define('act.view.actaEstudiante.PanelImportacionController',
    {
        extend: 'Ext.ux.uji.grid.PanelController',
        alias: 'controller.actaEstudiantePanelImportacionController',

        onActivate: function () {
            var panel = this.getView();
            var vm = this.getViewModel();
            vm.notify();
        },

        muestraCursosMoodle: function () {
            var view = this.getView();
            var vm = this.getViewModel();
            var convocatoria = vm.get('selectedConvocatoria')
            var actaId = convocatoria.get('actaId');
            var cursosMoodleStore = vm.getStore('cursosMoodleStore');

            view.setLoading(true);

            Ext.Ajax.request(
                {
                    url: '/act/rest/actas/' + actaId + '/cursos-moodle',
                    method: 'GET',
                    success: function (response) {
                        var responseObject = Ext.JSON.decode(response.responseText);
                        var cursos = responseObject.data;
                        cursosMoodleStore.loadData(cursos);

                        view.setLoading(false);
                        if (cursos.length > 0) {
                            var modal =
                                {
                                    xtype: 'gridCursosMoodle',
                                    viewModel:
                                        {
                                            data:
                                                {
                                                    store: cursosMoodleStore
                                                }
                                        }
                                };
                            view.add(modal).show();
                        }
                    },
                    failure: function () {
                        view.setLoading(false);
                    }
                });
        },

        onImportarMoodle: function (cursoId) {
            var view = this.getView();
            var vm = this.getViewModel();
            var invalidImportStore = vm.getStore('invalidImportStore');
            var validImportStore = vm.getStore('validImportStore');
            var onlyChangesImporStore = vm.getStore('onlyChangesImporStore');
            var convocatoria = vm.get('selectedConvocatoria')
            var actaId = convocatoria.get('actaId');

            view.setLoading(true);

            Ext.Ajax.request(
                {
                    url: '/act/rest/actas/' + actaId + '/cursos-moodle/' + cursoId + '/importar',
                    method: 'GET',
                    success: function (response) {
                        view.setLoading(false);

                        var responseObject = Ext.JSON.decode(response.responseText);
                        var data = responseObject.data;

                        vm.set('csvUploaded', true);
                        validImportStore.loadData(data);
                        invalidImportStore.loadData(data);
                        onlyChangesImporStore.loadData(data);

                        if (invalidImportStore.getCount() > 0) {
                            var modal =
                                {
                                    xtype: convocatoria.get('desgloseActivo') ? 'formErrorImportacionDesglose' : 'formErrorImportacion',
                                    viewModel:
                                        {
                                            data:
                                                {
                                                    store: invalidImportStore
                                                }
                                        }
                                };
                            view.add(modal).show();
                        }

                        if (validImportStore.getCount() === 0) {
                            vm.set('csvUploaded', false);
                        }
                    },
                    failure: function () {
                        view.setLoading(false);
                    }
                });
        },

        subirCSV: function () {
            var view = this.getView();
            var form = this.getView().getForm();
            var vm = this.getViewModel();
            var invalidImportStore = vm.getStore('invalidImportStore');
            var validImportStore = vm.getStore('validImportStore');
            var onlyChangesImporStore = vm.getStore('onlyChangesImporStore');
            var convocatoria = vm.get('selectedConvocatoria')
            var actaId = convocatoria.get('actaId');

            if (form.isValid()) {
                form.submit(
                    {
                        url: '/act/rest/actas/' + actaId + '/importar-previo',
                        waitMsg: 'Pujant el arxiu CSV...',
                        success: function (form, action) {
                            var data = action.result.data;
                            vm.set('csvUploaded', true);
                            validImportStore.loadData(data);
                            invalidImportStore.loadData(data);
                            onlyChangesImporStore.loadData(data);

                            if (invalidImportStore.getCount() > 0) {
                                var modal =
                                    {
                                        xtype: convocatoria.get('desgloseActivo') ? 'formErrorImportacionDesglose' : 'formErrorImportacion',
                                        viewModel:
                                            {
                                                data:
                                                    {
                                                        store: invalidImportStore
                                                    }
                                            }
                                    };
                                view.add(modal).show();
                            }

                            if (validImportStore.getCount() === 0) {
                                vm.set('csvUploaded', false);
                            }
                        },
                        failure: function (form, response) {
                            Ext.Msg.alert('Error', response.result.message);
                        }
                    });
            }
        }
    });
