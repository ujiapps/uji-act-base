Ext.define('act.view.actaEstudiante.FormRevisionController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.formRevisionController',
    onClose : function()
    {
        var win = Ext.WindowManager.getActive();
        if (win)
        {
            var vm = this.getViewModel();
            var revisionesStore = vm.get('revisionesStore');
            revisionesStore.rejectChanges();
            win.destroy();
        }
    },
    onSave : function()
    {
        var view = this.getView();
        var vm = this.getViewModel();
        var form = view.down('form');
        var revisionesStore = vm.get('revisionesStore');

        if (form.isValid())
        {
            var data = form.getValues();
            data.asignaturas = data.asignaturas ? data.asignaturas.join(',') : '';
            data.grupos = data.grupos ? data.grupos.join(',') : '';

            var revision;
            if (!data.id)
            {
                revision = Ext.create('act.model.Revision', {});
                revisionesStore.add(revision);
            }
            else
            {
                revision = revisionesStore.getById(data.id);
            }

            revision.set(data);

            var fecha = Ext.Date.parseDate(data.fecha + ' ' + data.hora, 'd/m/Y G:i');
            revision.set('fecha', fecha);

            var fechaInicio = Ext.Date.parseDate(data.fecha + ' ' + data.hora, 'd/m/Y G:i');
            revision.set('fechaInicio', fechaInicio);

            var fechaFin = Ext.Date.parseDate(data.fecha + ' ' + data.horaFin, 'd/m/Y G:i');
            revision.set('fechaFin', fechaFin);

            var muestraNotasInicio = Ext.Date.parseDate(data.muestraNotasInicio, 'd/m/Y');
            revision.set('muestraNotasInicio', muestraNotasInicio);

            var muestraNotasFin = Ext.Date.parseDate(data.muestraNotasFin, 'd/m/Y');
            revision.set('muestraNotasFin', muestraNotasFin);

            if (!revision.dirty)
            {
                this.onClose();
            }

            view.setLoading(true);
            revisionesStore.sync(
            {
                success : function()
                {
                    view.setLoading(false);
                    this.onClose();
                },
                callback : function()
                {
                    view.setLoading(false);
                },
                scope : this
            });
        }
    }
});
