Ext.define('act.view.actaEstudiante.FormNuevoParcial',
{
    extend : 'Ext.window.Window',
    alias : 'widget.formNuevoParcial',

    title : 'Nou examen parcial',
    width : 640,
    manageHeight : true,
    modal : true,
    bodyPadding : 10,
    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },

    requires : [ 'act.view.actaEstudiante.FormNuevoParcialController' ],
    controller : 'formNuevoParcialController',

    items : [
    {
        xtype : 'form',
        layout: 'anchor',
        items : [
        {
            xtype : 'textfield',
            name: 'descripcion',
            allowBlank: false,
            anchor: '100%',
            padding: 10,
            fieldLabel : 'Descripció',
            flex : 1
        } ]
    } ],
    bbar : [ '->', '->',
    {
        xtype : 'button',
        text : 'Desar',
        handler : 'onSave'
    },
    {
        xtype : 'panel',
        html : '<a style="text-decoration: none; color: #222;" href="#">Cancel·lar</a>',
        listeners :
        {
            render : function(component)
            {
                component.getEl().on('click', 'onClose');
            }
        }
    } ]
});