Ext.define('act.view.actaEstudiante.FormRevision',
    {
        extend: 'Ext.window.Window',
        alias: 'widget.formRevision',

        bind: {
            title: '{title}'
        },
        width: 640,
        manageHeight: true,
        modal: true,
        bodyPadding: 10,
        layout:
            {
                type: 'vbox',
                align: 'stretch'
            },

        requires: ['act.view.actaEstudiante.FormRevisionController'],
        controller: 'formRevisionController',

        items: [{
            xtype: 'form',
            layout: 'anchor',
            flex: 1,
            padding: 10,
            border: 0,
            items: [
                {
                    boxLabel: 'Revisió tots els alumnes',
                    name: 'conjunta',
                    bind:
                        {
                            value: '{revision.conjunta}'
                        },
                    xtype: 'checkbox',
                    checked: true,
                    inputValue: '1'
                },
                {
                    xtype: 'hidden',
                    name: 'actaId',
                    bind:
                        {
                            value: '{revision.actaId}'
                        }
                },
                {
                    xtype: 'hidden',
                    name: 'id',
                    bind:
                        {
                            value: '{revision.id}'
                        }
                },
                {
                    xtype: 'tagfield',
                    name: 'asignaturas',
                    fieldLabel: 'Assignatures',
                    width: '100%',
                    allowBlank: false,
                    bind:
                        {
                            disabled: '{revision.conjunta}',
                            store: '{asignaturasStore}',
                            value: '{revision.asignaturas}'
                        },
                    valueField: 'id',
                    displayField: 'nombre'
                },
                {
                    xtype: 'tagfield',
                    name: 'grupos',
                    bind:
                        {
                            disabled: '{revision.conjunta}',
                            store: '{gruposStore}',
                            value: '{revision.grupos}'
                        },
                    fieldLabel: 'Grups',
                    allowBlank: false,
                    width: '100%',
                    valueField: 'id',
                    displayField: 'nombre'
                },
                {
                    fieldLabel: 'Nom',
                    allowBlank: false,
                    xtype: 'textfield',
                    labelWidth: 40,
                    width: '100%',
                    name: 'nombre',
                    emptyText: 'Nom',
                    bind:
                        {
                            value: '{revision.nombre}'
                        }
                },
                {
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    flex: 1,
                    items: [
                        {
                            allowBlank: false,
                            xtype: 'datefield',
                            startDay: 1,
                            name: 'fecha',
                            labelWidth: 40,
                            fieldLabel: 'Data',
                            emptyText: 'Data',
                            format: 'd/m/Y',
                            altFormats: 'd/m/Y H:i:s',
                            flex: 1,
                            padding: '0 10 0 0',
                            bind:
                                {
                                    value: '{revision.fecha}'
                                }
                        },
                        {
                            allowBlank: false,
                            xtype: 'timefield',
                            fieldLabel: 'Inici',
                            labelAlign: 'right',
                            labelWidth: 40,
                            width: 135,
                            minValue: '7:00',
                            maxValue: '23:00',
                            name: 'hora',
                            emptyText: 'Hora',
                            format: 'G:i',
                            altFormats: 'd/m/Y H:i:s',
                            padding: '0 10 0 0',
                            bind:
                                {
                                    value: '{revision.hora}'
                                }
                        },
                        {
                            allowBlank: false,
                            xtype: 'timefield',
                            fieldLabel: 'Fi',
                            labelAlign: 'right',
                            labelWidth: 40,
                            width: 135,
                            minValue: '7:00',
                            maxValue: '23:00',
                            name: 'horaFin',
                            emptyText: 'Hora',
                            format: 'G:i',
                            altFormats: 'd/m/Y H:i:s',
                            padding: '0 10 0 0',
                            bind:
                                {
                                    value: '{revision.horaFin}'
                                }
                        }]
                },
                {
                    fieldLabel: 'Lloc',
                    allowBlank: false,
                    xtype: 'textfield',
                    labelWidth: 40,
                    width: '100%',
                    name: 'lugar',
                    emptyText: 'Lloc',
                    bind:
                        {
                            value: '{revision.lugar}'
                        }
                },
                {
                    xtype: 'textareafield',
                    name: 'observaciones',
                    width: '100%',
                    fieldLabel: 'Observacions',
                    emptyText: 'Observacions',
                    labelAlign: 'top',
                    bind:
                        {
                            value: '{revision.observaciones}'
                        }
                },
                {
                    xtype: 'fieldset',
                    title: 'Publicació de notes en el tauler de notes provisional (Opcional)',
                    padding: 10,
                    layout: 'vbox',
                    flex: 1,
                    items: [{
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        items: [{
                            xtype: 'datefield',
                            startDay: 1,
                            name: 'muestraNotasInicio',
                            labelWidth: 120,
                            emptyText: 'Data inici',
                            format: 'd/m/Y',
                            altFormats: 'd/m/Y H:i:s',
                            flex: 1,
                            padding: '0 10 0 0',
                            bind:
                                {
                                    value: '{revision.muestraNotasInicio}'
                                }
                        }, {
                            xtype: 'datefield',
                            startDay: 1,
                            name: 'muestraNotasFin',
                            labelWidth: 120,
                            emptyText: 'Data fi',
                            format: 'd/m/Y',
                            altFormats: 'd/m/Y H:i:s',
                            flex: 1,
                            padding: '0 10 0 0',
                            bind:
                                {
                                    value: '{revision.muestraNotasFin}'
                                }
                        },
                            {
                                xtype: 'timefield',
                                minValue: '7:00',
                                maxValue: '23:00',
                                fieldLabel: 'hora',
                                labelWidth: 35,
                                name: 'muestraNotasHora',
                                emptyText: 'Hora publicació',
                                format: 'G:i',
                                altFormats: 'd/m/Y H:i:s',
                                bind:
                                    {
                                        value: '{selectedConvocatoria.muestraNotasHora}'
                                    }
                            }
                        ]
                    }]
                }]
        }],
        bbar: ['->', '->',
            {
                xtype: 'button',
                text: 'Desar',
                handler: 'onSave',
                bind: {
                    disabled: '{!selectedConvocatoria.editable}'
                }
            },
            {
                xtype: 'panel',
                html: '<a style="text-decoration: none; color: #222;" href="#">Cancel·lar</a>',
                listeners:
                    {
                        render: function (component) {
                            component.getEl().on('click', 'onClose');
                        }
                    }
            }],
        listeners: {
            close: 'onClose'
        }
    });