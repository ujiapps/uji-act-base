Ext.define('act.view.actaEstudiante.ComboActa',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.actaEstudianteComboActa',
    fieldLabel : 'Acta',
    showClearIcon : true,
    labelWidth : 40,
    typeAhead: true,
    editable: true,
    anchor: '50%',
    matchFieldWidth: false,
    displayField : 'descripcion',
    valueField: 'id',

    bind: {
        store: '{actasPersonaStore}'
    },

    listeners :
    {
        select : function(combo, record)
        {
            combo.up('actaEstudiantePanelFiltros').fireEvent('actaSelected', record.get('cursoId'), record.get('asiCodigos'));
        },
        beforequery: function(record){
            record.query = new RegExp(record.query, 'i');
            record.forceAll = true;
        }
    }

});