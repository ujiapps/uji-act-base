Ext.define('act.view.actaEstudiante.GridCursosMoodleController',
    {
        extend: 'Ext.ux.uji.grid.PanelController',
        alias: 'controller.gridCursosMoodleController',

        onCancel: function () {
            var win = Ext.WindowManager.getActive();
            if (win) {
                win.destroy();
            }
        },

        onImportarMoodle: function () {
            var view = this.getView();
            if (view.down('grid').getSelectionModel().getSelection().length === 0) {
                return;
            }
            var selected = view.down('grid').getSelectionModel().getSelection()[0];
            view.up('actaEstudiantePanelImportacion').fireEvent('onImportarMoodle', selected.get('id'));

            var win = Ext.WindowManager.getActive();
            if (win) {
                win.destroy();
            }
        }
    });
