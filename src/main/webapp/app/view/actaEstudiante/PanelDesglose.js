Ext.define('act.view.actaEstudiante.PanelDesglose',
{
    extend : 'Ext.panel.Panel',

    alias : 'widget.actaEstudiantePanelDesglose',
    requires : [ 'act.view.actaEstudiante.PanelDesgloseController', 'act.view.actaEstudiante.PanelDesgloseGridGrupos', 'act.view.actaEstudiante.PanelDesgloseGridPreguntas' ],
    controller : 'panelDesgloseController',
    title : 'Desglossament de notes',
    cls : 'no-border',
    margin : '5 0 5 0',

    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },

    items : [
    {
        xtype : 'form',
        layout : 'anchor',
        padding : '10 10 0 10',
        height : 50,
        border : 0,
        items : [
        {
            boxLabel : 'Activar desglossament',
            name : 'conjunta',
            bind :
            {
                value : '{selectedConvocatoria.desgloseActivo}',
                disabled : '{!selectedConvocatoria.editable}'
            },
            xtype : 'checkbox',
            listeners :
            {
                change : 'toggleDesgloseActivo'
            }
        } ]
    },
    {
        xtype : 'tabpanel',
        alias : 'panelDesgloseTabPanel',
        border : 0,
        padding : '10 10 0 10',
        scrollable : true,
        flex : 1,
        bind :
        {
            visible : '{selectedConvocatoria.desgloseActivo}'
        },
        items : [
        {
            xtype : 'panelDesgloseGridGrupos'
        },
        {
            xtype : 'panelDesgloseGridPreguntas',
            bind :
            {
                disabled : '{countGrupoDesglose == 0}'
            }
        } ]
    } ],
    listeners :
    {
        activate : 'onLoad'
    }
});
