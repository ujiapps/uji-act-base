Ext.define('act.view.actaEstudiante.PanelResumenTraspasos',
{
    extend : 'Ext.panel.Panel',

    alias : 'widget.actaEstudiantePanelResumenTraspasos',
    requires : [ 'act.view.actaEstudiante.GridResumenTraspasos', 'act.view.actaEstudiante.GridResumenActaUnica', 'act.view.actaEstudiante.PanelResumenTraspasosController' ],
    cls : 'no-border',
    scrollable : true,
    controller : 'panelResumenTraspasosController',
    margin : 5,
    border : 0,
    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },
    items : [
    {
        border : 0,
        cls : 'titulo',
        bind :
        {
            html : '{informacionTraspaso}',
        },
        margin : 10,
        height : 50
    },
    {
        xtype : 'panel',
        layout :
        {
            type : 'fit',
            align : 'center'
        },
        border : 0,
        padding : '0 50 0 50',
        items : [
        {
            xtype : 'gridResumenActaUnica',
            reference : 'gridResumenActaUnica',
            emptyText: 'Encara no n\'hi han notes traspassades',
            bind :
            {
                hidden : '{!mostrarResumenTraspasoTemporalActaUnica}'
            }
        },
        {
            xtype : 'actaEstudianteGridResumenTraspasos',
            height : 600,
            reference : 'gridResumenTraspasos',
            bind :
            {
                hidden : '{!gridConvocatoria.selection.fechaTraspaso}'
            }
        } ]
    } ],
    listeners :
    {
        activate : 'onActivate',
        habilitarCambioNota: 'habilitarCambioNota'
    }
});
