Ext.define('act.view.actaEstudiante.PanelDesgloseGridPreguntas',
{
    extend : 'Ext.ux.uji.grid.Panel',
    reloadAfterInsert: true,

    requires : [ 'act.view.actaEstudiante.PanelDesgloseGridPreguntasController' ],
    controller : 'panelDesgloseGridPreguntasController',
    alias : 'widget.panelDesgloseGridPreguntas',
    emptyText : 'No n\'hi han preguntes definides',
    title : 'Preguntes',
    xtype : 'ujigridpanel',
    bind :
    {
        store : '{preguntasDesgloseStore}',
        disabled : '{!selectedConvocatoria.editable}'
    },

    features : [
    {
        ftype : 'groupingsummary'
    } ],

    columns : [
    {
        text : 'id',
        dataIndex : 'id',
        hidden : true
    },
    {
        dataIndex : 'visible',
        text : 'Visible',
        xtype : 'checkcolumn',
        listeners :
        {
            checkchange : function(cb, rowIndex, checked, rec)
            {
                this.up('grid').fireEvent('togglevisible', rec)
            }
        }
    },
    {
        dataIndex : 'agrupacion',
        text : 'Agrupació',
        hidden : true
    },
    {
        text : 'actaId',
        dataIndex : 'actaId',
        hidden : true
    },
    {
        xtype : 'foreigncolumn',
        header : 'Agrupació',
        dataIndex : 'desgloseGrupoId',
        editable : true,
        allowBlank : false,
        displayField : 'etiqueta',
        model : 'GrupoDesglose',
        bindStore : '{gruposDesgloseStore}'
    },
    {
        dataIndex : 'nombre',
        text : 'Nom',
        flex : 3,
        editor :
        {
            field :
            {
                allowBlank : false
            }
        }
    },
    {
        dataIndex : 'orden',
        text : 'Ordre',
        align : 'center',
        editor :
        {
            xtype : 'numberfield',
            minValue : 0,
            maxValue : 1000,
            defaultValue : 10,
            allowBlank : false,
            decimalSeparator : '.'
        }
    },
    {
        xtype : 'foreigncolumn',
        header : 'Parcial',
        dataIndex : 'parcialActaId',
        editable : true,
        allowBlank : true,
        displayField : 'descripcion',
        model : 'Acta',
        bindStore : '{actasParcialesStore}',
        flex : 1
    },
    {
        dataIndex : 'etiqueta',
        text : 'Etiqueta',
        align : 'center',
        editor :
        {
            field :
            {
                allowBlank : false,
                maxLength: 10,
                maskRe: /[a-zA-Z0-9.]+/
            }
        }
    },
    {
        dataIndex : 'peso',
        text : 'Pes (0-100)',
        align : 'center',
        summaryType : 'sum',
        summaryRenderer : function(value)
        {
            return Ext.String.format('{0} total', value);
        },
        editor :
        {
            xtype : 'numberfield',
            minValue : 0,
            maxValue : 100,
            allowBlank : false,
            decimalSeparator : '.'
        }
    },
    {
        dataIndex : 'notaMinima',
        text : 'Nota Mín.',
        align : 'center',
        editor :
        {
            xtype : 'numberfield',
            minValue : 0,
            maxValue : 10,
            allowBlank : false,
            decimalSeparator : '.'
        }
    },
    {
        dataIndex : 'notaMaxima',
        text : 'Nota Màx.',
        align : 'center',
        editor :
        {
            xtype : 'numberfield',
            minValue : 0,
            maxValue : 10,
            allowBlank : false,
            decimalSeparator : '.'
        }
    } ],
    flex : 1,
    tbar : [
    {
        xtype : 'button',
        ref : 'add',
        iconCls : 'fa fa-plus',
        text : 'Afegir',
        handler : 'onAdd',
        bind :
        {
            disabled : '{!selectedConvocatoria.editable}'
        }
    },
    {
        xtype : 'button',
        ref : 'edit',
        iconCls : 'fa fa-edit',
        text : 'Editar',
        handler : 'onEdit',
        bind :
        {
            disabled : '{!selectedConvocatoria.editable}'
        }
    },
    {
        xtype : 'button',
        ref : 'remove',
        iconCls : 'fa fa-remove',
        text : 'Borrar',
        handler : 'onDelete',
        bind :
        {
            disabled : '{!selectedConvocatoria.editable}'
        }
    }, '->',
    {
        xtype : 'ujicombo',
        name: 'grupos',
        showClearIcon : true,
        allowBlank : true,
        width : 180,
        labelWidth : 40,
        bind :
        {
            store : '{gruposDesgloseStore}'
        },
        listeners :
        {
            change : function(combo, recordId)
            {
                combo.up('grid').fireEvent('grupoSelected', recordId);
            }
        }
    } ],
    listeners :
    {
        activate : 'onLoad',
        togglevisible : 'onToggleVisible',
        grupoSelected : 'onGrupoSelected',
        beforeedit : 'onBeforeEdit'
    }
});