Ext.define('act.view.actaEstudiante.PanelConfiguracion',
    {
        extend: 'Ext.form.Panel',

        alias: 'widget.panelConfiguracion',
        requires: ['act.view.actaEstudiante.PanelConfiguracionController', 'act.view.actaEstudiante.FormEnvioNotas', 'act.view.actaEstudiante.PanelConfiguracionGridRevisiones',
            'act.view.actaEstudiante.PanelConfiguracionGridNotificaciones'],
        border: 0,
        padding: 10,
        controller: 'panelConfiguracionController',
        flex: 1,
        scrollable: true,
        layout:
            {
                type: 'vbox',
                align: 'stretch',
                pack: 'start'
            },

        width: 300,
        items: [
            {
                xtype: 'fieldset',
                title: 'Revisions',
                items: [
                    {
                        xtype: 'panel',
                        name: 'revision',
                        border: 0,
                        items: [
                            {
                                xtype: 'panelConfiguracionGridRevisiones'
                            }]
                    }]
            },
            {
                xtype: 'fieldset',
                title: 'Notificacions',
                items: [
                    {
                        margin: '10 0 0 0',
                        xtype: 'panelConfiguracionGridNotificaciones'
                    }],
            }],
        listeners:
            {
                activate: 'onActivateConfiguracion'
            }
    });
