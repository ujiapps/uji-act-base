Ext.define('act.view.actaEstudiante.PanelDesgloseGridGrupos',
{
    extend : 'Ext.ux.uji.grid.Panel',
    requires : [ 'act.view.actaEstudiante.PanelDesgloseGridGruposController' ],
    reloadAfterInsert: true,
    controller : 'panelDesgloseGridGruposController',
    alias : 'widget.panelDesgloseGridGrupos',
    title : 'Agrupacions',
    layout : 'fit',
    emptyText : 'No n\'hi han agrupacions definides',
    reference : 'gridDesgloseGrupos',
    features : [
    {
        ftype : 'summary'
    } ],
    bind :
    {
        store : '{gruposDesgloseStore}'
    },
    columns : [
    {
        text : 'id',
        dataIndex : 'id',
        hidden : true
    },
    {
        dataIndex : 'visible',
        text : 'Visible',
        xtype : 'checkcolumn',
        listeners :
        {
            checkchange : function(cb, rowIndex, checked, rec)
            {
                this.up('grid').fireEvent('togglevisible', rec)
            }
        }
    },
    {
        dataIndex : 'nombre',
        text : 'Nom',
        flex : 1,
        editor :
        {
            field :
            {
                allowBlank : false
            }
        }
    },
    {
        dataIndex : 'orden',
        text : 'Ordre',
        align : 'center',
        editor :
        {
            xtype : 'numberfield',
            minValue : 0,
            maxValue : 1000,
            defaultValue: 10,
            allowBlank : false
        }
    },
    {
        dataIndex : 'etiqueta',
        align : 'center',
        text : 'Etiqueta',
        editor :
        {
            field :
            {
                allowBlank : false,
                maxLength: 10,
                maskRe: /[a-zA-Z0-9.]+/
            }
        }
    },
    {
        dataIndex : 'peso',
        text : 'Pes (0-100)',
        align : 'center',
        summaryType : 'sum',
        summaryRenderer : function(value)
        {
            return Ext.String.format('{0} total', value);
        },
        editor :
        {
            xtype : 'numberfield',
            minValue : 0,
            maxValue : 100,
            allowBlank : false,
            decimalSeparator : '.'
        }
    },
    {
        dataIndex : 'notaMinima',
        align : 'center',
        text : 'Nota Mín.',
        editor :
        {
            xtype : 'numberfield',
            minValue : 0,
            maxValue : 10,
            allowBlank : false,
            decimalSeparator : '.'
        }
    },
    {
        dataIndex : 'notaMaxima',
        align : 'center',
        text : 'Nota Màx.',
        editor :
        {
            xtype : 'numberfield',
            minValue : 0,
            maxValue : 10,
            allowBlank : false,
            decimalSeparator : '.'
        }
    },
    {
        dataIndex : 'notaMinimaAprobado',
        align : 'center',
        width : 180,
        text : 'Nota Min. Aprovat',
        editor :
        {
            xtype : 'numberfield',
            minValue : 0,
            maxValue : 10,
            allowBlank : false,
            decimalSeparator : '.'
        }
    } ],
    tbar : [
    {
        xtype : 'button',
        ref : 'add',
        iconCls : 'fa fa-plus',
        text : 'Afegir',
        handler : 'onAdd',
        bind :
        {
            disabled : '{!selectedConvocatoria.editable}'
        }
    },
    {
        xtype : 'button',
        ref : 'edit',
        iconCls : 'fa fa-edit',
        text : 'Editar',
        handler : 'onEdit',
        bind :
        {
            disabled : '{!selectedConvocatoria.editable}'
        }
    },
    {
        xtype : 'button',
        ref : 'remove',
        iconCls : 'fa fa-remove',
        text : 'Borrar',
        handler : 'onDelete',
        bind :
        {
            disabled : '{!selectedConvocatoria.editable}'
        }
    }, '->',
    {
        xtype : 'button',
        ref : 'remove',
        iconCls : 'fa fa-mail-forward',
        text : 'Anar a les preguntes d\'aquesta agrupació',
        bind :
        {
            disabled : '{!gridDesgloseGrupos.selection}'
        },
        handler : 'onGrupoSeleccionado'
    } ],
    listeners :
    {
        activate : 'onLoad',
        togglevisible : 'onToggleVisible',
        beforeedit : 'onBeforeEdit'
    }
});