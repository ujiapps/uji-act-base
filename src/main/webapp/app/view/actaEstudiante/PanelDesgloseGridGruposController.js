Ext.define('act.view.actaEstudiante.PanelDesgloseGridGruposController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.panelDesgloseGridGruposController',

    onAdd : function()
    {
        var grid = this.getView();

        var rec = Ext.create(grid.getStore().model.entityName, {id : null});
        grid.getStore().insert(0, rec);
        var editor = grid.plugins[0];
        editor.cancelEdit();
        editor.startEdit(rec, 0);
    },

    onBeforeEdit: function() {
        var vm = this.getViewModel();
        var editable = vm.get('selectedConvocatoria').get('editable');
        return editable;
    },

    onToggleVisible: function() {
        var grid = this.getView();
        grid.getStore().sync();
    },

    onEdit : function()
    {
        var grid = this.getView();

        var selection = grid.getView().getSelectionModel().getSelection()[0];
        if (selection)
        {
            var editor = grid.plugins[0];
            editor.cancelEdit();
            editor.startEdit(selection);
        }
    },

    onGrupoSeleccionado: function() {
        var grid = this.getView();
        var vm = this.getViewModel();
        var actaId = vm.get('selectedConvocatoria').get('actaId');
        var tabpanel = grid.up('tabpanel');
        var record = grid.getSelectedRow();

        tabpanel.setActiveTab(1);
        var combogrupos = tabpanel.down('ujicombo');
        setTimeout(function() {
            combogrupos.getStore().reload({
                callback: function() {
                    combogrupos.select(record);
                }
            })
        }, 200);
    }
});
