Ext.define('act.view.actaEstudiante.GridPreviewImportacionController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.gridPreviewImportacionController',

    onCancel : function()
    {
        var vm = this.getViewModel();

        vm.set('csvUploaded', false);
    },

    onImportar : function()
    {
        var view = this.getView().up('actaEstudianteMain')
        var vm = this.getViewModel();
        var convocatoria = vm.get('selectedConvocatoria')
        var actaId = convocatoria.get('actaId');
        var store = vm.getStore('onlyChangesImporStore');
        var acta = [];

        var tab = convocatoria.get('desgloseActivo') ? view.down('actaEstudianteGridDesgloseNotas') : view.down('actaEstudianteGrid');
        store.each(function(item)
        {
            acta.push(item.data);
        })

        Ext.Ajax.request(
        {
            url : '/act/rest/actas/' + actaId + '/importar',
            method : 'POST',
            jsonData :
            {
                data : acta
            },
            success : function(response)
            {
                vm.set('csvUploaded', false);
                view.down('actaEstudianteTabs').setActiveTab(tab);
                if (store.getCount() > 0)
                {
                    return Ext.Msg.alert('Informació', "S'han importat " + store.getCount() + " notes correctament.");
                }
            },
            scope : this
        });
    }
});
