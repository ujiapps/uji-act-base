Ext.define('act.view.actaEstudiante.GridPreviewImportacion',
{
    extend : 'Ext.grid.Panel',
    alias : 'widget.gridPreviewImportacion',

    title : 'Previsualització de les notes que es poden importar des de l\'arxiu CSV',
    requires : [ 'act.view.actaEstudiante.GridPreviewImportacionController' ],
    controller : 'gridPreviewImportacionController',

    bind :
    {
        store : '{validImportStore}'
    },
    anchor : '100%',
    padding : '10 0 10 0',
    flex : 1,
    columns : [
    {
        xtype : 'actioncolumn',
        dataIndex : 'estado',
        sortable: false,
        menuDisabled : true,
        width : 30,
        align : 'center',
        items : [
        {
            getTip : function(value, metadata, record)
            {
                return Ext.util.Format.htmlEncode(record.get('mensaje'));
            },
            getClass : function(estado)
            {
                if (estado === 1)
                    return 'x-fa fa-warning yellowiconcolor';
                return estado === 2 ? 'x-fa fa-check greeniconcolor' : 'x-fa fa-file';
            }
        } ]
    },
    {
        dataIndex : 'mensaje',
        text : 'Estat',
        flex : 1
    },
    {
        dataIndex : 'nombre',
        text : 'Alumne',
        filter : 'string',
        flex : 1
    },
    {
        dataIndex : 'dni',
        text : 'DNI',
        width : 120,
        align: 'center',
        filter : 'string',
        flex : 1
    },
    {
        dataIndex : 'nota',
        text : 'Nota',
        align : 'center',
        xtype : 'numbercolumn',
        format : '0.0',
        width : 80,
    },
    {
        dataIndex : 'calificacionId',
        text : 'Qualificació',
        align : 'center',
        filter : 'list',
        width : 100,
        renderer : function(value)
        {
            var vm = this.getView().up('actaEstudianteMain').getViewModel();
            var store = vm.getStore('calificacionesStore');

            var record = store.findRecord('id', value, 0, false, false, true);
            return record ? record.get('nombre') : '';

        }
    },
    {
        dataIndex : 'notaAnterior',
        text : 'Nota ant.',
        align : 'center',
        xtype : 'numbercolumn',
        format : '0.0',
        width : 100,
    },
    {
        dataIndex : 'calificacionIdAnterior',
        text : 'Qualificació ant.',
        align : 'center',
        filter : 'list',
        width : 140,
        renderer : function(value)
        {
            var vm = this.getView().up('actaEstudianteMain').getViewModel();
            var store = vm.getStore('calificacionesStore');

            var record = store.findRecord('id', value, 0, false, false, true);
            return record ? record.get('nombre') : '';

        }
    } ],
    bbar : [ '->', '->',
    {
        xtype : 'button',
        text : 'Importar',
        handler : 'onImportar'
    },
    {
        xtype : 'panel',
        html : '<a style="text-decoration: none; color: #222;" href="#">Cancel·lar</a>',
        listeners :
        {
            render : function(component)
            {
                component.getEl().on('click', 'onCancel');
            }
        }
    } ]
});