Ext.define('act.view.actaEstudiante.GridActaTraspasada',
    {
        extend: 'Ext.ux.uji.grid.Panel',

        alias: 'widget.actaEstudiantesGridActaTraspasada',
        allowEdit: false,
        flex: 1,
        scrollable: true,
        bind:
            {
                store: '{actaDiligenciasStore}'
            },

        requires: ['Ext.grid.plugin.CellEditing', 'act.view.actaEstudiante.GridController', 'Ext.ux.grid.SubTable', 'act.view.diligencias.ComboCalificacion'],
        controller: 'actaEstudianteGridController',
        viewConfig:
            {
                getRowClass: function (record, index, params, store) {
                    var clsSignoMas = record.get('tieneDiligencias') ? '' : 'hide-diligencias ';
                    var clsDisabled = record.get('editable') ? '' : 'disabled-row';

                    return clsSignoMas + ' ' + clsDisabled;
                }
            },
        selModel: 'cellmodel',
        plugins: ['gridfilters', Ext.create('Ext.grid.plugin.CellEditing',
            {
                clicksToEdit: 1,
                listeners:
                    {
                        beforeedit: 'checkNotaEditable'
                    }
            }),
            {
                ptype: "subtable",
                header: 'Diligències anteriors',
                headerWidth: 24,
                columns: [
                    {
                        text: 'Nota Anterior',
                        align: 'center',
                        width: 120,
                        dataIndex: 'notaAnterior'
                    },
                    {
                        text: 'Qualificació Anterior',
                        align: 'center',
                        width: 160,
                        dataIndex: 'calificacionAnterior'
                    },
                    {
                        text: 'Nota Diligència',
                        width: 120,
                        dataIndex: 'notaNueva',
                        align: 'center'
                    },
                    {
                        text: 'Qualificació Diligència',
                        align: 'center',
                        width: 160,
                        dataIndex: 'calificacionNueva'
                    },
                    {
                        text: 'Data',
                        align: 'left',
                        width: 120,
                        dataIndex: 'fecha',
                        xtype: 'datecolumn',
                        format: 'd/m/Y',
                        renderer: function (fecha) {
                            return Ext.Date.format(fecha, 'd/m/Y');
                        }
                    },
                    {
                        text: 'Modificat per',
                        align: 'left',
                        dataIndex: 'personaNombre'
                    },
                    {
                        text: 'Comentaris',
                        align: 'left',
                        dataIndex: 'comentarioDiligencia',
                        renderer: function (val, meta, record) {
                            return '<div style="white-space: normal !important;">' + Ext.util.Format.nl2br(val) + '</div>';
                        }
                    }],
                listeners:
                    {
                        dblclick: 'onDblClick'
                    },
                getAssociatedRecords: function (record) {
                    var diligencias = record.get('diligencias');

                    if (diligencias.length == 0) {
                        return {}
                    }

                    if (diligencias.hasOwnProperty('personaId')) {
                        diligencias = [diligencias];
                    }

                    return diligencias.map(function (diligencia) {
                        return Ext.create('act.model.Diligencia', diligencia);
                    })
                }
            }],

        tbar: [
            {
                xtype: 'panel',
                name: 'infoActa',
                flex: 1,
                layout: 'hbox',
                border: 0,
                items: [
                    {
                        xtype: 'displayfield',
                        name: 'resumenAlumnos',
                        renderer: function (num) {
                            var msg = '';
                            if (num > 0) {
                                msg = 'Alumnes en acta: ' + num + '.&nbsp;';
                            }
                            return msg;
                        },
                        cls: 'label-resaltado'
                    },
                    {
                        xtype: 'displayfield',
                        name: 'resumenMatriculas',
                        renderer: function (num) {
                            var msg = '';
                            if (num) {
                                msg = 'Matrícules: ' + num + '.&nbsp;';
                            }
                            return msg;
                        },
                        bind:
                            {
                                value: '{numeroMatriculasActa}'
                            },
                        cls: 'label-resaltado'
                    },
                    {
                        xtype: 'displayfield',
                        border: 0,
                        flex: 1,
                        margin: 0,
                        padding: 0,
                        cls: 'infoacta',
                        renderer: function (convocatoria) {
                            if (!convocatoria || !convocatoria.get || !convocatoria.get('fechaTraspaso')) {
                                return '';
                            }
                            return 'Acta traspassada el ' + Ext.Date.format(convocatoria.get('fechaTraspaso'), 'd/m/Y');
                        },
                        bind:
                            {
                                value: '{selectedConvocatoria}'
                            }
                    }]
            },
            {
                xtype: 'checkSobresaliente',
                margin: '0 10 10 0'
            },
            {
                xtype: 'comboAsignatura',
                margin: '0 10 10 0'
            },
            {
                xtype: 'comboGrupo',
                margin: '0 10 10 0'
            }],

        border: 0,
        columns: [
            {
                text: 'ID',
                dataIndex: 'id',
                hidden: true
            },
            {
                dataIndex: 'index',
                width: 45,
                sortable: false,
                align: 'right',
                renderer: function (value, metaData, record, rowIndex) {
                    return rowIndex + 1;
                }
            },
            {
                xtype: 'actioncolumn',
                dataIndex: 'expedienteAbierto',
                menuDisabled: true,
                width: 30,
                align: 'center',
                items: [
                    {
                        getTip: function (expedienteAbierto) {
                            return (!expedienteAbierto) ? Ext.util.Format.htmlEncode("El expedient de l'alumne ja està tancat") : null;
                        },
                        getClass: function (expedienteAbierto) {
                            return expedienteAbierto ? 'x-fa fa-check greeniconcolor' : 'x-fa fa-lock rediconcolor';
                        }

                    }]
            },
            {
                dataIndex: 'alumnoNombre',
                text: 'Alumne',
                filter: 'string',
                flex: 3,
                sorter: {
                    sorterFn: function(data1, data2) {
                        return data1.get('alumnoNombre').localeCompare(data2.get('alumnoNombre'));
                    }
                }
            },
            {
                dataIndex: 'identificacionOfuscado',
                text: 'Identificació',
                align: 'center',
                filter: 'string',
                flex: 1
            },
            {
                dataIndex: 'asignaturaId',
                text: 'Assignatura',
                menuDisabled: true,
                align: 'center',
                filter: 'list'
            },
            {
                dataIndex: 'grupoId',
                text: 'Grup',
                menuDisabled: true,
                align: 'center',
                filter: 'list'
            },
            {
                dataIndex: 'nota',
                text: 'Nota',
                align: 'center',
                xtype: 'numbercolumn',
                width: 120,
                renderer: function (value, meta) {
                    return value === null ? null : Math.trunc(value * 10) / 10;
                }
            },
            {
                dataIndex: 'calificacionId',
                text: 'Qualificació',
                align: 'center',
                filter: 'list',
                flex: 1,
                renderer: function (value) {
                    var vm = this.getView().up('actaEstudianteMain').getViewModel();
                    var store = vm.getStore('calificacionesStore');

                    var record = store.findRecord('id', value, 0, false, false, true);
                    return record ? record.get('nombre') : '';

                },
            },
            {
                xtype: 'checkcolumn',
                dataIndex: 'noPresentado',
                text: 'NP',
                width: 45,
                disabled: true
            },
            {
                xtype: 'actioncolumn',
                text: 'Info',
                dataIndex: 'comentarioTraspaso',
                menuDisabled: true,
                width: 75,
                align: 'center',
                items: [{
                    getTip: function (comentarioTraspaso, meta, rec) {
                        return rec.get("personaNota") && Ext.util.Format.htmlEncode("Nota desada per " + rec.get("personaNota") + " (" + rec.get("fechaNota") + ")");
                    },
                    getClass: function (comentarioTraspaso, cell, rec) {
                        if (!rec.get('personaNota')) return null;
                        if (!comentarioTraspaso) return 'x-fa fa-file grayiconcolor'
                        return (comentarioTraspaso === 'Correcte') ? 'x-fa fa-file greeniconcolor' : 'x-fa fa-warning rediconcolor';
                    }
                }]
            }
        ],

        saveData: function () {
            this.getStore().sync();
        },

        listeners:
            {
                activate: 'onActivate',
                grupoSelected: 'onGrupoSelected',
                sobresalienteSelected: 'onSobresalienteSelected',
                asignaturaSelected: 'onAsignaturaSelected'
            }
    });