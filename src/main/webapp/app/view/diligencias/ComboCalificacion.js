Ext.define('act.view.diligencias.ComboCalificacion',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.diligenciasComboCalificacion',
    width : 240,
    labelWidth : 100,
    displayField : 'nombre',
    valueField : 'id',
    editable : false,
    bind: {
        store: '{calificacionesStore}'
    }
});