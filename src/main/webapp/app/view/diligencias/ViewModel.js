Ext.define('act.view.diligencias.ViewModel',
    {
        extend: 'Ext.app.ViewModel',
        alias: 'viewmodel.diligenciasViewModel',
        requires: ['act.model.ActaAsignatura', 'act.model.ActaEstudiante', 'act.model.ActaPersona'],
        formulas:
            {
                cursoYconvocatoriaSeleccionados: function (get) {
                    return get('comboConvocatoria.selection') && get('comboCurso.selection');
                },
                cursoYconvocatoriaYActaSeleccionados: function (get) {
                    return get('comboConvocatoria.selection') && get('comboCurso.selection') && get('comboActa.selection');
                },
                selectedActa:
                    {
                        bind:
                            {
                                bindTo: '{comboActa.selection}',
                                deep:
                                    true
                            }
                        ,
                        get: function (sel) {
                            return sel;
                        }
                        ,
                        set: function (acta) {
                            this.set('selectedActa', acta);
                        }
                    }
            },
        stores:
            {
                cursosStore: Ext.create('Ext.ux.uji.data.Store',
                    {
                        model: 'act.model.Curso',
                        url: '/act/rest/cursos'
                    }),
                actaAsignaturasStore:
                    Ext.create('Ext.ux.uji.data.Store',
                        {
                            model: 'act.model.ActaAsignatura',
                            url: '/act/rest/actaAsignaturas',
                            autoLoad: false
                        }),
                convocatoriasOficialesStore:
                    Ext.create('Ext.ux.uji.data.Store',
                        {
                            model: 'act.model.Convocatoria',
                            url: '/act/rest/convocatorias/oficiales',
                            autoLoad: true
                        }),
                actaDiligenciasStore:
                    Ext.create('Ext.ux.uji.data.Store',
                        {
                            model: 'act.model.ActaDiligencia',
                            url: '/act/rest/actadiligencias',
                            sorters: [{
                                sorterFn: function(data1, data2) {
                                    return data1.get('alumnoNombre').localeCompare(data2.get('alumnoNombre'));
                                }
                            }],
                            autoLoad: false,
                            autoSync: false
                        }),
                actasStore:
                    Ext.create('Ext.ux.uji.data.Store',
                        {
                            model: 'act.model.ActaDepartamento',
                            url: '/act/rest/actas/admin',
                            autoLoad: false,
                            sorters: ['codigo']
                        }),
                calificacionesStore:
                    Ext.create('act.store.Calificaciones'),
                actasPersonaStore:
                    Ext.create('Ext.ux.uji.data.Store',
                        {
                            model: 'act.model.ActaPersona',
                            url: '/act/rest/actas',
                            autoLoad: false,
                            queryModel: 'local',
                            sorters: ['descripcion']
                        })
            }
    })
;
