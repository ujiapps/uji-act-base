Ext.define('act.view.diligencias.MainController',
    {
        extend: 'Ext.ux.uji.grid.PanelController',
        alias: 'controller.diligenciasMainController',

        mockupDiligencia: function () {
            var convocatoriaId = 1;
            var cursoId = 2017;
            var actaId = 123551;

            var view = this.getView();
            var panel = view.down('panel[name=diligenciasGrid]')

            var store = this.getStore('actaDiligenciasStore');
            store.getProxy().url = '/act/rest/actas/' + actaId + '/actadiligencias';
            store.load(
                {
                    params:
                        {
                            convocatoriaId: convocatoriaId
                        },
                    callback: function () {
                        view.down('diligenciasComboCurso').select(2017);
                        var convocatoria = view.down('diligenciasComboConvocatoria')
                        convocatoria.select(convocatoriaId);
                        view.down('diligenciasComboActa').select(actaId);
                    },
                    scope: this
                });
        }
    });
