Ext.define('act.view.diligencias.ComboActa',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.diligenciasComboActa',
    reference: 'comboActa',
    fieldLabel : 'Acta',
    showClearIcon : true,
    // labelWidth : 100,
    typeAhead: true,
    minWidth: 350,
    width: 350,
    editable: true,
    allowBlank: true,
    displayField : 'codigo',
    valueField: 'id',
    bind: {
        store: '{actasStore}'
    },

    listeners :
    {
        select : function(combo, record) {
            combo.up('diligenciasPanelFiltros').fireEvent('actaSelected', record.get('actaId'));
        }
    }

});