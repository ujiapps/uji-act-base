Ext.define('act.view.diligencias.Grid',
    {
        extend: 'Ext.ux.uji.grid.Panel',

        alias: 'widget.diligenciasGrid',
        allowEdit: false,
        flex: 1,
        scrollable: true,
        bind:
            {
                store: '{actaDiligenciasStore}'
            },

        requires: ['Ext.grid.plugin.CellEditing', 'act.view.diligencias.GridController', 'Ext.ux.grid.SubTable', 'act.view.diligencias.ComboCalificacion'],
        controller: 'diligenciasGridController',
        viewConfig:
            {
                getRowClass: function (record, index, params, store) {
                    var clsSignoMas = record.get('tieneDiligencias') ? '' : 'hide-diligencias ';
                    var clsDisabled = record.get('editable') ? '' : 'disabled-row';

                    return clsSignoMas + ' ' + clsDisabled;
                }
            },
        selModel: 'cellmodel',
        plugins: ['gridfilters', Ext.create('Ext.grid.plugin.CellEditing',
            {
                clicksToEdit: 1,
                listeners:
                    {
                        beforeedit: 'checkNotaEditable'
                    }
            }),
            {
                ptype: "subtable",
                header: 'Diligències anteriors',
                headerWidth: 24,
                columns: [
                    {
                        text: 'Nota Anterior',
                        align: 'center',
                        width: 120,
                        dataIndex: 'notaAnterior'
                    },
                    {
                        text: 'Qualificació Anterior',
                        align: 'center',
                        width: 160,
                        dataIndex: 'calificacionAnterior'
                    },
                    {
                        text: 'Nota Diligència',
                        width: 120,
                        dataIndex: 'notaNueva',
                        align: 'center'
                    },
                    {
                        text: 'Qualificació Diligència',
                        align: 'center',
                        width: 160,
                        dataIndex: 'calificacionNueva'
                    },
                    {
                        text: 'Data',
                        align: 'left',
                        width: 120,
                        dataIndex: 'fecha',
                        xtype: 'datecolumn',
                        format: 'd/m/Y',
                        renderer: function (fecha) {
                            return Ext.Date.format(fecha, 'd/m/Y');
                        }
                    },
                    {
                        text: 'Modificat per',
                        align: 'left',
                        dataIndex: 'personaNombre'
                    },
                    {
                        text: 'Traspàs',
                        align: 'left',
                        dataIndex: 'comentarioTraspaso',
                        renderer: function(val) {
                            return val.replace("1Correcte", "Correcte");
                        }
                    },
                    {
                        text: 'Comentaris',
                        align: 'left',
                        dataIndex: 'comentarioDiligencia',
                        renderer: function (val, meta, record) {
                            return '<div style="white-space: normal !important;">' + Ext.util.Format.nl2br(val) + '</div>';
                        }
                    }],
                listeners:
                    {
                        dblclick: 'onDblClick'
                    },
                getAssociatedRecords: function (record) {
                    var diligencias = record.get('diligencias');

                    if (diligencias.length == 0) {
                        return {}
                    }

                    if (diligencias.hasOwnProperty('personaId')) {
                        diligencias = [diligencias];
                    }

                    return diligencias.map(function (diligencia) {
                        return Ext.create('act.model.Diligencia', diligencia);
                    })
                }
            }],

        border: 0,
        bbar: ['->',
            {
                xtype: 'button',
                text: 'Imprimir acta',
                iconCls: 'fa fa fa-file-pdf-o',
                handler: 'imprimirActa'
            }, '->',
            {
                xtype: 'button',
                text: 'Traspassar i signar digitalment les diligències',
                iconCls: 'fa fa-pencil-square-o',
                handler: 'traspasarDiligencia',
                bind:
                    {
                        disabled: '{!selectedActa.editable}'
                    }
            }],

        columns: [
            {
                text: 'ID',
                dataIndex: 'id',
                hidden: true
            },
            {
                dataIndex: 'index',
                width: 45,
                sortable: false,
                align: 'right',
                renderer: function (value, metaData, record, rowIndex) {
                    return rowIndex + 1;
                }
            },
            {
                xtype: 'actioncolumn',
                dataIndex: 'expedienteAbierto',
                menuDisabled: true,
                width: 30,
                align: 'center',
                items: [{
                    getTip: function (value, metadata, record) {
                        var msg;
                        if (!record.get('expedienteAbierto')) {
                            msg = Ext.util.Format.htmlEncode("El expedient de l'alumne ja està tancat") + '<br/>';
                        }
                        if (record.get('aprobadoConvocatoriaAnterior')) {
                            msg = Ext.util.Format.htmlEncode("L'alumne ja ha superat l'assignatura") + '<br/>';
                        }
                        if (record.get('calificadoConvocatoriaPosterior')) {
                            msg = Ext.util.Format.htmlEncode("L'alumne té una qualificació en una convocatòria posterior") + '<br/>';
                        }
                        return msg;
                    },
                    getClass: function (value, metadata, record) {
                        return (!record.get('expedienteAbierto') || record.get('aprobadoConvocatoriaAnterior') || record.get('calificadoConvocatoriaPosterior')) ? 'x-fa fa-lock rediconcolor' : 'x-fa fa-check greeniconcolor';
                    }
                }]
            },
            {
                dataIndex: 'alumnoNombre',
                text: 'Alumne',
                filter: 'string',
                flex: 3
            },
            {
                dataIndex: 'identificacionOfuscado',
                text: 'Identificació',
                align: 'center',
                filter: 'string',
                flex: 1
            },
            {
                dataIndex: 'nota',
                text: 'Nota',
                align: 'center',
                xtype: 'numbercolumn',
                width: 120,
                bind:
                    {
                        disabled: '{!selectedActa.editable}'
                    },
                editor:
                    {
                        field:
                            {
                                xtype: 'numberfield',
                                forceDecimalPrecision: true,
                                hideTrigger: true,
                                minValue: 0,
                                maxValue: 10,
                                keyNavEnabled: false,
                                mouseWheelEnabled: false,
                                decimalPrecision: 2,
                                decimalSeparator: '.',
                                allowBlank: true
                            }
                    },
                renderer: function (value, meta) {
                    meta.tdCls = 'editor-visible';
                    return value === null ? null : Math.trunc(value * 10) / 10;
                }
            },
            {
                dataIndex: 'calificacionId',
                text: 'Qualificació',
                align: 'center',
                filter: 'list',
                flex: 1,
                renderer: function (value) {
                    var vm = this.getView().up('diligenciasMain').getViewModel();
                    var store = vm.getStore('calificacionesStore');

                    var record = store.findRecord('id', value, 0, false, false, true);
                    return record ? record.get('nombre') : '';

                },
                editor:
                    {
                        xtype: 'diligenciasComboCalificacion'
                    }
            },
            {
                xtype: 'actioncolumn',
                text: 'Info',
                dataIndex: 'comentarioTraspaso',
                menuDisabled: true,
                width: 75,
                align: 'center',
                items: [{
                    getTip: function (comentarioTraspaso, meta, rec) {
                        return rec.get("personaNota") && Ext.util.Format.htmlEncode("Nota desada per " + rec.get("personaNota") + " (" + rec.get("fechaNota") + ")");
                    },
                    getClass: function (comentarioTraspaso, cell, rec) {
                        if (!rec.get('personaNota')) return null;
                        if (!comentarioTraspaso) return 'x-fa fa-file grayiconcolor'
                        return (comentarioTraspaso === 'Correcte') ? 'x-fa fa-file greeniconcolor' : 'x-fa fa-warning rediconcolor';
                    }
                }]
            },
            {
                xtype: 'actioncolumn',
                text: 'Comentari',
                align: 'center',
                dataIndex: 'comentario',
                width: 180,
                menuDisabled: true,
                sortable: false,
                bind:
                    {
                        disabled: '{!selectedActa.editable}'
                    },
                items: [
                    {
                        handler: 'onAddComentario',
                        getTip: function (value, metadata) {
                            return value ? Ext.util.Format.htmlEncode(value) : 'Afegir comentari';
                        },
                        getClass: function (comentario) {
                            return comentario ? 'x-fa fa-file-text-o' : 'x-fa fa-plus';
                        },
                        width: 180
                    }]
            }],

        saveData: function () {
            this.getStore().sync();
        },

        listeners:
            {
                activate: 'onActivate',
                grupoSelected: 'onGrupoSelected',
                actaConvocatoriaSelected: 'onActaConvocatoriaSelected',
                asignaturaSelected: 'onAsignaturaSelected',
                render: 'onLoad',
                botonActaClick: 'onBotonActaClick',
                beforeedit: 'onBeforeEdit',
                edit: 'onEdit'
            }
    });