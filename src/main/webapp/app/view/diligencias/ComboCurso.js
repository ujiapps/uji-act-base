Ext.define('act.view.diligencias.ComboCurso',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.diligenciasComboCurso',
    fieldLabel : 'Curs',
    showClearIcon : true,
    width : 240,
    labelWidth : 100,
    requires : [ 'act.store.Cursos' ],

    displayField : 'id',

    store :
    {
        type : 'cursos'
    },

    listeners :
    {
        change : function(combo, recordId)
        {
            combo.up('diligenciasPanelFiltros').fireEvent('cursoSelected', recordId);
        }
    }
});