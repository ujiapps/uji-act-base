Ext.define('act.view.diligencias.FormResumenDiligencias',
{
    extend : 'Ext.window.Window',
    alias : 'widget.formResumenDiligencias',

    title : 'Resum de diligències',
    width : 800,
    manageHeight : true,
    modal : true,
    bodyPadding : 10,
    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },

    requires : [ 'act.view.diligencias.FormResumenDiligenciasController' ],
    controller : 'formResumenDiligenciasController',

    items : [
    {
        xtype : 'grid',
        allowBlank : true,
        viewConfig :
        {
            markDirty : false
        },
        bind :
        {
            store : '{store}'
        },
        anchor : '100%',
        padding : 10,
        flex : 1,
        columns : [
        {
            dataIndex : 'alumnoNombre',
            text : 'Alumne',
            filter : 'string',
            flex : 1
        },
        {
            dataIndex : 'nota',
            text : 'Nota',
            align : 'center',
            xtype : 'numbercolumn',
            format : '0.0',
            width : 120,
        },
        {
            dataIndex : 'calificacionId',
            text : 'Qualificació',
            align : 'center',
            filter : 'list',
            flex : 1,
            renderer : function(value)
            {
                var vm = this.getView().up('diligenciasMain').getViewModel();
                var store = vm.getStore('calificacionesStore');

                var record = store.findRecord('id', value, 0, false, false, true);
                return record ? record.get('nombre') : '';

            }
        },
        {
            text : 'Comentari',
            dataIndex : 'comentario',
            width : 180,
            renderer : function(value, metadata)
            {
                metadata.tdAttr = 'data-qtip="' + value + '"';
                return value;
            }
        } ]
    } ],
    bbar : [ '->', '->',
    {
        xtype : 'button',
        text : 'Traspassar diligències',
        handler : 'onTraspasar'
    },
    {
        xtype : 'panel',
        html : '<a style="text-decoration: none; color: #222;" href="#">Cancel·lar</a>',
        listeners :
        {
            render : function(component)
            {
                component.getEl().on('click', 'onClose');
            }
        }
    } ]
});