Ext.define('act.view.diligencias.Main',
{
    extend : 'Ext.panel.Panel',

    alias : 'widget.diligenciasMain',
    title : 'Diligències',

    requires : [ 'act.view.diligencias.MainController', 'act.view.diligencias.ViewModel', 'act.view.diligencias.PanelFiltros', 'act.view.diligencias.FormDiligenciasComentario',
            'act.view.diligencias.Grid', 'act.view.diligencias.FormResumenDiligencias' ],
    controller : 'diligenciasMainController',

    cls : 'no-border',
    margin : 5,
    viewModel :
    {
        type : 'diligenciasViewModel'
    },
    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },

    items : [
    {
        xtype : 'diligenciasPanelFiltros'
    },
    {
        xtype : 'diligenciasGrid',
        bind :
        {
            hidden : '{!cursoYconvocatoriaYActaSeleccionados}'
        }
    } ],
    listeners :
    {
        // render : 'mockupDiligencia',
        actaConvocatoriaSelected : 'onActaConvocatoriaSelected',
        volverASelector : 'onVolverASelector'
    }
});
