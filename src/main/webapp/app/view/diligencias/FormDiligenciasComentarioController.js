Ext.define('act.view.diligencias.FormDiligenciasComentarioController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.formDiligenciasComentarioController',

    onClose : function()
    {
        var win = Ext.WindowManager.getActive();
        if (win)
        {
            win.destroy();
        }
    },

    onSave : function()
    {
        var view = this.getView();
        var vm = this.getViewModel();
        var form = view.down('form');
        var record = vm.get('record');
        var store = vm.get('store');

        if (form.isValid())
        {
            var comentario = form.getValues().comentario;

            record.set('comentario', comentario);
            this.onClose();
        }
    }
});
