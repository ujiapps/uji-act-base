Ext.define('act.view.diligencias.FormDiligenciasComentario',
{
    extend : 'Ext.window.Window',
    alias : 'widget.formDiligenciasComentario',

    width : 640,
    manageHeight : true,
    modal : true,
    bodyPadding : 10,
    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },

    bind: {
        title: '{title}'
    },

    requires : [ 'act.view.diligencias.FormDiligenciasComentarioController' ],
    controller : 'formDiligenciasComentarioController',

    items : [
    {
        xtype : 'form',
        layout : 'anchor',
        items : [
        {
            xtype : 'textarea',
            name : 'comentario',
            allowBlank : true,
            anchor : '100%',
            padding : 10,
            flex : 1,
            bind :
            {
                disabled : '{!selectedConvocatoria.editable}',
                value : '{record.comentarioDiligencia}'
            }
        } ]
    } ],

    bbar : [ '->', '->',
    {
        xtype : 'button',
        text : 'Desar',
        handler : 'onSave',
        bind :
        {
            disabled : '{!selectedActa.editable}'
        }
    },
    {
        xtype : 'panel',
        html : '<a style="text-decoration: none; color: #222;" href="#">Cancel·lar</a>',
        listeners :
        {
            render : function(component)
            {
                component.getEl().on('click', 'onClose');
            }
        }
    } ]
});