Ext.define('act.view.diligencias.GridController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.diligenciasGridController',

    traspasarDiligencia : function(view, rowIndex, colIndex, item, event, record)
    {
        var view = this.getView().up('diligenciasMain')
        var vm = this.getViewModel();
        var store = vm.getStore('actaDiligenciasStore');

        var invalid;
        for (var i = 0; i < store.getData().length; i++)
        {
            var record = store.getData().items[i];
            if (!record.isValid())
            {
                invalid = 'La nota introduïda ' + record.get('nota') + ' per al alumne/a ' + record.get('alumnoNombre') + ' no és correcta respecte a la qualificació.';
                break;
            }
        }

        var data = store.getUpdatedRecords();
        for (var i = 0; i < data.length; i++)
        {
            var item = data[i];

            if (!item.get('comentario'))
            {
                invalid = 'Es obligatori introduïr comentari en la diligència de l\'alumne/a ' + item.get('alumnoNombre');
                break;
            }

            if (item.get('nota') === item.previousValues.nota && item.get('calificacion') === 5 && item.get('nota'))
            {
                continue;
            }

            if (item.get('nota') === item.previousValues.nota)
            {
                invalid = 'Es obligatori introduïr nota en la diligència de l\'alumne/a ' + item.get('alumnoNombre');
                break;
            }
        }

        if (invalid)
        {
            return Ext.Msg.alert('Error', invalid);
        }

        if (data.length === 0)
        {
            return;
        }

        var diligenciasStore = Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'act.model.ActaDiligencia'
        });

        diligenciasStore.loadData(data);

        var modal =
        {
            xtype : 'formResumenDiligencias',
            viewModel :
            {
                data :
                {
                    store : diligenciasStore
                }
            }
        };

        view.add(modal).show();
    },

    checkNotaEditable : function(editor, context)
    {
        var rec = context.record;
        return rec.get('editable');
    },

    onAddComentario : function(view, rowIndex, colIndex, item, event, record)
    {
        if (record.get('motivoExclusion'))
        {
            return;
        }

        var view = this.getView().up('diligenciasMain')
        var vm = this.getViewModel();
        var modal =
        {
            xtype : 'formDiligenciasComentario',
            viewModel :
            {
                data :
                {
                    record : record,
                    title : 'Comentari: ' + record.get('alumnoNombre'),
                    store : vm.getStore('diligenciasStore')
                }
            }
        };

        view.add(modal).show();
    },

    imprimirActa : function()
    {
        var vm = this.getViewModel();
        var acta = this.getView().up('diligenciasMain').down('diligenciasComboActa').getSelection();
        var referencia = acta.get('referencia');
        return window.open('https://e-ujier.uji.es/cocoon/4/2/' + referencia + '/CA/documento.pdf');
    },

    onEdit : function(re, obj)
    {
        obj.store.suspendAutoSync();
        return false;
    },

    onBeforeEdit : function()
    {
        return true;
    },

    onEditComplete : function()
    {
        return false;
    },

    onDblClick : function()
    {

    }
});
