Ext.define('act.view.diligencias.FormResumenDiligenciasController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.formResumenDiligenciasController',

    onClose : function()
    {
        var win = Ext.WindowManager.getActive();
        if (win)
        {
            win.destroy();
        }
    },

    onTraspasar : function()
    {
        var view = this.getView();
        var vm = this.getViewModel();
        var store = vm.get('store');
        var invalid = false;

        var data = store.getData().items.map(function(record)
        {
            var nota = record.get('nota');
            if (nota) {
                var nota = Math['floor'](record.get('nota') * 10) / 10;
            }

            var diligencia =
            {
                actaEstudianteId : record.get('actaEstudianteId'),
                actaId : record.get('actaId'),
                alumnoId : record.get('alumnoId'),
                cursoId : record.get('cursoId'),
                asignaturaId : record.get('asignaturaId'),
                alumnoNombre : record.get('alumnoNombre'),
                calificacionId : record.get('calificacionId'),
                comentario : record.get('comentario'),
                noPresentado : record.get('noPresentado'),
                cuenta: record.get('cuenta'),
                nota : nota
            };

            if (!diligencia.comentario) {
                invalid = 'Es obligatori introduïr un comentari en la diligència de l\'alumne/a ' + diligencia.alumnoNombre;
            }
            return diligencia;
        });

        if (invalid) {
            return Ext.Msg.alert('Error', invalid);
        }

        var actaId = vm.get('comboActa.selection').get('actaId');
        Ext.Ajax.request(
        {
            url : '/act/rest/actas/' + actaId + '/actadiligencias',
            method : 'POST',
            jsonData :
            {
                diligencias : data
            },
            success : function(response)
            {
                var panel = view.up('diligenciasMain');
                var store = panel.getViewModel().getStore('actaDiligenciasStore');
                store.reload();
                this.onClose();
            },
            scope : this
        });
    }
});
