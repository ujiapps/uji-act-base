Ext.define('act.view.diligencias.PanelFiltros',
{
    extend : 'Ext.panel.Panel',

    alias : 'widget.diligenciasPanelFiltros',
    border : 0,
    padding : 10,
    requires : [ 'act.view.diligencias.PanelFiltrosController', 'act.view.diligencias.ComboCurso', 'act.view.diligencias.ComboConvocatoria', 'act.view.diligencias.ComboActa' ],
    controller : 'diligenciasPanelFiltrosController',

    layout : 'anchor',
    items : [
    {
        xtype : 'diligenciasComboCurso',
        reference : 'comboCurso'
    },
    {
        xtype : 'diligenciasComboConvocatoria',
        reference : 'comboConvocatoria'
    },
    {
        xtype : 'diligenciasComboActa',
        reference : 'comboActa',
        bind :
        {
            disabled : '{!cursoYconvocatoriaSeleccionados}'
        }
    } ],

    listeners :
    {
        cursoSelected : 'onCursoSelected',
        convocatoriaSelected : 'onConvocatoriaSelected',
        actaSelected : 'onActaSelected'
    }
});
