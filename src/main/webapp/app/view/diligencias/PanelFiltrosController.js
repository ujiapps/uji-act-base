Ext.define('act.view.diligencias.PanelFiltrosController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.diligenciasPanelFiltrosController',
    loadActas : function(cursoId, convocatoriaId)
    {
        var panel = this.getView();
        var vm = this.getViewModel();
        var store = this.getStore('actasStore');
        store.getProxy().url = '/act/rest/cursos/' + cursoId + '/actas/traspasadas';
        panel.setLoading(true)
        store.load(
        {
            params :
            {
                convocatoriaId : convocatoriaId
            },
            callback : function()
            {
                panel.setLoading(false);
            }
        });
    },

    onConvocatoriaSelected : function(convocatoriaId)
    {
        var panel = this.getView();
        var vm = this.getViewModel();

        var comboActa = panel.down('diligenciasComboActa');
        comboActa.reset();

        if (!convocatoriaId)
        {
            return comboActa.getStore().removeAll();
        }

        var comboCurso = vm.get('comboCurso.selection');
        if (comboCurso)
        {
            this.loadActas(comboCurso.get('id'), convocatoriaId);
        }
    },

    onCursoSelected : function(cursoId)
    {
        var panel = this.getView();
        var vm = this.getViewModel();
        var comboActa = panel.down('diligenciasComboActa');
        comboActa.reset();

        if (!cursoId)
        {
            return comboActa.getStore().removeAll();
        }

        var comboConvocatoria = vm.get('comboConvocatoria.selection');
        if (comboConvocatoria)
        {
            this.loadActas(cursoId, comboConvocatoria.get('id'));
        }

    },

    onActaSelected : function(actaId)
    {
        var panel = this.getView();
        var vm = this.getViewModel();
        var store = vm.getStore('actaDiligenciasStore');

        if (actaId == null)
        {
            return;
        }

        var main = panel.up('diligenciasMain');
        main.setLoading(true);
        store.load(
        {
            url: '/act/rest/actas/' + actaId + '/actadiligencias',
            callback : function(records)
            {
                main.setLoading(false);
            }
        });
    }
});
