Ext.define('act.view.diligencias.ComboConvocatoria',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.diligenciasComboConvocatoria',
    fieldLabel : 'Convocatoria',
    showClearIcon : true,
    labelWidth : 100,
    minWidth: 350,
    displayField : 'nombre',
    allowBlank : true,
    bind :
    {
        store : '{convocatoriasOficialesStore}'
    },

    listeners :
    {
        change : function(combo, recordId)
        {
            combo.up('diligenciasPanelFiltros').fireEvent('convocatoriaSelected', recordId);
        }
    }

});