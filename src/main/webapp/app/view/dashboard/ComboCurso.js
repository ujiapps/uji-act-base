Ext.define('act.view.dashboard.ComboCurso',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.dashboardComboCurso',
    fieldLabel : 'Curs',
    showClearIcon : true,
    width : 180,
    labelWidth : 60,
    allowBlank : true,
    padding : '10 10 0 10',
    displayField : 'id',

    bind :
    {
        store : '{cursosStore}'
    },

    listeners :
    {
        change : function(combo, recordId)
        {
            combo.up('panelDashboard').fireEvent('cursoSelected', recordId);
        }
    }

});