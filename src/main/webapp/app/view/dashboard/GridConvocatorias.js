Ext.define('act.view.dashboard.GridConvocatorias',
{
    extend : 'Ext.grid.Panel',
    alias : 'widget.dashboardGridConvocatorias',
    bind :
    {
        store : '{convocatoriasStore}'
    },
    padding : 10,
    title : 'Convocatòries',
    emptyText : 'Carregant dades...',
    columns : [
    {
        text : 'ID',
        dataIndex : 'id',
        hidden : true
    },
    {
        dataIndex : 'tipoEstudio',
        text : 'Tipus estudi',
        minWidth : 150,
        flex : 1
    },
    {
        dataIndex : 'nombre',
        text : 'Nombre',
        minWidth : 150,
        flex : 1
    },
    {
        dataIndex : 'extraordinaria',
        text : 'Extraordinària',
        width : 120,
        align : 'center',
        renderer : function(v)
        {
            return v === 'true' ? 'Sí' : 'No';
        }
    },
    {
        dataIndex : 'semestre',
        text : 'Semestre',
        width : 80,
        align : 'center'
    },
    {
        xtype : 'datecolumn',
        format : 'd/m/Y',
        dataIndex : 'fechaInicioTraspaso',
        text : 'Data inici traspàs',
        width : 140,
        align : 'center'
    },
    {
        xtype : 'datecolumn',
        format : 'd/m/Y',
        dataIndex : 'fechaFinTraspaso',
        text : 'Data fi traspàs',
        width : 140,
        align : 'center'
    },
    {
        xtype : 'datecolumn',
        format : 'd/m/Y',
        dataIndex : 'fechaInicioActaUnica',
        text : 'Data inici pas de notes a l\'expedient',
        width : 280,
        align : 'center'
    } ]
});