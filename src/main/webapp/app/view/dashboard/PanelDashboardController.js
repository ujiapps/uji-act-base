Ext.define('act.view.dashboard.PanelDashboardController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.panelDashboardController',

    onLoad : function()
    {
        var panel = this.getView();
        var vm = this.getViewModel();
        var cursosStore = vm.get('cursosStore');
        var combo = panel.down('dashboardComboCurso')

        cursosStore.load(
        {
            callback : function(records)
            {
                Ext.Array.map(records, function(curso)
                {
                    if (curso.get('activo'))
                    {
                        combo.setValue(curso.get('id'));
                    }
                });
            }
        });
    },

    onCursoSelected : function(cursoId)
    {
        var panel = this.getView();
        var vm = this.getViewModel();

        panel.setLoading(true);
        var store = this.getStore('convocatoriasStore');

        store.load(
        {
            params :
            {
                cursoId : cursoId
            },
            callback : function(data)
            {
                panel.setLoading(false);
            },
            scope : this
        });
    },

});
