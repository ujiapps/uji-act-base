Ext.define('act.view.dashboard.PanelDashboard',
{
    extend : 'Ext.panel.Panel',
    alias : 'widget.panelDashboard',
    name : 'panelDashboard',
    controller : 'panelDashboardController',
    title : 'Dashboard',
    frame : true,
    scrollable : true,
    requires : [ 'act.view.dashboard.ViewModel', 'act.view.dashboard.ComboCurso', 'act.view.dashboard.GridConvocatorias', 'act.view.dashboard.PanelDashboardController' ],
    viewModel :
    {
        type : 'dashboardViewModel'
    },

    items : [
            {
                xtype : 'panel',
                padding : 10,
                border : false,
                layout :
                {
                    type : 'vbox',
                    align : 'stretch'
                },
                bind :
                {
                    hidden : '{ocultarNoticias}'
                },
                items : [
                        {
                            xtype : 'label',
                            width : '100%',
                            padding : 10,
                            text : 'Aplicació de Gestió d\'actes Acadèmiques',
                            style : 'font-size : 30px; font-weight : bold; text-align : center; margin-top : 30px'
                        },
                        {
                            xtype : 'label',
                            width : '100%',
                            text : 'Versió ' + appversion,
                            style : 'font-size : 12px; text-align : center'
                        },
                        {
                            xtype : 'dataview',
                            cls : 'informacion',
                            margins : '50px',
                            bind :
                            {
                                store : '{informacionesStore}'
                            },
                            itemSelector : 'div:first-child',
                            tpl : new Ext.XTemplate('<tpl for=".">', '<div class="headline">', '<table><tr><td class="date">{fechaInicio:date("d/m/Y")}</td>', '<td>{texto}</td>', '</tr>', '</table>',
                                    '</div>', '</tpl>')
                        },
                        {
                            xtype : 'label',
                            flex : 1,
                            align : 'center',
                            padding : 10,
                            bind: {
                                html: '<p>Has realitzat {numDiligenciasEnSeisMeses} diligències en {numActasDiligenciasEnSeisMeses} actes en els últims sis mesos.</p>',
                                hidden : '{!tieneDiligencias}'
                            },
                            style : 'font-size : 1rem; text-align : center; margin : 1rem 0'
                        }]
            },
            {
                xtype : 'dashboardComboCurso',
                reference : 'comboCurso'
            },
            {
                xtype : 'dashboardGridConvocatorias',
                bind :
                {
                    hidden : '{!comboCurso.selection}'
                }
            } ],
    listeners :
    {
        render : 'onLoad',
        cursoSelected : 'onCursoSelected'
    }
});
