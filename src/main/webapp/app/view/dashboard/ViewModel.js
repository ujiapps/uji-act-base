Ext.define('act.view.dashboard.ViewModel',
{
    extend : 'Ext.app.ViewModel',
    alias : 'viewmodel.dashboardViewModel',
    requires : [ 'act.model.Curso', 'act.model.ProfesorDiligencia', 'act.model.ConvocatoriaDetalle', 'act.model.Informacion' ],
    formulas :
    {
        ocultarNoticias : function(get)
        {
            return get('informacionesStore').getCount() === 0;
        },
        numDiligenciasEnSeisMeses : function(get)
        {
            var resumen = get('resumenDiligenciasStore').first();
            return resumen.get('diligencias');
        },
        numActasDiligenciasEnSeisMeses : function(get)
        {
            var resumen = get('resumenDiligenciasStore').first();
            return resumen.get('actasAfectadas');
        },
        tieneDiligencias : function(get)
        {
            var roles = get('rolesStore').first();
            return roles.get('tieneDiligencias');
        }
    },
    stores :
    {
        cursosStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'act.model.Curso',
            url : '/act/rest/cursos',
            autoLoad: false
        }),
        convocatoriasStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'act.model.ConvocatoriaDetalle',
            url : '/act/rest/convocatorias/detalle',
            autoLoad : false
        }),
        resumenDiligenciasStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'act.model.ResumenDiligencias',
            url : '/act/rest/profesor/diligencias/resumen',
            autoLoad : true
        }),
        informacionesStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'act.model.Informacion',
            url : '/act/rest/informaciones',
            autoLoad : true
        }),
        rolesStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'act.model.Rol',
            url : '/act/rest/usuario/roles',
            autoLoad : true
        })
    }
});
