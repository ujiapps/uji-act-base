Ext.define('act.view.misDiligencias.Main',
{
    extend : 'Ext.panel.Panel',

    alias : 'widget.misDiligenciasMain',
    title : 'Les meues diligéncies',

    requires : [ 'act.view.misDiligencias.PanelFiltros', 'act.view.misDiligencias.Grid', 'act.view.misDiligencias.MainController', 'act.view.misDiligencias.ViewModel',
            'act.view.misDiligencias.ComboCurso', 'act.view.misDiligencias.ComboConvocatoria', 'act.view.misDiligencias.ComboActa' ],
    controller : 'misDiligenciasMainController',

    cls : 'no-border',
    margin : 5,
    viewModel :
    {
        type : 'misDiligenciasViewModel'
    },
    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },

    items : [
    {
        xtype : 'misDiligenciasPanelFiltros'
    },
    {
        xtype : 'misDiligenciasGrid'
    } ],
    listeners :
    {
        render : 'onLoad',
        convocatoriaSelected : 'onCursoConvocatoriaSelected',
        filtroDepartamentoSelected : 'onFiltroDepartamentoSelected',
        filtroEstudioSelected : 'onFiltroEstudioSelected',
        filtroActaGeneradaSelected : 'onFiltroActaGeneradaSelected',
        filtroActaTraspasadaSelected : 'onFiltroActaTraspasadaSelected',
        enviarNotificacion : 'onEnviarNotificacion',
        cursoSelected: 'onReload',
        convocatoriaSelected: 'onReload',
        actaSelected: 'onReload'
    }
});
