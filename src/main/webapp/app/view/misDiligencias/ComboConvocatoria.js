Ext.define('act.view.misDiligencias.ComboConvocatoria',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.misDiligenciasComboConvocatoria',
    fieldLabel : 'Convocatoria',
    showClearIcon : true,
    width : 450,
    labelWidth : 100,
    displayField : 'nombre',
    allowBlank: true,
    bind: {
        store: '{convocatoriasStore}'
    },

    listeners :
    {
        change : function(combo, recordId)
        {
            combo.up('misDiligenciasMain').fireEvent('convocatoriaSelected', recordId);
        }
    }

});