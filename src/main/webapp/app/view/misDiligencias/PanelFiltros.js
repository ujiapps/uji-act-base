Ext.define('act.view.misDiligencias.PanelFiltros',
{
    extend : 'Ext.panel.Panel',

    alias : 'widget.misDiligenciasPanelFiltros',
    border : 0,
    requires : [ 'act.view.misDiligencias.ComboActa' ],

    layout : 'anchor',
    items : [
    {
        xtype : 'fieldset',
        title : 'Filtres',
        items : [
        {
            xtype : 'fieldcontainer',
            layout : 'hbox',
            items : [
            {
                xtype : 'misDiligenciasComboCurso',
                reference : 'comboCurso',
                flex : 1
            },
            {
                xtype : 'box',
                width : 50
            },
            {
                xtype : 'misDiligenciasComboConvocatoria',
                reference : 'comboConvocatoria',
                flex : 2
            } ]
        },
        {
            xtype : 'fieldcontainer',
            layout : 'hbox',
            items : [
            {
                xtype : 'misDiligenciasComboActa',
                flex: 1
            } ]
        } ]

    } ],
    listeners : {}
});
