Ext.define('act.view.misDiligencias.ComboActa',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.misDiligenciasComboActa',
    fieldLabel : 'Acta',
    showClearIcon : true,
    labelWidth : 60,
    typeAhead: true,
    editable: true,
    anchor: '50%',
    matchFieldWidth: false,
    displayField : 'descripcion',
    valueField: 'id',

    bind: {
        store: '{actasStore}'
    },

    listeners :
    {
        select : function(combo, record)
        {
            combo.up('misDiligenciasMain').fireEvent('actaSelected', record);
        },
        beforequery: function(record){
            record.query = new RegExp(record.query, 'i');
            record.forceAll = true;
        }
    }

});