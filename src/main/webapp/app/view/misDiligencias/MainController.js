Ext.define('act.view.misDiligencias.MainController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.misDiligenciasMainController',

    onLoad : function()
    {
        var vm = this.getViewModel();
    },

    onReload : function()
    {
        var view = this.getView();
        var cursoId = view.down('misDiligenciasComboCurso').getValue();
        var convocatoriaId = view.down('misDiligenciasComboConvocatoria').getValue();
        var actaId = view.down('misDiligenciasComboActa').getValue();
        var grid = view.down('misDiligenciasGrid');
        grid.getStore().reload(
        {
            params :
            {
                cursoId : cursoId,
                convocatoriaId : convocatoriaId,
                actaId : actaId
            }
        });
    },

    onVerComentario : function(view, rowIndex, colIndex, item, event, diligencia)
    {
        return Ext.Msg.alert('Comentari diligència', diligencia.get('comentarioDiligencia'));
    }

});
