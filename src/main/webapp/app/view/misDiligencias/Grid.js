Ext.define('act.view.misDiligencias.Grid',
{
    extend : 'Ext.ux.uji.grid.Panel',
    allowEdit : false,

    alias : 'widget.misDiligenciasGrid',
    flex : 1,
    scrollable : true,
    controller : 'misDiligenciasMainController',
    bind :
    {
        store : '{profesorDiligenciasStore}'
    },

    emptyText : 'Selecciona un curs i una convocatoria',

    border : 0,
    title : 'Llista de diligències',
    border : 0,
    columns : [
    {
        text : 'ID',
        dataIndex : 'id',
        hidden : true
    },
    {
        text : 'Data',
        align : 'left',
        width : 120,
        dataIndex : 'fecha',
        xtype : 'datecolumn',
        format : 'd/m/Y H:i'
    },
    {
        dataIndex : 'cursoAcademico',
        text : 'Curs',
        align : 'center',
        width : 120,
    },
    {
        dataIndex : 'convocatoria',
        text : 'Convocatòria',
        align : 'center',
        flex : 1,
    },
    {
        dataIndex : 'acta',
        text : 'Acta',
        menuDisabled : true,
        align : 'center',
        filter : 'list',
        flex : 1
    },
    {
        dataIndex : 'alumnoNombre',
        text : 'Alumne',
        filter : 'string',
        flex : 2
    },
    {
        dataIndex : 'identificacion',
        text : 'Identificació',
        align : 'center',
        filter : 'string',
        flex : 1
    },
    {
        dataIndex : 'notaNueva',
        text : 'Nota',
        align : 'center',
        xtype : 'numbercolumn',
        width : 120,
        renderer : function(value, meta)
        {
            return value === null ? null : Math.trunc(value * 10) / 10;
        }
    },
    {
        dataIndex : 'calificacionNuevaId',
        text : 'Qualificació',
        align : 'center',
        filter : 'list',
        renderer : function(value)
        {
            var store = Ext.getStore('act.store.Calificaciones');
            var data = store.getById(value);
            return data ? data.get('nombre') : '';
        }
    },
    {
        dataIndex : 'notaAnterior',
        text : 'Nota Anterior',
        align : 'center',
        xtype : 'numbercolumn',
        width : 120,
        renderer : function(value, meta)
        {
            return value === null ? null : Math.trunc(value * 10) / 10;
        }
    },
    {
        dataIndex : 'calificacionAnteriorId',
        text : 'Qualificació Anterior',
        align : 'center',
        filter : 'list',
        flex : 1,
        renderer : function(value)
        {
            var store = Ext.getStore('act.store.Calificaciones');
            var data = store.getById(value);
            return data ? data.get('nombre') : '';
        }
    },
    {
        dataIndex : 'ip',
        text : 'Adreça IP',
        align : 'center',
        flex : 1,
        renderer : function(value)
        {
            return value ? value : 'No disponible'
        }
    },
    {
        xtype : 'actioncolumn',
        text : 'Comentaris',
        dataIndex : 'comentarioDiligencia',
        width : 95,
        menuDisabled : true,
        sortable : false,
        align : 'center',
        items : [
        {
            getTip : function(value, metadata)
            {
                return Ext.util.Format.htmlEncode(value);
            },
            handler: 'onVerComentario',
            getClass : function(comentario)
            {
                return 'x-fa fa-file-text-o';
            }

        } ]
    } ]
});