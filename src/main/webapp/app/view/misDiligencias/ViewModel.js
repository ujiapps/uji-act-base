Ext.define('act.view.misDiligencias.ViewModel',
{
    extend : 'Ext.app.ViewModel',
    alias : 'viewmodel.misDiligenciasViewModel',
    requires : [ 'act.model.ActaAsignatura', 'act.model.ActaEstudiante', 'act.model.ActaPersona', 'act.model.Convocatoria' ],
    formulas :
    {
        cursoYconvocatoriaSeleccionados : function(get)
        {
            return get('comboConvocatoria.selection') && get('comboCurso.selection');
        }

    },
    stores :
    {
        cursosStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'act.model.Curso',
            url : '/act/rest/profesor/diligencias/cursos',
            autoLoad : true
        }),
        convocatoriasStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'act.model.Convocatoria',
            url : '/act/rest/profesor/diligencias/convocatorias',
            autoLoad : true
        }),
        actasStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'act.model.Acta',
            url : '/act/rest/profesor/diligencias/actas',
            autoLoad : true,
            queryModel : 'local'
        }),
        profesorDiligenciasStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'act.model.ProfesorDiligencia',
            url : '/act/rest/profesor/diligencias',
            sorters : [ 'alumnoNombre' ],
            autoLoad : true,
            autoSync : false
        })
    }
});
