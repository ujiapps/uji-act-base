Ext.define('act.view.misDiligencias.ComboCurso',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.misDiligenciasComboCurso',
    fieldLabel : 'Curs',
    showClearIcon : true,
    width : 180,
    labelWidth : 60,
    allowBlank : true,
    displayField : 'id',
    bind :
    {
        store : '{cursosStore}'
    },
    listeners :
    {
        select : function(combo, record)
        {
            combo.up('misDiligenciasMain').fireEvent('cursoSelected', record.get("id"));
        }
    }
});