Ext.define('act.view.diligenciasExtra.FormDiligenciasExtraComentarioDenegacionController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.formDiligenciasExtraComentarioDenegacionController',

    onClose : function()
    {
        var win = Ext.WindowManager.getActive();
        if (win)
        {
            win.destroy();
        }
    },

    onDenegar : function()
    {
        var view = this.getView();
        var vm = this.getViewModel();
        var form = view.down('form');
        var record = vm.get('record');
        var store = vm.get('store');

        if (form.isValid())
        {
            var comentarioDenegacion = form.getValues().comentarioDenegacion;

            record.set('comentarioDenegacion', comentarioDenegacion);
            Ext.Ajax.request(
                {
                    url: '/act/rest/diligenciasFueraPlazo/' + record.get('id') + '/denegar',
                    jsonData: {
                        comentarioDenegacion: comentarioDenegacion
                    },
                    method: 'PUT',
                    success: function (response) {
                        store.reload();
                        this.onClose();
                    },
                    scope: this
                });

        }
    }
});
