Ext.define('act.view.diligenciasExtra.FormDiligenciasExtraComentario',
{
    extend : 'Ext.window.Window',
    alias : 'widget.formDiligenciasExtraComentario',

    width : 640,
    manageHeight : true,
    modal : true,
    bodyPadding : 10,
    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },

    bind: {
        title: '{title}'
    },

    requires : [ 'act.view.diligenciasExtra.FormDiligenciasExtraComentarioController' ],
    controller : 'formDiligenciasExtraComentarioController',

    items : [
    {
        xtype : 'form',
        layout : 'anchor',
        items : [
        {
            xtype : 'textarea',
            name : 'comentario',
            allowBlank : true,
            anchor : '100%',
            padding : 10,
            flex : 1,
            bind :
            {
                value : '{record.comentarioDiligencia}'
            }
        } ]
    } ],

    bbar : [ '->', '->',
    {
        xtype : 'button',
        text : 'Desar',
        handler : 'onSave',
        bind: {
            disabled: '{!selectedEstudiante.editableFueraPlazo}'
        }
    },
    {
        xtype : 'panel',
        html : '<a style="text-decoration: none; color: #222;" href="#">Cancel·lar</a>',
        listeners :
        {
            render : function(component)
            {
                component.getEl().on('click', 'onClose');
            }
        }
    } ]
});