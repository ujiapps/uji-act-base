Ext.define('act.view.diligenciasExtra.ComboActa',
    {
        extend: 'Ext.ux.uji.combo.Combo',
        alias: 'widget.diligenciasExtraComboActa',
        reference: 'comboActa',
        fieldLabel: 'Acta',
        showClearIcon: true,
        labelWidth: 60,
        typeAhead: true,
        minWidth: 350,
        editable: true,
        allowBlank: true,
        displayField: 'codigo',
        valueField: 'id',
        bind: {
            store: '{actasStore}'
        },

        listeners:
            {
                change: function (combo, recordId) {
                    combo.up('diligenciasExtraPanelFiltros').fireEvent('actaSelected', recordId);
                }
            }

    });