Ext.define('act.view.diligenciasExtra.PanelFiltros',
    {
        extend: 'Ext.panel.Panel',

        alias: 'widget.diligenciasExtraPanelFiltros',
        border: 0,
        requires: ['act.view.diligenciasExtra.PanelFiltrosController', 'act.view.diligenciasExtra.ComboCurso', 'act.view.diligenciasExtra.ComboConvocatoria', 'act.view.diligenciasExtra.ComboActa', 'act.view.diligenciasExtra.ComboEstudiante'],
        controller: 'diligenciasExtraPanelFiltrosController',

        layout: 'anchor',
        items: [{
            xtype: 'panel',
            layout: 'vbox',
            border: 0,
            items: [{
                xtype: 'fieldcontainer',
                layout: 'hbox',
                padding: '20 20 0 20',
                flex: 1,
                items: [

                    {
                        xtype: 'diligenciasExtraComboCurso',
                        margin: '0 0 0 30',
                        reference: 'comboCurso'
                    },
                    {
                        xtype: 'diligenciasExtraComboConvocatoria',
                        margin: '0 0 0 30',
                        reference: 'comboConvocatoria'
                    },
                    {
                        xtype: 'diligenciasExtraComboActa',
                        reference: 'comboActa',
                        flex: 1,
                        margin: '0 0 0 30',
                        bind:
                            {
                                disabled: '{!cursoYconvocatoriaSeleccionados}'
                            }
                    }]
            },
                {
                    xtype: 'panel',
                    layout: 'vbox',
                    border: 0,
                    flex: 1,
                    items: [
                        {
                            xtype: 'diligenciasExtraComboEstudiante',
                            margin: '0 0 20 50',
                            reference: 'comboEstudiante',
                            flex: 1,
                            bind:
                                {
                                    disabled: '{!cursoYconvocatoriaYActaSeleccionados}'
                                }
                        }]
                }]
        }],

        listeners:
            {
                cursoSelected: 'onCursoSelected',
                convocatoriaSelected:
                    'onConvocatoriaSelected',
                actaSelected:
                    'onActaSelected',
                estudianteSelected:
                    'onEstudianteSelected'
            }
    })
;
