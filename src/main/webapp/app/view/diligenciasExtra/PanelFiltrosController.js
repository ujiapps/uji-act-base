Ext.define('act.view.diligenciasExtra.PanelFiltrosController',
    {
        extend: 'Ext.ux.uji.grid.PanelController',
        alias: 'controller.diligenciasExtraPanelFiltrosController',
        loadActas: function (cursoId, convocatoriaId) {
            var panel = this.getView();
            var vm = this.getViewModel();
            var store = this.getStore('actasStore');
            store.getProxy().url = '/act/rest/cursos/' + cursoId + '/actas/traspasadasProfesor';
            panel.setLoading(true)
            store.load(
                {
                    params:
                        {
                            convocatoriaId: convocatoriaId
                        },
                    callback: function () {
                        panel.setLoading(false);
                    }
                });
        },

        onConvocatoriaSelected: function (convocatoriaId) {
            var panel = this.getView();
            var vm = this.getViewModel();

            var comboActa = panel.down('diligenciasExtraComboActa');
            comboActa.reset();

            if (!convocatoriaId) {
                return comboActa.getStore().removeAll();
            }

            var comboCurso = vm.get('comboCurso.selection');
            if (comboCurso) {
                this.loadActas(comboCurso.get('id'), convocatoriaId);
            }
        },

        onCursoSelected: function (cursoId) {
            var panel = this.getView();
            var vm = this.getViewModel();
            var comboActa = panel.down('diligenciasExtraComboActa');
            comboActa.reset();

            if (!cursoId) {
                return comboActa.getStore().removeAll();
            }

            var comboConvocatoria = vm.get('comboConvocatoria.selection');
            if (comboConvocatoria) {
                this.loadActas(cursoId, comboConvocatoria.get('id'));
            }

        },

        onActaSelected: function (recId) {
            var panel = this.getView();
            var vm = this.getViewModel();
            var store = vm.getStore('actaDiligenciasAlumnosStore');
            store.removeAll();
            panel.down('diligenciasExtraComboEstudiante').clearValue()

            var storePendientes = vm.getStore('actaDiligenciasStore');
            storePendientes.removeFilter('alumnoId');
            storePendientes.removeAll();

            var storeActa = vm.getStore('actasStore');
            if (recId == null || storeActa.getById(recId) == null) {
                return;
            }

            var actaId = storeActa.getById(recId).get('actaId');

            var main = panel.up('diligenciasExtraMain');
            main.setLoading(true);
            store.load({
                url: '/act/rest/actas/' + actaId + '/actadiligencias/fueraplazo',
                callback: function (records) {
                    main.setLoading(false);
                }
            });
        },

        onEstudianteSelected: function (actaEstudianteId) {
            var panel = this.getView();
            var vm = this.getViewModel();
            var store = vm.getStore('actaDiligenciasStore');

            var storeActa = vm.getStore('actasStore');
            var recId = panel.down('diligenciasExtraComboActa').getValue();
            if (recId) {
                var actaId = storeActa.getById(recId).get('actaId');
            }

            store.removeAll();

            if (actaEstudianteId == null) {
                return;
            }

            var main = panel.up('diligenciasExtraMain');
            main.setLoading(true);
            store.load({
                url: '/act/rest/actas/' + actaId + '/actadiligencias/fueraplazo',
                callback: function (records) {
                    var grid = panel.up('diligenciasExtraMain').down('diligenciasExtraGrid');
                    grid.getStore().addFilter(new Ext.util.Filter(
                        {
                            id: 'actaEstudianteId',
                            property: 'actaEstudianteId',
                            value: actaEstudianteId
                        }));
                    main.setLoading(false);
                }
            });

        }

    });
