Ext.define('act.view.diligenciasExtra.GridPendientesController',
    {
        extend: 'Ext.ux.uji.grid.PanelController',
        alias: 'controller.diligenciasExtraGridPendientesController',

        traspasarDiligencia: function (view, rowIndex, colIndex, item, event, record) {
            var view = this.getView().up('diligenciasExtraMain')
            var vm = this.getViewModel();
            var store = vm.getStore('actaDiligenciasStore');

            var invalid;
            for (var i = 0; i < store.getData().length; i++) {
                var record = store.getData().items[i];
                if (!record.isValid()) {
                    invalid = 'La nota introduïda ' + record.get('nota') + ' per al alumne/a ' + record.get('alumnoNombre') + ' no és correcta respecte a la qualificació.';
                    break;
                }
            }

            var data = store.getUpdatedRecords();
            for (var i = 0; i < data.length; i++) {
                var item = data[i];

                if (!item.get('comentario')) {
                    invalid = 'Es obligatori introduïr comentari en la diligència de l\'alumne/a ' + item.get('alumnoNombre');
                    break;
                }

                if (item.get('nota') === item.previousValues.nota && item.get('calificacion') === 5 && item.get('nota')) {
                    continue;
                }

                if (item.get('nota') === item.previousValues.nota) {
                    invalid = 'Es obligatori introduïr nota en la diligència de l\'alumne/a ' + item.get('alumnoNombre');
                    break;
                }
            }

            if (invalid) {
                return Ext.Msg.alert('Error', invalid);
            }

            if (data.length === 0) {
                return;
            }

            var diligenciasStore = Ext.create('Ext.ux.uji.data.Store',
                {
                    model: 'act.model.ActaDiligencia'
                });

            diligenciasStore.loadData(data);
        },

        onDeleteDiligenciaFueraPlazo: function (grid, rowIndex, colIndex, item, ev, record) {
            var grid = this.getView();

            Ext.Msg.confirm('Esborrar', 'Esteu segur/a de voler esborrar aquesta sol·licitud?', function (result) {
                if (result === 'yes') {
                    Ext.Ajax.request(
                        {
                            url: '/act/rest/diligenciasFueraPlazo/' + record.get('id'),
                            method: 'DELETE',
                            success: function (response) {
                                grid.getStore().reload();
                            },
                            scope: this
                        });
                }
            });
        },

        onDenegarDiligenciaFueraPlazo: function (view, rowIndex, colIndex, item, event, record) {
            var view = this.getView().up('diligenciasExtraMain')
            var vm = this.getViewModel();
            var modal =
                {
                    xtype: 'formDiligenciasExtraComentarioDenegacion',
                    viewModel:
                        {
                            data:
                                {
                                    record: record,
                                    title: 'Comentari denegació',
                                    store: vm.getStore('actaDiligenciasGestionPendientesStore')
                                }
                        }
                };

            view.add(modal).show();
        },

        onFirmarDiligenciaExtra: function (diligenciaPendienteId) {
            var grid = this.getView();
            grid.setLoading(true);

            Ext.Msg.confirm('Esborrar', 'Esteu segur/a de voler signar aquesta sol·licitud?', function (result) {
                if (result === 'yes') {
                    Ext.Ajax.request(
                        {
                            url: '/act/rest/diligenciasFueraPlazo/' + diligenciaPendienteId + '/firmar',
                            method: 'PUT',
                            jsonData: {},
                            success: function (response) {
                                grid.getStore().reload();
                                grid.setLoading(false);
                            },
                            failure: function() {
                                grid.setLoading(false);
                            },
                            scope: this
                        });
                }
            });
        },

        onValidarDiligenciaExtra: function (diligenciaPendienteId) {
            var grid = this.getView();

            Ext.Msg.confirm('Esborrar', 'Esteu segur/a de voler validar aquesta sol·licitud?', function (result) {
                if (result === 'yes') {
                    Ext.Ajax.request({
                        url: '/act/rest/diligenciasFueraPlazo/' + diligenciaPendienteId + '/validar',
                        method: 'PUT',
                        jsonData: {},
                        success: function (response) {
                            grid.getStore().reload();
                        },
                        scope: this
                    });
                }
            });
        },

        onEdit: function (re, obj) {
            obj.store.suspendAutoSync();
            return false;
        },

        onBeforeEdit: function () {
            return true;
        },

        onEditComplete: function () {
            return false;
        },

        onDblClick: function () {

        }
    });
