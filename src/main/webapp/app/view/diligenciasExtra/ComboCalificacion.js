Ext.define('act.view.diligenciasExtra.ComboCalificacion',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.diligenciasExtraComboCalificacion',
    width : 240,
    labelAlign: 'right',
    labelWidth : 100,
    displayField : 'nombre',
    valueField : 'id',
    editable : false,
    bind: {
        store: '{calificacionesStore}'
    }
});