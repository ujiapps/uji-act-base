Ext.define('act.view.diligenciasExtra.ComboConvocatoria',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.diligenciasExtraComboConvocatoria',
    fieldLabel : 'Convocatòria',
    showClearIcon : true,
    labelWidth : 100,
    minWidth: 350,
    displayField : 'nombre',
    allowBlank : true,
    bind :
    {
        store : '{convocatoriasOficialesStore}'
    },

    listeners :
    {
        change : function(combo, recordId)
        {
            combo.up('diligenciasExtraPanelFiltros').fireEvent('convocatoriaSelected', recordId);
        }
    }

});