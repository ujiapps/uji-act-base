Ext.define('act.view.diligenciasExtra.ComboEstudiante',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.diligenciasExtraComboEstudiante',
    reference: 'comboEstudiante',
    fieldLabel : 'Alumne/a',
    showClearIcon : true,
    labelWidth : 100,
    typeAhead: true,
    minWidth: 350,
    width: 600,
    editable: true,
    allowBlank: true,
    displayField : 'alumnoNombre',
    valueField: 'actaEstudianteId',
    bind: {
        store: '{actaDiligenciasAlumnosStore}'
    },

    listeners :
    {
        select : function(combo, rec) {
            combo.up('diligenciasExtraPanelFiltros').fireEvent('estudianteSelected', rec.get('actaEstudianteId'));
        }
    }

});