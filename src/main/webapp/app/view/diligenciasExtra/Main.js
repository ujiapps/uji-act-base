Ext.define('act.view.diligenciasExtra.Main',
    {
        extend: 'Ext.tab.Panel',

        alias: 'widget.diligenciasExtraMain',
        title: 'Diligències extraordinàries',

        requires: ['act.view.diligenciasExtra.ViewModel', 'act.view.diligenciasExtra.PanelFiltros', 'act.view.diligenciasExtra.FormDiligenciasExtraComentario', 'act.view.diligenciasExtra.FormDiligenciasExtraComentarioDenegacion',
            'act.view.diligenciasExtra.Grid', 'act.view.diligenciasExtra.GridPendientes', 'act.view.diligenciasExtra.GridGestionPendientes'],

        cls: 'no-border',
        margin: 5,
        viewModel:
            {
                type: 'diligenciasExtraViewModel'
            },
        layout:
            {
                type: 'vbox',
                align: 'stretch'
            },

        items: [{
            xtype: 'panel',
            title: 'Sol·licituts',
            items: [{
                xtype: 'fieldset',
                title: 'Sol·licitud de diligència en període extraordinari',
                items: [
                    {
                        xtype: 'diligenciasExtraPanelFiltros'
                    },
                    {
                        xtype: 'diligenciasExtraGrid',
                        bind:
                            {
                                disabled: '{!cursoYconvocatoriaYActaYEstudianteSeleccionados}'
                            }
                    }]
            },
                {
                    xtype: 'fieldset',
                    title: 'Sol·licitud de diligències extra',
                    flex: 1,
                    items: [
                        {
                            xtype: 'diligenciasExtraGridPendientes'
                        }]
                }]
        },
            {
                xtype: 'panel',
                title: 'Gestió de sol·licituts',
                items: [
                    {
                        xtype: 'fieldset',
                        title: 'Sol·licituds pendents d\'aprovació',
                        flex: 1,
                        items: [
                            {
                                xtype: 'diligenciasExtraGridGestionPendientes'
                            }]
                    }]
            }],
        listeners:
            {
                // render : 'mockupDiligencia',
                actaConvocatoriaSelected: 'onActaConvocatoriaSelected',
                volverASelector: 'onVolverASelector'
            }
    });
