Ext.define('act.view.diligenciasExtra.GridController',
    {
        extend: 'Ext.ux.uji.grid.PanelController',
        alias: 'controller.diligenciasExtraGridController',

        traspasarDiligencia: function (view, rowIndex, colIndex, item, event, record) {
            var view = this.getView().up('diligenciasExtraMain')
            var vm = this.getViewModel();
            var store = vm.getStore('actaDiligenciasStore');
            var storePendientes = vm.getStore('actaDiligenciasPendientesStore');
            var comboEstudiante = view.down('diligenciasExtraComboEstudiante');

            var invalid;
            for (var i = 0; i < store.getData().length; i++) {
                var record = store.getData().items[i];
                if (!record.isValid()) {
                    invalid = 'La nota introduïda ' + record.get('nota') + ' per al alumne/a ' + record.get('alumnoNombre') + ' no és correcta respecte a la qualificació.';
                    break;
                }
            }

            var data = store.getUpdatedRecords();
            for (var i = 0; i < data.length; i++) {
                var item = data[i];

                if (!item.get('comentario')) {
                    invalid = 'Es obligatori introduïr comentari en la diligència de l\'alumne/a ' + item.get('alumnoNombre');
                    break;
                }

                if (item.get('nota') === item.previousValues.nota && item.get('calificacion') === 5 && item.get('nota')) {
                    continue;
                }

                if (item.get('nota') === item.previousValues.nota) {
                    invalid = 'Es obligatori introduïr nota en la diligència de l\'alumne/a ' + item.get('alumnoNombre');
                    break;
                }
            }

            if (invalid) {
                return Ext.Msg.alert('Error', invalid);
            }

            if (data.length === 0) {
                return;
            }

            var record = store.getData().items[0];

            Ext.Ajax.request(
                {
                    url: '/act/rest/diligenciasFueraPlazo',
                    method: 'POST',
                    jsonData:
                        {
                            actaEstudianteId: record.get('actaEstudianteId'),
                            nota: record.get('nota'),
                            comentario: record.get('comentario'),
                            calificacionId: record.get('calificacionId')
                        },
                    success: function (response) {
                        store.removeAll();
                        comboEstudiante.clearValue();
                        storePendientes.reload();
                    },
                    scope: this
                });

        },

        checkNotaEditable: function (editor, context) {
            var rec = context.record;
            return rec.get('editableFueraPlazo');
        },

        onAddComentario: function (view, rowIndex, colIndex, item, event, record) {
            if (record.get('motivoExclusion')) {
                return;
            }

            var view = this.getView().up('diligenciasExtraMain')
            var vm = this.getViewModel();
            var modal =
                {
                    xtype: 'formDiligenciasExtraComentario',
                    viewModel:
                        {
                            data:
                                {
                                    record: record,
                                    title: 'Comentari: ' + record.get('alumnoNombre'),
                                    store: vm.getStore('diligenciasStore')
                                }
                        }
                };

            view.add(modal).show();
        },

        onEdit: function (re, obj) {
            obj.store.suspendAutoSync();
            return false;
        },

        onBeforeEdit: function () {
            return true;
        },

        onEditComplete: function () {
            return false;
        },

        onDblClick: function () {

        }
    });
