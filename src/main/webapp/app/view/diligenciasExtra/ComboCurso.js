Ext.define('act.view.diligenciasExtra.ComboCurso',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.diligenciasExtraComboCurso',
    fieldLabel : 'Curs',
    showClearIcon : true,
    width : 220,
    labelWidth : 100,
    fieldStyle: 'padding-right: 30px',
    requires : [ 'act.store.Cursos' ],

    displayField : 'id',

    bind: {
        store: '{cursosFueraPlazoStore}'
    },

    listeners :
    {
        change : function(combo, recordId)
        {
            combo.up('diligenciasExtraPanelFiltros').fireEvent('cursoSelected', recordId);
        }
    }
});