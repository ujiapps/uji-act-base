Ext.define('act.view.diligenciasExtra.GridPendientes',
    {
        extend: 'Ext.ux.uji.grid.Panel',

        alias: 'widget.diligenciasExtraGridPendientes',
        allowEdit: false,
        flex: 1,
        scrollable: true,
        emptyText: 'No n\'hi han sol·licituds pendents',
        bind:
            {
                store: '{actaDiligenciasPendientesStore}'
            },

        requires: ['Ext.grid.plugin.CellEditing', 'act.view.diligenciasExtra.GridPendientesController', 'Ext.ux.grid.SubTable', 'act.view.diligenciasExtra.ComboCalificacion'],
        controller: 'diligenciasExtraGridPendientesController',
        border: 0,
        itemConfig: {
            viewModel: true
        },
        columns: [
            {
                text: 'ID',
                dataIndex: 'id',
                hidden: true
            },
            {
                dataIndex: 'index',
                width: 45,
                sortable: false,
                align: 'right',
                renderer: function (value, metaData, record, rowIndex) {
                    return rowIndex + 1;
                }
            },
            {
                dataIndex: 'estado',
                text: 'Estat',
                hidden: true
            },
            {
                xtype : 'datecolumn',
                format : 'd/m/Y',
                dataIndex : 'fecha',
                dateFormat : 'd/m/Y',
                text : 'Data sol·licitud',
                align : 'center'
            },
            {
                xtype : 'datecolumn',
                format : 'd/m/Y',
                dataIndex : 'fechaModeracion',
                dateFormat : 'd/m/Y',
                text : 'Data moderació',
                align : 'center'
            },
            {
                dataIndex: 'estadoTxt',
                text: 'Estat',
                align: 'center',
                filter: 'string'
            },
            {
                dataIndex: 'diligencia',
                text: 'Diligència',
                align: 'center',
                filter: 'string',
                flex: 1
            },
            {
                dataIndex: 'notaNueva',
                text: 'Nota',
                align: 'center',
                xtype: 'numbercolumn',
                width: 80,
                bind:
                    {
                        disabled: '{!selectedActa.editable}'
                    },
                renderer: function (value, meta) {
                    meta.tdCls = 'editor-visible';
                    return value === null ? null : Math.trunc(value * 10) / 10;

                    var vm = this.getView().up('diligenciasExtraMain').getViewModel();
                    var store = vm.getStore('calificacionesStore');
                    var calificacionAnterior = store.findRecord('id', rec.get('calificacionAnteriorId'), 0, false, false, true);

                    if (rec.get('nota') === null || rec.get('noPresentado') === true) {
                        calificacionAnterior = "No presentat";
                    }

                    return value === null ? null : Math.trunc(value * 10) / 10 + " (" + calificacionAnterior.get('nombre') + ")";

                }
            },
            {
                dataIndex: 'calificacionNuevaId',
                text: 'Qualificació',
                align: 'center',
                filter: 'list',
                renderer: function (value) {
                    var vm = this.getView().up('diligenciasExtraMain').getViewModel();
                    var store = vm.getStore('calificacionesStore');

                    var record = store.findRecord('id', value, 0, false, false, true);
                    return record ? record.get('nombre') : '';

                }
            },
            {
                xtype: 'actioncolumn',
                text: 'Comentari',
                align: 'center',
                dataIndex: 'comentarioDiligencia',
                width: 100,
                menuDisabled: true,
                sortable: false,
                items: [
                    {
                        handler: 'onAddComentario',
                        getTip: function (value, metadata) {
                            return Ext.util.Format.htmlEncode(value);
                        },
                        getClass: function (comentario) {
                            return 'x-fa fa-file-text-o';
                        },
                        width: 180
                    }]
            },
            {
                xtype: 'actioncolumn',
                text: 'Comentari denegació',
                align: 'center',
                dataIndex: 'comentarioDenegacion',
                width: 160,
                menuDisabled: true,
                sortable: false,
                items: [
                    {
                        getTip: function (value, metadata) {
                            return Ext.util.Format.htmlEncode(value);
                        },
                        getClass: function (comentario) {
                            return comentario ? 'x-fa fa-file-text-o' : 'x-hide-display';
                        },
                        width: 180
                    }]
            },
            {
                text: 'Accions',
                width: 120,
                align: 'center',
                stopSelection: true,
                xtype: 'widgetcolumn',
                widget:
                    {
                        xtype: 'button',
                        _btnText: "Signar",
                        width: 80,
                        bind: {
                            hidden: '{record.estado !== 1}'
                        },
                        defaultBindProperty: null, //important
                        handler: function (widgetColumn) {
                            var record = widgetColumn.getWidgetRecord();
                            if (record.get('estado') === 1) {
                                this.up('diligenciasExtraGridPendientes').fireEvent('firmarDiligenciaExtra', record.get('id'));
                            }
                        },
                        listeners:
                            {
                                beforerender: function (widgetColumn) {
                                    var record = widgetColumn.getWidgetRecord();
                                    widgetColumn.setText(widgetColumn._btnText); //can be mixed with the row data if needed
                                }
                            }
                    }
            },
            {
                xtype: 'actioncolumn',
                name: 'borrarDiligenciaExtraordinaria',
                dataIndex: 'id',
                text: 'Cancel·lar',
                align: 'center',
                width: 100,
                items: [
                    {
                        tooltip: 'Esborrar',
                        handler: 'onDeleteDiligenciaFueraPlazo',
                        getClass: function (convocatoriaId, meta, rec) {
                            return rec.get('estado') === 0 ? 'x-fa fa-remove': 'x-hide-display';
                        }
                    }]
            }],


        listeners:
            {
                activate: 'onActivate',
                grupoSelected: 'onGrupoSelected',
                actaConvocatoriaSelected: 'onActaConvocatoriaSelected',
                firmarDiligenciaExtra: 'onFirmarDiligenciaExtra',
                asignaturaSelected: 'onAsignaturaSelected',
                render: 'onLoad',
                botonActaClick: 'onBotonActaClick',
                beforeedit: 'onBeforeEdit',
                edit: 'onEdit'
            }
    });