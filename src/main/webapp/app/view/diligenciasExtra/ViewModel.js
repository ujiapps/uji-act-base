Ext.define('act.view.diligenciasExtra.ViewModel',
    {
        extend: 'Ext.app.ViewModel',
        alias: 'viewmodel.diligenciasExtraViewModel',
        requires: ['act.model.ActaAsignatura', 'act.model.ActaEstudiante', 'act.model.ActaPersona'],
        formulas:
            {
                cursoYconvocatoriaSeleccionados: function (get) {
                    return get('comboConvocatoria.selection') && get('comboCurso.selection');
                },
                cursoYconvocatoriaYActaSeleccionados: function (get) {
                    return get('comboConvocatoria.selection') && get('comboCurso.selection') && get('comboActa.selection');
                },
                cursoYconvocatoriaYActaYEstudianteSeleccionados: function (get) {
                    return get('comboConvocatoria.selection') && get('comboCurso.selection') && get('comboActa.selection') && get('comboEstudiante.selection');
                },
                selectedActa:
                    {
                        bind:
                            {
                                bindTo: '{comboActa.selection}',
                                deep:
                                    true
                            }
                        ,
                        get: function (sel) {
                            return sel;
                        }
                        ,
                        set: function (acta) {
                            this.set('selectedActa', acta);
                        }
                    },
                selectedEstudiante:
                    {
                        bind:
                            {
                                bindTo: '{comboEstudiante.selection}',
                                deep:
                                    true
                            }
                        ,
                        get: function (sel) {
                            return sel;
                        }
                        ,
                        set: function (acta) {
                            this.set('selectedActa', acta);
                        }
                    }

            },
        stores:
            {
                cursosFueraPlazoStore: Ext.create('Ext.ux.uji.data.Store',
                    {
                        model: 'act.model.Curso',
                        url: '/act/rest/cursos/diligenciasFueraPlazo',
                        autoLoad: true
                    }),
                actaAsignaturasStore:
                    Ext.create('Ext.ux.uji.data.Store',
                        {
                            model: 'act.model.ActaAsignatura',
                            url: '/act/rest/actaAsignaturas',
                            autoLoad: false
                        }),
                convocatoriasOficialesStore:
                    Ext.create('Ext.ux.uji.data.Store',
                        {
                            model: 'act.model.Convocatoria',
                            url: '/act/rest/convocatorias/oficiales',
                            autoLoad: true
                        }),
                actaDiligenciasAlumnosStore:
                    Ext.create('Ext.ux.uji.data.Store',
                        {
                            model: 'act.model.ActaDiligencia',
                            url: '/act/rest/actadiligencias',
                            sorters: ['alumnoNombre'],
                            autoLoad: false,
                            autoSync: false
                        }),
                actaDiligenciasStore:
                    Ext.create('Ext.ux.uji.data.Store',
                        {
                            model: 'act.model.ActaDiligencia',
                            url: '/act/rest/actadiligencias',
                            sorters: ['alumnoNombre'],
                            autoLoad: false,
                            autoSync: false
                        }),
                actaDiligenciasPendientesStore:
                    Ext.create('Ext.ux.uji.data.Store',
                        {
                            model: 'act.model.DiligenciaFueraPlazo',
                            url: '/act/rest/diligenciasFueraPlazo',
                            sorters: ['caducada', 'estado', 'fecha'],
                            autoLoad: true,
                            autoSync: false
                        }),

                actaDiligenciasGestionPendientesStore:
                    Ext.create('Ext.ux.uji.data.Store',
                        {
                            model: 'act.model.DiligenciaFueraPlazo',
                            url: '/act/rest/diligenciasFueraPlazo/moderacion',
                            sorters: ['caducada', 'estado', 'fecha'],
                            autoLoad: true,
                            autoSync: false
                        }),

                actasStore:
                    Ext.create('Ext.ux.uji.data.Store',
                        {
                            model: 'act.model.ActaDepartamento',
                            url: '/act/rest/actas/admin',
                            autoLoad: false,
                            sorters: ['codigo']
                        }),
                calificacionesStore:
                    Ext.create('act.store.Calificaciones'),
                actasPersonaStore:
                    Ext.create('Ext.ux.uji.data.Store',
                        {
                            model: 'act.model.ActaPersona',
                            url: '/act/rest/actas',
                            autoLoad: false,
                            queryModel: 'local',
                            sorters: ['descripcion']
                        })
            }
    })
;
