Ext.define('act.view.diligenciasExtra.GridGestionPendientes',
    {
        extend: 'Ext.ux.uji.grid.Panel',

        alias: 'widget.diligenciasExtraGridGestionPendientes',
        allowEdit: false,
        flex: 1,
        scrollable: true,
        emptyText: 'No tens cap sol·licitud pendent',
        bind:
            {
                store: '{actaDiligenciasGestionPendientesStore}'
            },

        requires: ['Ext.grid.plugin.CellEditing', 'act.view.diligenciasExtra.GridPendientesController', 'Ext.ux.grid.SubTable', 'act.view.diligenciasExtra.ComboCalificacion'],
        controller: 'diligenciasExtraGridPendientesController',
        border: 0,
        columns: [
            {
                text: 'ID',
                dataIndex: 'id',
                hidden: true
            },
            {
                dataIndex: 'index',
                width: 45,
                sortable: false,
                align: 'right',
                renderer: function (value, metaData, record, rowIndex) {
                    return rowIndex + 1;
                }
            },
            {
                dataIndex: 'estadoTxt',
                text: 'Estat',
                align: 'center',
                filter: 'string'
            },
            {
                dataIndex: 'solicitante',
                text: 'Sol·licitant',
                align: 'center',
                filter: 'string',
                flex: 1
            },
            {
                dataIndex: 'diligencia',
                text: 'Diligència',
                align: 'center',
                filter: 'string',
                flex: 2
            },
            {
                dataIndex: 'notaNueva',
                text: 'Nota',
                align: 'center',
                xtype: 'numbercolumn',
                width: 150,
                bind:
                    {
                        disabled: '{!selectedActa.editable}'
                    },
                renderer: function (value, meta, rec) {
                    var vm = this.getView().up('diligenciasExtraMain').getViewModel();
                    var store = vm.getStore('calificacionesStore');
                    var calificacion = store.findRecord('id', rec.get('calificacionNuevaId'), 0, false, false, true);
                    var calificacionStr = (rec.get('notaNueva') === null || rec.get('noPresentado') === true) ? "No presentat" : calificacion.get('nombre');
                    return value === null ? null : Math.trunc(value * 10) / 10 + " (" + calificacionStr + ")";
                }
            },
            {
                dataIndex: 'notaAnterior',
                text: 'Nota anterior',
                align: 'center',
                xtype: 'numbercolumn',
                width: 150,
                bind:
                    {
                        disabled: '{!selectedActa.editable}'
                    },
                renderer: function (value, meta, rec) {
                    var vm = this.getView().up('diligenciasExtraMain').getViewModel();
                    var store = vm.getStore('calificacionesStore');
                    var calificacion = store.findRecord('id', rec.get('calificacionAnteriorId'), 0, false, false, true);
                    var calificacionStr = (rec.get('notaAnterior') === null || rec.get('calificacionAnteriorId') === null || rec.get('noPresentado') === true) ? "No presentat" : calificacion.get('nombre');

                    return value === null ? null : Math.trunc(value * 10) / 10 + " (" + calificacionStr + ")";
                }
            },
            {
                xtype: 'actioncolumn',
                text: 'Comentaris',
                align: 'center',
                dataIndex: 'comentarioDiligencia',
                width: 100,
                menuDisabled: true,
                sortable: false,
                items: [
                    {
                        handler: 'onAddComentario',
                        getTip: function (value, metadata) {
                            return value ? Ext.util.Format.htmlEncode(value) : 'Afegir comentari';
                        },
                        getClass: function (comentario) {
                            return comentario ? 'x-fa fa-file-text-o' : 'x-fa fa-plus';
                        },
                        width: 180
                    },
                ]
            },
            {
                text: 'Accions',
                width: 100,
                align: 'center',
                stopSelection: true,
                xtype: 'widgetcolumn',
                widget:
                    {
                        xtype: 'button',
                        _btnText: "Acceptar",
                        width: 80,
                        defaultBindProperty: null, //important
                        handler: function (widgetColumn) {
                            var record = widgetColumn.getWidgetRecord();
                            if (record.get('estado') === 0) {
                                this.up('diligenciasExtraGridGestionPendientes').fireEvent('validarDiligenciaExtra', record.get('id'));
                            }
                        },
                        listeners:
                            {
                                beforerender: function (widgetColumn) {
                                    var record = widgetColumn.getWidgetRecord();
                                    widgetColumn.setText(widgetColumn._btnText); //can be mixed with the row data if needed
                                }
                            }
                    }
            },
            {
                xtype: 'actioncolumn',
                name: 'denegarDiligenciaExtraordinaria',
                dataIndex: 'id',
                align: 'center',
                width: 50,
                items: [
                    {
                        iconCls: 'x-fa fa-ban rediconcolor',
                        tooltip: 'Denegar',
                        handler: 'onDenegarDiligenciaFueraPlazo'
                    }]
            }],


        listeners:
            {
                activate: 'onActivate',
                grupoSelected: 'onGrupoSelected',
                actaConvocatoriaSelected: 'onActaConvocatoriaSelected',
                validarDiligenciaExtra: 'onValidarDiligenciaExtra',
                asignaturaSelected: 'onAsignaturaSelected',
                render: 'onLoad',
                botonActaClick: 'onBotonActaClick',
                beforeedit: 'onBeforeEdit',
                edit: 'onEdit'
            }
    });