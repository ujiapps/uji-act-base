Ext.define('act.view.diligenciasExtra.FormDiligenciasExtraComentarioDenegacion',
    {
        extend: 'Ext.window.Window',
        alias: 'widget.formDiligenciasExtraComentarioDenegacion',

        width: 640,
        manageHeight: true,
        modal: true,
        bodyPadding: 10,
        layout:
            {
                type: 'vbox',
                align: 'stretch'
            },

        bind: {
            title: '{title}'
        },

        requires: ['act.view.diligenciasExtra.FormDiligenciasExtraComentarioDenegacionController'],
        controller: 'formDiligenciasExtraComentarioDenegacionController',

        items: [
            {
                xtype: 'form',
                layout: 'anchor',
                items: [
                    {
                        xtype: 'textarea',
                        name: 'comentarioDenegacion',
                        allowBlank: true,
                        anchor: '100%',
                        padding: 10,
                        flex: 1
                    }]
            }],

        bbar: ['->', '->',
            {
                xtype: 'button',
                text: 'Denegar',
                handler: 'onDenegar'
            },
            {
                xtype: 'panel',
                html: '<a style="text-decoration: none; color: #222;" href="#">Cancel·lar</a>',
                listeners:
                    {
                        render: function (component) {
                            component.getEl().on('click', 'onClose');
                        }
                    }
            }]
    });