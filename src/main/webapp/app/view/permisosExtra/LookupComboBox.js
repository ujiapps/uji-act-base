Ext.define('act.view.permisosExtra.LookupComboBox',
{
    extend : 'Ext.form.ComboBox',
    alias : 'widget.ujilookupcombobox',

    //TODO: ¿Vale la pena heredar del combo uji?
    mode : 'local',
    triggerAction : 'all',
    editable : false,
    valueField : 'id',
    displayField : 'nombre',
    store : {},
    extraFields : [],
    showIdField: true,
    showNombreField: true,
    appPrefix : '',
    bean : 'base',
    windowTitle : 'Cercar registres',
    windowLayout : 'border',
    windowModal : true,
    windowHidden : true,
    windowWidth : 400,
    windowHeight : 400,
    windowCloseAction : 'hide',
    windowClearAfterSearch : false,
    windowHelpText: '',
    autoSearchString: '',
    minChars: 3,
    windowWidthIdField: 50,
    showClearIcon: false,
    filterParams: null,


    listConfig :
    {
        maxHeight: 0
    },

    triggers: {
        clear: {
            cls: 'x-form-clear-trigger',
            handler: function (cmp) {
                cmp.clearValue();
            },
            scope: 'this',
            weight: -1
        }
    },

    initEvents: function () {
        this.callParent(arguments);

        var trigger = this.getTrigger("clear");
        trigger.hide();

        this.addManagedListener(this.bodyEl, 'mouseover', function () {
            if (this.showClearIcon || this.allowBlank) {
                trigger.show();
            }
        }, this);

        this.addManagedListener(this.bodyEl, 'mouseout', function () {
            trigger.hide();
        });
    },

    initComponent : function()
    {
        this.store = Ext.create('Ext.data.ArrayStore',
        {
            fields : [ 'id', this.displayField ],
            data : [ [ '', '' ] ]
        });

        this.on('expand', function(combo)
        {
            if (!this.lookupWindow)
            {
                this.lookupWindow = this.getLookupWindow(combo);
            }

            this.lookupWindow.show();
        });

        this.oldWidth = this.width;

        this.callParent(arguments);
    },

    executeSearch : function(grid, query)
    {
        grid.store.load(
        {
            query : query,
            bean : this.bean
        });
    },

    removeStore : function()
    {
        this.store.removeAll();
        this.clearValue();
        this.store.loadData([ [ '', '' ] ]);
    },

    loadRecord : function(id, nombre)
    {
        this.store.removeAll();
        this.clearValue();

        this.store.loadData([ [ id, nombre ] ]);

        this.setValue(id);
    },

    getLookupWindow : function(combo)
    {
        var ref = this;
        var window = Ext.create('act.view.permisosExtra.LookupWindow',
        {
            bean : ref.bean,
            appPrefix : ref.appPrefix,
            extraFields : ref.extraFields,
            showIdField : ref.showIdField,
            showNombreField : ref.showNombreField,
            width : ref.windowWidth,
            height : ref.windowHeight,
            modal : ref.windowModal,
            title : ref.windowTitle,
            layout : ref.windowLayout,
            hidden : ref.windowHidden,
            closeAction : ref.windowCloseAction,
            clearAfterSearch : ref.windowClearAfterSearch,
            minChars: ref.minChars,
            widthIdField: ref.windowWidthIdField,
            filterParams: ref.filterParams,
            autoSearchString : ref.autoSearchString,
            helpText : ref.windowHelpText,
        });

        window.on('LookoupWindowClickSeleccion', function(record)
        {
            combo.store.removeAll();
            combo.clearValue();

            combo.store.add([ record.data ]);
            combo.store.commitChanges();

            combo.setValue(record.data[combo.valueField || 'id']);
        });

        this.relayEvents(window, [ 'LookoupWindowClickSeleccion' ]);

        window.show();

        return window;
    }
});
