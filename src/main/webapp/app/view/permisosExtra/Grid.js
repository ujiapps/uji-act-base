Ext.define('act.view.permisosExtra.Grid',
    {
        extend: 'Ext.ux.uji.grid.Panel',
        allowEdit: false,

        alias: 'widget.permisosExtraGrid',
        flex: 1,
        scrollable: true,
        controller: 'permisosExtraGridController',
        bind:
            {
                store: '{permisosExtraStore}'
            },

        emptyText: 'Selecciona un curs i una convocatoria',

        tbar: [

            {
                xtype: 'button',
                iconCls: 'fa fa-remove',
                text: 'Esborrar permís',
                handler: 'onDelete'
            }],

        border: 0,
        columns: [
            {
                dataIndex: 'id',
                hidden: true
            },
            {
                dataIndex: 'cursoAcademicoId',
                text: 'Curs',
                align: 'center'
            },
            {
                dataIndex: 'asignaturaId',
                text: 'Assignatura',
                align: 'center'
            },
            {
                dataIndex: 'nombreCompleto',
                text: 'Persona',
                flex: 1
            },
            {
                dataIndex: 'fecha',
                xtype: 'datecolumn',
                width: 240,
                format: 'd/m/Y',
                text: 'Data de creació del permís',
                align: 'center'
            },
        ],
        listeners:
            {
                updateElementCount: 'updateElementCount',
                render: 'updateElementCount',
            }
    });