Ext.define('act.view.permisosExtra.MainController',
    {
        extend: 'Ext.ux.uji.grid.PanelController',
        alias: 'controller.permisosExtraMainController',

        onLoad: function () {
            var vm = this.getViewModel();
            var premisosExtrasStore = vm.get('permisosExtraStore');

            var panel = this.getView();

            panel.setLoading(true);
            premisosExtrasStore.load({
                callback: function (data) {
                    panel.setLoading(false);
                }
            });
        },


        loadActas: function (cursoId, convocatoriaId) {
            var panel = this.getView();
            var vm = this.getViewModel();
            var store = this.getStore('actasStore');
            store.getProxy().url = '/act/rest/cursos/' + cursoId + '/actas/traspasadas';
            panel.setLoading(true)
            store.load(
                {
                    params:
                        {
                            convocatoriaId: convocatoriaId
                        },
                    callback: function () {
                        panel.setLoading(false);
                    }
                });
        },

        onAdd: function () {
            var panel = this.getView();
            var grid = panel.down('permisosExtraGrid')

            var cursoAcademicoId = panel.down('permisosExtraComboCurso').getValue();
            var convocatoriaId = panel.down('permisosExtraComboConvocatoria').getValue();
            var asignaturaId = panel.down('permisosExtraComboActa').getValue();
            var personaId = panel.down('ujilookupcombobox').getValue();

            console.log(cursoAcademicoId, convocatoriaId, asignaturaId, personaId);
            grid.getStore().add({
                cursoAcademicoId: cursoAcademicoId,
                asignaturaId: asignaturaId,
                personaId: personaId
            });

            grid.getStore().sync({
                success: function () {
                    grid.getStore().reload();
                },
                scope: this
            })
        },

        onConvocatoriaSelected: function (convocatoriaId) {
            var panel = this.getView();
            var vm = this.getViewModel();

            var comboActa = panel.down('permisosExtraComboActa');
            comboActa.reset();

            if (!convocatoriaId) {
                return comboActa.getStore().removeAll();
            }

            var comboCurso = vm.get('comboCurso.selection');
            if (comboCurso) {
                this.loadActas(comboCurso.get('id'), convocatoriaId);
            }
        },

        onCursoSelected: function (cursoId) {
            var panel = this.getView();
            var vm = this.getViewModel();
            var comboActa = panel.down('permisosExtraComboActa');
            comboActa.reset();

            if (!cursoId) {
                return comboActa.getStore().removeAll();
            }

            var comboConvocatoria = vm.get('comboConvocatoria.selection');
            if (comboConvocatoria) {
                this.loadActas(cursoId, comboConvocatoria.get('id'));
            }

        },

    });
