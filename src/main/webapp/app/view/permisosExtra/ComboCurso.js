Ext.define('act.view.permisosExtra.ComboCurso',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.permisosExtraComboCurso',
    fieldLabel : 'Curs',
    showClearIcon : true,
    width : 180,
    labelWidth : 60,
    allowBlank : true,
    displayField : 'id',

    bind :
    {
        store : '{cursosStore}'
    },
    listeners :
    {
        change : function(combo, cursoId)
        {
            combo.up("permisosExtraMain").fireEvent('cursoSelected', cursoId);
        }
    }

});