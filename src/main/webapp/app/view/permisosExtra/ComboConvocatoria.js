Ext.define('act.view.permisosExtra.ComboConvocatoria',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.permisosExtraComboConvocatoria',
    fieldLabel : 'Convocatoria',
    showClearIcon : true,
    labelWidth : 100,
    minWidth: 350,
    displayField : 'nombre',
    allowBlank : true,
    bind :
    {
        store : '{convocatoriasOficialesStore}'
    },

    listeners :
    {
        change : function(combo, recordId)
        {
            combo.up('permisosExtraMain').fireEvent('convocatoriaSelected', recordId);
        }
    }

});