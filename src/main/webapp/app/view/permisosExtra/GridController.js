Ext.define('act.view.permisosExtra.GridController',
    {
        extend: 'Ext.ux.uji.grid.PanelController',
        alias: 'controller.permisosExtraGridController',

        onReload: function () {
            var grid = this.getView();

            grid.getStore().reload();
        },

    });
