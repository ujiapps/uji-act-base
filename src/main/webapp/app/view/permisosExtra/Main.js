Ext.define('act.view.permisosExtra.Main',
    {
        extend: 'Ext.panel.Panel',

        alias: 'widget.permisosExtraMain',
        title: 'Gestió de permisos extraordinaris',

        requires: ['act.view.permisosExtra.Grid', 'act.view.permisosExtra.MainController', 'act.view.permisosExtra.GridController', 'act.view.permisosExtra.ViewModel',
            'act.view.permisosExtra.ComboCurso', 'act.view.permisosExtra.ComboActa', 'act.view.permisosExtra.ComboConvocatoria', 'act.view.permisosExtra.LookupComboBox'],
        controller: 'permisosExtraMainController',

        cls: 'no-border',
        margin: 5,
        viewModel:
            {
                type: 'permisosExtraViewModel'
            },
        layout:
            {
                type: 'vbox',
                align: 'stretch'
            },

        items: [
            {
                xtype: 'container',
                padding: 2,
                layout: 'anchor',
                items: [
                    {
                        xtype: 'fieldset',
                        title: 'Afegir nou permís',
                        items: [
                            {
                                xtype: 'fieldcontainer',
                                layout: 'hbox',
                                items: [
                                    {
                                        xtype: 'permisosExtraComboCurso',
                                        labelWidth: 70,
                                        reference: 'comboCurso',
                                        padding: '0 10 0 0'
                                    },
                                    {
                                        xtype: 'permisosExtraComboConvocatoria',
                                        reference: 'comboConvocatoria',
                                        padding: '0 10 0 0',
                                        bind:
                                            {
                                                disabled: '{!comboCurso.selection}'
                                            }
                                    },
                                ]
                            },
                            {
                                xtype: 'fieldcontainer',
                                layout: 'hbox',
                                items: [{
                                    xtype: 'permisosExtraComboActa',
                                    reference: 'comboActa',
                                    labelWidth: 70,
                                    flex: 1,
                                    padding: '0 20 0 0',
                                    bind:
                                        {
                                            disabled: '{!comboConvocatoria.selection}'
                                        }
                                },
                                    {
                                        xtype: 'ujilookupcombobox',
                                        fieldLabel: 'PDI',
                                        reference: 'comboResponsable',
                                        labelWidth: 40,
                                        padding: '0 20 0 0',
                                        flex: 1,
                                        name: 'personaInNombre',
                                        bean: 'pdi-activo',
                                        windowClearAfterSearch: true,
                                        listeners: [{
                                            LookoupWindowClickSeleccion: 'onSelectResponsableIn'
                                        }],
                                        bind:
                                            {
                                                disabled: '{!comboActa.selection}'
                                            }
                                    },
                                    {
                                        xtype: 'button',
                                        text: 'Afegir permís',
                                        iconCls: 'fa fa-plus',
                                        handler: 'onAdd',
                                        bind:
                                            {
                                                disabled: '{!comboResponsable.selection}'
                                            }

                                    }]
                            }]

                    }],
            },
            {
                xtype: 'permisosExtraGrid',
                flex: 1
            }],
        listeners:
            {
                render: 'onLoad',
                cursoSelected: 'onCursoSelected',
                convocatoriaSelected: 'onConvocatoriaSelected',
                onAdd: 'onAdd'
            }
    });
