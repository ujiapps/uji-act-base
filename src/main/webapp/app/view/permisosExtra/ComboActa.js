Ext.define('act.view.permisosExtra.ComboActa',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.permisosExtraComboActa',
    fieldLabel : 'Acta',
    showClearIcon : true,
    labelWidth : 40,
    typeAhead: true,
    editable: true,
    anchor: '50%',
    matchFieldWidth: false,
    displayField : 'codigo',
    valueField: 'codigo',

    bind: {
        store: '{actasStore}'
    },

    listeners :
    {
        beforequery: function(record){
            record.query = new RegExp(record.query, 'i');
            record.forceAll = true;
        }
    }

});