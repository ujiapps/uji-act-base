Ext.define('act.view.permisosExtra.ViewModel',
    {
        extend: 'Ext.app.ViewModel',
        alias: 'viewmodel.permisosExtraViewModel',
        requires: ['act.model.ActaAsignatura', 'act.model.ActaPersona', 'act.model.PermisosExtra'],
        stores:
            {
                cursosStore: Ext.create('Ext.ux.uji.data.Store',
                    {
                        model: 'act.model.Curso',
                        url: '/act/rest/cursos',
                        autoLoad: true
                    }),
                actasStore: Ext.create('Ext.ux.uji.data.Store',
                    {
                        model: 'act.model.Acta',
                        url: '/act/rest/actas/traspasadas',
                        autoLoad: false,
                        queryModel: 'local'
                    }),
                convocatoriasOficialesStore:
                    Ext.create('Ext.ux.uji.data.Store',
                        {
                            model: 'act.model.Convocatoria',
                            url: '/act/rest/convocatorias/oficiales',
                            autoLoad: true
                        }),

                permisosExtraStore: Ext.create('Ext.ux.uji.data.Store',
                    {
                        model: 'act.model.PermisosExtra',
                        url: '/act/rest/permisosExtra',
                        autoLoad: false,
                        queryModel: 'local'
                    }),

            }
    });
