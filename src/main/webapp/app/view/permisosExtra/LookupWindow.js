Ext.define('act.view.permisosExtra.LookupWindow',
    {
        extend: 'Ext.Window',

        alias: 'widget.ujilookupWindow',
        appPrefix: '',
        bean: 'base',
        lastQuery: '',
        queryField: '',
        formularioBusqueda: '',
        gridBusqueda: '',
        botonBuscar: '',
        botonCancelar: '',
        extraFields: [],
        showIdField: true,
        showNombreField: true,
        title: 'Cercar registres',
        layout: 'border',
        modal: true,
        hidden: true,
        width: 400,
        height: 400,
        closeAction: 'hide',
        clearAfterSearch: true,
        minChars: 3,
        widthIdField: 50,
        filterParams: null,
        padding: '10 0 0 0',
        helpText: '',

        store:null,

        initComponent: function () {
            this.callParent(arguments);
            this.initUI();
            this.add(this.formularioBusqueda);
            this.add(this.gridBusqueda);

//        this.addEvents('LookoupWindowClickSeleccion');
        },

        initUI: function () {
            this.buildQueryField();
            this.buildBotonBuscar();
            this.buildFormularioBusqueda();
            this.buildBotonCancelar();
            this.buildBotonSeleccionar();
            this.buildGridBusqueda();
            this.buildHelpText();
        },

        executeSearch: function (query) {

            var filter = this.getFilter(this.filterParams);
            if (filter.error && filter.error.length > 0) {
                Ext.MessageBox.show({
                    title: 'Atenció',
                    msg: filter.error,
                    icon: Ext.MessageBox.ERROR
                });
            } else {
                this.gridBusqueda.store.load({
                    params: {
                        query: query,
                        filter: Ext.encode(filter),
                        bean: this.bean
                    }
                });
            }

        },

        buildQueryField: function () {
            var ref = this;
            this.queryField = Ext.create('Ext.form.TextField',
                {
                    name: 'query',
                    value: '',
                    listeners: {
                        specialkey: function (field, e) {
                            if (e.getKey() == e.ENTER) {
                                ref.botonBuscar.handler.call(ref.botonBuscar.scope);
                            }
                        }
                    }
                });
        },

        buildBotonBuscar: function () {
            var ref = this;
            this.botonBuscar = Ext.create('Ext.Button',
                {
                    text: 'Cerca',
                    handler: function (boton, event) {
                        if (ref.queryField.getValue().length < ref.minChars) {
                            Ext.Msg.alert("Error", "Per fer una cerca cal introduir al menys " + ref.minChars + " caracters.");
                        } else {
                            ref.lastQuery = ref.queryField.getValue();
                            ref.executeSearch(ref.queryField.getValue());
                        }
                    }
                });
        },

        buildFormularioBusqueda: function () {
            this.formularioBusqueda = Ext.create('Ext.Panel',
                {
                    layout: 'hbox',
                    region: 'north',
                    height: 45,
                    // padding: '5 0 0 0',
                    items: [
                        {
                            xtype: 'label',
                            text: 'Expressió: ',
                            width: 100,
                            padding: '5 0 0 10',
                            border: false,
                        }, this.queryField, this.botonBuscar
                    ]
                });
        },

        buildBotonCancelar: function () {
            this.botonCancelar = Ext.create('Ext.panel.Panel', {
                xtype: 'panel',
                html: '<a style="text-decoration: none; color: #222;" href="#">Cancel·lar</a>',
                listeners: {
                    render: function (component) {
                        var ref = this;
                        component.getEl().on('click', function () {
                            ref.up('window').close();
                        });
                    }
                }
            });
        },

        buildBotonSeleccionar: function () {
            var ref = this;

            this.botonSeleccionar = Ext.create('Ext.Button',
                {
                    text: 'Seleccionar',
                    handler: function (e) {
                        var record = ref.gridBusqueda.getSelectionModel().getSelection()[0];

                        if (record) {
                            ref.fireEvent('LookoupWindowClickSeleccion', record);
                            ref.hide();
                        }
                    }
                });
        },

        buildGridBusqueda: function () {
            var ref = this;

            var resultColumnList = [{
                header: 'Codi',
                width: ref.widthIdField,
                hidden: !ref.showIdField,
                dataIndex: 'id'
            }, {
                header: 'Nom',
                flex: 1,
                hidden: !ref.showNombreField,
                dataIndex: 'nombre'
            }];

            var fixedSize = resultColumnList.length;

            var renderer = function (value, metaData, record, rowIndex, colIndex) {
                var value = value[(colIndex - fixedSize)].value;

                if (Ext.isDefined(value)) {
                    return value;
                }
            };

            var rendererByKey = function (value, metaData, record, rowIndex, colIndex) {
                var column = this.getHeaderContainer().getHeaderAtIndex(colIndex);
                for (var i in value) {
                    if (value[i].key === column.config.fieldName) {
                        return value[i].value;
                    }
                }
            };

            for (var extraField in this.extraFields) {

                if (this.extraFields.hasOwnProperty(extraField)) {
                    var field = this.extraFields[extraField];
                    if (typeof field === 'object') {
                        resultColumnList.push(
                            {
                                header: field.header,
                                width: field.width || 200,
                                fieldName: extraField,
                                dataIndex: 'extraParam',
                                align: field.align || 'left',
                                renderer: field.renderer || ((!Array.isArray(this.extraFields)) ? rendererByKey : renderer)
                            });
                    } else {
                        resultColumnList.push(
                            {
                                header: this.extraFields[extraField],
                                width: 200,
                                fieldName: extraField,
                                dataIndex: 'extraParam',
                                renderer: (!Array.isArray(this.extraFields)) ? rendererByKey : renderer
                            });
                    }
                }
            }

            if(!this.store){
                this.store =  Ext.create('Ext.data.Store',
                {
                    model: 'act.model.PDIActivo',
                    autoSync: false,

                    proxy: {
                        type: 'ajax',
                        url: '/act/rest/pdi',

                        reader: {
                            type: 'json',
                            rootProperty: 'data'
                        }
                    },
                    listeners: {
                        load: function (store, records, successful, eOpts) {
                            if (ref.gridBusqueda.store.data.length === 0) {
                                Ext.Msg.alert("Aviso", "La búsqueda realitzada no ha produït cap resultat");
                            }
                        }
                    }
                })
            }

            this.gridBusqueda = Ext.create('Ext.grid.Panel',
                {
                    region: 'center',
                    flex: 1,
                    loadMask: true,
                    style: 'border-top: solid #d0d0d0 1px;',
                    store: this.store,
                    columns: resultColumnList,
                    forceFit: true,
                    listeners: {
                        celldblclick: function (grid, td, cellindex, record) {
                            ref.botonSeleccionar.handler.call(ref.botonSeleccionar.scope);
                        }
                    },

                    bbar: {
                        defaultButtonUI: 'default',
                        items: ['->', this.botonSeleccionar, this.botonCancelar]
                    }

                });
        },

        listeners: {
            afterrender: function (window) {
                setTimeout(function () {
                    window.queryField.focus(true);
                }, 250);
            },
            'show': function (window) {
                setTimeout(function () {
                    let queryField = window.queryField,
                        autoSearchString = window.autoSearchString;

                    queryField.focus(true);

                    if (autoSearchString) {
                        queryField.setValue(window.autoSearchString);
                        window.executeSearch(autoSearchString);
                    }
                }, 250);
            },
            beforehide: function (ref) {
                if (ref.clearAfterSearch) {
                    var query = ref.queryField;
                    query.setValue('');
                    ref.gridBusqueda.store.removeAll(true);
                    ref.gridBusqueda.getView().refresh();
                }
            },
        },

        getFilter: function () {
            return this._getFilter(this.filterParams);
        },

        _getFilter: function (value) {
            if (value) {
                if (typeof value == "string" || typeof value == "number" || value instanceof Date) {
                    return {value: value};
                } else if (typeof value == "object") {
                    return value;
                } else if (typeof value == "function") {
                    return this._getFilter(value());
                }
            }
            return {}
        },

        buildHelpText: function () {
            if (this.helpText != '') {
                this.tools = [
                    {
                        type: 'help',
                        tooltip: this.helpText
                    }
                ]
            }

        }
    });
