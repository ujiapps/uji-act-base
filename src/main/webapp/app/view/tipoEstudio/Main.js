Ext.define('act.view.tipoEstudio.Main',
{
    extend : 'Ext.panel.Panel',

    alias : 'widget.tipoEstudioMain',
    title : 'Gestió: TipoEstudio',
    requires : [ 'act.view.tipoEstudio.Grid' ],

    items : [
    {
        xtype : 'tipoEstudioGrid'
    } ]
});
