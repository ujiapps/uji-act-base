Ext.define('act.view.tipoEstudio.Grid',
{
    extend : 'Ext.ux.uji.grid.Panel',

    alias : 'widget.tipoEstudioGrid',

    store : Ext.create('Ext.ux.uji.data.Store',
    {
        model : 'act.model.TipoEstudio',
        url : '/act/rest/tipoEstudios'
    }),

    title : 'TipoEstudio',

    columns : [
    {
        text : 'ID',
        dataIndex : 'id',
        hidden : true
    },
    {
        dataIndex : 'nombre',
        text : 'Nom',
        editor :
        {
            field :
            {
                allowBlank : false
            }
        }
    },
    {
        dataIndex : 'oficial',
        text : 'Oficial',
        editor :
        {
            field :
            {
                allowBlank : true
            }
        }
    } ]
});