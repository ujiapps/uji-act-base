Ext.define('act.view.actaLog.ComboCurso',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.actaLogComboCurso',
    fieldLabel : 'Curs',
    showClearIcon : true,
    width : 180,
    labelWidth : 60,
    allowBlank: true,
    displayField : 'id',

    bind: {
        store : '{cursosStore}'
    }
});