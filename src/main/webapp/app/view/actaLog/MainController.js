Ext.define('act.view.actaLog.MainController',
{
    extend : 'Ext.ux.uji.grid.PanelController',
    alias : 'controller.actaLogMainController',

    onLoad : function()
    {
        var vm = this.getViewModel();
        var actasPersonaStore = vm.get('actasPersonaStore');

        actasPersonaStore.removeAll();
    },

    getValoresUnicos : function(store, id, name)
    {
        var items = [];

        var data = store.getData();
        var snapshot = data.getSource();
        var unfilteredCollection = snapshot || data;
        var allRecords = unfilteredCollection.getRange();

        var map = {};

        allRecords = allRecords.filter(function(item)
        {
            var id = item.get(id);
            return isNaN(id) ? id !== null : id > 0;
        });

        allRecords.map(function(item)
        {

            var mapId = item.get(id);
            if (!map.hasOwnProperty(item.get(id)))
            {
                if (typeof item.get(id) === 'object')
                {
                    mapId = Ext.Date.format(item.get(id), "Ymd");
                    map[mapId] = Ext.Date.format(item.get(id), "d-m-Y");
                    return;
                }
                map[mapId] = item.get(name);
            }
        });

        return Object.keys(map).map(function(id)
        {
            var nombre = map[id];

            var rec =
            {
                id : id,
                nombre : map[id]
            };

            return rec;
        });
    },

    onCursoConvocatoriaSelected : function()
    {
        var view = this.getView();
        var cursoId = view.down('actaLogComboCurso').getValue();
        var convocatoriaId = view.down('actaLogComboConvocatoria').getValue();

        if (cursoId == null)
        {
            return;
        }

        var store = this.getStore('actasPersonaStore');
        store.getProxy().url = "/act/rest/cursos/" + cursoId + '/actas/log';

        if (!(cursoId && convocatoriaId))
        {
            return store.removeAll();
        }

        store.load(
        {
            params :
            {
                convocatoriaId : convocatoriaId
            }
        });
    },

    onActaSelected : function(cursoId, actaId)
    {
        var view = this.getView();
        var vm = this.getViewModel();

        var store = vm.getStore('logsStore');
        var comboFechas = view.down('actaLogComboFiltroFecha');
        var comboEstudiantes = view.down('actaLogComboFiltroEstudiante');

        store.load(
        {
            params :
            {
                actaId : actaId
            },
            callback : function()
            {
                var fechas = this.getValoresUnicos(store, 'fecha', 'fecha');

                comboFechas.clearValue();
                comboFechas.getStore().loadData(fechas);
                var estudiantes = this.getValoresUnicos(store, 'estudianteId', 'nombreCompletoAlumno');
                comboEstudiantes.clearValue();
                comboEstudiantes.getStore().loadData(estudiantes);
            },
            scope : this
        })
    },

    onFiltroEstudianteSelected : function(estudianteId)
    {
        var panel = this.getView();
        var grid = panel.down('actaLogGrid');

        grid.getStore().removeFilter('estudianteId');
        if (estudianteId)
        {
            var filter = new Ext.util.Filter(
            {
                id : 'estudianteId',
                property : 'estudianteId',
                value : estudianteId
            });

            grid.getStore().addFilter(filter);
        }
    },

    onFiltroFechaSelected : function(fecha)
    {
        var view = this.getView();
        var grid = view.down('actaLogGrid');
        var comboFechas = view.down('actaLogComboFiltroFecha');

        grid.getStore().removeFilter('fecha');
        if (fecha)
        {
            var filter = new Ext.util.Filter(
            {
                filterFn : function(item)
                {
                    var selectedDate = comboFechas.getValue();
                    var actualDate = Ext.Date.format(item.get("fecha"), "Ymd");
                    return selectedDate === actualDate;
                }
            });

            grid.getStore().addFilter(filter);
        }
    },

// exportCSV: function() {
//     var cmp = this.getView();
//     cmp.onCmpClick();
// }

});
