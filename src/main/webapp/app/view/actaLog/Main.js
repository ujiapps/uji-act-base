Ext.define('act.view.actaLog.Main',
{
    extend : 'Ext.panel.Panel',

    alias : 'widget.actaLogMain',
    title : 'Control d\'Actes',

    requires : [ 'act.view.actaLog.PanelFiltros', 'act.view.actaLog.Grid', 'act.view.actaLog.MainController', 'act.view.actaLog.ViewModel', 'act.view.actaLog.ComboCurso',
            'act.view.actaLog.ComboActa', 'act.view.actaLog.ComboConvocatoria' ],
    controller : 'actaLogMainController',

    cls : 'no-border',
    margin : 5,
    viewModel :
    {
        type : 'actaLogViewModel'
    },
    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },

    items : [
    {
        xtype : 'panel',
        border : 0,
        padding : 10,
        items : [
        {
            xtype : 'container',
            flex : 1,
            layout :
            {
                type : 'hbox',
                align : 'right'
            },
            items : [

            {
                xtype : 'actaLogComboCurso',
                reference : 'comboCurso'
            },
            {
                xtype : 'box',
                width : 50
            },
            {
                xtype : 'actaLogComboConvocatoria',
                reference : 'comboConvocatoria',
                bind :
                {
                    disabled : '{!comboCurso.selection}'
                }
            },
            {
                xtype : 'box',
                width : 50
            },
            {
                xtype : 'actaLogComboActa',
                reference : 'comboActa',
                bind :
                {
                    disabled : '{!comboConvocatoria.selection}'
                }
            } ]
        } ]
    },
    {
        xtype : 'actaLogPanelFiltros',
        bind :
        {
            disabled : '{!cursoYconvocatoriaSeleccionados}'
        }
    },
    {
        xtype : 'actaLogGrid',
        bind :
        {
            disabled : '{!cursoYconvocatoriaSeleccionados}'
        }
    } ],
    listeners :
    {
        render : 'onLoad',
        convocatoriaSelected : 'onCursoConvocatoriaSelected',
        actaSelected: 'onActaSelected',
        filtroEstudianteSelected: 'onFiltroEstudianteSelected',
        filtroFechaSelected: 'onFiltroFechaSelected'
    }
});
