Ext.define('act.view.actaLog.ComboFiltroEstudiante',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.actaLogComboFiltroEstudiante',
    fieldLabel : 'Alumne/a',
    showClearIcon : true,
    width : 250,
    padding : '0 10 0 20',
    flex : 1,
    labelWidth : 130,
    allowBlank : true,
    displayField : 'nombre',
    emptyText : 'Mostrar tots',
    store : Ext.create('Ext.data.Store',
    {
        model : 'act.model.Lookup',
        autoLoad : true,
        sorters : [ 'nombre' ],
        proxy :
        {
            type : 'memory'
        }
    }),
    listeners :
    {
        change : function(combo, recordId)
        {
            combo.up('actaLogMain').fireEvent('filtroEstudianteSelected', recordId);
        }
    }

});