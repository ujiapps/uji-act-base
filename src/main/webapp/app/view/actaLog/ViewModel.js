Ext.define('act.view.actaLog.ViewModel',
{
    extend : 'Ext.app.ViewModel',
    alias : 'viewmodel.actaLogViewModel',
    requires : [ 'act.model.ActaAsignatura', 'act.model.ActaEstudiante', 'act.model.ActaPersona', 'act.model.Convocatoria', 'act.model.Log' ],
    stores :
    {
        actasPersonaStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'act.model.ActaPersona',
            url : '/act/rest/actas/logs',
            autoLoad : false,
            queryModel : 'local',
            sorters : [ 'codigo' ]
        }),
        cursosStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'act.model.Curso',
            url : '/act/rest/cursos',
            autoLoad : true
        }),
        convocatoriasStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'act.model.Convocatoria',
            url : '/act/rest/convocatorias',
            autoLoad : true
        }),
        logsStore : Ext.create('Ext.ux.uji.data.Store',
        {
            model : 'act.model.Log',
            url : '/act/rest/logs',
            autoLoad : false,
            queryModel : 'local',
            sorters : [ 'fecha' ]
        })
    }
});
