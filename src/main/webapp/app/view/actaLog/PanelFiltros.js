Ext.define('act.view.actaLog.PanelFiltros',
{
    extend : 'Ext.panel.Panel',

    alias : 'widget.actaLogPanelFiltros',
    border : 0,
    requires : [ 'act.view.actaLog.PanelFiltrosController', 'act.view.actaLog.ComboFiltroFecha', 'act.view.actaLog.ComboFiltroEstudiante' ],
    controller : 'actaLogPanelFiltrosController',

    layout : 'anchor',
    items : [
    {
        xtype : 'fieldset',
        title : 'Filtres',
        items : [
        {
            xtype : 'fieldcontainer',
            layout : 'hbox',
            items : [
            {
                xtype : 'actaLogComboFiltroFecha',
                flex: 1
            },
            {
                xtype : 'actaLogComboFiltroEstudiante',
                flex: 1
            } ]
        } ]

    } ],
    listeners : {}
});
