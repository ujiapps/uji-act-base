Ext.define('act.view.actaLog.ComboConvocatoria',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.actaLogComboConvocatoria',
    fieldLabel : 'Convocatoria',
    showClearIcon : true,
    width : 450,
    labelWidth : 100,
    displayField : 'nombre',
    allowBlank: true,
    bind: {
        store: '{convocatoriasStore}'
    },

    listeners :
    {
        change : function(combo, recordId)
        {
            combo.up('actaLogMain').fireEvent('convocatoriaSelected', recordId);
        }
    }

});