Ext.define('act.view.actaLog.ComboActa',
{
    extend : 'Ext.ux.uji.combo.Combo',
    alias : 'widget.actaLogComboActa',
    fieldLabel : 'Acta',
    showClearIcon : true,
    labelWidth : 40,
    typeAhead: true,
    editable: true,
    displayField : 'codigo',
    valueField: 'id',

    bind: {
        store: '{actasPersonaStore}'
    },

    listeners :
    {
        select : function(combo, record)
        {
            combo.up('actaLogMain').fireEvent('actaSelected', record.get('cursoId'), record.get('id'));
        },
        beforequery: function(record){
            record.query = new RegExp(record.query, 'i');
            record.forceAll = true;
        }
    }

});