Ext.define('act.view.actaLog.Grid',
{
    extend : 'Ext.ux.uji.grid.Panel',
    allowEdit : false,

    alias : 'widget.actaLogGrid',
    flex : 1,
    scrollable : true,
    controller : 'actaLogMainController',
    bind :
    {
        store : '{logsStore}'
    },

    emptyText : 'Selecciona un curs i una convocatoria',

    border : 0,
    columns : [
    {
        text : 'Id',
        dataIndex : 'id',
        hidden : true
    },
    {
        dataIndex : 'nombreCompletoAlumno',
        text : 'Nom Alumne/a',
        flex : 1
    },
    {
        dataIndex : 'nota',
        text : 'Nota',
        align: 'center'
    },
    {
        dataIndex : 'calificacionId',
        text : 'Qualificació',
        align : 'center',
        width: 180,
        renderer : function(value)
        {
            var store = Ext.getStore('act.store.Calificaciones');
            var data = store.getById(value);
            return data ? data.get('nombre') : '';
        }
    },
    {
        dataIndex : 'nombreCompletoEditor',
        text : 'Nom editor',
        flex : 1
    },
    {
        xtype : 'datecolumn',
        format : 'd/m/Y H:i:s',
        dataIndex : 'fecha',
        text : 'Data modificació',
        width : 150,
        align : 'center'
    } ]
});