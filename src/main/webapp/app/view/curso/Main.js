Ext.define('act.view.curso.Main',
{
    extend : 'Ext.panel.Panel',

    alias : 'widget.cursoMain',
    title : 'Gestió: Curso',
    requires : [ 'act.view.curso.Grid' ],

    items : [
    {
        xtype : 'cursoGrid'
    } ]
});
