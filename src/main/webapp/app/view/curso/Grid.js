Ext.define('act.view.curso.Grid',
{
    extend : 'Ext.ux.uji.grid.Panel',

    alias : 'widget.cursoGrid',

    store : Ext.create('Ext.ux.uji.data.Store',
    {
        model : 'act.model.Curso',
        url : '/act/rest/cursos'
    }),

    title : 'Curso',

    columns : [
    {
        text : 'ID',
        dataIndex : 'id'
    },
    {
        dataIndex : 'activo',
        text : 'Actiu',
        editor :
        {
            field :
            {
                allowBlank : false
            }
        }
    } ]
});