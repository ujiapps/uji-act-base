Ext.define('act.view.actaAsignatura.Main',
{
    extend : 'Ext.panel.Panel',

    alias : 'widget.actaAsignaturaMain',
    title : 'Gestió: ActaAsignatura',
    requires : [ 'act.view.actaAsignatura.Grid' ],

    items : [
    {
        xtype : 'actaAsignaturaGrid'
    } ]
});
