Ext.define('act.view.actaAsignatura.Grid',
{
    extend : 'Ext.ux.uji.grid.Panel',

    alias : 'widget.actaAsignaturaGrid',

    store : Ext.create('Ext.ux.uji.data.Store',
    {
        model : 'act.model.ActaAsignatura',
        url : '/act/rest/actaAsignaturas'
    }),

    title : 'ActaAsignatura',

    columns : [
    {
        text : 'ID',
        dataIndex : 'id',
        hidden : true
    },
    {
        dataIndex : 'actaId',
        text : 'Acta',
    },
    {
        dataIndex : 'asignaturaId',
        text : 'Asignatura',
    },
    {
        dataIndex : 'personaId',
        text : 'Alumno',
    },
    {
        dataIndex : 'grupoId',
        text : 'Grupo',
    },
    {
        xtype : 'datecolumn',
        format: 'd/m/Y',
        dataIndex : 'fechaRevision',
        text : 'Fecha revisión',
    },
    {
        dataIndex : 'lugarRevision',
        text : 'Lugar revisión',
    },
    {
        dataIndex : 'observacionesRevision',
        text : 'Observaciones revisión',
    },
  ]
});